"""
Utils to compute and plot several useful quantities: 
====================================================

- PDFs & Phase Diagrams
    - PDF, plot_phase_diag, get_levels.

- Radial plots
    - vs_r, res_vs_r
    - half_mass_radius,
    - rad_prof, rad_Zfrac, rad_col,
    - check_folder.

- Stars-related
    - compute_sfr, plot_sfr, plot_burst
    - plot_mass_evol,
    - link_stars_cells,
    - stars_density.

- Angular momentum & CGM
    - is_sat,
    - ang_mom, in_cone,
    - in_cgm, plot_cgm_time.

- Flows
    - calculate_flow, init_flows
    - flow_vs_rad, flow_vs_t

- Things against time
    - Ms_Mh_relation, content_t, gas_fraction
    - MZR, MZRg_stars, MZRg_obs

- Pies
    - pie_baryons, pie_metals, plot_pie

- Ions
    - ion_fraction

- Nicolas
    - azimuth_frac

- Output times
    -compute_runtime, plot_runtime_estimate

- Utils
    - plot_dexter, plot_data, get_col_pap,
    - runtime, plot_runtime,
    - strongscaling,
    - axis_format_10pow, add_z_top_axis
    - get_label, foldername, get_aout.

The use of all these functions is shown in plots.ipynb.


Common parameters:
==================
    Simulation folder-related:
    --------------------------
        RamsesDir: path
            Path to the folder containing the Ramses output files. Basically genpath+folders[i].
        genpath: path
            Path to the main folder.
        folders: array of subpaths
            Subpaths to the folders containing the simulations.

        timestep: int
            Timestep of the output considered.
        timesteps: array
            Array containing the timesteps to consider. If only one is used as input, a list from the third output up to the chosen one will be used.

        factor: 'fullbox' or float
            Multiplicative factor to increase the radius of data read. This works for cosmological simulations. For non_cosmological simulations, the whole data box is loaded. 
            For a cosmological simulation, by default the radius is r200 and the center is the center of the zoom region.
        qty: array or tuple
            Values to to plot on the PDF.
            data and the second one as the title for the x axis (resp. label). 
        what2plot: string (depending on the functions)
            Selects the quantity (and the label of the x_axis) of which you want to plot the PDF.
        weight: array
            An array of weights, of the same shape as qty. Each value in a only contributes its associated weight towards the bin count (instead of 1).


    Plot-related:
    -------------
        bins: int
            Integer number of bins.
        density: boolean
            If set to True, normalises the curve so that its integral is 1. 
        x_log, y_log: booleans
            If set to True, returns the considered axis in logarithmic scale. It is wise to set it to 
            True if the range of values is considerable (several orders of magnitude).
        xlims, ylims: float, float
            Limits of the plot for each axis.
        vmin, vmax: scalar
            Limit values of the colorbar.
        savefig: boolean
            If set to true, the plot is saved in the current folder.


    Outflows:
    ---------
        dist_kpc: one or two integers in a list
            The distance from the center in kpc at which the slabs are taken.
"""
# Astro libraries
import minirats.utils.py.readwrite.ramses_info as ri
import minirats.utils.py.cellutils as cu
import minirats.utils.py.f90utils as dm
import ratatouille.readNsave as ras
from ratatouille.constants import *
from astropy.cosmology import FlatLambdaCDM

# Data manipulation and basic libraries
from scipy.spatial import cKDTree as ckdt
from collections import deque
from scipy import stats
import numpy as np
import h5py, os, sys, re

# Plotting libraries
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.legend_handler import HandlerTuple
from matplotlib.collections import LineCollection
from matplotlib.colors import LogNorm, SymLogNorm
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib.cm as cm


###################################################################################################################################
###################################################### PDFs & Phase Diagrams ######################################################
###################################################################################################################################
def PDF(what2plot, genpath, folders, timesteps, weight_str=None, density=False, bins=300, \
    x_log=True, y_log=True, xlims=None, ylims=None, factor=1, savefig=False, cumsum=False, norm_max=False, alt_path=None):
    """ 
    Plots PDF for 'T', 'Tmu', 'nH', 'rho', 'pres', 'mass', 'star_mass', 'star_age', 'star_mets' or 'star_minit'.

    Example:
    --------
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        plot_pdfs2('nH', '/some/path/', folders, 17, weight_str='mass', density=True, bins=300, savefig=False)
    """
    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
    if (not isinstance(folders,list)):
        folders=[folders]
    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps=[timesteps]

    fig, ax = plt.subplots(nrows=1, ncols=1)
    for ifold, folder in enumerate(folders):
        RamsesDir = genpath + folder
        PDF_all = np.empty((len(timesteps), bins))
        for istep, timestep in enumerate(timesteps):
            # Load quantity wanted
            if what2plot[:4]=='star':
                if weight_str!=None:
                    raise ValueError('No weight parameter setup for stars PDFs.')
                else:
                    weight2use=None
                temp_var = ras.extract_stars(RamsesDir, timestep, [what2plot], factor=factor, alt_path=alt_path)
                if what2plot in ['star_mass', 'star_minit']:
                    temp_var *= g2Msun
            else:
                # Loading weight
                temp_var, weight2use = ras.extract_cells(RamsesDir, timestep, [what2plot,weight_str], factor=factor, alt_path=alt_path)
                if weight_str=='volume':
                    weight2use *= (1/kpc2cm)**3
                elif weight_str in ion_mass or weight_str in ['mass']:
                    weight2use *= g2Msun
                if what2plot=='mass':
                    temp_var *= g2Msun

            # Common xlims needed for all histograms if stacked.
            if len(timesteps)>1 and not xlims and timestep==timesteps[0]: 
                if x_log:
                    xlims = [np.log10(temp_var.min()), np.log10(temp_var.max())]
                else:
                    xlims = [temp_var.min(), temp_var.max()]
            if x_log:
                temp_var = np.log10(temp_var)
            PDF_all[istep,:], bin_edges = np.histogram(temp_var, bins=bins, range=xlims, weights=weight2use, density=density)

        # Median on each bin
        x_axis = (bin_edges[:-1]+bin_edges[1:])*0.5
        med_PDF = np.median(PDF_all, axis=0)
        if norm_max: # Normalise by max
            med_PDF /= med_PDF.max()
        if cumsum:  # Compute cumulative PDF
            PDF_cum_sum  = np.cumsum(med_PDF)
            col = next(ax._get_lines.prop_cycler)['color']
            ax.plot(x_axis, 1-PDF_cum_sum, color=col, label=foldername(folder))    # Survival function
            ax.plot(x_axis, PDF_cum_sum, color=col)      # Cumulative function
        else:
            ax.plot(x_axis, med_PDF, label=foldername(folder))
            ax.fill_between(x_axis,np.percentile(PDF_all,perc_min, axis=0), np.percentile(PDF_all,perc_max, axis=0), alpha=0.25)

    if x_log:
        ax.xaxis.set_major_formatter(plt.FuncFormatter(axis_format_10pow))
    else:
        ax.set_xlim(xlims)
    if y_log: 
        ax.set_yscale('log')
    ax.set_ylim(ylims)

    ax.set_xlabel(get_label(what2plot))
    if weight_str==None:
        weight_in_lab=''
    else:
        weight_in_lab= ' (' + weight_str + ' weighted)'
    if density:
        ax.set_ylabel('Normalised PDF'+weight_in_lab)
    else:
        ax.set_ylabel('PDF'+weight_in_lab)

    plt.tight_layout()
    plt.legend()
    plt.show()

    if savefig:
        fig.savefig(f'PDF_{what2plot}.pdf', bbox_inches='tight')


def plot_phase_diag(genpath, folders, timesteps, x_str='nH', y_str='T', var_str='mass', cgm_gas=False, no_sat=False, bins=300, plot_PDF=False, contour=False, mass_frac=[0.5], \
    xlims=None, ylims=None, vmin=None, vmax=None, factor=1, savefig=False, density=True, hnum=None, alt_path=None, use_yt=False):
    """
    Plots phase diagram from different output files (for the same timestep) one after another.
    Appends all timesteps together and make a phase diagram out of it.
    If not stack_PDF, take median of PDFs over timesteps (was going with percentiles).

    Example:
    --------
        timestep = 17
        genpath = '/mnt/lyoccf/scratch/mrey/outputs/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        plot_phase_diag(genpath, folders, timestep)
    """
    contourion = not cgm_gas and var_str in ['M_MgII', 'M_CIV', 'M_OVI', 'M_HI']

    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
    if (not isinstance(folders,list)):
        folders=[folders]
    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps=[timesteps]

    if not isinstance(folders,(list,np.ndarray)):
        raise ValueError(f"The variable folder ({folders}) should be a list. If you just want one simulation, use a list with a single element.")

    for folder in folders:
        RamsesDir = genpath + folder
        yvar_all, xvar_all, weight_all = [], [], []      
        weight_gas = []

        ######### Read data #########
        for timestep in timesteps:
            xvar, yvar, weight = ras.extract_cells(RamsesDir,timestep,[x_str, y_str, var_str],factor=factor,alt_path=alt_path,use_yt=use_yt)
            yvar_all       = np.append(yvar_all, yvar)
            xvar_all      = np.append(xvar_all, xvar)
            weight_all  = np.append(weight_all, weight) # TODO: /!\ WEIGHTS (var_str) NEED TO BE ADDITIVE, eg. nH*vol, not nH, Z, f_, etc.. do smthg smart about it.
            if contourion and var_str!='gas':
                weight_gas  = np.append(weight_gas, np.array(ras.extract_cells(RamsesDir,timestep,['mass'],factor=factor,alt_path=alt_path))*g2Msun)
        xvar_all = np.log10(xvar_all)
        yvar_all = np.log10(yvar_all)

        ######## Phase diag  ########
        fig, ax = plt.subplots(nrows=1, ncols=1)
        phase_diag,xedges,yedges = np.histogram2d(xvar_all,yvar_all,bins=(bins,bins),weights=weight_all, density=density)
        phase_diag = np.ma.masked_where(phase_diag <= 0, phase_diag)
        extent = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        cmap = cm.BuPu
        cmap.set_bad(cmap(0))   # ensure log(0) cells don't cause issues in PDF
        norm = LogNorm(vmin=vmin,vmax=vmax)
        im = ax.imshow(phase_diag.T, interpolation='nearest', origin='lower', norm=norm, cmap=cmap, extent=extent)

        ########## Contour ##########
        if var_str=='gas':
            weight_gas = weight_all
        if contourion: # Plots gas mass contour.
            PDcontemp,xedges,yedges = np.histogram2d(xvar_all,yvar_all,bins=(bins,bins),weights=weight_gas)
            PDcontour = np.ma.masked_where(PDcontemp <= 0, PDcontemp)
            plt.contour(PDcontour.T,levels=get_levels(PDcontour, fractions=mass_frac),origin='lower',norm=norm,colors='orange', extent=extent, linewidths=2)
        elif contour: # Plots contour of 1st folder on the others
            if folder==folders[0]:
                extent_0 = extent 
                phase_diag_0 = np.ma.masked_where(phase_diag <= 0, phase_diag)
                ls = None
            else:
                ls = 'dashed'
                plt.contour(phase_diag.T,levels=get_levels(phase_diag, fractions=mass_frac),origin='lower',norm=norm,colors='k', extent=extent, linewidths=2)
            plt.contour(phase_diag_0.T,levels=get_levels(phase_diag_0, fractions=mass_frac),origin='lower',norm=norm,colors='orange', extent=extent_0, linewidths=2, linestyles=ls)

        ############ PDF ############
        if plot_PDF:
            PDF_nHall, nHbin_edges = np.histogram(xvar_all, bins=bins, range=np.log10(xlims), weights=weight_all, density=True)
            PDF_Tall ,  Tbin_edges = np.histogram(yvar_all , bins=bins, range=np.log10(ylims), weights=weight_all, density=True)
            yaxis_T   = PDF_Tall  - min(PDF_Tall )+np.log10(xlims[0])
            yaxis_nH  = PDF_nHall - min(PDF_nHall)+np.log10(ylims[0])
            xaxis_T  = ( Tbin_edges[:-1]+ Tbin_edges[1:])*0.5
            xaxis_nH = (nHbin_edges[:-1]+nHbin_edges[1:])*0.5
            ax.plot(yaxis_T, xaxis_T, '-k', lw=2)
            ax.plot(xaxis_nH, yaxis_nH, '-k', lw=2)

        ######## Plot params ########
        plt.xlabel(get_label('nH'))
        plt.ylabel(get_label('T'))
        ax.set_xlim(np.log10(xlims)) if xlims else None
        ax.set_ylim(np.log10(ylims)) if ylims else None
        ax.xaxis.set_major_formatter(plt.FuncFormatter(axis_format_10pow))
        ax.yaxis.set_major_formatter(plt.FuncFormatter(axis_format_10pow))

        # Text on top right corner of the figure.
        txt_on_fig = foldername(folder) if var_str=='mass' else None # get_label(var_str)
        plt.text(0.95, 0.95, txt_on_fig, horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, size=20)

        # Colorbar
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="4%", pad=0)
        plt.colorbar(im, cax=cax).set_label(label=get_label(var_str),size=16)

        if savefig:
            if savefig==True:
                plt.savefig(f'PhaseDiag_{var_str}_{foldername(RamsesDir)}_{timesteps[-1]}.pdf', bbox_inches='tight')
            else:
                plt.savefig(f'{savefig}/PhaseDiag_{var_str}_{foldername(RamsesDir)}_{timesteps[-1]}.pdf', bbox_inches='tight')


def get_levels(counts, fractions=[0.1,0.5,0.9]):
    # get levels that contain some fraction of the total ...
    c = np.ravel(counts)
    d = np.sort(c)
    cc = np.cumsum(d)
    cc = cc / cc.max()
    values = []
    for frac in fractions:
        values.append(d[np.where(cc >= frac)[0][0]])
    return values


###################################################################################################################################
########################################################## Radial plots ##########################################################
###################################################################################################################################
def vs_r(genpath, folders, timesteps, gas_var=True, var='metal', density=True, factor=2, nbins=100, logx = False, xlims=None, ylims=None, savefig=False, hnum=None, alt_path=None):
    """
    Returns whichever quantity denoted by var against radius. gas_var=True if a variable related to gas, False for stars.
    var must be something that can be summed. For example, it can be nH*vol, but not nH.
    """
    np.seterr(divide='ignore', invalid='ignore') # Ignore zero or NaN division warning
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    filename = 'vs_r'
    for i_fold, folder in enumerate(folders):
        RamsesDir = genpath+folder

        # Compute x_axis to bin stuff. 0.1 kpc to factor*r200 by default. xlims if set up.
        info_t0 = ras.read_info(RamsesDir, timesteps[0], alt_path=alt_path)
        r200_t0 = ras.get_frad(RamsesDir,timesteps[0],factor=factor)[1]*info_t0['cu2cm']
        dx_min  = info_t0['boxlen_cMpc']*1e3/2**info_t0['levelmax']
        if xlims==None: xlims = [None, None]
        min_r = max(xlims[0] *kpc2cm, dx_min*2) if xlims[0] !=None else 0.1*kpc2cm  # 0.1 kpc by default, but at least 2 cells
        max_r = min(xlims[-1]*kpc2cm, r200_t0)  if xlims[-1]!=None else r200_t0     # r200 by default, but at most the smallest region loaded
        if logx:
            bins = np.logspace(np.log10(min_r), np.log10(max_r), num=nbins)
            x_axis = 10**((np.log10(bins[:-1])+np.log10(bins[1:]))*0.5) / kpc2cm
            plt.xscale('log')
        else:
            bins = np.linspace(min_r, max_r, num=nbins)
            x_axis = (bins[:-1]+bins[1:])*0.5 / kpc2cm

        binned_data = []
        for timestep in timesteps:
            # Read data
            data_string = f'{var}_{density}_{min_r}_{max_r}_{logx}' # bin 150..
            try:
                niu_M_sum = ras.read_prop(RamsesDir, timestep, data_string, filename, hnum=hnum, alt_path=alt_path)
            except:
                if gas_var:
                    qty2plot, cell_dx, rad = ras.extract_cells(RamsesDir,timestep,[var,'cell_dx','radius'],factor=factor,alt_path=alt_path)
                    if var=='M_Z':
                        lab = 'Z_{{gas}}'
                        qty2plot *= g2Msun
                else:
                    star_mass, rad, star_mets, _ = ras.extract_stars(RamsesDir,timestep,['star_mass','star_rad','star_mets'],factor=factor,alt_path=alt_path)
                    if var=='M_Z':
                        qty2plot = star_mets*star_mass*g2Msun       # TODO: not very clean
                        lab = 'Z_{{stars}}'
                # Sum masses in each bin
                niu_M_sum, _, _ = stats.binned_statistic(rad, qty2plot, statistic='sum', bins=bins)
                if density:     # Divide by the volume to get radial density.
                    if gas_var:
                        vol , _, _ = stats.binned_statistic(rad, (cell_dx**3), statistic='sum', bins=bins)  # Use the volume of the cells
                    else:
                        vol = (4 / 3) * np.pi * (bins[1:]** 3 - bins[:-1]** 3)                          # Use the volume of the bins
                    niu_M_sum /= g2Msun*vol#/kpc2cm**3             # TODO: not very clean
                ras.add_prop(RamsesDir, timestep, data_string, niu_M_sum, filename, hnum=hnum, alt_path=alt_path)
            if timestep==timesteps[-1] and len(timesteps)!=1: plt.plot(x_axis, niu_M_sum, color=colors[i_fold], ls='--', alpha=0.5)
            binned_data.append(niu_M_sum)
        binned_data = np.array(binned_data)

        # Plot median
        plt.plot(x_axis, np.median(binned_data, axis=0), color=colors[i_fold], label=foldername(folder))
        plt.fill_between(x_axis, np.percentile(binned_data, perc_min, axis=0), np.percentile(binned_data, perc_max, axis=0), color=colors[i_fold], alpha=0.25)
        
        # Plot mean
        # log_binned_data = np.log10(binned_data)
        # mean_log_binned_data = np.mean(log_binned_data, axis=0)
        # std_log_binned_data = np.std(log_binned_data, axis=0)
        # factor_upper = 10**(mean_log_binned_data + std_log_binned_data)
        # factor_lower = 10**(mean_log_binned_data - std_log_binned_data)
        # plt.plot(x_axis, 10**mean_log_binned_data, color=colors[i_fold], label=foldername(folder))
        # plt.fill_between(x_axis, factor_lower, factor_upper, color=colors[i_fold], alpha=0.25)

    if density==True:
        ylabel = rf'$\rho_\mathrm{{{lab}}}\rm\ [g\,cm^{{-3}}]$'
    elif density=='rhor':
        ylabel = rf'$\rho_\mathrm{{{lab}}}r\rm\ [g\,cm^{{-3}}]$'
    else:
        ylabel = rf'$\rm {var.split('_')[0]}_\mathrm{{{var.split('_')[1]}}}\rm\ [M_\odot]$'
        # ylabel = rf'$M_\mathrm{{{lab}}}\rm\ [M_\odot]$'
    plt.xlabel('Radius [kpc]')
    plt.ylabel(ylabel)

    plt.yscale('log')
    plt.xlim(xlims)
    plt.ylim(ylims)
    plt.legend(frameon=False)
    plt.legend()

    if savefig:
        if savefig==True:
            plt.savefig(f'{var}_vs_r.pdf',bbox_inches='tight')
        else:
            plt.savefig(f'{savefig}/{var}_vs_r.pdf',bbox_inches='tight')


def res_vs_r(genpath, folders, timesteps, factor=2, nbins=100, logx=False, xlims=None, ylims=None, savefig=False, hnum=None, alt_path=None):
    """ Plots either the median/percentile or mass-weighted/1-sigma resolution of the cells as a function of radius. """
    fig, ax = plt.subplots(nrows=1, ncols=1)
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    for i_fold, folder in enumerate(folders):
        RamsesDir = genpath+folder

        # Compute x_axis to bin stuff.
        if xlims==None: xlims = [None, None]
        min_r = 0.001
        max_r = 2
        if logx:
            bins = np.logspace(np.log10(min_r), np.log10(max_r), num=nbins)
            x_axis = 10**((np.log10(bins[:-1])+np.log10(bins[1:]))*0.5)# / kpc2cm
            plt.xscale('log')
        else:
            bins = np.linspace(min_r, max_r, num=nbins)
            x_axis = (bins[:-1]+bins[1:])*0.5# / kpc2cm

        stack_mass = []
        stack_res = []
        stack_rad = []
        for timestep in timesteps:
            info = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            gas_mass, cell_dx, rad = ras.extract_cells(RamsesDir,timestep,['mass','cell_dx','radius'],factor=factor,alt_path=alt_path)
            r200   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*info['cu2cm']
            stack_mass.append(gas_mass)
            stack_res.append(cell_dx/kpc2cm)    # *info['aexp'] if cpc
            stack_rad.append(rad/r200)
        stack_mass = np.concatenate(stack_mass, axis=0)
        stack_res  = np.concatenate(stack_res, axis=0)
        stack_rad  = np.concatenate(stack_rad, axis=0)

        massweighted = False
        if massweighted:    # BUG: sigma goes lower than min res..
            mass_weighted_cell_dx = np.zeros(len(bins) - 1)
            mass_weighted_cell_dx_std = np.zeros(len(bins) - 1)
            for i_bin in range(len(bins) - 1):
                mask = (stack_rad >= bins[i_bin]) & (stack_rad < bins[i_bin+1]) # cells in the bin            
                if np.sum(mask) > 0:  # If bin is not empty
                    mask_stack_res = np.log10(stack_res[mask])                  # Compute in log space because ylog
                    weighted_values = stack_mass[mask] * mask_stack_res
                    mass_weighted_cell_dx[i_bin] = np.sum(weighted_values) / np.sum(stack_mass[mask])
                    mass_weighted_cell_dx_std[i_bin] = np.sqrt(np.sum(stack_mass[mask] * (mask_stack_res - mass_weighted_cell_dx[i_bin])**2) / np.sum(stack_mass[mask]))
            sig_neg = 10**(mass_weighted_cell_dx - mass_weighted_cell_dx_std)   # Revert back to linear space
            sig_pos = 10**(mass_weighted_cell_dx + mass_weighted_cell_dx_std)
            mass_weighted_cell_dx = 10**mass_weighted_cell_dx
            plt.fill_between(x_axis, sig_neg, sig_pos,color=colors[i_fold], alpha=0.3)
            plt.plot(x_axis, mass_weighted_cell_dx, color=colors[i_fold], label=foldername(folder))
        else:
            # Median and percentiles
            binned_res = []
            bin_pmin_res = []
            bin_pmax_res = []
            for i_bin in range(len(x_axis)):
                bin_mask = (stack_rad >= bins[i_bin]) & (stack_rad < bins[i_bin + 1])
                bin_data = stack_res[bin_mask]
                median_bin_data = np.median(bin_data, axis=0)
                pmin_res = np.percentile(bin_data, perc_min, axis=0)
                pmax_res = np.percentile(bin_data, perc_max, axis=0)
                binned_res.append(median_bin_data)
                bin_pmin_res.append(pmin_res)
                bin_pmax_res.append(pmax_res)
            binned_res = np.array(binned_res)
            plt.plot(x_axis, binned_res, color=colors[i_fold], label=foldername(folder))
            plt.fill_between(x_axis, bin_pmin_res, bin_pmax_res, color=colors[i_fold], alpha=0.25)
        # plt.scatter(stack_rad, stack_res, label=foldername(folder))  # Plot all cells..


    RamNel24_x = [0.0001,0.1,0.15,0.18933428455360066,0.22443853142951667,0.2557252356127788,0.2739399742993036,0.28801266495903177,0.30308713322088354,0.31661250063427104,0.34595381292565064,0.3778682603653768,0.4222797099094676,0.4585093751279834,0.4966046694798858,0.5365825716532177,0.5763975431323654,0.615610871803857,0.6548217053879212,0.6940343211772907,0.733235174411645,0.7724313939122055,0.8131168093141854,0.8508511402590602,0.8885981050593219,0.9292982533584924,0.9704338823815801,0.9970257619479073,1.0143078424377574,1.0256941739129533,1.037025049099732,1.0471679656368238,1.0721566735840093,1.0999218268417614,1.134780376454252,1.1753485992905741,1.2148535241685603,1.254075942087109,1.2932958649182305,1.3325140055440463,1.3717310768466793,1.4109467223850678,1.4501612986002732,1.4893749837128256,1.5285872430611342,1.5677993241889123,1.6070101577729763,1.646220456695449,1.6854311120589824,1.7246397079852744,1.7638485910446433,1.8030581572827125,1.8422678522356095,1.8814772303520078,1.920687915418963,1.959897679679844,1.9919821277231893]
    RamNel24_y = [450,450,75,75.5353959937754,89.08110576394199,110.06513518747896,135.4596977960243,165.06091800725173,198.08750034316762,242.4298169631213,285.1226576854488,334.006975185997,381.98654922507984,423.43946875775083,481.55035077573757,551.4558120842964,592.6910748782386,615.9825006073527,633.2044993579738,656.0285361216685,645.4176990740938,622.1726211339612,610.8749925984088,651.9200274010408,735.462690525098,770.4324456619851,812.0395056404601,1020.2090000027914,1307.9324508092886,1602.3038010338678,1950.4063677002312,2374.7441491115956,2835.7357776268204,3452.4479744643954,3936.6462754697145,4571.3140843204455,5129.555482064683,5548.500582073919,5936.1814962712115,6301.378759771586,6657.667873291699,6990.144771716169,7304.800210078379,7603.769681080363,7865.513269213994,8129.893761627015,8357.194082768236,8570.677811748177,8803.400803983459,8960.934134524894,9132.809017257938,9335.981799492698,9549.07705477944,9753.439624981333,10019.587083424858,10251.409197503186,10617.86277000164]
    plt.plot(RamNel24_x, np.array(RamNel24_y)*1e-3, color=colors[5], label='Ramesh+24') # 3.5 times KI at 204, but 10 x better us in the ISM.

    # "radial profile of the size of gas cells", z=2.25
    # R_cgm,max = 300kpc on the plot. assume it's R200.
    Suresh19_x = np.array([-0.37072243346007616,-0.19961977186311797,-0.14496197718631187,-0.06178707224334601,0.0071292775665398045,0.06178707224334601,0.11406844106463876,0.16159695817490494,0.23051330798479086,0.3113117870722434,0.41349809885931565,0.4824144486692016,0.5608365019011406,0.6131178707224335,0.6772813688212928,0.7723384030418251,0.8317490494296579,0.8816539923954371,0.9386882129277568,1.0171102661596958,1.0717680608365021,1.1549429657794676,1.2595057034220534,1.3498098859315588,1.4615019011406845,1.549429657794677,1.6112167300380227,1.6539923954372626,1.7015209125475286,1.7300380228136882,1.8203422053231941,1.9058935361216731,1.991444866920152,2.0365969581749055,2.0769961977186315,2.1577946768060836,2.2124524714828895,2.276615969581749,2.345532319391635,2.414448669201521,2.483365019011407,2.538022813688213,2.6045627376425857,2.6948669201520916,2.782794676806084,2.868346007604563,2.9134980988593155,2.9538973384030416])
    Suresh19_y = np.array([-1.4154310635872063,-1.193979997896846,-1.1473370193877608,-1.1006608331811314,-1.0627348752774226,-1.0160918967683379,-0.9781853100215294,-0.9461066741937445,-0.9081807162900362,-0.902264211510895,-0.9108788417155098,-0.925354630536692,-0.9369081419739762,-0.9484920938006762,-0.9513285846325845,-0.9948860145781795,-1.0384849541457044,-1.073361338491596,-1.1053182127617183,-1.099404475290706,-1.0789623701440665,-1.0293749757860537,-0.9652066348979693,-0.9330781875238678,-0.8892800017710771,-0.7989301586774484,-0.702788339670469,-0.6241359080368163,-0.533833109181375,-0.4813981547589399,-0.5016714541097294,-0.5190390799253934,-0.4636265019564869,-0.3791488867119397,-0.29467680608365,-0.2480033871851497,-0.2013604086760643,-0.15179515278308164,-0.1109579867279904,-0.014807865796625075,0.08425346328612315,0.16583093961180195,0.25615587693227293,0.29992915691190547,0.3029427554640498,0.36126654158433924,0.5243467769162226,0.6815990613290828])
    plt.plot(10**Suresh19_x/300, 10**Suresh19_y, color=colors[6], label='Suresh+19') # 3.5 times KI at 204, but 10 x better us in the ISM.

    # plt.axvline(x=0.1, color='grey', linestyle='-', alpha=0.5)
    # plt.text(0.2, 0.95, 'ISM', color='grey', ha='center', va='top', fontsize=12, transform=plt.gca().transAxes)
    # plt.text(0.8, 0.95, 'CGM', color='grey', ha='center', va='top', fontsize=12, transform=plt.gca().transAxes)

    plt.xlabel(r'Radius [R$_{200}$]')
    plt.ylabel('Resolution [kpc]')

    plt.yscale('log')
    plt.xlim(xlims)
    plt.ylim(ylims)
    plt.legend(frameon=False)
    plt.legend()

    [ax.axvline(x=x, color='grey', linestyle='-', alpha=0.5) for x in [0.1, 1]] # Grey lines at 0.1R200 and R200
    y_pos = 10**(np.log10(ax.get_ylim()[0]) + 0.95 * (np.log10(ax.get_ylim()[1]) - np.log10(ax.get_ylim()[0]))) # Center pos.
    x_pos = [10**((np.log10(ax.get_xlim()[0]) + np.log10(0.1)) / 2),            # Between xlims[0] and 0.1
             10**((np.log10(0.1) + np.log10(1)) / 2),                           # Between 0.1 and 1
             10**((np.log10(1) + np.log10(ax.get_xlim()[1])) / 2)]              # Between R200 and xlims[1]
    [ax.text(x, y_pos, label, color='grey', ha='center', va='top', fontsize=12) for x, label in zip(x_pos, ['ISM', 'CGM', 'IGM'])]

    if savefig:
        if savefig==True:
            plt.savefig(f'res_vs_r.pdf',bbox_inches='tight')
        else:
            plt.savefig(f'{savefig}/res_vs_r.pdf',bbox_inches='tight')


def half_mass_radius(genpath, folders, timesteps, var_str='gas', hnum=None, alt_path=None):
    """
    Returns the evolution of the half mass radius within 0.1*r200 either for gas or for stars.
    Adapted from Marion Farcy 03/19 by Maaaaax 07/21    
    """
    # timesteps = [7, 8, 9, 10, 18, 31, 45, 75, 100, 125, 150, 175, 204] # 4, 5, 6, 7, 8, 9, 
    _, ax   = plt.subplots(nrows=1, ncols=1)
    for ind_f, folder in enumerate(folders):
        time_gyr, hmr, r200ISM = [], [], []
        for timestep in timesteps:
            RamsesDir = genpath+folder
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            cu2cm = info['cu2cm']
            r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
            r200ISM.append(0.1*r200_cm/kpc2cm)

            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            time_gyr.append(cosmo.age(1/info['aexp']-1).value)

            if var_str=='gas':
                # Gas prop
                T, prop = ras.extract_cells(RamsesDir,timestep,['T','mass'],factor=0.1, alt_path=alt_path)
            else:
                # Stellar prop
                prop, rad = ras.extract_stars(RamsesDir, timestep, ['star_mass', 'star_rad'], factor=0.1, alt_path=alt_path)
            if False: # Kret version
                prop, rad = prop[(T<5e4)], rad[(T<5e4)] # T won't work with stars
            rad = rad/kpc2cm
            find = [(rad[i],prop[i]) for i in range(0,len(rad))]
            find = sorted(find)
            tot = np.sum(prop)
            lim = tot * 0.5
            somme = 0
            compt = 0
            while somme < lim:
                somme += find[compt][1]
                compt += 1
            compt = compt-1
            hmr.append(find[compt][0]) # Half mass radius

        ax.plot(time_gyr, hmr, label=foldername(folder))
        ax.plot(time_gyr, r200ISM, ls='--')
    ax.plot(np.nan, ls='-', c='k', label='R$_{1/2}$')
    ax.plot(np.nan, ls='--', c='k', label='0.1 R$_{200}$')
    ax.set_xlabel('Time [Gyr]')
    ax.set_ylabel('Radius [kpc]')
    ax.legend()
    add_z_top_axis(ax, cosmo)


def rad_prof(genpath, folders, timesteps, var_str='H', overwhat='V', r_max=150, factor=2, nbins=100, split_flow=False, xlims=None, ylims=None, xlog=False, savefig=False, hnum=None, alt_path=None):
    """
    Plots the average density of the CGM and it's average mass-weighted temperature through radial chunks. It is also possible to remove satellites from the selection.
    Stacks timesteps up to the rvir of the first timestep (the smaller one).

    Parameters:
    -----------
        var_str: 'gas' or 'HI'
            The variable to plot: all the gas or neutral gas.
        overwhat: 'v' or 'r'
            Choose if either sum(var*dV)/sum(dV) or sum(var*dV)/sum(r) (a la Mitchell) is plotted
        nbins: int
            Number of bins along x axis (number of points in the graph).

    Example:
    --------
        timestep = 17
        genpath = '/mnt/lyoccf/scratch/mrey/outputs/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        plot_cgm_time(genpath, folders, timestep, shape="no_rebloch", xlims=None, ylims=None, savefig=False)
    """                                         # TODO: redundant with vs_r, no???
    # If there is just one folder
    if (not isinstance(folders,list)):
        folders=[folders]
    # timesteps must be an array
    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps = [timesteps]

    r_max_cm = r_max*kpc2cm
    len_tstep = len(timesteps)

    if overwhat=='V' or overwhat=='v':
        ylabel = fr'$ \sum_{{{len_tstep}\ snaps}} dm_{{{var_str}}} \left/ \sum_{{{len_tstep}\ snaps}} dV \right. \ [n_H.cm^{{-3}}]$'
        info   = ras.read_info(RamsesDir[0], timestep[-1])   # TODO: shouldn't this and multp_fact be in the loop? Oo
        multp_fact = info['unit_nH']/info['unit_d']/g2Msun
    elif overwhat=='r':
        ylabel = fr'$ \sum_{{{len_tstep}\ snaps}} dm_{{{var_str}}}\ \left/ \ d\left(r/R_{{200, min}}\right)\  \right. \ \ [M_\odot]$'
        multp_fact = 1

    fig, ax = plt.subplots(nrows=1, ncols=1)
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color'] 
    for index, folder in enumerate(folders):
        RamsesDir = genpath+folder
        print('Folder', folder)
        var_out_arr, var_in_arr = [], []
        
        # Create file to store data.
        filepath = f'{RamsesDir}/ratadat/'
        if not os.path.exists(filepath):
            os.mkdir(filepath)
        filename = f'{filepath}radprof.h5'
        if os.path.exists(filename):
            file = h5py.File(filename,'r+')
        else:
            file = h5py.File(filename,'w')

        for timestep in timesteps:
            # If the data is already computed,load it and go to next iteration. Else, compute it.
            str_out = f'out_{var_str}_{overwhat}_{r_max:.0f}_{timestep:05d}_{nbins}'
            str_in  =  f'in_{var_str}_{overwhat}_{r_max:.0f}_{timestep:05d}_{nbins}'
            try:
                var_out_arr.append(file[str_out][:])
                var_in_arr. append(file[str_in ][:])
                continue
            except KeyError:
                pass
            print('\t Computing... \t timestep', timestep)
            # Get radius of cells
            var, v_rproj, norm_r, cell_dx = ras.extract_cells(RamsesDir, timestep, [var_str, 'v_rproj', 'norm_r', 'cell_dx'], factor=factor, alt_path=alt_path)
            if var_str=='H':
                var *= g2Msun

            # Split between outflows/inflows                                   # TODO: add to extract_cells?
            outflow_cells = (v_rproj > 0) # & (top_cone | bot_cone)
            inflow_cells  = (v_rproj < 0) # & (top_cone | bot_cone)

            vol_out    = cell_dx[outflow_cells & (norm_r<r_max_cm)]**3 # nsat & 
            vol_in     = cell_dx[inflow_cells  & (norm_r<r_max_cm)]**3 # nsat & 
            norm_r_out = norm_r[outflow_cells  & (norm_r<r_max_cm)]    # nsat & 
            norm_r_in  = norm_r[inflow_cells   & (norm_r<r_max_cm)]    # nsat & 

             # Bin radially var*vol (mass)
            var_out, rad = np.histogram(norm_r_out, range=[0,r_max_cm], weights=var[outflow_cells & (norm_r<r_max_cm)]*vol_out, bins=nbins) # nsat & 
            var_in , _   = np.histogram(norm_r_in , range=[0,r_max_cm], weights=var[inflow_cells  & (norm_r<r_max_cm)]*vol_in , bins=nbins) # nsat & 
            # Choose normalisation
            if overwhat=='V' or overwhat=='v': # divide by total volume
                nrmlz_out, _ = np.histogram(norm_r_out, range=[0,r_max_cm], weights=vol_out, bins=nbins)
                nrmlz_in,  _ = np.histogram(norm_r_in , range=[0,r_max_cm], weights=vol_in, bins=nbins)
            elif overwhat=='r': # divide by dr/r200 (r_max_cm/nbins/r_max_cm)
                nrmlz_out = 1/nbins
                nrmlz_in  = 1/nbins
            else:
                raise ValueError('The overwhat option you picked is not available.')
            # Append
            varnrmlz_out = var_out/nrmlz_out
            varnrmlz_in  = var_in /nrmlz_in
            var_out_arr.append(varnrmlz_out)
            var_in_arr. append(varnrmlz_in )
            file.create_dataset(str_out,data=varnrmlz_out)
            file.create_dataset(str_in ,data=varnrmlz_in )

        temp_arr = np.linspace(0, r_max, num=nbins+1)
        x_axis = (temp_arr[:-1]+temp_arr[1:])/2

        var_out_arr = np.array(var_out_arr) 
        var_in_arr  = np.array(var_in_arr) 

        if split_flow:
            # Plot mean outflows and std
            median_varout = np.median(var_out_arr*multp_fact, axis=0)
            ax.plot(x_axis, median_varout, label=foldername(folder), color=colors[index])
            ax.fill_between(x_axis,np.percentile(var_out_arr*multp_fact,perc_min, axis=0), np.percentile(var_out_arr*multp_fact,perc_max, axis=0), color=colors[index], alpha=0.25)

            # Plot mean inflows and std
            median_varin = np.median(var_in_arr*multp_fact, axis=0)
            ax.plot(x_axis, median_varin, ls='--', color=colors[index])
            ax.fill_between(x_axis,np.percentile(var_in_arr*multp_fact,perc_min, axis=0), np.percentile(var_in_arr*multp_fact,perc_max, axis=0), color=colors[index], alpha=0.25)
        else:
            var_tot = (var_in_arr+var_out_arr)*multp_fact
            median_vartot = np.median(var_tot, axis=0)
            ax.plot(x_axis, median_vartot, label=foldername(folder), color=colors[index])
            ax.fill_between(x_axis,np.percentile(var_tot,perc_min, axis=0), np.percentile(var_tot,perc_max, axis=0), color=colors[index], alpha=0.25)

    if split_flow:
        ax.plot(np.nan, 'k-', label = 'outflows')
        ax.plot(np.nan, 'k--', label = 'inflows')
    ax.legend(frameon=False)

    ax.set_yscale('log')
    if xlog:
        ax.set_xscale('log')
    ax.set_xlabel('$r$ [kpc]')
    ax.set_ylabel(ylabel)
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    
    plt.tight_layout()
    plt.show()

    if savefig:
        fig.savefig('mass_cgm.pdf', bbox_inches='tight')

def rad_Zfrac(genpath, folders, timesteps, r_max=150, factor=2, nbins=100, split_flow=False, xlims=None, ylims=None, logx=False, savefig=False, hnum=None, alt_path=None): # TODO: also redundant with vs_r, no?
    """
    Plots the average density of the CGM and it's average mass-weighted temperature through radial chunks. It is also possible to remove satellites from the selection.
    Stacks timesteps up to the rvir of the first timestep (the smaller one).

    Parameters:
    -----------
        var_str: 'gas' or 'HI'
            The variable to plot: all the gas or neutral gas.
        overwhat: 'v' or 'r'
            Choose if either sum(var*dV)/sum(dV) or sum(var*dV)/sum(r) (a la Mitchell) is plotted
        nbins: int
            Number of bins along x axis (number of points in the graph).

    Example:
    --------
        timestep = 17
        genpath = '/mnt/lyoccf/scratch/mrey/outputs/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        labels = ['1', '2', '3']
        plot_cgm_time(genpath, folders, timestep, shape="no_rebloch", xlims=None, ylims=None, savefig=False)
    """
    # If there is just one folder
    if (not isinstance(folders,list)):
        folders=[folders]
    # timesteps must be an array
    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps = [timesteps]
    r_max_cm = r_max*kpc2cm

    fig, ax = plt.subplots(nrows=1, ncols=1)
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color'] 
    for index, folder in enumerate(folders):
        RamsesDir = genpath+folder
        print('Folder', folder)
        var_arr = []

        for timestep in timesteps:
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            cu2cm = info['cu2cm']
            mass, Z, norm_r  = ras.extract_cells(RamsesDir, timestep, ['mass', 'Z', 'norm_r'], factor=factor, alt_path=alt_path)

            # Bin radially sum(mass*Z)/sum(mass)
            var_hist, rad = np.histogram(norm_r[norm_r<r_max_cm], range=[0,r_max_cm], weights=Z[norm_r<r_max_cm]*mass[norm_r<r_max_cm], bins=nbins)
            nrmlz, _ = np.histogram(norm_r[norm_r<r_max_cm], range=[0,r_max_cm], weights=mass[norm_r<r_max_cm], bins=nbins)
            var_arr.append(var_hist/nrmlz)

        x_axis = (rad[:-1]+rad[1:])/2/kpc2cm
        var_arr = np.array(var_arr) 

        median_vartot = np.median(var_arr, axis=0)
        ax.plot(x_axis, median_vartot, label=foldername(folder), color=colors[index])
        ax.fill_between(x_axis,np.percentile(var_arr,perc_min, axis=0), np.percentile(var_arr,perc_max, axis=0), color=colors[index], alpha=0.25)

    ax.legend(frameon=False)
    ax.set_yscale('log')
    if logx:
        ax.set_xscale('log')
    ax.set_xlabel('$r$ [kpc]')
    ax.set_ylabel('Metal mass fraction')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    
    plt.tight_layout()
    plt.show()

    if savefig:
        fig.savefig(f'rad_Zprof.pdf', bbox_inches='tight')


###################################################################################################################################
########################################################## Stars-related ##########################################################
###################################################################################################################################
def compute_sfr(star_age, star_minit, info, hist_lims=None, avgMyr=10, bins=1000):
    """ 
    Computes sfr and returns its value with the corresponding time array. The array "times" is the simulation time at which the star was formed.

    Example:
    --------
        star_age, star_minit = ras.extract_stars(RamsesDir, timestep, ['star_age', 'star_minit'], factor=1)
        compute_sfr(star_age, star_minit, info, hist_lims=None, bins=300)
    """
    star_init_mass = star_minit * g2Msun                    # [Msun]
    if info['is_cosmo']:
        cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
        star_birth = cosmo.age(1/info['aexp']-1).value*1000 - star_age
    else:
        star_birth = info['t_myr'] - star_age               # [Myr]
    if hist_lims==None:
        hist_lims=(0,star_birth.max())

    dM, bin_edges = np.histogram(star_birth, range=(hist_lims), density=False, weights=star_init_mass, bins=bins)
    times = (bin_edges[:-1]+bin_edges[1:])*0.5              # [Myr]
    dt = (times[1]-times[0])*1e6                            # [yr]
    sfr = dM / dt                                           # [Msun/yr]

    if avgMyr:
        dt = times[1] - times[0]
        if dt > avgMyr:
            raise ValueError('The time resolution is too large to compute the average SFR, increase the number of bins.')
    window_size = int(avgMyr/dt)                    # nb of points for convolution
    window = np.ones(window_size) / window_size     # array normalised by the number of points, ex: (1,1,1)/3
    sfr = np.convolve(sfr, window, 'same')          # averaged sfr over the window. zero paddnig at the extremities.

    return times, sfr

def plot_sfr(genpath, folders, timestep, factor=0.1, hist_lims=None, avgMyr=10, bins=1000, y_log=True, xlims=None, ylims=None, savefig=False, hnum=None, ax=None, alt_path=None, use_yt=False):
    """ 
    Plots sfr within 0.1 Rvir for one or several output files.
    To plot only only from one simulation, use either a single folder or RamsesDir instead of genpath with None or '' instead of folders.

    Example:
    --------
        timestep = 17
        genpath = '/some/path/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        labels = ['1', '2', '3']
        plot_sfr(genpath, folders, timestep, hist_lims=None, avgMyr=100, savefig=False)
    """
    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
    if (not isinstance(folders,list)):
        folders=[folders]

    do_legend = False
    if not ax:
        do_legend = True
        fig, ax = plt.subplots(nrows=1, ncols=1)
    for folder in folders:
        RamsesDir = genpath + folder
        info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
        star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_age','star_minit'],factor=factor, alt_path=alt_path, use_yt=use_yt)
        times, sfr = compute_sfr(star_age, star_minit, info, hist_lims=hist_lims, avgMyr=avgMyr, bins=bins)
        if not info['is_cosmo']: times *= 1e3 # [Gyr] to [Myr]
        ax.plot(times/1000, sfr, label=foldername(folder))
    
    if do_legend:
        plt.legend()
        if y_log:
            ax.set_yscale('log')
        ax.set_xlim(xlims)
        ax.set_ylim(ylims)

        if info['is_cosmo']:
            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            add_z_top_axis(ax, cosmo)
            ax.set_xlabel('Simulation time [Gyr]')
        else:
            ax.set_xlabel('Simulation time [Myr]')
        ax.set_ylabel(r'SFR [M$_\odot$ yr$^{-1}$]')

        plt.tight_layout()
        plt.show()

        if savefig:
            if savefig!=True:
                fig.savefig(f'{savefig}sfr.pdf', bbox_inches='tight')
            else:
                fig.savefig('sfr.pdf', bbox_inches='tight')


def plot_burst(genpath, folders, timestep, avgMyr=1, burst_period=100, ylims=None, savefig=False, alt_path=None):
    """
    avgMyr: as for plot_SFR, as small as possible to capture unsmoothed burstiness (i.e. check if SFR plots look bursty)
        Increasing moves the top of distribution a bit down, and extend it to lower values until the really smoothed (when no burst anymore on the SFR plots) and increase. 
    burst_period: Each burst for the violin is computed over this range. Needs to be large enough to capture all burst events. 
        Differences are washed out if too small, not enough points if too big. 100 is good.
    """
    burst_period2 = 500     # Second burstiness computation [Myr]
    time_cut      = 2e3     # Start time for burstiness computation [Myr]
    burst_tot, burst_list_l, burst_list_r  = {}, {}, {}
    for fold in folders:
        RamsesDir = genpath + fold
        info = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
        star_age, star_minit = ras.extract_stars(RamsesDir, timestep, var_list=['star_age', 'star_minit'], factor='fullbox', alt_path=alt_path)
        times, sfr = compute_sfr(star_age, star_minit, info, avgMyr=avgMyr, bins=10000)
        restr = times>time_cut   # Restrict sfr to after 2Gyr to skip initial phase.
        times, sfr = times[restr], sfr[restr]
        if burst_period>times[-1]-times[0]:
            raise ValueError("Burst period is longer than the simulation duration.")

        dt = times[1] - times[0]                # Myr
        half_window_l = round(burst_period /dt) // 2   # Half number of cells in the window (1/2 because centered)
        half_window_r = round(burst_period2/dt) // 2   # Half number of cells in the window (1/2 because centered)
        burstiness_values_l = []
        burstiness_values_r = []
        # Compute left violin (burst_period)
        for i in range(half_window_l, len(sfr) - half_window_l):
            window_sfr = sfr[i - half_window_l: i + half_window_l]
            sig_sfr, mu_sfr = np.std(window_sfr), np.mean(window_sfr)
            burstiness_values_l.append((sig_sfr - mu_sfr) / (sig_sfr + mu_sfr))
        burst_list_l[fold] = burstiness_values_l
        # Compute right violin (burst_period2)
        for i in range(half_window_r, len(sfr) - half_window_r):
            window_sfr = sfr[i - half_window_r: i + half_window_r]
            sig_sfr, mu_sfr = np.std(window_sfr), np.mean(window_sfr)
            burstiness_values_r.append((sig_sfr - mu_sfr) / (sig_sfr + mu_sfr))
        burst_list_r[fold] = burstiness_values_r

        # Compute burstiness
        sig_sfr = np.std(sfr)
        mu_sfr = np.mean(sfr)
        burst_tot[fold] = (sig_sfr / mu_sfr - 1) / (sig_sfr / mu_sfr + 1)
        print(fold, burst_tot[fold])

    # Plotting
    fig, ax = plt.subplots()
    colors  = plt.rcParams['axes.prop_cycle'].by_key()['color']
    simu_names = list(burst_list_l.keys())
    data_to_plot_l = [burstiness for burstiness in burst_list_l.values()]  # Left side
    data_to_plot_r = [burstiness for burstiness in burst_list_r.values()]  # Right side
    violin_parts_l = ax.violinplot(data_to_plot_l, showmeans=False, showextrema=False, showmedians=True)  # Left violon
    violin_parts_r = ax.violinplot(data_to_plot_r, showmeans=False, showextrema=False, showmedians=True)  # Right violon
    for violin_parts, is_left in zip([violin_parts_l, violin_parts_r], [True, False]): # Clip and color the violins
        for i, pc in enumerate(violin_parts['bodies']):
            x_vals = pc.get_paths()[0].vertices[:, 0]
            pos_violin = np.mean(x_vals)    # Clipping threshold
            pc.get_paths()[0].vertices[:, 0] = np.clip(x_vals, -np.inf if is_left else pos_violin, pos_violin if is_left else np.inf)
            pc.set_facecolor(colors[i])     # Set violon shaded area
            pc.set_edgecolor(colors[i])     # Set edge color
        for part in ['cbars', 'cmins', 'cmaxes', 'cmedians']:                           # Clip and color medians and extrema
            if part in violin_parts:
                violin_parts[part].set_edgecolor(colors)  # Set edge color for lines
                for line in violin_parts[part].get_paths():
                    x_vals = line.vertices[:, 0]
                    pos_violin = np.mean(x_vals)  # Clipping threshold
                    line.vertices[:, 0] = np.clip(x_vals, -np.inf if is_left else pos_violin, pos_violin if is_left else np.inf)
                    # Show ugly text... for numbers in paper
                    # if part == 'cmedians':
                    #     for median in violin_parts[part].get_paths():
                    #         median_value = median.vertices[0][1]  # y-position of the median
                    #         median_x_position = median.vertices[0][0]  # x-position of the median line
                    #         ax.text(median_x_position, median_value, f'{median_value:.2f}', color='black', ha='center', va='center')
    for i, fold in enumerate(folders, start=1):# Over the total simulation
        plt.scatter(i, burst_tot[fold], color=colors[i-1], marker='x')

    ax.set_ylim(ylims)
    ax.set_xticks(np.arange(1, len(simu_names) + 1))
    ax.set_xticklabels([foldername(simuname) for simuname in simu_names])
    ax.set_ylabel(rf'$<\mathrm{{Burstiness}}>\rm_{{{burst_period},{burst_period2}\,\mathrm{{Myr}}}}$')
    ax.minorticks_off()
    plt.show()

    if savefig:
        if savefig!=True:
            fig.savefig(f'{savefig}burstiness.pdf', bbox_inches='tight')
        else:
            fig.savefig('burstiness.pdf', bbox_inches='tight')


def plot_mass_evol(genpath, folders, timestep, bins=300, xlims=None, ylims=None, factor=0.1, savefig=False, hnum=None, alt_path=None):
    """ 
    Plots stellar mass evolution from one or several output files.
    To plot only only from one simulation, use either a single folder or RamsesDir instead of genpath with None or '' instead of folders.

    Parameters:
    -----------
        minit: boolean
            Sets if the quantity used should be star_mass or star_minit. 
            If set to True, it doesn't account for mass loss through supernova and only plot formed stellar mass.
            Otherwise it does but there will be an error on the x-axis being the time the star took to explode. 

    Example:
    --------
        timestep = 17
        genpath = '/some/path/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        labels = ['1', '2', '3']
        plot_mass_evol(genpath, folders, timestep, bins=300, savefig=False)
    """
    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
    if (not isinstance(folders,list)):
        folders=[folders]

    fig, ax = plt.subplots(nrows=1, ncols=1)
    for folder in folders:
        RamsesDir = genpath + folder
        info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
        star_age, star_minit = ras.extract_stars(RamsesDir, timestep, ['star_age', 'star_minit'], factor=factor, alt_path=alt_path)

        if info['is_cosmo']:
            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            star_birth = cosmo.age(1/info['aexp']-1).value*1e3 - star_age  # [Myr] 
        else:
            star_birth = info['t_myr'] - star_age                           # [Myr]
        hist_lims=(0,star_birth.max())
        dM, bin_edges = np.histogram(star_birth, range=(hist_lims), density=False, weights=star_minit, bins=bins)
        times = (bin_edges[:-1]+bin_edges[1:])*0.5                          # [Myr]
        mass_evol = np.cumsum(dM)                                           # [Msun/yr]

        ax.plot(times/1000, mass_evol, label=foldername(folder))

    ax.set_yscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    plt.legend()
    
    if info['is_cosmo']:
        add_z_top_axis(ax, cosmo)
        ax.set_xlabel('Simulation time [Gyr]')
    else:
        ax.set_xlabel('Simulation time [Myr]')
    ax.set_ylabel(r'Stellar mass formed [M$_\odot$]')

    plt.tight_layout()
    plt.show()

    if savefig:
        if savefig!=True:
            fig.savefig(f'{savefig}mass_evol.pdf', bbox_inches='tight')
        else:
            fig.savefig('mass_evol.pdf', bbox_inches='tight')


def link_stars_cells(cell_pos,cell_l,star_x,star_y,star_z):
    """
    Returns an array of index that link the stellar particles and the cells together. 
    This is done using a k-d tree and only considering the first neighbour.
    It also returns the distance to the nearest cell center (usefull when computing several neigbours).
    The star with index i has star_age[i], star_mass[i] and is in a cell with nH[idx_ccell[i]], T[idx_ccell[i]].

    Example:
    --------
        _,idx_ccell = link_stars_cells(cell_pos,cell_l,star_x,star_y,star_z)
    
    """
    # Construct k-d tree on cells
    cell_xyz = np.empty((cell_l.shape[0],3))
    cell_xyz[:,0],cell_xyz[:,1],cell_xyz[:,2] = cell_pos[:,0],cell_pos[:,1],cell_pos[:,2]
    kdtree = ckdt(cell_xyz,10)

    # Build array for stars position
    star_pos=np.empty((star_x.shape[0],3))
    star_pos[:,0],star_pos[:,1],star_pos[:,2] = star_x, star_y, star_z

    # Assign stars position to corresponding cells position (1st nearest neighbour)
    dist,idx_ccell = kdtree.query(star_pos,k=1) # distance_upper_bound=maxdist
    
    return dist,idx_ccell


def stars_density(genpath, folders, timesteps, nbins=50, factor=1, shade='perc', alt_path=None):
    """
    Plots the density at which stars aredepending on their age (useful to see the effect of runaway stars).

    Parameters:
    -----------
        shade: pltpts', 'std', 'perc'
            Select the kind of shaded ragion you want: all points, the standard deviation or the 1st and 3rd quartiles ?
    """
    #timesteps must be an array
    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps = [timesteps]
    
    thebins = np.linspace(0,50,nbins)
    _, ax = plt.subplots(nrows=1, ncols=1)
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']  
    for index, folder in enumerate(folders):
        RamsesDir = genpath+folder
        age_stars, nh_stars = [], []
        for timestep in timesteps:
            # Extract data and find cells with stars.
            nH, cell_pos, cell_l = ras.extract_cells(RamsesDir,timestep,['nH', 'cell_pos', 'cell_l'],factor=factor, alt_path=alt_path)
            star_x,star_y,star_z,star_age = ras.extract_stars(RamsesDir, timestep, ['star_x','star_y','star_z','star_age'], factor=factor, alt_path=alt_path)
            _,idx_ccell = link_stars_cells(cell_pos,cell_l,star_x,star_y,star_z)
            # stack all the data
            age_stars = np.concatenate((age_stars, star_age[(0<star_age)&(star_age<50)]))
            nh_stars = np.concatenate((nh_stars, nH[idx_ccell][(0<star_age)&(star_age<50)]))
            
        PDF, bin_edges = np.histogram(age_stars,bins=thebins,density=False,weights=nh_stars)
        nb, _ = np.histogram(age_stars,bins=thebins,density=False)
                   
        x_axis = (bin_edges[:-1]+bin_edges[1:])*0.5
        # Compute errors adn plot.
        if shade=='std':
            avg = PDF/nb
            ax.plot(x_axis,avg, label=foldername(folder), c=colors[index])
            nH2 = np.concatenate([nh_stars[(bin_edges[:-1][bini] < age_stars) & (age_stars <= bin_edges[1:][bini])] \
                    - PDF[bini]/nb[bini] for bini in range(nbins-1)]) # liste des xi - <x>
            PDF2, _ = np.histogram(age_stars,bins=thebins,density=False,weights=nh_stars**2) # sum((x_i-<x>)^2)
            sig = PDF2**0.5/nb
            ax.fill_between(x_axis,avg-sig, avg+sig, alpha=0.25, color=colors[index])
            err_lab = r'1 $\sigma$.'
        elif shade=='perc':
            err_bot, err_top = [], []
            median = []
            for bini in range(nbins-1):
                nH2 = nh_stars[(bin_edges[:-1][bini] < age_stars) & (age_stars <= bin_edges[1:][bini])]
                err_bot.append(np.percentile(nH2,25))
                err_top.append(np.percentile(nH2,75))
                median.append(np.percentile(nH2,50))
            ax.fill_between(x_axis,err_bot, err_top, alpha=0.25, color=colors[index])
            ax.plot(x_axis,median, label=foldername(folder), c=colors[index])
            err_lab = '25-75 percentiles.'

    ax.fill_between([],[], [], alpha=0.25, label=err_lab, color='k')
    plt.legend()
    ax.set_yscale('log')
    ax.set_xlabel('Stellar age [Myr]')
    ax.set_ylabel(fr'<n$_H$>$_{{{str(len(timesteps))}\ snaps}}$  [H.cm$^{{-3}}$]')


###################################################################################################################################
########################################################## Subhalos ##############################################################
###################################################################################################################################
def is_sat(RamsesDir, timestep, factor=1, hnum=None, alt_path=None):
    """
    Returns a boolean array corresponding to the position of the satellites (or the non-position of satelite). 
    isub is like myhalo in other functions. myhalo2hnum = hcat.hnum[myhalo] to use it as hnum.
    """
    # Extract cells position and halo catalog.
    info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
    cell_pos = ras.extract_cells(RamsesDir,timestep,['cell_pos'],factor=factor,alt_path=alt_path)
    cell_pos /= info['cu2cm']
    hcat   = ras.get_hcat(RamsesDir,timestep,alt_path=alt_path)
    myhalo = ras.mehalo  (RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
    center, r200 = ras.get_frad(RamsesDir,timestep,1,hnum=hnum,alt_path=alt_path)

    # remove cells which are in sub-halos 
    isubs = np.where((hcat.host == hcat.host[myhalo]) & (hcat.level>1))[0]
    ns = 0
    sat = np.full(np.shape(cell_pos[:,0]), False, dtype=bool)
    for isub in isubs:
        # Does sub overlap with shell ?  
        x,y,z = hcat.x_cu[isub],hcat.y_cu[isub],hcat.z_cu[isub]
        rsub = np.sqrt((x-center[0])**2 + (y-center[1])**2 + (z-center[2])**2)
        rmax = rsub + 0.5*hcat.rvir_cu[isub]    # Max dist to halo center.
        rmin = rsub - 0.5*hcat.rvir_cu[isub]    # Min dist to halo center.)
        # Only keep satellites within 0.1*r200 and r200
        if (rmax > 0.1*r200):   # Furthest part of the subhalo larger than 0.1 R200
            if (rmin > r200):   # Closest part of the subhalo within R200. Same as R200*factor as not subhalo if not within.
                continue
            # Only keep satellites with stars
            mstar = ras.get_mstar(RamsesDir, timestep, hnum=int(hcat.hnum[isub]))
            if mstar==0:
                continue
            ns = ns+1
            xx,yy,zz = cell_pos[:,0]-x,cell_pos[:,1]-y,cell_pos[:,2]-z
            d2 = (xx*xx + yy*yy + zz*zz)
            sat = (d2 < (0.2*hcat.rvir_cu[isub])**2) | sat   # Satellite = all cells within 0.2 Rvir_sub
    # print('Removed %i cells out of %i for %i satellites.'%(len(sat)-len(sat[sat==False]),len(sat), ns))
    return sat


###################################################################################################################################
############################################################ Outflows  ############################################################
###################################################################################################################################

def compute_flux(rho, v_away, cell_dx, surface, cond1, cond2, is_cosmo):
    """
    Returns the flux through a shell in [Msun/yr].
    Cosmological: F = S * sum(mv) / sum(V)
        Surface is only used here, and cond2 is used because we still want to divide by the whole shell volume.
    Idealised:    F = sum(rho v dx^2)
        S=1 because dx**2 is different for inflows and outflows.
    """
    if is_cosmo:    #  [g/cm^3] *   [cm/s]    *      [cm**3]      /          [cm^3]           * [cm^2] * [Msun/g] * [s/yr]
        return np.sum(rho[cond1]*v_away[cond1]*cell_dx[cond1]**3) / np.sum(cell_dx[cond2]**3) * surface * g2Msun * (Myr2s/1e6)
    else:           #   [g/cm^3  ] *      [cm/s]      *      [cm^2]      * [Msun/g] * [s/yr]
        return np.sum(rho[cond1]*np.abs(v_away[cond1])*cell_dx[cond1]**2) * g2Msun * (Myr2s/1e6)


def init_flows():
    return {
        'out': {'all': [], 'cold': [], 'warm': [], 'hot': []},
        'in': {'all': [], 'cold': [], 'warm': [], 'hot': []}
    }


def flow_vs_rad(genpath, folders, timesteps, flow_dir='out', T_split=True, radkpc=100, nbins=100, factor=1, xlims=None, ylims=None, savefig=False, hnum=None, alt_path=None):
    """ 
    Plots the inflow and outflow rates in shells of 0.1 kpc up to radmax kpc.
    If T_split, three additional plots of the cold, warm and hot phases of the outflows are plotted. Very similar to flow_vs_t.
    flow_dir: 'out' for outflows, and 'in' for inflows
    """
    #timesteps must be an array
    if isinstance(timesteps, (float, int)):
        timesteps = [timesteps]
    if (not isinstance(folders,list)):
        folders=[folders]


    #Initialisation
    two_by_two = False  # Whether the plot should be 2x2 or 4x1.
    filename   = 'flows'
    temp_formats = {
        'cold': f'_1e{np.log10(T_c)}',
        'warm': f'_1e{np.log10(T_c)}-1e{np.log10(T_h)}',
        'hot': f'_1e{np.log10(T_h)}'
    }
    if T_split:
        figsize_def = plt.rcParams['figure.figsize']
        figsize = figsize_def.copy()
        if two_by_two:
            figsize = [size * 1.5 for size in figsize_def]
            figsize[1] = figsize_def[1] * 0.9 # make the fig more long than high.
            fig, axs = plt.subplots(nrows=2, ncols=2, figsize=figsize, sharex=True, sharey=True)
        else:
            figsize[0] = figsize_def[0] * 0.75
            figsize[1] = figsize_def[1] * 2.5
            fig, axs = plt.subplots(nrows=4, ncols=1, figsize=figsize, sharex=True, sharey=True)
        axs = axs.ravel()
        ax = axs[0]
    else:
        fig, axs = plt.subplots(nrows=1, ncols=1)
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors     = prop_cycle.by_key()['color'] 

    #Set x-axis
    rad_bin = np.logspace(np.log10(0.1), np.log10(radkpc), nbins+1)
    x_axis = 10**((np.log10(rad_bin[:-1])+np.log10(rad_bin[1:]))*0.5)
    plt.xscale('log')

    # Loop on folders
    for index, folder in enumerate(folders):
        RamsesDir = genpath+folder
        flows_stack = init_flows()
        # Loop on timesteps
        for timestep in timesteps:
            flows_1tstep = init_flows()
            try:    # try to read saved data.
                base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
                flows_1tstep[flow_dir]['all'] = ras.read_prop(RamsesDir, timestep, f'{base_name}_all', filename, hnum=hnum, alt_path=alt_path)
                if T_split:
                    for temp in ['cold', 'warm', 'hot']:
                        var_name = f'{base_name}_{temp_formats[temp]}'
                        flows_1tstep[flow_dir][temp] = ras.read_prop(RamsesDir, timestep, var_name, filename, hnum=hnum, alt_path=alt_path)
            except: # otherwise, compute and save.
                print(f'{folder}: timestep {timestep}', end='\r')
                info  = ras.read_info(RamsesDir, timestep)
                cu2cm = info['unit_l'] * info['boxlen']
                v_cut = 0.5*ras.get_vcirc(RamsesDir, timestep, factor=1, hnum=None, alt_path=None)*1e2  # velocity cut [cm/s]
                sign = 1 if flow_dir == 'out' else -1

                # Case 1: cosmo
                if info['is_cosmo']:
                    # Check that the data for all timesteps reaches radkpc
                    r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
                    if timestep==timesteps[0] and factor!='fullbox' and r200_cm*factor < np.max(x_axis)*kpc2cm:
                        raise ValueError(f'The region loaded with factor = {factor} is too small for {radkpc} kpc, use at least {np.max(x_axis)*kpc2cm/r200_cm}.')

                    # Read data and exclude satellites 
                    cell_dx, rho, T, v_rproj, norm_r  = ras.extract_cells(RamsesDir,timestep,['cell_dx', 'rho', 'T', 'v_rproj', 'norm_r'],factor=factor,alt_path=alt_path)
                    nsat = ~is_sat(RamsesDir, timestep, factor=factor, alt_path=alt_path)
                    cell_dx, rho, T, v_rproj, norm_r = cell_dx[nsat], rho[nsat], T[nsat], v_rproj[nsat], norm_r[nsat]

                    # Compute outflow in each bin
                    flow_mask = ((v_rproj > v_cut) if flow_dir == 'out' else (v_rproj < -v_cut))
                    for dist_cm in x_axis*kpc2cm:
                        surface = 4*np.pi*dist_cm**2
                        shells = (norm_r <= dist_cm+cell_dx/2) & (norm_r >= dist_cm-cell_dx/2) & flow_mask                  # F through a spherical shell at dist_cm 
                        fluxes = sign*compute_flux(rho, v_rproj, cell_dx, surface, shells, shells, info['is_cosmo'])
                        flows_1tstep[flow_dir]['all'].append(fluxes)
                        if T_split:
                            conditions = [(shells & (T < T_c)), 
                                        (shells & (T > T_c) & (T < T_h)), 
                                        (shells & (T > T_h))]
                            temp_values = tuple([sign*compute_flux(rho, v_rproj, cell_dx, surface, cond, shells, info['is_cosmo']) for cond in conditions])
                            for i, temp in enumerate(['cold', 'warm', 'hot']):
                                flows_1tstep[flow_dir][temp].append(temp_values[i])

                # Case 2: idealised
                else:
                    cell_dx, rho, T, vz, cell_pos = ras.extract_cells(RamsesDir,timestep,['cell_dx', 'rho', 'T', 'vz', 'cell_pos'],factor=factor, alt_path=alt_path, use_yt=False)
                    z_pos = cell_pos[:,2] - ras.get_Gcen(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)[2]*cu2cm  # Center the z coordinates on the galaxy
                    flow_mask = vz*z_pos > 0 if flow_dir == 'out' else (vz*z_pos < 0)

                    # Compute outflow in each bin
                    for dist_cm in x_axis*kpc2cm:
                        shells = (np.abs(z_pos) <= dist_cm+cell_dx/2) & (np.abs(z_pos) >= dist_cm-cell_dx/2) & flow_mask    # F through a slab at dist_cm
                        fluxes = compute_flux(rho, vz, cell_dx, None, shells, None, info['is_cosmo'])
                        flows_1tstep[flow_dir]['all'].append(fluxes)
                        if T_split:
                            conditions = [(shells & (T < T_c)), 
                                        (shells & (T > T_c) & (T < T_h)), 
                                        (shells & (T > T_h))]
                            temp_values = tuple([compute_flux(rho, vz, cell_dx, surface, cond, None, info['is_cosmo'])  for cond in conditions])
                            for i, temp in enumerate(['cold', 'warm', 'hot']):
                                flows_1tstep[flow_dir][temp].append(temp_values[i])

                # Save it with add_prop
                base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
                ras.add_prop(RamsesDir, timestep, f'{base_name}_all', flows_1tstep[flow_dir]['all'], filename, hnum=hnum, alt_path=alt_path)
                if T_split:
                    for temp in ['cold', 'warm', 'hot']:
                        var_name = f'{base_name}_{temp_formats[temp]}'
                        ras.add_prop(RamsesDir, timestep, var_name, flows_1tstep[flow_dir][temp], filename, hnum=hnum, alt_path=alt_path)

            # Add timestep to stack.
            base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
            flows_stack[flow_dir]['all'].append(flows_1tstep[flow_dir]['all'])
            if T_split:
                for temp in ['cold', 'warm', 'hot']:
                    var_name = f'{base_name}_{temp_formats[temp]}'
                    flows_stack[flow_dir][temp].append(flows_1tstep[flow_dir][temp])

        # Plot everything
        medians = {f'{flow_dir}_{temp}': np.median(flows_stack[flow_dir][temp], axis=0) for temp in (['all'] + ['cold', 'warm', 'hot'] if T_split else ['_all'])}
        ax.plot(x_axis, medians[f'{flow_dir}_all'], c=colors[index], label=foldername(folder))
        ax.fill_between(x_axis, np.percentile(flows_stack[flow_dir]['all'], perc_min, axis=0), np.percentile(flows_stack[flow_dir]['all'], perc_max, axis=0), color=colors[index], alpha=0.25)
        if T_split:
            for i, temp in enumerate(['hot', 'warm', 'cold'], start=1):
                axs[i].plot(x_axis, medians[f'{flow_dir}_{temp}'], c=colors[index])
                axs[i].fill_between(x_axis, np.percentile(flows_stack[flow_dir][temp], perc_min, axis=0), np.percentile(flows_stack[flow_dir][temp], perc_max, axis=0), color=colors[index], alpha=0.25)
    # Plot settings
    ax.legend()
    ax.set_yscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ylabel = r'Outflow rate  [M$_\odot$/yr]' if flow_dir=='out' else r'Inflow rate  [M$_\odot$/yr]'
    ax.set_ylabel(ylabel)

    # Write text for total/cold/warm/hot and axes labels.
    if not T_split:
        ax.set_xlabel(r'Radius [kpc]')
        ax.text(0.95, 0.95, 'Total', transform=ax.transAxes, verticalalignment='top', horizontalalignment='right')
    else:
        ax.text(0.95, 0.05 if two_by_two else 0.95, 'Total', transform=ax.transAxes, verticalalignment='bottom' if two_by_two else 'top', horizontalalignment='right')
        filows = [f'Hot gas\nT > 10$^{{{np.log10(T_h):.1f}}}$', f'Warm gas\n10$^{{{np.log10(T_c):.1f}}}$ < T < 10$^{{{np.log10(T_h):.1f}}}$', f'Cold gas\nT < 10$^{{{np.log10(T_c):.1f}}}$']
        for i, title in enumerate(filows, start=1):
            axs[i].set_yscale('log')
            axs[i].text(0.05 if i in [1, 3] else 0.95, 0.05 if i == 1 else 0.95, f'{title}', transform=axs[i].transAxes, 
                        verticalalignment='bottom'  if (i == 1 and two_by_two) else 'top',
                        horizontalalignment='right' if (i == 2 or not two_by_two)  else 'left')
            axs[i].set_ylim(ylims) if ylims else None
        axs[2].set_ylabel(ylabel)
        if not two_by_two: [axs[ii].set_ylabel(ylabel) for ii in [0, 1, 3]]
        for i in ([2, 3] if two_by_two else [3]):
            axs[i].set_xlabel(r'Radius [kpc]')

    # Add lines and text to delimit the ISM and CGM.
    avgR200 = 97    # Average R200: np.mean(median or mean(simu over 1.3-1) for simu in simus) 
    for ii, axi in enumerate(axs.flatten()):
        [axi.axvline(x=x, color='grey', linestyle='-', alpha=0.5) for x in [0.1 * avgR200]] # Grey line at 0.1R200
        if ii<(2 if two_by_two else 1):    # Only put text on the top two columns.
            y_pos = 10**(np.log10(axi.get_ylim()[0]) + 0.95 * (np.log10(ax.get_ylim()[1]) - np.log10(ax.get_ylim()[0]))) # Center pos.
            x_pos = [10**((np.log10(axi.get_xlim()[0]) + np.log10(0.1 * avgR200)) / 2),                  # Between xlims[0] and 0.1R200
                    10**((np.log10(0.1 * avgR200) + np.log10(axi.get_xlim()[1])) / 2)]                   # Between 0.1R200 and xlims[1]
            [axi.text(x, y_pos, label, color='grey', ha='center', va='top', fontsize=12) for x, label in zip(x_pos, ['ISM', 'CGM'])]
    plt.show()

    if savefig:
        if savefig!=True:
            fig.savefig(f'{savefig}{flow_dir}flow_rate_radius.pdf', bbox_inches='tight')
        else:
            fig.savefig('{flow_dir}flow_rate_radius.pdf', bbox_inches='tight')

# TODO: these two are obsolete
# def calculate_flow(momentum, cell_dx, shell, surface, temperature=None, T_c=None, T_h=None):
#     """
#     Computes outflows within a shell: sum(m*v)/vol*surface (more realistic than sum(m*v/dx) when considering radial outflows).
#     """
#     if temperature is None or T_c is None or T_h is None:
#         return np.sum(momentum[shell]) / np.sum(cell_dx[shell]**3) * surface  # [Msun*cm/yr] / [cm^3] * [cm^2]   # S * sum(p) / sum(V).
#     else:
#         cold = np.sum(momentum[shell & (temperature < T_c)]) / np.sum(cell_dx[shell]**3) * surface
#         warm = np.sum(momentum[shell & (temperature > T_c) & (temperature < T_h)]) / np.sum(cell_dx[shell]**3) * surface
#         hot  = np.sum(momentum[shell & (temperature > T_h)]) / np.sum(cell_dx[shell]**3) * surface
#         return cold, warm, hot


# def flow_vs_t(genpath, folders, timesteps, T_split=True, radkpc=100, nbins=100, factor=1, xlims=None, ylims=None, savefig=False, hnum=None, alt_path=None):
#     """ 
#     Plots the inflow and outflow rate in a shell of 0.1 kpc at radkpc kpc for all timesteps.
#     If T_split, three additional plots of the cold, warm and hot phases of the outflows are plotted. Very similar to flow_vs_rad.
#     """
#     #timesteps must be an array
#     if isinstance(timesteps, (float, int)):
#         timesteps = [timesteps]
#     if (not isinstance(folders,list)):
#         folders=[folders]

#     #Initialisation
#     size_shell = 0.1*kpc2cm
#     rad_cm     = radkpc*kpc2cm
#     filename   = 'flows_t'

#     temp_formats = {
#         'cold': f'_1e{np.log10(T_c)}',
#         'warm': f'_1e{np.log10(T_c)}-1e{np.log10(T_h)}',
#         'hot': f'_1e{np.log10(T_h)}'
#     }
#     if T_split:
#         figsize = plt.rcParams['figure.figsize']
#         figsize = [size * 1.5 for size in figsize]
#         fig, axs = plt.subplots(nrows=2, ncols=2, figsize=figsize, sharex=True, sharey=True)
#         axs = axs.ravel()
#         ax = axs[0]
#     else:
#         fig, axs = plt.subplots(nrows=1, ncols=1)
#     prop_cycle = plt.rcParams['axes.prop_cycle']
#     colors     = prop_cycle.by_key()['color'] 

#     # Loop on folders
#     for index, folder in enumerate(folders):
#         RamsesDir = genpath+folder
#         flows_array = init_flows()
#         simu_time = []

#         for timestep in timesteps:
#             flows_1tstep = init_flows()
#             try:    # try to read saved data.
#                 simu_time.append(ras.read_prop(RamsesDir, timestep, 't', filename, hnum=hnum, alt_path=alt_path))
#                 for flow_dir in ['out', 'in']:
#                     base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
#                     flows_1tstep[flow_dir]['all'] = ras.read_prop(RamsesDir, timestep, f'{base_name}_all', filename, hnum=hnum, alt_path=alt_path)
#                     if T_split:
#                         for temp in ['cold', 'warm', 'hot']:
#                             var_name = f'{base_name}_{temp_formats[temp]}'
#                             flows_1tstep[flow_dir][temp] = ras.read_prop(RamsesDir, timestep, var_name, filename, hnum=hnum, alt_path=alt_path)
#             except: # otherwise, compute and save.
#                 info    = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
#                 r200_cm = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*info['cu2cm']
#                 if timestep==timesteps[0] and r200_cm*factor < rad_cm:
#                     raise ValueError(f'The region loaded when using factor = {factor} is too small for {radkpc} kpc, use at least {rad_cm/r200_cm}.')

#                 # Compute time
#                 if info['is_cosmo']:
#                     cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
#                     time_temp = cosmo.age(1/info['aexp']-1).value
#                 else:
#                     time_temp = info['t_myr']                           # [Myr]
#                 simu_time.append(time_temp)
#                 ras.add_prop(RamsesDir, timestep, 't', time_temp, filename, hnum=hnum, alt_path=alt_path)

#                 # Compute flows and save them
#                 norm_r, v_rproj, T, mass, cell_dx  = ras.extract_cells(RamsesDir,timestep,['norm_r','v_rproj','T','mass','cell_dx'],factor=factor,alt_path=alt_path)
#                 momentum = mass*g2Msun * v_rproj* (Myr2s/1e6)                                           # TODO: in extract_cells?cf previous todo   # [Msun*cm/yr]
#                 shell = (norm_r <= rad_cm+size_shell) & (norm_r >= rad_cm-size_shell)
#                 shells = {'out': ((v_rproj > 0) & (norm_r <= rad_cm) & shell), 'in': ((v_rproj < 0) & (norm_r <= rad_cm) & shell)}
#                 surface = 4*np.pi*rad_cm**2
#                 for flow_dir in ['out', 'in']:
#                     sign = 1 if flow_dir == 'out' else -1
#                     base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
#                     flows_1tstep[flow_dir]['all'] = calculate_flow(sign*momentum, cell_dx, shells[flow_dir], surface)
#                     ras.add_prop(RamsesDir, timestep, f'{base_name}_all', flows_1tstep[flow_dir]['all'], filename, hnum=hnum, alt_path=alt_path)
#                     if T_split:
#                         temp_values = calculate_flow(sign*momentum, cell_dx, shells[flow_dir], surface, T, T_c, T_h)
#                         for i, temp in enumerate(['cold', 'warm', 'hot']):
#                             var_name = f'{base_name}_{temp_formats[temp]}'
#                             flows_1tstep[flow_dir][temp] = temp_values[i]
#                             ras.add_prop(RamsesDir, timestep, var_name, flows_1tstep[flow_dir][temp], filename, hnum=hnum, alt_path=alt_path)

#             # Combine timesteps
#             for flow_dir in ['out', 'in']:
#                 base_name = f'{flow_dir}_{radkpc}kpc_{nbins}bins'
#                 flows_array[flow_dir]['all'].append(flows_1tstep[flow_dir]['all'])
#                 if T_split:
#                     for temp in ['cold', 'warm', 'hot']:
#                         var_name = f'{base_name}_{temp_formats[temp]}'
#                         flows_array[flow_dir][temp].append(flows_1tstep[flow_dir][temp])
        
#         # Plot everything
#         for flow_dir in ['out', 'in']:
#             linestyle = '--' if flow_dir == 'in' else '-'
#             ax.plot(simu_time, flows_array[flow_dir]['all'], c=colors[index], ls=linestyle, label=foldername(folder) if flow_dir == 'out' else None)
#             if T_split:
#                 for i, temp in enumerate(['cold', 'warm', 'hot'], start=1):
#                     axs[i].plot(simu_time, flows_array[flow_dir][temp], c=colors[index], ls=linestyle)

#     ax.plot(np.nan, label='Outflows', c='k')
#     ax.plot(np.nan, label='Inflows' , c='k', ls='--')
#     ax.legend(loc=(0.775, 0.05))
#     ax.set_yscale('log')
#     ax.set_xlim(xlims)
#     ax.set_ylim(ylims)
#     ax.text(0.95, 0.95, 'Total', transform=ax.transAxes, verticalalignment='top', horizontalalignment='right', fontsize=24)
#     ax.set_ylabel(r'Outflow rate  [M$_\odot$/yr$^{-1}$]')
#     info = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
#     if info['is_cosmo']:
#         cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
#         add_z_top_axis(ax, cosmo)
#         xlabel = 'Simulation time [Gyr]'
#     else:
#         xlabel = 'Simulation time [Myr]'
#     if not T_split:
#         ax.set_xlabel(xlabel)
#     else:
#         filows = [f'Cold gas\nT < 10$^{{{np.log10(T_c):.1f}}}$', f'Warm gas\n10$^{{{np.log10(T_c):.1f}}}$ < T < 10$^{{{np.log10(T_h):.1f}}}$', f'Hot gas\nT > 10$^{{{np.log10(T_h):.1f}}}$']
#         for i, title in enumerate(filows, start=1):
#             axs[i].set_yscale('log')
#             axs[i].text(0.95, 0.95, f'{title}', transform=axs[i].transAxes, verticalalignment='top', horizontalalignment='right', fontsize=24)
#             axs[i].set_ylim(ylims) if ylims else None
#         axs[2].set_ylabel(r'Outflow rate  [M$_\odot$/yr$^{-1}$]')
#         add_z_top_axis(axs[1], cosmo)
#         for i in [2, 3]:
#             axs[i].set_xlabel(xlabel)
#     plt.show()

#     if savefig:
#         if savefig!=True:
#             fig.savefig(f'{savefig}Outflow_rate_time.pdf', bbox_inches='tight')
#         else:
#             fig.savefig('Outflow_rate_time.pdf', bbox_inches='tight')


###################################################################################################################################
############################################################## M*-Mh ##############################################################
###################################################################################################################################
def Ms_Mh_relation(genpath, folders, timesteps, xmin=None, ymin=None, norm=False, savefig=False, hnum=None, alt_path=None):
    """
    Plots the M_star - M_halo relation for specific snapshots a a simulation (with a different namelist, you need to change the timesteps considered ^^').
    Also includes several observational relations read from specific files. The path has to be changed if this is not the original simulation. Very specific one-time use function.
    """
    fig, ax = plt.subplots(nrows=1, ncols=1) # , figsize=(12, 7.2)
    shape_s = ['o', 'X', 'P', 'D', "^", "v", "*", "1", "H", "$a$", "$b$", "$c$", 'x', '+']
    vmin, vmax = 1, 7               # zmin, zmax, integer please

    colors = ['blue', 'cyan', 'lightgreen', 'yellow', 'orange', 'red', 'darkmagenta']
    cm = LinearSegmentedColormap.from_list('custom_cmap', colors, N=100)
    mark, lab = [], []

    # DM from DM-only simulation
    # If the aout in any simulation is not a subset of the DM one -> use the halo DM mass from each simulation.
    folder_DM = '/Xnfs/cosmos/mrey/ICs/Zoom-56-35796/dmRun2-LSS1Rvir'
    aout_DM = get_aout(folder_DM)
    DM_only = all(set(get_aout(genpath+folder)) <= set(aout_DM) for folder in folders)
        
    if DM_only: # There is a correction factor: in DM-only simu, baryons mass is in the DM mass.
        timesteps = np.array([tstep for tstep in timesteps if tstep <= len(aout_DM)+1])
        redshift = np.array([1 / ras.read_info(folder_DM, timestep)['aexp'] - 1 for timestep in timesteps])
        M_DM = np.array([ras.get_m200(folder_DM, timestep, hnum=hnum) for timestep in timesteps])*(1-omega_b/omega_m)

    # Compute M_star from the simulations
    for ind_f, folder in enumerate(folders):
        RamsesDir = genpath+folder
        last_output = max([int(filename.split("_")[1]) for filename in os.listdir(RamsesDir) if filename.startswith("output_")])
        timesteps_cut = [tstep for tstep in timesteps if tstep <= last_output]
        M_star = np.array([ras.get_mstar(RamsesDir, timestep, hnum=hnum) for timestep in timesteps_cut])

        if not DM_only:
            redshift = np.array([1/ras.read_info(RamsesDir, timestep, alt_path=alt_path)['aexp'] - 1 for timestep in timesteps_cut])
            M_halo = np.array([ras.get_m200(RamsesDir, timestep, hnum=hnum) for timestep in timesteps_cut])
        else:
            M_halo = M_DM[timesteps <= last_output]

        # Plot selected simulation points with segments.
        x = M_halo
        y = M_star/M_halo if norm else M_star
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc = LineCollection(segments, cmap=cm, norm=plt.Normalize(vmin,vmax))
        lc.set_array(redshift)
        line = ax.add_collection(lc)

        # Plot markers markers at all snapshots within 0.1 of redshift_values.
        redshift_values = [5, 4, 3, 1]
        subsel = [np.argmin(np.abs(redshift - z)) for z in redshift_values] # index of redshifts closest to redshift_values.
        starplot = plt.scatter(M_halo[subsel], M_star[subsel]/M_halo[subsel] if norm else M_star[subsel], vmin=vmin, vmax=vmax, c=redshift[subsel], cmap=cm, edgecolors='k', marker=shape_s[ind_f], s=100, linewidth=0.75)
        mark.append((starplot))
        lab.append(foldername(folder))

    # Observations
    Ms_Mh_data(colors, vmin)

    # Legend
    leg_plot = plt.legend(mark, lab, handler_map={tuple: HandlerTuple(ndivide=None)}, loc='lower right')
    ax.add_artist(leg_plot)
    ax.legend(loc="upper left")

    ax.set_ylabel(r'$\mathrm{M_\star / M_{halo}}$' if norm else r'$\mathrm{M_\star [M_\odot]}$')
    ax.set_xlabel(r'$\mathrm{M_{200, DM-only\ simulation} [M_\odot]}$' if DM_only else r'$\mathrm{M_{200} [M_\odot]}$')
    ax.set_yscale('log')
    ax.set_xscale('log')
    fig.colorbar(line, ax=ax, label='Redshift')

    plt.xlim(xmin)
    plt.ylim(ymin)

    if savefig:
        if savefig==True:
            fig.savefig(f'Ms_Mh.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/Ms_Mh.pdf',bbox_inches='tight')

def Ms_Mh_data(colors, vmin):
    """ Plots the data for the Ms_Mh relation. """
    #     # Behroozi+10 (From Behroozi+19, fig35)
    # Beh10_med_x = [264345305462.58487, 359590869633.57227, 478998432970.1781, 633610438470.0238, 832287454850.3298, 1033776671219.1348, 1348467695307.1682, 1698510895617.92, 2065903820109.2393, 2584048635062.9185, 3232148216860.1064, 4042796236107.8228, 5423061069232.906, 6504468953573.495, 10247790071474.965, 13842995200965.705, 18183651252870.188, 22903870728680.11, 40357341636198.2, 66773073483705.59, 94724354679688.64, 124426441362089.11, 151340248211372.78, 196033843734290.94]
    # Beh10_med_y = [0.006531843933632206,0.008716339324897923,0.0107997297473229,0.012424304203974562,0.01382965849275277,0.014530855012702713,0.015142268082960566,0.01501796121931284,0.014894674823422325,0.014530855012702713,0.01382965849275277,0.013054245924892084,0.011922636822473257,0.011347301686224434,0.009465284050527023,0.008227624505091926,0.0072706824822588115,0.006425040876027985,0.004697185470435389,0.0034058033663133167,0.002748785248262832,0.0023118615064762213,0.002042972542821134,0.0017182388415104067]
    # Beh10_max_x = [262503075721.41235, 330645172886.00885, 385638791968.3854, 452935589160.12305, 574515093282.1042, 781517120757.6617, 924339335788.6669, 1093262303528.1824, 1284044592681.1409, 1561787231423.9763, 2080402200554.307, 2676003716313.0146, 3232148216860.1064, 3796181782916.679, 4521443882743.429, 5163974091453.418, 5897812537047.772, 7123534305572.052, 8135840573837.574, 9099085000872.797, 10392131456009.457, 11952224643157.475, 13461121532378.117, 21657645345966.914, 33180336770776.23, 44198367257823.04, 53011925176017.89, 62262894869975.74, 88326210392082.23, 131586194282088.55, 197409596670561.1]
    # Beh10_max_y = [0.011347301686224434, 0.014175921925341686, 0.01617448825577667, 0.01845481879161462, 0.021406660727314033, 0.024626807437783654, 0.02587544504383157, 0.02674284684975636, 0.027187391541020827, 0.027187391541020827, 0.026523307782115264, 0.025243407108815445, 0.02422413120116342, 0.023055178893662067, 0.02176250232628404, 0.020883778063121645, 0.019876016820685013, 0.01845481879161462, 0.017420078965167805, 0.016579460932921897, 0.01564987019989839, 0.014651129994530877, 0.01382965849275277, 0.0107997297473229, 0.008503432567217948, 0.007210995536353713, 0.0064782223078111755, 0.005868085324511785, 0.0047360650750151775, 0.0036984443905611953, 0.002864445560519111]
    # Beh10_min_x = [262503075721.41235, 352125144914.35675, 434323383409.4464, 616131624161.3215, 770662248133.5919, 977527719110.0569, 1311268787245.5032, 1628715023940.8306, 2023012104774.4614, 2512764919526.324, 3099331522679.73, 4071168320675.573, 5021520402626.234, 7024592061740.961, 9227246733096.297, 12551891819191.975, 15160507630953.033, 18699496652196.51, 24908946111063.902, 30723559246960.652, 37631410214171.06, 47732643817239.37, 62262894869975.74, 80088233907880.66, 96058558874762.55, 153471894235566.34, 197409596670561.1]
    # Beh10_min_y = [0.0036680828872482584, 0.004894828388731585, 0.005772135473090304, 0.0072706824822588115, 0.007960762252382552, 0.008503432567217948, 0.008861230495083054, 0.008861230495083054, 0.008716339324897923, 0.008433625651445526, 0.008026655177087024, 0.007452723960353898, 0.006919830639764742, 0.006114996057683258, 0.005403770515798789, 0.004658625038761443, 0.004219862783868517, 0.003759923403210732, 0.00321484400637789, 0.0028409305730834763, 0.002510506421172746, 0.002164323603205032, 0.0018203009598402758, 0.0015436333495289982, 0.0013640957905292246, 0.0009890693191911843, 0.0008182534180902327]
    # plt.fill(np.append(Beh10_min_x, Beh10_max_x[::-1]), np.append(Beh10_min_y, Beh10_max_x[::-1]), alpha=0.15)#, label = 'Moster+21 (ML)')
    # plt.plot(Beh10_med_x, np.array(Beh10_med_x)*np.array(Beh10_med_y),color=colors[1-vmin], label='Behroozi+10')

    #     # Moster+18 (From Moster+21, Fig.8 || 16)
    # Emerge_max_x = [34561123262.43144, 51990949676.43952, 67743913085.53959, 101140735699.6458, 130793019139.9158, 167864533969.81686, 222059635359.61404, 394510996655.7903, 716969730984.5684, 664752682486.4519, 2114088446045.5894, 846740651548.3726, 5957184500805.885, 10268198434168.865, 13379404427086.94, 40356298905712.07, 91325305357712.5]
    # Emerge_max_y = [1000000000, 2334435173.301266, 3898603702.5490637, 7538288028.438266, 12458178394.657818, 18543077702.866684, 29698004774.064377, 67185062772.68003, 156839173656.99777, 138327679298.00018, 231012970008.31485, 174144230622.7662, 343845730694.55164, 377809372325.8568, 381784402637.0492, 485695004164.2992, 373875728896.5411]
    # Emerge_min_x = [60479639721.904594, 88939949829.77155, 147614738277.26743, 228878981034.76572, 336584099771.8205, 441896128872.9354, 654774796144.0243, 1054358990834.6793, 1527242657916.8938, 2672571866873.9673, 9236708571873.846, 47660755193358.36, 103853314394179.42]
    # Emerge_min_y = [13832767.929800017, 45613238.62890215, 212457823.10305718, 730527154.2664449, 1913454685.9970384, 4756338196.350247, 10318970327.677687, 17414423062.276623, 26192793448.156273, 36998304144.0695, 78605791569.53827, 153590246132.41513, 231012970008.31485]
    # plt.fill(np.append(Emerge_min_x, Emerge_max_x[::-1]), np.append(Emerge_min_y, Emerge_max_y[::-1]), c=colors[1-vmin], alpha=0.15)#, label = 'Moster+18 (EM)')
    #     #           (From Behroozi+19, fig35)
    # Mos_18_x = [100701793583.22214, 124209089683.44821, 151075882722.5385, 194327627370.8357, 226648617946.73035, 281518185131.7502, 344814421476.5709, 437371437044.6454, 550906895165.3152, 713599967459.715, 963950359643.9623, 1214178281309.9763, 1518703678265.233, 1834330556314.3938, 2246759403042.9443, 2869847172464.241, 3466277481079.657, 4186661816393.1665, 6325036295723.441, 8484488481132.529, 11461079170024.625, 14639549568563.736, 17558809397013.246, 21657645345966.914, 29461054776605.523, 37369155876128.27, 50833538486543.8, 67241682628734.17, 85889638428775.8, 103016816418157.86, 121843134505751.06, 166907296516028.84, 252156671539915.38, 335889091549166.56, 391754890506251.5]
    # Mos_18_y = [0.0017324610703646928, 0.002429078373674846, 0.0032682841597133886, 0.004814792392208043, 0.0060647964457767566, 0.008295726305500354, 0.011070130603229814, 0.014894674823422325, 0.019231340022372853, 0.024025268933126023, 0.029040705314171564, 0.03051313802202619, 0.03127711812436227, 0.03001421360329168, 0.02856585684044021, 0.02587544504383157, 0.023828039177781066, 0.02176250232628404, 0.017277072952541963, 0.014651129994530877, 0.012221152736935208, 0.010535933747808, 0.009465284050527023, 0.008295726305500354, 0.006977107551806339, 0.006015008935695309, 0.004976194818681469, 0.004254791486907912, 0.0036379706295061784, 0.0032682841597133886, 0.0029604680732897557, 0.002469456793435058, 0.0019125945402616866, 0.0016085846276721548, 0.0014691442481084567]
    # plt.plot(Mos_18_x, np.array(Mos_18_x)*np.array(Mos_18_y),c=colors[1-vmin], label='Moster+18 (EM)')

    #     # Moster+21 SL (Fig.8)
    # SL_max_x = [34348037347.372395, 73307576397.23538, 101217111746.13046, 161586957863.84976, 606508829748.5438, 718442961193.2822, 817399420997.2246, 915105368846.3055, 2186526856904.5923, 3876513770055.119, 6238731890721.809, 43224183186205.03, 58725486049892.52, 98399920161613.31]
    # SL_max_y = [983492572.3505298, 3937138774.632533, 6706655894.364825, 17224418871.199104, 113611341189.69832, 141842778180.91693, 163854295601.7703, 165682679069.76456, 241619805349.35965, 269976088718.1332, 308430008974.50385, 439919784547.0047, 486123916248.0206, 459886437939.44556]
    # SL_min_x = [48196140216.16203, 120868110737.36241, 234167783599.51483, 347660913444.9283, 468546895633.9343, 652169929893.5431, 1675591413207.0874, 5309379673582.687, 12788809628273.73, 38298996120122.96, 98399920161613.31]
    # SL_min_y = [16753146.472553099, 227314168.42952728, 1418427781.8091695, 3850722022.043664, 8850910553.03853, 13643938684.635931, 35041146982.744354, 77045448555.40263, 129793520415.5378, 223561796240.47647, 348472631291.7241]
    # plt.fill(np.append(SL_min_x, SL_max_x[::-1]), np.append(SL_min_y, SL_max_y[::-1]), alpha=0.15, label = 'Moster+21') # Close to Emerge

        # Moster+21 RL (Fig.16)
    RL_max_x = [34170697684.702477, 48696752516.585915, 74989420933.24496, 181809002735.65106, 267845661017.07532, 621254046883.7229, 1068676240164.4325, 1275760733063.5112, 3817075667445.068, 5090152210727.123, 11676365802593.455, 26784566101707.64, 39459706090564.36, 53206113733242.05, 58133045681649.41, 100000000000000]
    RL_max_y = [231333437.12884152, 412946737.909936, 704177459.3038597, 2863872257.1206, 5775497550.177955, 22438477072.948475, 51122168980.26944, 62330697492.061745, 129593369364.53812, 137744049861.3495, 186862940554.64877, 231333437128.84058, 265363422367.11035, 286387225712.06, 394481362981.35547, 333563719521.11554]
    RL_min_x = [42169650342.85788, 74989420933.24496, 203091762090.47223, 377505320532.4376, 749894209332.4528, 1033767981785.2883, 1425102670302.9905, 1964577793749.6038, 3417069768470.2617, 8952061908580.012, 27082721744346.07, 64938163157621.13, 105691110613668.11]
    RL_min_y = [14865702.268326197, 42573472.30887426, 323544316.19296795, 1237984570.0928924, 4257347230.887417, 7716444173.83005, 13565953719.434082, 20791259295.79767, 41929198074.19238, 77164441738.3005, 129593369364.53812, 201667428978.9986, 295255961705.3296]
    plt.fill(np.append(RL_min_x, RL_max_x[::-1]), np.append(RL_min_y, RL_max_y[::-1]), alpha=0.15, c='k', label = 'Moster+21 (z=1)') # ML

        # Behroozi+19 (Fig.34 with Ms/Mp in y, but same result as directly Ms in Fig.9))
    Beh19_min_x =             np.array([125080781104.62529, 198447743733.561, 314848585411.3762, 499525113616.99945, 792524885598.7438, 1257385619204.0852, 1994913502541.6802, 3165043262656.5547, 4986525288128.421, 7911404803841.905, 12551891819191.975, 19914287303841.285, 31595144742528.973, 50127486666761.016, 79530096791865, 125299658143399.39])
    Beh19_min_y = Beh19_min_x*np.array([0.0015183931932691924, 0.002637795057169129, 0.0047360650750151775, 0.008364391799015582, 0.013603527888951457, 0.017709651861758803, 0.01785623842215006, 0.014293259050409513, 0.010363659279768827, 0.00751441174390953, 0.005584916783863631, 0.004150863240620584, 0.0031363176906469676, 0.0023893601868803104, 0.0017905369743185424, 0.0013417912770145285, ])
    Beh19_max_x =             np.array([125080781104.62529, 198447743733.561, 314848585411.3762, 499525113616.99945, 792524885598.7438, 1248622864065.4294, 1994913502541.6802, 3165043262656.5547, 5021520402626.234, 7911404803841.905, 12551891819191.975, 19914287303841.285, 31595144742528.973, 50127486666761.016, 79530096791865, 126179003104049.05])
    Beh19_max_y = Beh19_max_x*np.array([0.0021465561011664233, 0.0036680828872482584, 0.0063722960246139285, 0.01144122571911281, 0.018607573257395406, 0.02483064876814417, 0.024626807437783654, 0.019712849367879905, 0.01441156739985285, 0.010278581273361623, 0.007639323346159569, 0.005772135473090304, 0.0043613218618972, 0.0033501146179572673, 0.002573363835286576, 0.0019767089141561143, ])
    Beh19_med_x =             np.array([125958590000.26115, 144000000000.00018, 172799999999.99963, 207359999999.9997, 248831999999.9998, 298598399999.99994, 358318080000.0002, 429981696000.0005, 515978035200.003, 619173642240.004, 743008370688.0052, 891610044825.6033, 1069932053790.7246, 1283918464548.8704, 1540702157458.6455, 1848842588950.3757, 2218611106740.452, 2662333328088.5557, 3194799993706.2686, 3833759992447.525, 4600511990937.014, 5520614389124.42, 6624737266949.309, 7949684720339.176, 9539621664407.018, 11447545997288.428, 13737055196746.121, 16484466236095.424, 19781359483314.523, 23737631379977.44, 28485157655972.832, 34182189187167.42, 41018627024600.93, 49222352429521.15, 59066822915425.414, 70880187498510.55, 85056224998212.72, 102067469997855.73, 125299658143399.39])
    Beh19_med_y = Beh19_med_x*np.array([0.002026201243572885, 0.002372572676329606, 0.0029149503353085933, 0.0036497672500147836, 0.0046875986184944885, 0.005949967831002487, 0.007467572991274133, 0.009439430475757914, 0.011852187229873107, 0.014349385231618956, 0.0174768286768866, 0.019803237493961443, 0.021830341046528444, 0.02377913464039349, 0.023324163454101276, 0.022950362085377797, 0.021421284671326056, 0.01930016409592523, 0.017346368212884183, 0.015081754541322722, 0.01323783956296105, 0.011402081191971369, 0.009972022197694653, 0.00891092759985151, 0.00776435305727777, 0.006889003766414966, 0.006122372354347712, 0.005526888291594167, 0.0048609442780154135, 0.004462579356903079, 0.004020949007833696, 0.003634885148354026, 0.003276254426743153, 0.002965007728200003, 0.002676265985512631, 0.0024177484688510135, 0.0021725224112377644, 0.0019577474656364007, 0.0017324610703646928, ])
    plt.fill(np.append(Beh19_min_x, Beh19_max_x[::-1]), np.append(Beh19_min_y, Beh19_max_y[::-1]), c=colors[1-vmin], alpha=0.15)
    plt.plot(Beh19_med_x, Beh19_med_y, c=colors[1-vmin], label='Behroozi+19 (z=1)') # EM


def content_t(genpath, folders, timesteps, is_ISM=True, DM=False, xlims=None, ylims=None, hnum=None, alt_path=None):
    """
    Plots several quantities against time (galaxy stellar and gas mass, dark matter mass and SFR).
    If not is_ISM, the CGM is selected instead.
    """
    _, ax   = plt.subplots(nrows=1, ncols=1)
    ax2 = ax.twinx()
    # timesteps = [3,4,5,6,7,8,9,10,18,31,45,187]
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']  
    g2Msun  = 1/g2Msun

    for ind_f, folder in enumerate(folders):
        print('Folder ', folder)
        RamsesDir = genpath+folder
        M_sgal, M_ggal = [], []
        M_scgm, M_gcgm = [], []
        time_gyr, M_dm = [], []
        for timestep in timesteps:
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)    
            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            time_gyr.append(cosmo.age(1/info['aexp']-1).value)
            # Stars
            star_mass,rad_stars,star_age,star_minit = ras.extract_stars(RamsesDir, timestep, ['star_mass','star_rad','star_age','star_minit'], factor=1, alt_path=alt_path)
            cu2cm = info['cu2cm']
            r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
            # Gas
            mass, rad_gas = ras.extract_cells(RamsesDir,timestep,['mass','radius'],factor=1,alt_path=alt_path)
            # Create arrays
            if is_ISM:
                M_sgal.append(np.sum(star_mass[rad_stars<0.1*r200_cm])/g2Msun)
                M_ggal.append(np.sum(mass[rad_gas<0.1*r200_cm])*g2Msun)
            else:
                M_scgm.append(np.sum(star_mass[(rad_stars>0.1*r200_cm)])/g2Msun)
                M_gcgm.append(np.sum(mass[(rad_gas>0.1*r200_cm)])*g2Msun)
            if DM:
                M_dm.append(ras.get_m200(genpath+folder,timestep,hnum=hnum))                    # M_200

        M_sgal, M_ggal = np.array(M_sgal), np.array(M_ggal)
        M_scgm, M_gcgm = np.array(M_scgm), np.array(M_gcgm)
        if DM:
            M_dm   = np.array(M_dm)
            ax.plot(time_gyr, M_dm  , c=colors[ind_f], ls='-' , label=foldername(folder))
        if is_ISM:
            ax.plot(time_gyr, M_sgal, c=colors[ind_f], ls='--')
            ax.plot(time_gyr, M_ggal, c=colors[ind_f], ls='dotted' )
        else:
            ax.plot(time_gyr, M_scgm, c=colors[ind_f], ls='--')
            ax.plot(time_gyr, M_gcgm, c=colors[ind_f], ls='dotted' )

        sel = tuple([rad_stars<0.1*r200_cm])
        times, sfr = compute_sfr(star_age[sel], star_minit[sel], info, hist_lims=None, bins=300)
        ax2.plot(times/1000, sfr, c=colors[ind_f])

    if DM:
        dmrun = '../ICs/Zoom-56-35796/dmRun2-LSS1Rvir/'
        M_dmonly, t_dm = [], []
        for snap_dm in timesteps[:-1]:
            info = RamsesDir(dmrun, snap_dm)
            t_dm.append(cosmo.age(1/info['aexp']-1).value)
            print(snap_dm)
            try:
                M_dmonly.append(ras.get_m200(dmrun,snap_dm,hnum=hnum))
            except:
                os.chdir('/Xnfs/cosmos/mrey/outputs')
                M_dmonly.append(ras.get_m200(dmrun,snap_dm,hnum=hnum))
        ax.plot(t_dm, M_dmonly, c='k', ls='-', label='DM only run')

        ax.set_xlim(xlims)
    add_z_top_axis(ax, cosmo)

    if is_ISM:
        legend_elements = [Line2D([0], [0], ls='--'    , color='k', label='M$_{*, ISM}$'),
                           Line2D([0], [0], ls='dotted', color='k', label='M$_{gas, ISM}$')]
    else:
        legend_elements = [Line2D([0], [0], ls='--'    , color='k', label='M$_{*, CGM}$'),
                           Line2D([0], [0], ls='dotted', color='k', label='M$_{gas, CGM}$')]
    ax2.legend(handles=legend_elements)

    ax2.set_ylim([0,None])
    ax.set_yscale('log')
    ax.set_ylim(ylims)
    ax.set_ylabel(r'$[M_\odot]$')
    ax2.set_ylabel(r'SFR $[M_\odot.yr^{-1}]$')
    ax.set_xlabel('Time [Gyr]')


def gas_fraction(genpath, folders, timesteps, is_ISM=True, var_str='mass', hnum=None, alt_path=None):
    """
    Plots the cold and hot gas mass or volume (var_str) fraction against time.
    If not is_ISM, the CGM is selected instead.
    Not sure what var_str does but it's either mass or volume.
    """
    _, ax   = plt.subplots(nrows=1, ncols=1)
    # timesteps = [3,4,5,6,7,8,9,10,11,12,13,18,25,26,36,38,39,41,61,81,101,121,141,161,181,201]

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']  

    for ind_f, folder in enumerate(folders):
        print('Folder ', folder)
        RamsesDir = genpath+folder
        cold_ISM, hot_ISM = [], []
        cold_CGM, hot_CGM = [], []
        time_gyr = []

        for timestep in timesteps:
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            time_gyr.append(cosmo.age(1/info['aexp']-1).value)
            # Gas
            cu2cm = info['cu2cm']
            r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
            T, rad_gas, qty = ras.extract_cells(RamsesDir,timestep,['T','radius',var_str],factor=1, alt_path=alt_path)
            if is_ISM:
                ISM = (rad_gas<0.1*r200_cm)
                tot_ISM = np.sum(qty[ISM])
                cold_ISM.append(np.sum(qty[(T<T_c) & ISM]) / tot_ISM)
                hot_ISM .append(np.sum(qty[(T>T_h) & ISM]) / tot_ISM)
            else:
                CGM = (rad_gas>0.1*r200_cm) & (rad_gas<r200_cm)
                tot_CGM = np.sum(qty[CGM])
                cold_CGM.append(np.sum(qty[(T<T_c) & CGM]) / tot_CGM)
                hot_CGM .append(np.sum(qty[(T>T_h) & CGM]) / tot_CGM)

        if is_ISM:
            ax.plot(time_gyr, cold_ISM, c=colors[ind_f], ls='--')
            ax.plot(time_gyr,  hot_ISM, c=colors[ind_f], ls='-', label=foldername(folder))
        else:
            ax.plot(time_gyr, cold_CGM, c=colors[ind_f], ls='--')
            ax.plot(time_gyr,  hot_CGM, c=colors[ind_f], ls='-', label=foldername(folder))
    add_z_top_axis(ax, cosmo)

    if is_ISM:
        legend_elements = [ Line2D([0], [0], ls='-' , color='k', label=' hot ISM'),
                            Line2D([0], [0], ls='--', color='k', label='cold ISM')]
    else:
        legend_elements = [ Line2D([0], [0], ls='-' , color='k', label=' hot CGM'),
                            Line2D([0], [0], ls='--', color='k', label='cold CGM')]
    ax.legend(handles=legend_elements)
    if var_str=='mass':
        ax.set_ylabel('Gas mass fraction')
    elif var_str=='volume':
        ax.set_ylabel('Gas volume fraction')
    ax.set_xlabel('Time [Gyr]')
    ax.set_yscale('log')


def MZR(genpath, folders, timesteps, starsorgas, xlims=None, ylims=None, hnum=None, savefig=False, alt_path=None):
    """ Plots the MZR relation within 0.1 Rvir for gas or stars (starsorgas='gas' or 'stars') alongside selected observations. """
    if starsorgas=='gas':
        N_O_sun = ZO_sun/atm_weight_O
        N_H_sun = X_sun/atm_weight_H
        log_OH_sun = 12+np.log10(N_O_sun / N_H_sun)

    colors = ['blue', 'cyan', 'lightgreen', 'yellow', 'orange', 'red', 'darkmagenta']
    cm = LinearSegmentedColormap.from_list('custom_cmap', colors, N=100)
    zmin,zmax = 1, 7
    norm = plt.Normalize(zmin,zmax)
    shape_s = ['o', 'X', 'P', 'D']
    mark, lab = [], []
    filename = 'props'

    fig, ax = plt.subplots(nrows=1, ncols=1)
    # for folder in folders:
    for ind_f, folder in enumerate(folders):
        RamsesDir = genpath+folder
        Zstar_Zsun, log_OH_sim = [], []
        M_star,     redshift   = [], []
        for timestep in timesteps:
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            redshift.append(1/info['aexp']-1)

            # Compute Zstar/Zsun and M_star
            if starsorgas=='stars':
                try:
                    Zstar_Zsun.append(ras.read_prop(RamsesDir, timestep, 'Zstar_Zsun', filename, hnum=hnum, alt_path=alt_path))
                except:
                    star_mass, star_mets = ras.extract_stars(RamsesDir, timestep, ['star_mass', 'star_mets'], factor=0.1, hnum=None, alt_path=alt_path)
                    Zstar_Zsun_temp = np.sum(star_mass*star_mets)/np.sum(star_mass)/Z_sun
                    Zstar_Zsun.append(Zstar_Zsun_temp)
                    ras.add_prop(RamsesDir, timestep, 'Zstar_Zsun', Zstar_Zsun_temp, filename, hnum=hnum, alt_path=alt_path)
            elif starsorgas=='gas':
                try:
                    log_OH_sim.append(ras.read_prop(RamsesDir, timestep, 'log_OH', filename, hnum=hnum, alt_path=alt_path))
                except:
                    mass, Z, rad = ras.extract_cells(RamsesDir,timestep,['mass','Z','radius'],factor=0.1, alt_path=alt_path)
                    Z_gal = np.sum(mass*Z)/np.sum(mass)
                    log_OH_sim_temp = log_OH_sun + np.log10(Z_gal/Z_sun)
                    log_OH_sim.append(log_OH_sim_temp)
                    ras.add_prop(RamsesDir, timestep, 'log_OH', log_OH_sim_temp, filename, hnum=hnum, alt_path=alt_path)
            else:
                raise ValueError('Pick either stars or gas.')
            M_star.append(ras.get_mstar(RamsesDir,timestep,hnum=hnum,alt_path=alt_path))
        redshift = np.array(redshift)
        
        # Plot
        if starsorgas=='stars':
            x = np.array(M_star)
            y = np.array(Zstar_Zsun)
            ax.set_ylabel(r'$Z_{*}\rm [Z_\odot]$')
        elif starsorgas=='gas':
            x = np.array(M_star)
            y = np.array(log_OH_sim)
            ax.set_ylabel(r'12+log(O/H)')

        col_plot = list(plt.rcParams['axes.prop_cycle'])[ind_f]['color']
        # Plot data with colored segments
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc = LineCollection(segments, colors=col_plot, cmap=cm, norm=norm)
        line = ax.add_collection(lc)
        # Plot markers
        starplot = ax.scatter(x[-1], y[-1], vmin=zmin, vmax=zmax, c=col_plot, cmap=cm, marker=shape_s[ind_f], s=200)
        mark.append((starplot))
        lab.append(foldername(folder))
    # fig.colorbar(line, ax=ax, label='Redshift')

    # Plotobs
    if starsorgas=='stars':
        MZRg_stars(ax, cm, norm)
        # ax.set_yscale('log')
    elif starsorgas=='gas':
        MZRg_obs(ax, cm, norm)
    leg_plot = ax.legend(mark, lab, handler_map={tuple: HandlerTuple(ndivide=None)})#, loc='lower right')
    ax.add_artist(leg_plot)

    ax.minorticks_off()
    ax.set_xscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ax.legend(frameon=False, loc="lower right")
    ax.set_xlabel(r'$M_{*}\rm\ [M_\odot]$')

    if savefig:

        if savefig==True:
            fig.savefig(f'MZR_{starsorgas}.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/MZR_{starsorgas}.pdf',bbox_inches='tight')


def MZRg_stars(ax, cm, norm):
    """ Plots observational data for the stellar MZR."""

    mG05,mG14,mK21,mC23,mC22 = '*', 's', '^', 'v', '3'
    [next(ax._get_lines.prop_cycler) for _ in range(4)]


    # Removed because J. wants no time variation.
    # Gallazi+05
    # x_Ga05 = [811130830.789689, 1329523371.5615625, 2048686048.1335912, 3222528161.4507985, 5174398955.753044, 8308509099.651883, 12802728434.367826, 20557283388.256943, 33008737354.036682, 51921857814.292015, 83370693255.34662, 131139862596.21928, 206279483715.96677, 324472090787.0837, 510385889101.3516, 819524323538.6213]
    # y_Ga05 = [0.3563040249104583, 0.3472292054076783, 0.31520960284273974, 0.3472292054076783, 0.426827213116382, 0.5489032450921556, 0.8347734492114153, 1.1015819387358996, 1.3805623406389336, 1.5405474165780155, 1.6538167833956718, 1.7754143258483055, 1.8694277429178836, 1.905952376390452, 1.9431906233500134, 1.9811564262830599]
    # ax.scatter(x_Ga05, y_Ga05, c='k', marker=mG05, label='Gallazi+05') #g

    # Nope
        # # Kirby+13 - Lower masses
        # x_Ki13 = [1414.2350925464643, 4864.455974621802, 5618.591033219686, 8139.190662564849, 18932.074797471283, 8657.786170698157, 38126.071380410314, 145359.73189041455, 382571.28492774116, 554199.5513891251, 331222.03799536184, 243215.39318841803, 136652.7827309446, 305036.4330474291, 627072.0397899065, 709526.635488343, 1867398.302811071, 3918722.95002407, 786462.6320469168, 1211874.1619115176, 3060849.857210594, 14940551.091266977, 9304722.548629887, 8394484.639275324, 6693193.536780019, 7730837.196502732, 1458594.0117250353, 819524.3235386213, 24998477.644782826, 67162011.68714349, 103491130.1682709, 84233404.03238747, 101382092.77031094, 465221962.12292856]
        # y_Ki13 = [0.007384946253574328, 0.005742534875883623, 0.006703946382825402, 0.007826316787824876, 0.008082822192522723, 0.003656160144085201, 0.004186477288289955, 0.0035401332397365843, 0.006285203269240104, 0.007626985859023444, 0.010735254011650354, 0.012134827677491444, 0.018694277429178844, 0.012695255816739264, 0.011749733361395634, 0.011749733361395634, 0.014916587080139316, 0.021544346900318846, 0.020593278373701618, 0.024040991835099744, 0.027000546158529752, 0.0248289288056033, 0.03472292054076785, 0.03825014212787164, 0.04186477288289953, 0.04855953393399652, 0.03751713723289102, 0.0458209855188918, 0.09375378188199622, 0.0777600090603804, 0.06575475604916373, 0.09195713545875653, 0.15208025397468092, 0.12371916345321607]
        # ax.scatter(x_Ki13, y_Ki13, c='k', marker='^', label='Kirby+13') #r

        # Kudritzki+16 - Lower masses
        # x_Ku16 = [26591277.07094469, 45418001.245757915, 107841739.69290385, 135253197.4983535, 173160995.74513307, 474899909.082385, 996574308.6967776, 1600194879.2681324, 2006936040.3293743, 3499163475.2272944, 20557283388.256943, 35842345613.704735, 85105042045.7956, 65119473308.1623, 96295625245.98991]
        # y_Ku16 = [0.10129831955925686, 0.13454003632888825, 0.1622123939129172, 0.21405836942033304, 0.24989588437948013, 0.226851835158303, 0.44366873309786115, 0.43798232293313283, 0.38250142127871617, 0.7058940086618741, 0.9745306848412371, 1.2695255816739244, 1.2213348058636213, 0.9999999999999994, 1.1087099042901354]
        # ax.scatter(x_Ku16, y_Ku16, c='k', marker='*', label='Kudritzki+16') #b
        
        # Zahid+17 - Star forming galaxies
        # x_Ma19 = [363377200.8114161, 446453727.25168025, 702259728.9437839, 559934347.4780844, 880761698.4337599, 1175018513.5469239, 1443655226.0057871, 1810606783.2225304, 2270830918.9041266, 2848035868.4357934, 3571956080.204906, 4573078367.43524, 5618591033.219664, 7193329518.2085495, 9021746317.961271, 11314914243.761562, 14190964790.130428, 17798056382.597298, 22322006691.075977, 28578258977.598732, 36587968881.72279, 44036744069.10872, 56379047545.05524, 70709601626.37488, 90527582732.48112]
        # y_Ma19 = [0.20329338594462165, 0.29552092352028864, 0.2974331398424134, 0.32136812662333525, 0.3517373500969382, 0.3494760085991057, 0.38997468376124217, 0.44366873309786115, 0.495082837240096, 0.5453743116818148, 0.6085744643306688, 0.6449466771037622, 0.7196856730011517, 0.7876958246729112, 0.8565902153685493, 0.9315103293454985, 1.0129831955925674, 1.0944997993195433, 1.1015819387358996, 1.1450475699382812, 1.1599139465015849, 1.1674193588234578, 1.1303717326410918, 1.08746319142195, 1.0394574653722624]
        # ax.scatter(x_Ma19, y_Ma19, c='k', marker='^', label='Zahid+17') #k

    # Gallazzi+14, z~0.7
    colG14 = 'darkblue' if False else next(ax._get_lines.prop_cycler)['color']
    xG14 = np.array([32765308258.65762,73793042076.39066,130209762056.51378,260148652052.00052])
    yG14 = np.array([0.6202473400345054,0.7965823742940011,1.0150722791519904,0.9544582608961604])
    xG14_min = np.array([18486697926.789856,49941154083.07388,99336722439.83708,199349267196.8885])
    xG14_max = np.array([49941154083.07388,99336722439.83708,199349267196.8885,403620322474.32263])
    yG14_max = np.array([1.116076037401083,1.3835179274766871,1.5854206431338445,1.5013167505138165])
    yG14_min = np.array([0.34226949111228405,0.5591601923602614,0.6273076362274843,0.6242099852748019])
    ax.errorbar(xG14, yG14, xerr=[xG14-xG14_min,xG14_max-xG14], yerr=[yG14-yG14_min,yG14_max-yG14], color=colG14, marker=mG14, ls='')
    ax.scatter(xG14, yG14, color=colG14, marker=mG14, edgecolor='k', s=100,zorder=3, label='Gallazzi+14')

    # Nope
        # Kriek+19, z~1.4 - Only five galaxies
        # xK19 = np.array([62325006644.26702,149691490164.16394,527715208502.0901])
        # yK19 = np.array([0.6184685187770618,1.1333737006315212,1.414421495536317])
        # yK19_max = np.array([0.8593624374059621,1.796271032373387,1.557190995740044])
        # yK19_min = np.array([0.43729209696029914,0.7043378551333991,1.2718263619402885])
        # ax.errorbar(xK19, yK19, yerr=[yK19-yK19_min,yK19_max-yK19], color=cm(norm(1.4)), marker=mK19, ls='')
        # ax.scatter(xK19, yK19, color=cm(norm(1.4)), marker=mK19, edgecolor='k', s=100,zorder=3, label='Kriek+19')

    # Carnall+22, z~1.15
    colC22 = cm(norm(1.15)) if False else next(ax._get_lines.prop_cycler)['color']
    ax.errorbar([111003442522.99281], [1.0984517817571273], xerr=[[111003442522.99281-105305733906.80215],[118171819909.25787-111003442522.99281]], yerr=[[1.0984517817571273-0.7909401043099277],[1.512496940075728-1.0984517817571273]], color=colC22, marker=mC22, ls='', label='Carnall+22',zorder=2)

    # Removed because J. wants no time variation.
    # Kashino+21 - 1.6 < z < 3.0,med=2.22
    # xK21 = np.array([1147915765.4048111,1733857929.5433447,2515185035.4926553,4189535978.526301,6616432587.947128,10106584420.102194,15949795306.994392,23713706215.85908,39233345759.63288,60589226295.99965,101059885733.49539])
    # yK21 = np.array([0.05237102179681379,0.0530751298260984,0.08177061812986505,0.1064404036631989,0.14517002368523624,0.1608887575074448,0.194288505691127,0.2062862934735215,0.21051690574651197,0.24354402565674413,0.4000567591325948])
    # yK21_max = np.array([0.07517584451485318,0.07403697926095625,0.10172194327453725,0.11474890698419185,0.15099937909847055,0.16675349718407315,0.20136822355416137,0.2100111185026633,0.22372344090667123,0.2644406957485264,0.4733388972055485])
    # yK21_min = np.array([0.03072472251678076,0.03504105766791935,0.030889087902496632,0.09944277488130665,0.13956745399994458,0.1541211554806637,0.1861229525958405,0.1976115631007496,0.20166678793437293,0.22269881369240324,0.3357000604902978])
    # ax.errorbar(xK21, yK21, yerr=[yK21-yK21_min,yK21_max-yK21], color=cm(norm(2.2)), marker=mK21, ls=None)
    # ax.scatter(xK21, yK21, color=cm(norm(2.2)), marker=mK21, edgecolor='k', s=100,zorder=3, label='Kashino+21')

    # Chartab+23- z~2.5
    # xC23 = np.array([1408861585.8883274,2299827706.77384,3398652888.2914686,4688843434.528636,6614092450.8543415,9376238279.468887,13217062407.679361,19558424256.650078,44835538199.23185])
    # yC23 = np.array([0.06605716045164903,0.10620853629249176,0.0998479346542562,0.10911220361556062,0.1366005460559341,0.1597691200856408,0.17773678106218183,0.21085886894929506,0.19661567695027038])
    # yC23_max = np.array([0.07172413007770738,0.11206633518531309,0.10649357425630063,0.11431325036285576,0.1420857909597199,0.16918754741000822,0.18753963077336164,0.22731804753780835,0.21045120720011137])
    # yC23_min = np.array([0.06127422916792449,0.09958192753412073,0.09395384839160655,0.10377959314848113,0.13084643143949032,0.15141974815384543,0.16784243020168732,0.1969989976721844,0.18369660557339684])
    # ax.errorbar(xC23, yC23, yerr=[yC23-yC23_min,yC23_max-yC23], color=cm(norm(2.5)), marker=mC23, ls=None)
    # ax.scatter(xC23, yC23, color=cm(norm(2.5)), marker=mC23, edgecolor='k', s=100,zorder=3, label='Chartab+23')


def MZRg_obs(ax, cm, norm, Te=True):
    """ Plots observational data for the gas MZR."""
    mT23, mLi22, mSa21 = 's', '*', '^'
    mYa11 = 's'
    bluh = '-.' # linestyle None or '' to link or not obs points.
    # Photoionisation model - HAVE TO CHECK LINES USED.
    if not Te:
        # Tremonti+04, z~0.1 (Curti+20b should be more accurate)
        med_x = np.array([382275944.02097136, 419586070.0010346, 460537663.6758892, 508262782.47396296, 557869221.8580765, 612317248.9271898, 675771154.5511274, 745800733.3999921, 823087416.7889386, 908383252.1695068, 1008025083.1886014, 1118596766.1893628, 1241297211.9416888, 1385023252.492162, 1553879773.289593, 1743322608.8398693, 1955861560.6780565, 2206365838.1966643, 2488954387.0751343, 2807736520.251714, 3167347866.273785, 3573017779.1361, 4030645381.885924, 4546885349.7970495, 5129244680.544354, 5786191858.580065, 6527280001.145303, 7363285776.667421, 8306366115.649925, 9370234992.895288, 10570362851.771482, 11924201570.486607, 13451438241.759722, 15174281456.271347, 17117784252.936287, 19310208432.239967, 21783435530.365974, 24573430430.370922, 27720764352.092747, 31271204825.94569, 35276381229.81355, 39794535567.06728, 44891369403.28255, 50640999277.54198, 57127034481.60983, 64443792879.70197, 72697672448.9144, 82008698484.81966, 92512268971.19858, 104361123494.52333, 117727564334.51945, 132805962029.20097, 149815581849.5099, 167162273830.16187])
        med_y = np.array([8.44673626187907, 8.465339764173631, 8.484035135615327, 8.503419525660526, 8.521460329428894, 8.539156623895511, 8.557714191616506, 8.575812413601833, 8.593818766440027, 8.611641380983952, 8.629712042225139, 8.647745955807473, 8.665348084398278, 8.68362085776316, 8.70222436005772, 8.720993226817123, 8.738604542322642, 8.756794633455103, 8.774984724587563, 8.79257349039333, 8.809560930872406, 8.826172543022297, 8.842257995511332, 8.858268282334532, 8.873226249836017, 8.88825938300334, 8.90246569384646, 8.916596839023743, 8.930201824540172, 8.943280650395742, 8.95598364792213, 8.968310817119335, 8.979886329658173, 8.991161179533664, 9.002060201079974, 9.012733725628774, 9.022655593519206, 9.032126467414619, 9.041221512980849, 9.049940730217896, 9.058359284791598, 9.066026182706933, 9.073542749290594, 9.080382824881726, 9.086922237809512, 9.093010656742276, 9.098723247345859, 9.103759346956913, 9.108569949570455, 9.112854392523143, 9.116687841480811, 9.120070296443458, 9.12315208874276, 9.125465520902397])
        p68_x = np.array([381346361.95917684, 426538516.51630646, 473326123.7122197, 512737182.3129353, 554821570.5993049, 593330774.0483109, 661124715.1899241, 745800733.3999921, 841321949.0367695, 936453338.0808825, 997041442.9521437, 1064785139.5187566, 1168708151.9878976, 1318394815.4664414, 1487253157.6786656, 1668573141.511958, 1870421938.02503, 2102527860.4333484, 2369219833.6366158, 2629102875.0142384, 2933518725.457152, 3309240097.3998523, 3733083387.9481707, 4211211991.636498, 4750578702.783925, 5359026820.821281, 6045404205.060747, 6819691937.456124, 7651121627.46102, 8490383873.312897, 9473459679.111235, 10686808425.376638, 12055561346.033585, 13599622411.389578, 15341444867.121096, 17306357742.241665, 19522934175.79857, 22023406918.386898, 24844137050.572838, 28026142734.18034, 31615695685.374542, 35664994043.26408, 40232921418.66379, 45385903160.86402, 51198872293.96576, 57756359169.12118, 65153720678.0567, 73498526902.70969, 82912125365.20122, 93531405624.96637, 105510789883.25677, 119350933335.63371, 134268983369.48383, 151465985114.57214, 168080501132.87344])
        p68_y = np.array([8.63809969535844, 8.64562461368275, 8.656335721064467, 8.67477385889419, 8.69348366691615, 8.707702057955563, 8.71755815588839, 8.729810159419758, 8.739205867649336, 8.751680584264664, 8.77085061296655, 8.791245563630216, 8.813882956658011, 8.819557964428675, 8.826398040019807, 8.843859024193652, 8.861122650311557, 8.870565385265401, 8.883959906917486, 8.902499100809054, 8.918864336609815, 8.93136689236064, 8.94177733707901, 8.955231991263764, 8.969588633438557, 8.985298257598409, 8.998226752122306, 9.005968815703477, 9.018401216832855, 9.035984971594232, 9.051068215205447, 9.059411604113311, 9.069333472003743, 9.081435144203438, 9.090379858437995, 9.095415958049049, 9.102331199306017, 9.110975250877228, 9.118040823465869, 9.124129242398634, 9.130292826997238, 9.136155748932493, 9.14111668287771, 9.145626622827907, 9.15043722544145, 9.154947165391647, 9.158555117351804, 9.162538897641145, 9.16817632257889, 9.172385599865741, 9.172686262529087, 9.1765447667087, 9.181255148434461, 9.18373561540707, 9.183641658324774])
        m68_x = np.array([383954905.7104504, 431237363.9368668, 478540394.2865489, 525245929.06223875, 577036516.9858346, 640449395.9458973, 702187508.759239, 749897438.8353702, 783491671.3619204, 836071248.292198, 809671345.2712811, 900456247.8143754, 942859009.5310056, 975431898.1580852, 1019129721.7583748, 1070634035.7094189, 1130919488.1146524, 1227771792.5549428, 1286292381.6914747, 1423483330.594156, 1605801277.047712, 1814781040.9578078, 2043481099.9062533, 2305207577.3045206, 2600455651.2443213, 2933518725.457152, 3309240097.3998523, 3716080707.12552, 4211211991.636498, 4724626191.700394, 5242877054.975835, 5817975581.196053, 6491621333.607438, 7243266485.192756, 8081942349.88773, 9067260584.821247, 10228583864.310492, 11538647962.13922, 13016503414.390743, 14683640725.740194, 16564302877.548706, 18685837861.600037, 21079096365.91217, 23778880395.645504, 26824449352.810085, 30260090934.022312, 34135765148.12502, 38507830818.77065, 43439865136.53735, 49003588178.24461, 55279905837.5348, 62360086332.68603, 70347087400.05803, 79357053472.80022, 89521004616.31447, 100986741780.4394, 113920996073.92682, 128511853315.27765, 144971489116.98618, 163539254275.9156, 174650882279.65698])
        m68_y = np.array([8.25474811819919, 8.267691645856255, 8.284058969592177, 8.300848056230851, 8.324306842951426, 8.344270927315037, 8.364054029458794, 8.382037415010204, 8.399400683818461, 8.430317932869901, 8.412423135424655, 8.448885999921997, 8.469556558027065, 8.486837144602903, 8.504613824573262, 8.52445756035413, 8.546616398642762, 8.563913521665084, 8.575513838873649, 8.583387442370034, 8.592181825272919, 8.598733765811676, 8.613566457203435, 8.628486841872002, 8.64562461368275, 8.663138213822682, 8.680050488635919, 8.696344734641167, 8.70913960131469, 8.723721740486992, 8.742187439060853, 8.760515333914014, 8.777465191560172, 8.79524187153053, 8.812687822571208, 8.830832814304166, 8.844137137157247, 8.856389140688616, 8.871497439521775, 8.887131898015792, 8.903292516170662, 8.919227637328026, 8.933734610834492, 8.946963768021735, 8.958013120899718, 8.96861147978268, 8.978758844670624, 8.987177399244324, 8.994393303164639, 9.000782384760752, 9.006720472361843, 9.012884056960447, 9.018746978895702, 9.023031421848389, 9.026489042476873, 9.028443349788626, 9.030547988432051, 9.034832431384737, 9.038565659454623, 9.036711573030653, 9.03543375671143])
        p95_x = np.array([378110589.8941703, 406019298.0587018, 433606163.50176454, 468168668.8590855, 516684582.1081709, 579676709.2998796, 653920969.822689, 733644411.0299448, 805248093.7314515, 869433969.9856126, 959532658.4912381, 1082428389.049758, 1221064449.5031044, 1377456841.416773, 1495422683.1591103, 1562415280.7355819, 1632409041.916555, 1730635774.2241664, 1871998935.891062, 2111762194.384889, 2382234028.0928435, 2671335860.4496403, 2998507377.5831275, 3382552413.9788895, 3815785453.406714, 4304506433.147367, 4855822178.488748, 5471085560.279512, 6179333014.662048, 6970774179.776083, 7863582129.355498, 8870739793.080698, 10006892937.862001, 11237148315.070232, 12734388046.38513, 14365392377.844816, 16205293683.352232, 18280847223.40834, 20622235038.474533, 23263504846.620384, 25429331263.94244, 28334885235.491417, 31963981536.166553, 36057887905.77669, 40676136630.677284, 45885884817.24294, 51762890969.1817, 58392616643.63792, 65871469206.01712, 74308203755.27731, 83825504606.04303, 94561769324.94763, 106673121264.09769, 120335679857.28032, 135748121692.84048, 153134569605.53696, 169003772285.2535])
        p95_y = np.array([8.783134830354705, 8.803923505934659, 8.823767241715526, 8.84433444703007, 8.86064351737497, 8.876909367462012, 8.885027259372366, 8.89446055043486, 8.915072049802486, 8.934325198208922, 8.951248210974423, 8.960568753538164, 8.962422839962134, 8.955682985258782, 8.964947153573146, 8.982310422381403, 8.999466985608612, 9.019684760059949, 9.039755782497036, 9.033780112063026, 9.034005609060534, 9.050572121810925, 9.067777542720926, 9.073768246288104, 9.079668751056278, 9.084667267834412, 9.091732840423054, 9.100752720323449, 9.110561839715126, 9.11976963378011, 9.127662028692956, 9.134351772952414, 9.140490302329072, 9.146675810246876, 9.154045177401608, 9.162614063306982, 9.168627316573911, 9.173813747516636, 9.177797527805977, 9.183885946738743, 9.197852667022008, 9.20703697181642, 9.20857786796607, 9.207563131477277, 9.21748499936771, 9.228384020914019, 9.234610243567484, 9.239734036455348, 9.24567212405644, 9.250633058001657, 9.254554200236134, 9.258600618580338, 9.263536497303608, 9.272656598091784, 9.285660258281519, 9.289794369902532, 9.288677622867246])
        m95_x = np.array([376044963.81175673, 399401308.48773324, 421890868.66454077, 440973195.3447672, 463642110.1353547, 507422945.046693, 547275084.6717288, 619062678.0368167, 697714140.0287992, 762323070.2092369, 805248093.7314515, 850590147.1408465, 903420735.4120835, 980789974.357351, 1076515060.0594435, 1166576038.9538445, 1214393748.8070264, 1268796689.6139338, 1324185225.3036206, 1385023252.492162, 1495422683.1591103, 1677738663.0139215, 1841485929.5048478, 1955861560.6780565, 2065992562.5841877, 2194312415.3501873, 2369219833.6366158, 2530195799.7251024, 2702109231.9322762, 2869938615.7735686, 3167347866.273785, 3452757090.2573185, 3708624081.5504375, 4165325794.708462, 4720314578.236797, 5300633806.997423, 5914378123.957597, 6491621333.607438, 7103945147.376909, 7950209177.497994, 8968461924.09858, 10117131195.951641, 11412920576.834106, 12794322030.004997, 14365392377.844816, 16205293683.352232, 18247496867.65676, 20622235038.474533, 23263504846.620384, 26363138558.786, 29604243782.298225, 33395917921.851524, 37707636241.6945, 42498363940.02097, 48029121817.53325, 54081787534.77847, 61175845862.56196, 68822408012.80806, 77637095076.01843, 87580756121.18927, 99250032354.50073, 111451910275.09164, 125726530126.94429, 141829425253.86426, 159994758843.10944, 172747851799.62997])
        m95_y = np.array([8.177743399071774, 8.158836728591671, 8.140977366388892, 8.125851197982344, 8.110718789407313, 8.11979823455233, 8.1259292000884, 8.134047091998756, 8.135093147514981, 8.150403140884803, 8.168427867552422, 8.186452594220043, 8.205993161815368, 8.224412925815663, 8.24230444221994, 8.256842734753837, 8.279924857971164, 8.30192751870967, 8.324421679385342, 8.34483041042108, 8.368601552241909, 8.379763653618646, 8.391339166157485, 8.410025350684467, 8.428050077352088, 8.449561238153429, 8.472643361370755, 8.490695648782516, 8.508747936194275, 8.525449747143172, 8.550443404829073, 8.567865732374774, 8.587626785923218, 8.596842096554788, 8.6057617555674, 8.620744778290831, 8.635711097532965, 8.653131784558182, 8.66915146708961, 8.688995202870476, 8.707200327136105, 8.721541936177731, 8.735710664187932, 8.756204046080672, 8.77085061296655, 8.787011231121422, 8.800754020358548, 8.813619876827582, 8.824819561037238, 8.840028080758179, 8.855532252098078, 8.867325745067843, 8.874341207212595, 8.883724387831197, 8.891980083462252, 8.907426627791677, 8.918185757682123, 8.923399331781958, 8.926606400190986, 8.928022020230909, 8.927074097667303, 8.925917381587483, 8.933058119841963, 8.940349189428114, 8.933960107832002, 8.928498069447874])
        ax.plot(med_x, med_y, 'k--', label='Tremonti+04')
        ax.fill(np.append(m68_x, p68_x[::-1]), np.append(m68_y, p68_y[::-1]), c='k', alpha=0.25)
        ax.fill(np.append(m95_x, p95_x[::-1]), np.append(m95_y, p95_y[::-1]), c='k', alpha=0.25)

        # Zahid+14 - Strong line method calibrated on photoionisation model
        logOH = lambda M_star, Z0, M0, gamma: Z0 + np.log10(1-np.exp(-(M_star/M0)**gamma)) # Eq. 5 in Zahid+14
        M_star = np.logspace(9,11)
        logOH078 = logOH(M_star, 9.10, 10**9.80, 0.52)
        logOH155 = logOH(M_star, 9.08, 10**10.06, 0.61)
        ax.plot(M_star, logOH078, c='darkblue', label='Zahid+14') # Photoionisation (Kobulnicky & Kewley 2004) - R23 
        ax.plot(M_star, logOH155, c=cm(norm(1.55))) # Calibrated on Te (Pettini & Pagel 2004) converted to (Kobulnicky & Kewley 2004)

        # Stephenson+23 - Photoionisation calibration (Maiolino+08) - Multiple lines
        xS23 = np.array([1897359676.501973,2062708383.0987813,2242469393.075362,2437894754.982929,2650349862.872564,2881319786.783743,3132421812.257837,3405401471.0785666,3702177050.7325087,4024797575.8675365,4375555266.688908,4747441633.579564,5317425853.845147,5780896322.481551,6276878034.841399,7426246168.594922,8778886067.84797,9543924058.522911,10375614642.040634,11310631187.7464,12262851701.124065,13331527259.9126,14493352577.431787,15756394646.379986,17129519924.642014,18622309191.53328,20245198959.799965,22009519695.23929,23927591961.97159,26012824452.472874,28279762930.546326,30744282902.468384,33423567012.81414,36336329149.592255,39502953448.798645,42267426498.160385])
        yS23 = np.array([8.103606263011496,8.125051797015454,8.146156022785924,8.167430902673138,8.188819551971513,8.210208201269888,8.231255542334779,8.252757961044315,8.273748417403624,8.296047221991293,8.316864315438355,8.336301399472006,8.36959061360141,8.387404059154033,8.408305633160872,8.451290972598624,8.492417017969867,8.514256952599183,8.53655575718685,8.557057204673622,8.57859355461105,8.599868434498264,8.620802006151992,8.642361309567109,8.663693074159903,8.685024838752698,8.706242833934331,8.727460829115964,8.748735709003176,8.76995370418481,8.791342353483184,8.812503463959237,8.83377834384645,8.855166993144826,8.876384988326457,8.893677938823016])
        ax.plot(xS23, yS23, 'g', label='Stephenson+23')

    # Direct Te method and strong line ratio calibrated on Te
    elif True:
        # Curti+20b, z=0.08 - Direct method (Te) - multiple line ratios (could result in difference, cf Curti VS Henry in Li+22)
        bf_x = np.array([77317360.43450522, 87047145.21108201, 98001347.26298317, 110334049.9228358, 124218726.70492311, 139850681.40781423, 157449795.28481105, 177263619.92429754, 199570859.34488407, 224685290.28497157, 252960175.83007845, 284793234.4606371, 320632234.415563, 360981292.06269956, 406407962.86991036, 457551224.71937275, 515130465.8595283, 579955603.9204242, 652938478.3279803, 735105676.3644217, 827612973.2880453, 931761589.629087, 1049016494.3390301, 1181027011.2480192, 1329650018.6837268, 1496976068.580673, 1685358792.475054, 1897448008.0139709, 2136226991.6596777, 2405054441.8721037, 2707711723.029388, 3048456054.6262527, 3432080393.916673, 3863980854.318509, 4350232607.896914, 4897675339.5812, 5514009455.126162, 6207904396.092654, 6989120585.420704, 7868646718.898694, 8858854333.689856, 9973671828.098026, 11228780380.359095, 12641834522.279675, 14232710470.34145, 16023785707.332523, 18040253747.138382, 20310478510.219555, 22866393294.46942, 25743949953.43228, 28983624600.086018, 32630986949.487766, 36737341308.73598, 41360448230.50457, 46565336981.02595, 52425220250.81184, 59022523974.56085, 66450046745.844475, 74812265135.0538, 84226803271.39981, 94826087360.30733, 106759208408.87483, 120194019360.75607, 135319496139.05968, 152348395808.01813, 171520249243.50082, 193104730407.6982, 217405449620.65417, 244764224185.3364, 275565886437.45544, 305269423034.51624])
        bf_y = np.array([8.208551729150889, 8.222532422311897, 8.236699524715052, 8.250680217876061, 8.26494052490029, 8.279200831924518, 8.292901911222305, 8.306696195141168, 8.32104970678647, 8.335123604568551, 8.349011093108485, 8.362898581648421, 8.376692865567282, 8.39067355872829, 8.404561047268226, 8.418262126566013, 8.432242819727023, 8.446130308266957, 8.459551773701525, 8.472973239136094, 8.486487909191734, 8.500002579247376, 8.513237635439797, 8.526565896253292, 8.539521338582492, 8.552383576290621, 8.565059404756601, 8.577735233222583, 8.589851833962122, 8.60215484394381, 8.613991830820131, 8.625362794591084, 8.636640553740964, 8.647918312890845, 8.658357230451063, 8.668516534148063, 8.678489428602916, 8.687809890710255, 8.696571125091154, 8.705239154850979, 8.713161547642217, 8.720617917328088, 8.727421854666446, 8.73413258738373, 8.740004478511352, 8.74568996039683, 8.750723009934791, 8.755476445609535, 8.75976385817891, 8.763771656885067, 8.766940614001562, 8.770202775739131, 8.773371732855626, 8.775329029898167, 8.777565940803928, 8.779616442467542, 8.781294125646864, 8.783158218068332, 8.785022310489799, 8.785115515110872, 8.7864203798059, 8.787818449122001, 8.78912331381703, 8.78912331381703, 8.78912331381703, 8.790987406238497, 8.79108061085957, 8.791173815480644, 8.791546633964936, 8.793224317144258, 8.793224317144258])
        ms1_x = np.array([74020981.37633647, 104557415.28389703, 210956809.40265065, 294682834.573264, 858758339.8375244, 5901572434.705682, 3127240560.7596197, 1657123357.047777, 10416982521.932018, 13917088440.153723, 17982029660.479855, 23234270019.973057, 36280847163.815544, 54791056839.1595, 101120383888.86363, 149345983903.73447, 192967366973.26303, 246567302473.92868, 297984413575.07074])
        ms1_y = np.array([7.952650176678445, 8.151943462897528, 8.230388692579506, 8.296113074204946, 8.4339222614841, 8.603533568904593, 8.542049469964665, 8.484805653710248, 8.658657243816254, 8.68409893992933, 8.701060070671378, 8.713780918727915, 8.730742049469965, 8.737102473498233, 8.741342756183746, 8.737102473498233, 8.730742049469965, 8.72226148409894, 8.711660777385159])
        ps1_x = np.array([74020981.37633647, 104557415.28389703, 208619470.46651122, 297984413.57507074, 398107170.5534969, 830529071.0979125, 1791537101.1560464, 3821712954.930261, 5707574814.517082, 8813761418.402777, 15214513311.549301, 21252956574.359768, 29033756208.204624, 38789097711.23479, 66958613406.34246, 94581555372.3393, 127776683872.19652, 186624106701.35287, 235820005500.75064, 297984413575.07074])
        ps1_y = np.array([8.281272084805654, 8.404240282685512, 8.421201413427562, 8.480565371024735, 8.503886925795053, 8.592932862190812, 8.662897526501768, 8.72226148409894, 8.749823321554771, 8.775265017667845, 8.796466431095407, 8.807067137809188, 8.817667844522969, 8.824028268551237, 8.832508833922262, 8.836749116607773, 8.838869257950531, 8.838869257950531, 8.836749116607773, 8.832508833922262])
        ms2_x = np.array([74020981.37633647, 110547362.93168975, 210956809.40265065, 152711227.3932278, 318585353.9306616, 897895523.66902, 1199588401.5942667, 2366974332.1043634, 4829153028.424976, 15214513311.549301, 28712070792.882053, 50680245464.58748, 74020981376.33647, 104557415283.89702, 147691274657.21765, 195129342263.59583, 297984413575.07074, ])
        ms2_y = np.array([7.935689045936396, 8.009893992932863, 8.084098939929328, 8.065017667844524, 8.177385159010601, 8.31095406360424, 8.317314487632508, 8.378798586572438, 8.446643109540636, 8.590812720848056, 8.637455830388692, 8.66713780918728, 8.681978798586572, 8.654416961130742, 8.648056537102473, 8.614134275618374, 8.552650176678446])
        ps2_x = np.array([74850300.52605584, 151019232.2460574, 206308028.5531685, 575012676.8665326, 1326113343.9968753, 2117417273.3072772, 4272136901.415519, 7710752692.535523, 13017156584.085075, 19012194914.420662, 28393949556.637306, 46358463386.66337, 70794578438.41402, 103398950105.65907, 151019232246.0574, 213320335507.6587, 301322982936.09186, ])
        ps2_y = np.array([8.397879858657245, 8.525088339222615, 8.508127208480566, 8.635335689045936, 8.711660777385159, 8.749823321554771, 8.785865724381626, 8.813427561837456, 8.830388692579506, 8.840989399293287, 8.851590106007068, 8.862190812720849, 8.870671378091872, 8.874911660777386, 8.881272084805653, 8.88339222614841, 8.85583038869258])
        ax.plot(bf_x, bf_y, 'k', label='Curti+20, z=0.08')
        ax.fill(np.append(ms1_x, ps1_x[::-1]), np.append(ms1_y, ps1_y[::-1]), c='k', alpha=0.25)
        ax.fill(np.append(ms2_x, ps2_x[::-1]), np.append(ms2_y, ps2_y[::-1]), c='k', alpha=0.25)

        # Sanders+21 - Strong line calibrated on direct method (Andrews & Martini 2013 for z~0, Bian+18 for higher z) - Multiple line ratios
            # z~0
        xS00 = np.array([532412185.9347662,665068447.9473629,836961178.1315227,1053281080.8591403,1325510745.638747,1668100537.2000558,2099235642.8360922,2641801369.8085465,3324597932.2709312,4215010994.233461,5265226630.931002,6675390103.893142,8338635841.034902,10493828866.881842,13206050292.482332,16619268957.01266,20914663699.467583,26320240607.04872,32878213595.72679,41683847069.616425,52457400227.1744,65527729593.91843,82463941764.90944,103010725918.90265,129634744801.62393,161934645379.8707,205304919430.4282])
        yS00 = np.array([8.409227393688184,8.455090464167043,8.490075455726316,8.506933559068065,8.523791662409815,8.532190551250055,8.581677053383345,8.614245126513614,8.646813199643884,8.676962408356077,8.696239376115903,8.725180125613594,8.750499389445928,8.773399788860186,8.787840973772932,8.798656781042174,8.810681047525918,8.816663017937154,8.821438475122962,8.821376203472617,8.826149714669352,8.824882875782652,8.822405631692378,8.817513415162175,8.8162446302864,8.819811628257707,8.805247846033344])
        plt.errorbar(xS00, yS00, xerr=[xS00*(1-10**-0.02576),xS00*(10**0.02576-1)], yerr=0.00966, c='k', marker=mSa21, ls=bluh)
        plt.scatter(xS00, yS00, color='k', marker=mSa21, edgecolor='k', s=100,zorder=10, label='Sanders+21')

            # z~2.3
        yS23 = np.array([8.297685245997343,8.40989681392939,8.479823985288322,8.570256043512316,8.64130215858838])
        xS23 = np.array([2130602417.4937363,4152957511.6362076,7742636826.811246,16742971122.282719,43904807495.852875])
        xS23_min = np.array([1892224340.9267828,3688312388.0036736,6876367803.938887,14869718177.774754,37573290220.070816])
        xS23_max = np.array([2399010816.659725,4710943444.418005,8782927371.536043,18992534796.611008,49435834307.28163])
        yS23_max = np.array([8.317020593429367,8.42802175615784,8.497948927516772,8.58959139094434,8.671513638950918])
        yS23_min = np.array([8.277141439350817,8.389351061293791,8.459278232652723,8.549710290876716,8.606256841367836])
        plt.errorbar(xS23, yS23, xerr=[xS23-xS23_min,xS23_max-xS23], yerr=[yS23-yS23_min,yS23_max-yS23], c=cm(norm(2.3)), marker=mSa21, markersize=10, ls=bluh)
        plt.scatter(xS23, yS23, color=cm(norm(2.3)), marker=mSa21, edgecolor='k', s=100,zorder=10)

            # z~3.3
        xS33 = np.array([1693025291.9960253,3399389793.914489,6626070290.590986,16253619196.37764,39869805992.85072])
        yS33 = np.array([8.188984242353479,8.293937271042223,8.350559715107199,8.478420927166495,8.5361915047847])
        xS33_min = np.array([1375581455.5767953,3041527779.75129,6198270584.906582,14328465355.034649,38418558243.718185])
        xS33_max = np.array([1836919722.7252586,3580512956.3957977,7350969240.324435,17635054720.471386,54438960376.36274])
        yS33_max = np.array([8.21677685829794,8.319316860535828,8.377145817826232,8.497756274598517,8.60869905765479])
        yS33_min = np.array([8.15031354748943,8.269768086752192,8.328809395235245,8.45304133767289,8.475768544059624])
        plt.errorbar(xS33, yS33, xerr=[xS33-xS33_min,xS33_max-xS33], yerr=[yS33-yS33_min,yS33_max-yS33], c=cm(norm(3.3)), marker=mSa21, ls=bluh)
        plt.scatter(xS33, yS33, color=cm(norm(3.3)), marker=mSa21, edgecolor='k', s=100,zorder=10)

        # Li+22 - Strong line ratio calibrated on Te (Bian+18) - O32, if not possible O3 (O3/Hβ)
            # 1.8<z<2.3, median=2
        xL2 = np.array([4365158.322401647, 36307805.4770101, 319889510.96913904, 1778279410.0389228, ])
        yL2 = np.array([8.031111111111112, 8.110303030303031, 8.22989898989899, 8.352727272727273])
        xL2_min = np.array([3311311.2148259077,29512092.26666378,260015956.31652668,1135010815.6723144])
        xL2_max = np.array([5688529.308438402,44668359.21509635,393550075.45577645,2818382931.264449])
        yL2_max = np.array([8.081212121212122,8.142626262626264,8.260606060606062,8.376969696969697])
        yL2_min = np.array([7.982626262626264,8.074747474747475,8.19919191919192,8.33010101010101])
        plt.errorbar(xL2, yL2, xerr=[xL2-xL2_min,xL2_max-xL2], yerr=[yL2-yL2_min,yL2_max-yL2], color=cm(norm(2)), marker=mLi22, ls=bluh)
        plt.scatter(xL2, yL2, color=cm(norm(2)), marker=mLi22, edgecolor='k', s=100,zorder=10, label='Li+22')

            # 2.65<z<3.4, median=3
        xL3 = np.array([29853826.189179573,245470891.56850335,4216965034.2858224])
        yL3 = np.array([8.003636363636364,8.144242424242425,8.341414141414141])
        xL3_min = np.array([24266100.95082414,192752491.31909367,3672823004.9808574])
        xL3_max = np.array([37153522.9097172,312607936.71239495,4786300923.22638])
        yL3_min = np.array([7.968080808080809,8.118383838383838,8.320404040404041])
        yL3_max = np.array([8.03919191919192,8.16848484848485,8.36080808080808])
        plt.errorbar(xL3, yL3, xerr=[xL3-xL3_min,xL3_max-xL3], yerr=[yL3-yL3_min,yL3_max-yL3], color=cm(norm(3)), marker=mLi22, ls=bluh)
        plt.scatter(xL3, yL3, color=cm(norm(3)), marker=mLi22, edgecolor='k', s=100,zorder=10)

        # Stephenson+23 - Te calibration (Bian+18) - multiple lines (R23, O32, O2 and O3)
        xS23 = np.array([1892856874.730716,2058537218.741892,2239508292.9624605,2441526077.7346125,2652330374.2912555,2887415767.459772,3143337607.9339776,3383567629.8965816,3928325553.5265822,4285011038.8859873,4661711444.416097,5066981225.34265,5521038029.046894,6006401849.59254,6535710064.598429,7112214477.485644,7738972082.132182,8419306408.76603,9171653414.79601,9971313685.291046,10847818423.059204,11715632811.165928,12796523697.26678,13921504121.266466,15155401910.476437,16487739006.231504,17937204116.618633,19527032610.62871,21243692157.454113,23111255761.986134,25168512729.459286,27371505866.651474,29777781387.96299,32463957912.161053,35266940898.43151,38326904040.933914,41866116475.66923])
        yS23 = np.array([8.312058593532708,8.322337436753878,8.33235862572036,8.34297710409544,8.353197389531454,8.363550405947157,8.37390342236286,8.382568859179665,8.399796894188679,8.411466289614705,8.421819306030407,8.431867821963294,8.442392607882123,8.452612893318138,8.462856601868216,8.473256464512044,8.483472846095713,8.494091324470794,8.503562627949995,8.5142000678936,8.526779172454365,8.53661833036299,8.544860924201645,8.554749382188437,8.565367860563518,8.575588145999532,8.585808431435545,8.596294178830938,8.606448098777108,8.616734749702966,8.627286862588202,8.637440782534371,8.647661067970386,8.657994565124369,8.668168004332259,8.67830240501671,8.686764313170722])
        xpS23 = np.array([1823942319.8761885,2034904886.096007,2393756742.3064733,2786215394.8722677,3277510476.611015,3708589999.9217343,4079499347.9171433,4440211795.293476,4765064508.909791,5391716142.731683,5889173251.627676,6734530038.552141,7728478448.691021,8775588080.59049,10142007355.425415,12534101877.616848,15435787003.504526,17776355479.984776,19622824336.497864,21891639270.750572,24422777360.81495,26770076044.359604,31488810820.218708,33318323394.835304,37301852400.09011,43262198310.83626])
        ypS23 = np.array([8.370762711864407,8.379237288135593,8.389830508474576,8.40042372881356,8.415254237288137,8.42584745762712,8.434322033898306,8.442796610169491,8.449152542372882,8.463983050847459,8.474576271186441,8.49364406779661,8.51271186440678,8.533898305084746,8.557203389830509,8.593220338983052,8.629237288135593,8.652542372881356,8.66949152542373,8.6885593220339,8.707627118644067,8.722457627118644,8.752118644067798,8.760593220338983,8.78177966101695,8.807203389830509])
        xmS23 = np.array([1818328413.1207438,2123805517.4481225,2420085538.198953,2728635098.438389,3065700417.4479585,3384166734.1734533,3722545792.3925858,4302203902.830036,4867912087.205318,5706001562.463614,6456395433.317249,6904380654.782653,8413661831.191959,10144891356.070726,11642610098.627848,13599267590.214254,16110746144.579449,19841920532.46731,23176727855.295074,26225278266.37735,30096764405.55783,34540034082.71978,39499238507.884224,43297171393.62171])
        ymS23 = np.array([8.24364406779661,8.271186440677967,8.292372881355933,8.313559322033898,8.332627118644067,8.347457627118644,8.362288135593221,8.383474576271187,8.402542372881356,8.421610169491526,8.436440677966102,8.440677966101696,8.459745762711865,8.476694915254237,8.485169491525424,8.497881355932204,8.510593220338983,8.52542372881356,8.536016949152543,8.544491525423728,8.555084745762713,8.563559322033898,8.574152542372882,8.578389830508476])
        ax.plot(xS23, yS23, c=cm(norm(2.2)), label='Stephenson+23')
        ax.fill(np.append(xmS23, xpS23[::-1]), np.append(ymS23, ypS23[::-1]), c=cm(norm(2.2)), alpha=0.25)

        
    # Topping+21 - Bian+18 - O3N2 (more robust than N2)
    if False: # Not comparable.. O3N2 Sanders+18 too different than Sanders+18 relacibrated in Sanders+21
        yT23 = np.array([8.27813953488372,8.332558139534884,8.385581395348837,8.395348837209301,8.470697674418604,8.572558139534884,8.611627906976745,8.727441860465117])
        xT23 = np.array([1607811060.6583169,2514661984.0934234,3483103878.554239,4797943791.79617,6756724496.156202,9567871803.396181,15130598777.438665,38048341542.1956,])
        xT23_min = np.array([1377490869.950067,2276743727.1140122,3188578565.335162,4515192378.428221,6393746240.363801,8710597807.724072,14004995500.204908,32959879215.101707,])
        xT23_max = np.array([1699087822.7476861,2657421422.2349277,3660573746.7383494,5299325836.000291,7140309327.37794,10055370599.572235,16619712286.886177,41106345947.30774,])
        yT23_max = np.array([8.350697674418605,8.410697674418605,8.435813953488372,8.463720930232558,8.523720930232559,8.589302325581395,8.65906976744186,8.746976744186046])
        yT23_min = np.array([8.233488372093023,8.28093023255814,8.29906976744186,8.38139534883721,8.44139534883721,8.52093023255814,8.582325581395349,8.68])
        plt.errorbar(xT23, yT23, xerr=[xT23-xT23_min,xT23_max-xT23], yerr=[yT23-yT23_min,yT23_max-yT23], c=cm(norm(1.5)), marker=mT23, ls='-')
        plt.scatter(xT23, yT23, color=cm(norm(1.5)), marker=mT23, edgecolor='k', s=100, zorder=10, label='Topping+21')
            # Topping+21 replotting Sanders18 which "should be a fair representation of" Sanders+21 if recalibrated
        # -> use it to calibrate Topping+21. x)
        xSaT = 10**np.array([9.396946514682883,9.737458143466919,9.918965956676415,10.258066016092434])
        ySaT = np.array([8.235222888348073,8.332554163946103,8.441641835341605,8.547634593696207])
        plt.scatter(xSaT, ySaT, color=cm(norm(2.2)), marker=mT23, edgecolor='k', s=100, zorder=10)

    # Yabe+11
    if False: # Sanders+21 say good agreement with Erb+06 but it's not the case -> probably different calibration.
        # z~1.4 Strong line calibrated on Te (Pettini & Pagel 2004, PP04)
        Y11med_x = np.array([5222927917.579868, 5847037041.839438, 6545736761.794329, 7327958188.854109, 8197176129.155901, 8986516360.435867, 9935928995.88697, 10501541606.64565, 11513693746.256285, 12495345424.809952, 13988748366.947554, 15342561170.896254, 17652889088.259144, 20308371654.396576, 22265965341.27215, 24600808117.863483, 27541536081.174488, 30833792372.854626, 34629786148.12774, 39043767896.71699, 54790165612.467896, 61339875555.13347, 68673423523.367935, 76884207118.47443, 86077042589.91907, 95875695632.39804, ])
        Y11med_y = np.array([8.333074638704774, 8.348752945613285, 8.364167894187045, 8.3790561260913, 8.393940553930719, 8.40675999423124, 8.41679435825706, 8.42480140644879, 8.436316585657975, 8.445430619819131, 8.458475343380124, 8.468228254968365, 8.48409532059585, 8.49818782632955, 8.508260000625304, 8.51843179449006, 8.529018506926704, 8.539605219363349, 8.553212597033276, 8.56499988818221, 8.586399313522767, 8.596538316790335, 8.605018162548966, 8.612707933303343, 8.619870987388216, 8.626544048660264])
        yY11 = np.array([8.413748378728924,8.492866407263294,8.571984435797665])
        xY11 = np.array([9496825876.764673,19033632255.034515,44080360944.35591,])
        xY11_min = np.array([4972353917.656105,12945165982.558977,29979962907.230328])
        xY11_max = np.array([13034583542.741669,29979962907.230328,99313997567.39883])
        yY11_max = np.array([8.457846952010376,8.527885862516213,8.591439688715953])
        yY11_min = np.array([8.367055771725031,8.455252918287938,8.553826199740596])
        ax.plot(Y11med_x, Y11med_y, c=cm(norm(1.4)))
        plt.scatter(xY11, yY11, color=cm(norm(1.4)), marker=mYa11, edgecolor='k', s=100, zorder=10, label='Yabe+11')
        plt.errorbar(xY11, yY11, xerr=[xY11-xY11_min,xY11_max-xY11], yerr=[yY11-yY11_min,yY11_max-yY11], c=cm(norm(1.4)), marker=mYa11, ls='')
        # Erb+06, z∼0.8, converted from photoionisation calibration following Kewley & Ellison (2008).
        EY_y = np.array([8.147100424328148,8.1548829045851,8.180304163228824,8.194176169849127,8.21837153023338,8.231316048038956,8.25272894197902,8.266036390190358,8.288441293906176,8.300272825134076,8.320314648652365,8.332788701117135,8.350768542256011,8.363640473980434,8.382416073638614,8.393330869545288,8.41072464528819,8.421074882785899,8.43758149531471,8.447162858026875,8.461486511374352,8.47070714325897,8.483068772837106,8.492350992911792,8.504651034299858,8.511883247476534,8.521539395848068,8.528951974438515,8.537750287305517,8.543205241283058,8.549122106686115,8.553041018387553])
        EY_x = np.array([2870093120.4405394,3001776028.2484345,3521008398.102325,3862747799.2853446,4535525564.818743,4970583762.121929,5770563203.812036,6363237005.890043,7509931391.787601,8230232721.814741,9653467535.126976,10699733599.14018,12408694147.717768,13910040181.691002,16483915158.778522,18270267570.94829,21762301943.124947,24120567795.909058,28730590775.08183,32007900565.564495,37696038696.28715,42473549116.910675,49687654480.01411,56650814914.25919,67754075458.845726,76339932887.90425,90227262947.5706,102870144174.99489,121582917106.99309,138617396390.3117,158039054472.98935,179338665669.565])
        ax.plot(EY_x, EY_y, c=cm(norm(2.2)), ls='--', label='Erb+06')
        # plt.scatter(EY_x, EY_y, color=cm(norm(0.8)), marker=mYa11, edgecolor='k', s=100, zorder=10)
        # Zahid+11, z∼3.1, converted from photoionisation calibration following Nagao, Maiolino, & Marconi (2006).
        ZY_y = np.array([8.35926449787836,8.366535118913678,8.375641372731023,8.399734086131827,8.405643619607497,8.430322887199434,8.438388007327518,8.460765049719258,8.469035463814238,8.49120721223908,8.498553803483027,8.522881138560285,8.528072143151816,8.547868347102568,8.555170946782178,8.573559420674211,8.580576075185643,8.598282679830483,8.60501338917374,8.621686192056707,8.626466608714443,8.642890126066177,8.647597223450024,8.665017882926685])
        ZY_x = np.array([3601367868.6349616,3789037593.672887,4051169614.4736476,4850590960.817696,5186038490.336509,6228612940.868173,6673173827.359752,7948929045.68842,8542595685.19298,10280879227.318823,10991982508.385088,13573307824.365686,14437499774.098892,17197279616.38598,18481525960.36898,22241592962.26516,24025668248.26503,29062552813.944416,31393693499.844513,38367386503.43333,41231777925.98808,50650472170.878075,54712372856.01415,70032529080.63417])
        ax.plot(ZY_x, ZY_y, c=cm(norm(0.8)), ls='-.', label='Zahid+11')
        print('Erb+06 and Zahid+11 are respectively converted to Te-based calibration by Yabe+11 by using Kewley & Ellison (2008) and Nagao+06.')


###################################################################################################################################
############################## PIES PIES PIES PIES PIES PIES PIES PIES PIES PIES PIES PIES PIES PIES ##############################
###################################################################################################################################
def pie_baryons(genpath, folders, timesteps, factor=1, hnum=None, alt_path=None):
    """
    Computes the fraction of baryons in stars, the ISM, the CGM, and "missing", relative to the baryonic content expected from the DM halo mass.
    """
    filename = 'pie_baryons'
    datas    = []
    arr200, temp_r200 = [], []

    # Take the smallest r200 at each timestep
    for timestep in timesteps:
        for folder in folders:
            temp_r200.append(ras.get_r200(genpath+folder, timestep, hnum=hnum, alt_path=alt_path))
        arr200.append(min(temp_r200))

    for folder in folders:
        RamsesDir = genpath+folder
        arr_Mstars = []
        arr_M_ISM = []
        arr_M_CGM = []
        arr_Miss_b = []
        for i_time, timestep in enumerate(timesteps):
            print(f'{folder}: timestep {timestep}', end='\r')
            try:
                arr_Mstars.append(ras.read_prop(RamsesDir, timestep, 'M_stars_r200'   , filename, hnum=hnum, alt_path=alt_path))
                arr_M_ISM .append(ras.read_prop(RamsesDir, timestep, 'M_ISM'          , filename, hnum=hnum, alt_path=alt_path))
                arr_M_CGM .append(ras.read_prop(RamsesDir, timestep, 'M_CGM'          , filename, hnum=hnum, alt_path=alt_path))
                arr_Miss_b.append(ras.read_prop(RamsesDir, timestep, 'M_issingbaryons', filename, hnum=hnum, alt_path=alt_path))
            except:
                info   = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
                center = ras.get_Gcen(RamsesDir, timestep, hnum=hnum, alt_path=alt_path)
                cu2cm  = info['cu2cm']
                r200_cu = arr200[i_time]
                r200_cm = r200_cu*cu2cm

                # Stars
                star_mass = ras.extract_stars(RamsesDir, timestep, ['star_mass'], factor=1, alt_path=alt_path)
                Mstars  = np.sum(star_mass) /g2Msun
                # Gas
                mass, rad_gas = ras.extract_cells(RamsesDir,timestep,['mass','radius'],factor=factor, alt_path=alt_path)
                M_ISM = np.sum(mass[rad_gas   < 0.1*r200_cm]) /g2Msun
                M_CGM = np.sum(mass[np.logical_and((rad_gas   > 0.1*r200_cm),(rad_gas   < r200_cm))]) /g2Msun

                # DM
                hcat = ras.get_hcat(RamsesDir,timestep,alt_path=alt_path)
                hmain = np.where((hcat.level==1) & (hcat.contam < 0.5) & (hcat.mvir > 4e7*1e-11))[0]
                myhalo = hmain[np.where(hcat.mvir[hmain]==max(hcat.mvir[hmain]))[0][0]]
                center = [hcat.x_cu[myhalo],hcat.y_cu[myhalo],hcat.z_cu[myhalo]]
                xc,yc,zc,rc = hcat.x_cu[myhalo],hcat.y_cu[myhalo],hcat.z_cu[myhalo],hcat.rvir_cu[myhalo]
                r_max = r200_cu*factor
                ndm = dm.dmpart_utils.get_npart_in_sphere(RamsesDir,timestep,xc,yc,zc,r_max)
                xdm,ydm,zdm,mdm,_ = dm.dmpart_utils.read_parts_in_sphere(RamsesDir,timestep,xc,yc,zc,r_max,ndm)
                rad_dm = np.sqrt((xdm-center[0])**2+(ydm-center[1])**2+(zdm-center[2])**2)
                rad_dm *= cu2cm
                M_DM_tot = np.sum(mdm[rad_dm < r200_cm])*info['unit_m']
                M_bLCDM = f_b*M_DM_tot /g2Msun
                M_bar = Mstars + M_ISM + M_CGM
                M_issingbaryons = M_bLCDM - M_bar

                # Save data in file props.h5
                ras.add_prop(RamsesDir, timestep, 'M_stars_r200'   , Mstars, filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'M_ISM'          , M_ISM , filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'M_CGM'          , M_CGM , filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'M_issingbaryons', M_issingbaryons, filename, hnum=hnum, alt_path=alt_path)

                arr_Mstars.append(Mstars)
                arr_M_ISM.append(M_ISM)
                arr_M_CGM.append(M_CGM)
                arr_Miss_b.append(M_issingbaryons)

        end_Mstars = np.sum(arr_Mstars)
        end_M_ISM = np.sum(arr_M_ISM)
        end_M_CGM = np.sum(arr_M_CGM)
        end_Miss_b = np.sum(arr_Miss_b)

        labels = [r'$\rm M_{stars}$', r'$\rm M_{ISM}$', r'$\rm M_{CGM}$', r'$M_\mathrm{b, exp} - M_\mathrm{b}$']
        labels = ['Stars', 'ISM', 'CGM', 'Missing\nbaryons']
        sizes = [end_Mstars, end_M_ISM, end_M_CGM, end_Miss_b]
        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        colors[2] = colors[3] # red
        colors[3] = 'whitesmoke'

        datemp = labels, sizes, colors, folder
        datas.append(datemp)
    return datas


def pie_metals(genpath, folders, timesteps, factor=1, hnum=None, alt_path=None):
    """
    Computes the mass of metals in stars, in the ISM and in the CGM, and compare it to the mass of metals created by stars in the halo. The difference is the metal mass ejected beyond R200.
    Once KI bug problem solved, delete bug-related parts!
    """
    Z_ave = 3.2e-4*Z_sun        # from namelist
    filename = 'pie_metals_temp' # CHANGE IT once bug solved, 
    datas = []

    # Take the smallest r200 at each timestep
    arr200, temp_r200 = [], []
    for timestep in timesteps:
        for folder in folders:
            temp_r200.append(ras.get_r200(genpath+folder, timestep, hnum=hnum, alt_path=alt_path))
        arr200.append(min(temp_r200))

    for folder in folders:
        RamsesDir = genpath+folder
        arr_Miss_Z   = []
        arr_M_Z_ISM  = []
        arr_M_Z_CGM  = []
        arr_M_Z_star = []
        for i_time, timestep in enumerate(timesteps):
            print(f'{folder}: timestep {timestep}', end='\r')
            try:
                arr_M_Z_star.append(ras.read_prop(RamsesDir, timestep, 'M_Z_star', filename, hnum=hnum, alt_path=alt_path))
                arr_M_Z_ISM .append(ras.read_prop(RamsesDir, timestep, 'M_Z_ISM' , filename, hnum=hnum, alt_path=alt_path))
                arr_M_Z_CGM .append(ras.read_prop(RamsesDir, timestep, 'M_Z_CGM' , filename, hnum=hnum, alt_path=alt_path))
                arr_Miss_Z  .append(ras.read_prop(RamsesDir, timestep, 'Miss_Z'  , filename, hnum=hnum, alt_path=alt_path))
            except:
                info   = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
                cu2cm  = info['cu2cm']
                r200_cu = arr200[i_time]
                r200_cm = r200_cu*cu2cm

                # Yield (from namelist)
                if ('DC' in foldername(folder)) or ('KI' in foldername(folder)):
                    eta = 0.075
                elif 'KR' in foldername(folder):
                    eta = 0.1

                # Stars
                star_mass, star_mets, star_minit = ras.extract_stars(RamsesDir, timestep, ['star_mass', 'star_mets', 'star_minit'], factor=1, alt_path=alt_path)
                metal_masss = star_mass*(star_mets-Z_ave)

                # Gas
                mass, Z, rad_gas = ras.extract_cells(RamsesDir,timestep,['mass','Z','radius'],factor=1,alt_path=alt_path)
                metal_massg = mass*(Z-Z_ave)

                # Main quantities
                M_Z_star = np.sum(metal_masss)
                M_Z_ISM  = np.sum(metal_massg[(rad_gas   < 0.1*r200_cm)])
                M_Z_CGM  = np.sum(metal_massg[(rad_gas   > 0.1*r200_cm)])

                M_ej   = star_minit-star_mass           # M_ej = M_*,now - M_*,init
                # Z_ej    = star_mets + (1-star_mets)*eta # Z_ej = Z_* + (1-Z_*)eta
                Z_crea = (1-star_mets)*eta              # Z_crea = Z_ej - Z_* = Z_* + (1-Z_*)eta - Z_* = (1 - Z_*) eta
                M_Znew = np.sum(M_ej*Z_crea)            # M_Z_ejected = M_ej x Z_ej - M_ej x Z_* = M_ej x (Z_ej - Z_*) = M_ej x (Z_* + (1-Z_*)eta - Z_*) = M_ej (1 - Z_*) eta
                
                # Consistency check
                # sum_of_pies = M_Z_star+M_Z_ISM+M_Z_CGM+M_Z_out+M_Z_init
                # print(f'{foldername(folder)} \t   (sum metal pies)) {sum_of_pies*g2Msun:.4e} <=> {M_Z_now*g2Msun:.4e} (M_Z_now) \t Δ = {((sum_of_pies)-M_Z_now)/((sum_of_pies)+M_Z_now)}')

                #####################################################################################################################
                #####################################################################################################################   BUG COMPUTATION
                # Compute the mass of metals disappearing (within 20 Rvir should be a good approx)
                # Bstar_mass, Bstar_mets, Bstar_minit = ras.extract_stars(RamsesDir, timestep, ['star_mass', 'star_mets', 'star_minit'], factor=20, alt_path=alt_path)
                # Bmass, BZ = ras.extract_cells(RamsesDir,timestep,['mass','Z'],factor=20,alt_path=alt_path)
                # BM_Z_now  = (np.sum(Bmass*BZ) + np.sum(Bstar_mass*Bstar_mets))
                # BM_b_now  = (np.sum(Bmass)   + np.sum(Bstar_mass))
                # BM_Z_init = BM_b_now*Z_ave
                # BM_ej     = Bstar_minit-Bstar_mass      # M_ej = M_*,now - M_*,init
                # BZ_crea   = (1-Bstar_mets)*eta          # Z_crea = Z_ej - Z_* = Z_* + (1-Z_*)eta - Z_* = (1 - Z_*) eta
                # BM_Znew   = np.sum(BM_ej*BZ_crea)       # M_Z_ejected = M_ej x Z_ej - M_ej x Z_* = M_ej x (Z_ej - Z_*) = M_ej x (Z_* + (1-Z_*)eta - Z_*) = M_ej (1 - Z_*) eta
                # MZ_BUG    = BM_Z_init + BM_Znew - BM_Z_now
                # M_b_now  = (np.sum(mass)+np.sum(star_mass))
                # M_Z_init = M_b_now*Z_ave
                # MZ_BUG_sc =  M_b_now / (BM_b_now-M_b_now)
                # print(MZ_BUG_sc)
                # print(f'{folder}: {M_Znew*g2Msun:.4e} created, {MZ_BUG*g2Msun:.4e} Msun disappeared in 20 Rvir. ')
                # print(f'{foldername(folder)} \t (M_Z_init + M_Z_ej) {(M_Z_init+M_Znew)*g2Msun:.4e} <=> {(M_Z_now)*g2Msun:.4e} (M_Z_now) \t Δ = {2*((M_Z_init+M_Znew)-M_Z_now)/((M_Z_init+M_Znew)+M_Z_now)}')
                #####################################################################################################################
                #####################################################################################################################

                Z_ej   = eta + (1-eta)*star_mets        # Z_ej = Z_* + (1-Z_*)eta
                M_Z_ej = np.sum(M_ej*Z_ej)              # M_Z_ejected = M_ej x Z_ej - M_ej x Z_* = M_ej x (Z_ej - Z_*) = M_ej x (Z_* + (1-Z_*)eta - Z_*) = M_ej (1 - Z_*) eta
                M_Z_star = np.sum(star_mass*(star_mets))
                Miss_Z  = M_Znew - (M_Z_ISM + M_Z_CGM + M_Z_star)# - MZ_BUG

                # Save data in .h5 file.
                ras.add_prop(RamsesDir, timestep, 'M_Z_star', M_Z_star, filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'M_Z_ISM' ,  M_Z_ISM, filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'M_Z_CGM' ,  M_Z_CGM, filename, hnum=hnum, alt_path=alt_path)
                ras.add_prop(RamsesDir, timestep, 'Miss_Z'  ,   Miss_Z, filename, hnum=hnum, alt_path=alt_path)
                arr_M_Z_star.append(M_Z_star)
                arr_M_Z_ISM .append( M_Z_ISM)
                arr_M_Z_CGM .append( M_Z_CGM)
                arr_Miss_Z  .append(  Miss_Z)

        end_M_Zstars = np.sum(arr_M_Z_star)
        end_M_Z_ISM = np.sum(arr_M_Z_ISM)
        end_M_Z_CGM = np.sum(arr_M_Z_CGM)
        end_M_iss_Z = np.sum(arr_Miss_Z)

        colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
        labels = [r'$\rm M_{Z, stars}$', r'$\rm M_{Z, ISM}$', r'$\rm M_{Z, CGM}$', 'Missing\nmetals']
        sizes = np.array([end_M_Zstars, end_M_Z_ISM, end_M_Z_CGM, end_M_iss_Z])*g2Msun
        colors[2] = colors[3] # red
        colors[3] = 'whitesmoke'
        datemp = labels, sizes, colors, folder
        datas.append(datemp)
    return datas


def plot_pie(datas, savefig=False, rotation_angle=0):
    """ Plots pie charts with a nice fancy legend. if you don't like it, comment the "Labels" section off. """
    fig, axes = plt.subplots(1, len(datas), figsize=(12, 5))
    if len(datas)==1:
        axes = [axes]
    fig.patch.set_facecolor('white')  # White background
    for i_fold, data in enumerate(datas):
        # Plot
        labels, sizes, colors, folder = data
        wedges, _, _ = axes[i_fold].pie(sizes, explode=[0.02] * len(sizes), autopct=lambda pct: f'{pct:.1f}%' if pct >= 0.5 else '', 
                                        radius=1, colors=colors, textprops={'fontsize': 15}, startangle=rotation_angle)  # %1.1f%%   '%.1f%%'   # f'{pct:.1f}%' if pct >= 0.5 else ''
        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        kw = dict(arrowprops=dict(arrowstyle="-"), bbox=bbox_props, zorder=0, va="center")

        # Labels
        if i_fold == 0:
            wedge_centers = [wedge.center for wedge in wedges]
            wedge_angles = [(wedge.theta1, wedge.theta2) for wedge in wedges]
            left_text_position = (-1.8, 0.5)
            line_spacing = 0.2

            for i, (center, label, angles, wedge_color) in enumerate(zip(wedge_centers, labels, wedge_angles, colors)):
                start_angle, end_angle = angles
                if np.sin(np.deg2rad((start_angle + end_angle) / 2)) > 0:
                    angle_legend = end_angle - 5    # top half points to end of wedge
                else:
                    angle_legend = start_angle + 5  # bottom half points to start of wedge
                leg_color  = 'black' if wedge_color == 'whitesmoke' else wedge_color
                text_start = (left_text_position[0] + 0.5, left_text_position[1] - (i * line_spacing)*1.4+0.07)
                line_start = (left_text_position[0] + 0.2, left_text_position[1] - (i * line_spacing))
                line_end   = (center[0] + np.cos(np.deg2rad(angle_legend)) - 0.03, center[1] + np.sin(np.deg2rad(angle_legend)))
                axes[i_fold].text(text_start[0], text_start[1], label, ha='right', va='center', fontsize=14, color=leg_color)   # text
                axes[i_fold].plot([line_start[0], line_end[0]], [line_start[1], line_end[1]], color=leg_color)                  # line
    plt.tight_layout()

    if savefig:
        if savefig == True:
            fig.savefig(f'pie_Rvir.pdf', bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/pie_Rvir.pdf', bbox_inches='tight')


###################################################################################################################################
############################################################## Ions ##############################################################
###################################################################################################################################
def ion_fraction(genpath, folders, timesteps, elem_str='O', r200min=0.3, r200max=2, factor=2, err=False, savefig=False, hnum=None, alt_path=None):
    """Computes the ionisation fraction of several ionisation states for elem_fraction."""
    # List of all ions you computed +1.
    ions_dict = {'C': ['CI','CII','CIII','CIV','CV', 'CVI'],
             'O': ['OI','OII','OIII','OIV','OV','OVI','OVII','OVIII','OIX'],
             'Mg': ['MgI','MgII','MgIII', 'MgIV']}

    try:
        str_ions = ions_dict[elem_str]
        abund = abund_dict[elem_str]
    except KeyError:
        print('Nupe. Choose either C, O, or Mg.')

    if isinstance(timesteps,float) or isinstance(timesteps,int):
        timesteps = [timesteps]
    len_fold = len(folders)
    N_atom_tot = np.zeros(len_fold)
    bottom, xaxis = np.zeros(len_fold), np.arange(len_fold)
    # Compute
    elem_fraction = np.zeros((len(str_ions),len_fold,len(timesteps)))  # [ion, folder, timesstep]
    for indfold, folder in enumerate(folders):
        RamsesDir = genpath+folder

        # Create file to store data.
        filepath = f'{RamsesDir}/ratadat/'
        if not os.path.exists(filepath):
            os.mkdir(filepath)
        filename = f'{filepath}frac_ion_within_xxrvir.h5'
        if os.path.exists(filename):
            file = h5py.File(filename,'r+')
        else:
            file = h5py.File(filename,'w')

        for istep, timestep in enumerate(timesteps):
            basename = f'frac{elem_str}{len(str_ions)}_{timestep:05d}_{r200min}_{r200max}'
            try:
                elem_fraction[:,indfold,istep] = file[basename][:]
                continue
            except KeyError:
                pass
            # Else, compute it    
            print(f'\t Computing... \t {folder}: timestep {timestep}', end='\r')
            norm_r, N_atom = ras.extract_cells(RamsesDir,timestep,['radius','N_'+elem_str],factor=factor,alt_path=alt_path)
            # Compute norm to select CGM cells and not have OI overtaking all other fractions
            info  = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
            cu2cm = info['cu2cm']
            r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
            # Compute ion fractions
            sel = (norm_r>r200min*r200_cm) & (norm_r<r200max*r200_cm)  # Restrict within 0.1 R200 and r200max*r200.
            N_atom_tot[indfold] = np.sum(N_atom[sel])
            for indion, str_ion in enumerate(str_ions[:-1]):
                N_ion_tot = np.sum(ras.extract_cells(RamsesDir,timestep,['N_'+str_ion],factor=factor)[sel],alt_path=alt_path)  # [Nb of atoms]
                elem_fraction[indion,indfold,istep] = N_ion_tot / N_atom_tot[indfold]
            elem_fraction[-1,indfold,:] = 1 - np.sum(elem_fraction[:-1,indfold,:], axis=0) # Compute remaining ion(s) fraction.
            file.create_dataset(basename,data=elem_fraction[:,indfold,istep])

    # Plot
    frac_stack = np.median(elem_fraction,axis=2) # If avg instead of median, give totals higher than 1.
    if err:
        yerr_arr = np.array([frac_stack-np.percentile(elem_fraction,perc_min, axis=2), np.percentile(elem_fraction,perc_max, axis=2)-frac_stack]) # Added or substracted value from main value.
    fig, ax = plt.subplots()
    for indion2, frac_elem in enumerate(frac_stack):
        if err=='OVI':
            if indion2==5:
                yerr=yerr_arr[:, indion2, :]
                error_kw=dict(lw=1, capsize=5, capthick=1)
            else:
                yerr=None
                error_kw=dict(lw=2, capsize=5, capthick=1.5)
        elif err:
            yerr=yerr_arr[:, indion2, :]
            error_kw=dict(lw=2, capsize=5, capthick=1.5)
        else:
            yerr= None
            error_kw=None
        lab = str_ions[indion2]
        if str_ions[indion2]==str_ions[-1]:
            lab = r'$\geq$'+lab
        rects = ax.bar(xaxis, frac_elem, yerr=yerr, bottom=bottom, label=lab, error_kw=error_kw)
        bottom += np.array(frac_elem)
        # Add percentage on bars.
        for indfold, rect in enumerate(rects):
            height = np.round(float(rect.get_height()*100), 1)
            if (height>5) or (indion2==5):          # Write fraction for OVI and those higher than 0.05.
                xloc = xaxis[indfold]
                yloc = rect.get_y() + rect.get_height() / 2
                if yloc < 0.127:                    # Avoid text being behind legend.
                    yloc=0.1277
                if (err=='OVI') and (indion2==5):   # If errorbar for OVI, move the text slightly up.
                    yloc+=0.02
                ax.annotate(str(height)+' %',xy=(xloc,yloc),xytext=(xloc,yloc),textcoords="offset points",ha='center',va='center',color='k',clip_on=True)
    plt.ylabel('Ion mass fraction')
    plt.xticks(ticks=xaxis, labels=[foldername(folder) for folder in folders])
    plt.legend(loc="lower center", ncol=int(len(str_ions)/2+1), fontsize='small')
    plt.tight_layout()
    plt.show()

    if savefig:
        if savefig==True:
            fig.savefig(f'{str_ions[0][:-1]}_fraction.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/{str_ions[0][:-1]}_fraction.pdf',bbox_inches='tight')


###################################################################################################################################
############################################################## M*-Mh ##############################################################
###################################################################################################################################
def azimuth_frac(RamsesDir, timestep, zoom=1, var_str='MgII', alphabin=5, proj='x', lmax=None, savefig=False, hnum=None, alt_path=None):
    """
    Plots fraction of cells against angle. Inspired by Schroetter et al. 2019 and Zabl et al. 2019.
    """
    if zoom>=1: # Does not look up to date.
        factor=1
    else:
        factor=int(1/zoom)
        print('factor is ', factor)

    info   = ras.read_info(RamsesDir, timestep, alt_path=alt_path)
    cu2cm  = info['cu2cm']
    cu2kpc =  cu2cm / kpc2cm
    center     , rad      = ras.get_frad(RamsesDir,timestep,factor,hnum=hnum)
    center_plot, rad_plot = [cen*cu2cm for cen in center], rad*cu2kpc/zoom

    qty, cell_l, cell_pos = ras.extract_cells(RamsesDir,timestep,[var_str,'cell_l','cell_pos'],factor=factor,alt_path=alt_path)

    # convert in cu ∈ [0,1] because make_map wants it
    cell_pos_cu    = cell_pos / cu2cm
    center_plot_cu = [cen/cu2kpc for cen in center_plot]
    rad_plot_cu    = rad_plot / cu2kpc
    
    if lmax==None:
        lmax = cell_l.max()

    # Axis along which the data is projected. The variables xloc, yloc and zloc are not the 'real' x, y, z anymore.
    # BEWAAAAAAAAAAAAAARE, HERE PROJ SHOULD BE EDGE-ON !
    if proj=='z':
        xloc, yloc, zloc = ras.read_pos(cell_pos_cu)                                        # cu ∈ [0,1]    # TODO: add in extract_cells?
        xmin, xmax, ymin, ymax, zmin, zmax = ras.compute_edges(center_plot_cu, rad_plot_cu) # cu ∈ [0,1]
        xmin_c, xmax_c, ymin_c, ymax_c = xmin-center_plot_cu[0], xmax-center_plot_cu[0], ymin-center_plot_cu[1], ymax-center_plot_cu[1]
    elif proj=='y':
        xloc, zloc, yloc = ras.read_pos(cell_pos_cu)                                        # cu ∈ [0,1]
        xmin, xmax, zmin, zmax, ymin, ymax = ras.compute_edges(center_plot_cu, rad_plot_cu) # cu ∈ [0,1]
        xmin_c, xmax_c, ymin_c, ymax_c = xmin-center_plot_cu[0], xmax-center_plot_cu[0], ymin-center_plot_cu[2], ymax-center_plot_cu[2]
    elif proj=='x':
        zloc, yloc, xloc = ras.read_pos(cell_pos_cu)                                        # cu ∈ [0,1]
        zmin, zmax, ymin, ymax, xmin, xmax = ras.compute_edges(center_plot_cu, rad_plot_cu) # cu ∈ [0,1]
        xmin_c, xmax_c, ymin_c, ymax_c = xmin-center_plot_cu[2], xmax-center_plot_cu[2], ymin-center_plot_cu[1], ymax-center_plot_cu[1]
    else:
        raise ValueError(f"{proj} is not a valid projection axis. \n Pick 'x', 'y' or 'z'.")
    nx,ny = cu.py_cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)                         # bins at res lmax.
    nb_pix = min(nx,ny)
    map2plot,_ = cu.py_cell_utils.make_map_new(lmax,False,xmin,xmax,ymin,ymax,zmin,zmax,\
        qty,np.ones_like(qty),xloc,yloc,zloc,cell_l,nb_pix,nb_pix)                          # False -> weight

    column_factor = (zmax-zmin)*cu2cm      # -> ergs s-1 cm-2
    map2plot = map2plot*column_factor

    x_lin = np.linspace(xmin_c, xmax_c, nb_pix)
    y_lin = np.linspace(ymin_c, ymax_c, nb_pix)
    x, y = np.meshgrid(x_lin, y_lin)
    r = np.sqrt(x**2+y**2)
    angle = 90 - abs(np.arcsin(x/r)*180/np.pi) # Angle from the major axis.
    fig, ax = plt.subplots(nrows=1, ncols=1)
    for thresh in [1e11,1e12,1e13,1e14,1e15]:
        fractions, cone_angle = [], []
        prev_angle = 0
        for coneangle_le_barbare in range(0+alphabin,90+alphabin,alphabin):
            cone_angle.append(coneangle_le_barbare)
            cellcone = map2plot[(prev_angle<angle) & (angle<coneangle_le_barbare)] # Array is now 1D.
            fractions.append(len(cellcone[cellcone>thresh]) / (len(cellcone)))
            prev_angle = coneangle_le_barbare
        ax.bar(*(cone_angle,fractions), width=-alphabin, label=f'N > {thresh:.0e} MgII.cm$^{{-2}}$', align="edge", alpha=0.5)
    ax.set_xlabel('Azimuthal angle distribution $\\alpha$ [degrees]')
    ax.set_ylabel('Fraction of cells above threshold')
    ax.set_xlim([0,90])
    ax.legend(frameon=False, bbox_to_anchor=(0,1,1,0), loc="lower left", ncol=3, fontsize='small')

    if savefig:
        if savefig==True:
            fig.savefig(f'Biconality_{foldername(RamsesDir)}_{timestep}.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/Biconality_{foldername(RamsesDir)}_{timestep}.pdf',bbox_inches='tight')


###################################################################################################################################
########################################################### Output time ###########################################################
###################################################################################################################################
def compute_runtime(folder, is_cosmo):
    """ Return the runtime and simutime from the log files. Format of logs should be log[something], with [something] being an incremented value. """
    log_files = sorted([os.path.join(folder, filename) for filename in os.listdir(folder) if filename.startswith('log') and os.path.isfile(os.path.join(folder, filename))])
    time_str = 'a=' if is_cosmo else 't='

    # Find all the initial aexp of the logs to truncate the previous ones
    # (sometimes the restart is slightly before the end of the previosus logfile)
    pattern = re.compile(r'Fine step', re.IGNORECASE)
    a_init = []     # List of the restart times for each log.
    log_bug = []    # List of logs which had problems (i.e. ignored)
    for filename in log_files:
        with open(filename, 'r') as file:
            found_line = None
            for line in file:
                if re.search(pattern, line):
                    found_line = line.strip()
                    break
            try:
                a_init.append(float(found_line.split()[found_line.split().index(time_str) + 1]))
            except:     # Ignore files without simulation ongoing...
                log_bug.append(filename)
                # print(f"{filename} does not contain any {time_str}, ignoring it.")
    for log in log_bug: # ...and remove them from the list
        log_files.remove(log)
    a_init.append(1) if is_cosmo else a_init.append(40) # Limit to stop the code. No aexp>1, and 35*cu ~ 520 Myr.

    # Initialisation
    runt = [0]
    aexp = [a_init[0]]
    indent = 0

    # Find the runtime and corresponding aexp
    for i_file, filename in enumerate(log_files):
        a_lim = a_init[i_file+1]                # Limit to the first aexp of the next log file
        flag_time = False                       # Flag to check if the code contains timestamps
        code_caca = True                        # Flag to get the aexp right after the total running time.
        with open(filename, 'r') as file:
            for line in file:
                if 'Total running time' in line:
                    code_caca, flag_time = False, True                
                    run_temp = float(line.split()[-2])
                if flag_time and 'Fine step' in line:
                    flag_time = False
                    a_temp = float(line.split()[line.split().index(time_str) + 1])
                    if a_temp>a_lim:
                        break
                    runt.append(run_temp+indent)
                    aexp.append(a_temp)
        if code_caca:   # Slower version, but works for all log files.
            with open(filename, 'r') as file:
                run_temp = 0
                for line in file:
                    if 'Time elapsed since last coarse step' in line:
                        flag_time = True
                        run_temp += float(line.split()[line.split().index('step:') + 1])
                    if flag_time and 'Fine step' in line:
                        flag_time = False
                        a_temp = float(line.split()[line.split().index(time_str) + 1])
                        if a_temp>a_lim:
                            break
                        runt.append(run_temp+indent)
                        aexp.append(a_temp)
        indent = runt[-1]
    return runt, aexp


def plot_runtime_estimate(genpath,folders,fmt='days',xlim=None,ylim=None):
    """ Plot runtime vs simutime using the log files. Format of logs should be log[something], with [something] being an incremented value. """
    _, ax = plt.subplots(figsize=(8,6))
    for folder in folders:
        RamsesDir = genpath + folder
        try:
            info = ras.read_info(RamsesDir, 1)
        except:
            output_dirs = [d for d in os.listdir(RamsesDir) if d.startswith('output_')]
            smallest_number = min(int(d.split('_')[1]) for d in output_dirs)
            info = ras.read_info(RamsesDir, smallest_number)
        # Read times
        runtime, simutime = compute_runtime(RamsesDir, info['is_cosmo'])              # Have to adapt it to read non_cosmo
        runtime = np.array(runtime)
        if info['is_cosmo']:
            cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
            simutime = [cosmo.age(1/simutime2-1).value for simutime2 in simutime]   # [Gyr]
        else:
            simutime = np.array(simutime)*info['unit_t']/365.25/24/3600/1e6         # [Myr]
        # else: # Rely on Marion's function
        #     a=np.loadtxt(genpath + folder + "/runtime.txt", skiprows=0)
        #     runtime = np.cumsum(a[:,1])
        #     runtime = np.insert(runtime,0,0)
        #     simutime = a[:,0]
        #     simutime = np.insert(simutime,0,0)
        #     simutime = simutime*info['unit_t']/1e6/365.25/24/3600/1e3 # [Gyr]

        # Convert in required units
        if fmt == 'sec':
            runtime = runtime
        elif fmt == 'min':
            runtime = runtime / 60
        elif fmt == 'hours':
            runtime = runtime / 3600
        elif fmt == 'days':
            runtime = runtime / 3600 / 24
        elif fmt == 'months':
            runtime = runtime / 3600 / 24 / 30
        elif fmt == 'years':
            runtime = runtime / 3600 / 24 / 30 / 12
        elif fmt == 'hCPU':
            runtime = runtime / 3600 * info['ncpu']
        elif fmt == 'MhCPU':
            runtime = runtime / 3600 * info['ncpu'] / 1e6
        else:
            raise ValueError(f"The format {fmt} is not accepted. Use 'sec', 'min', 'hours', 'days', 'months', 'years', 'hCPU' or 'MhCPU'")
        ax.plot(simutime, runtime, label=foldername(folder))
        print(f'{runtime[-1]:.1f} {fmt} \t t = {simutime[-1]:.2e} Gyr \t {foldername(folder)}')

    ax.legend(frameon=False)
    ax.set_xlabel("Time [Gyr]") if info['is_cosmo'] else ax.set_xlabel("Time [Myr]")
    ax.set_ylabel(f'Runtime [{fmt}]')
    ax.set_ylim(ylim)
    ax.set_xlim(xlim)
    if info['is_cosmo']:
        add_z_top_axis(ax, cosmo)
    plt.show()


###################################################################################################################################
############################################################## Utils ##############################################################
###################################################################################################################################
def plot_dexter(ion, path2files='data/Ions/', ax=None, nolab=False):
    """
    Plots all the data saved in path2files for the chosen ion (ex 'MgII').
    """
    if ax==None:
        _, ax = plt.subplots(nrows=1, ncols=1)
        ax.set_yscale('log')
        ax.set_xscale('log')

    if ion=='H': # For plots with N_Mg.
        ion='HI'

    for fileuh in sorted(os.listdir(path2files)):
        if ion in fileuh:
            dat = np.loadtxt(path2files+fileuh, usecols=(0,1,2,3,4,5), dtype=str)
            impact_param = dat[:,0].astype(np.float32)
            xerr         = dat[:,1].astype(np.float32)
            dattype      = dat[:,2]
            coldens      = dat[:,3].astype(np.float32)
            yerrm        = np.array([float(errstr[2:]) for errstr in dat[:,4]])
            yerrp        = dat[:,5].astype(np.float32)

            liminf  = dattype=='>'
            limsup  = dattype=='<'
            nonlim  = (dattype!='<') & (dattype!='>')

            bluh, = ax.plot(np.nan)
            str_dat = fileuh[:-4].split('_')[1]
            col = get_col_pap(str_dat)
            if nolab:
                str_dat = ''
            ax.errorbar(impact_param[nonlim], coldens[nonlim], yerr=[yerrm[nonlim], yerrp[nonlim]], fmt='o', capsize=5, color=col, label=str_dat)
            for indix, TruFal in enumerate(limsup):
                if TruFal:
                    ax.errorbar(impact_param[indix], coldens[indix], yerr=0, uplims=True, color=col, marker="|", markersize=12)
            for indix, TruFal in enumerate(liminf):
                if TruFal:
                    ax.errorbar(impact_param[indix], coldens[indix], yerr=0, lolims=True, color=col, capsize=4, marker="|", markersize=12)


def plot_data(ion, path2files='data/Ions2/', ax=None, nolab=False, logx=True):
    """
    Plots all the data saved in path2files for the chosen ion (ex 'MgII'). Similar to plot_dexter but for different data saving format ^^.
    """
    if ax==None:
        _, ax = plt.subplots(nrows=1, ncols=1)
        ax.set_yscale('log')
        if logx:
            ax.set_xscale('log')

    for fileuh in sorted(os.listdir(path2files)):
        if ion in fileuh:
            str_dat = fileuh[:-4].split('_')[1]
            dat = np.loadtxt(path2files+fileuh, usecols=(0,1,2,3,4,5,6), dtype=str)

            impact_param = dat[:,0].astype(np.float32)
            xerr         = dat[:,1].astype(np.float32)
            dattype      = dat[:,2]
            coldens      = dat[:,3].astype(np.float32)
            yerr_log     = dat[:,4].astype(np.float32)
            redshift     = dat[:,5].astype(np.float32) # If want to make selection at some point or size the points.
            galmasslum   = dat[:,6].astype(np.float32) # mass or luminosity depending on dat file.

            liminf  = dattype=='>'
            limsup  = dattype=='<'
            nonlim  = (dattype!='<') & (dattype!='>')

            bluh, = ax.plot(np.nan)
            col = get_col_pap(str_dat)
            if nolab:
                str_dat = ''
            yerr = [10**coldens[nonlim]*(1-10**-yerr_log[nonlim]), 10**coldens[nonlim]*(10**yerr_log[nonlim]-1)]
            ax.errorbar(impact_param[nonlim], 10**coldens[nonlim], yerr=yerr, xerr=xerr[nonlim], fmt='o', capsize=5, color=col, label=str_dat)
            for indix, TruFal in enumerate(limsup):
                if TruFal:
                    ax.errorbar(impact_param[indix], 10**coldens[indix], yerr=0, uplims=True, color=col, capsize=4, marker="|", markersize=12)
            for indix, TruFal in enumerate(liminf):
                if TruFal:
                    ax.errorbar(impact_param[indix], 10**coldens[indix], yerr=0, lolims=True, color=col, capsize=4, marker="|", markersize=12)


def get_col_pap(str_dat):
    colors = ['k', 'red', 'darkorchid', 'fuchsia', 'deepskyblue', 'darkblue', 'grey', 'brown', 'mediumblue'] # 'lightgray', 'peru', 'darkgreen'
    papers = ['Hummels 2013', 'Danforth & Shull 2008', 'Prochaska et al. 2017', 'Prochaska et al. 2011', 'Werk et al. 2013', 'Tumlinson et al. 2011', 'Bordoloi et al. 2014', 'Johnson et al. 2015', 'Wilde et al. 2021 > 1e9.9']

    return colors[papers.index(str_dat)]


def runtime(RamsesDir, logstr='logs'):
    """
    Routine that returns the list of expansion factor and runtime for each output and restart: aexp_log, output_times, aexp_restart, restart_times
    This routine only works if you create a new log for each restart and that it is called whatever (or whatever1), whatever2, whatever3, etc.

    Parameters:
    -----------
        logstr: str
            String befor the log number.

    Example:
    --------
        RamsesDir = '/path/to/simulation/' 
        aexp_log, output_times, aexp_restart, restart_times = runtime(RamsesDir)
    """
    # This value should increase with resolution if RT due to the lines "Performed level xx RT-step with [...]"
    line_history= deque(maxlen=22)
    check_var   = 0
    aexp_out_list = []
    restart_times, aexp_restart = [], []
    output_times, aexp_log = [], []

    # Extract the list of aexp from the outputs.
    last_out_nb = max([int(output_arr[7:]) for output_arr in os.listdir(RamsesDir) if 'output' in output_arr])
    for out_nb in range(last_out_nb):
        with open(RamsesDir+f'/output_{out_nb+1:05d}/info_{out_nb+1:05d}.txt') as out_file:
            for line in out_file:
                if 'aexp' in line:
                    aexp_out_list.append(float(f"{float(line.split('=')[1]):.3E}"))  # /!\ Heh, gotta improve that x). Need it rounded otherwise the following doesn't work.
    aexp2match = max(aexp_out_list)
    aexp_out_list = np.array(aexp_out_list)
                    
    # List of the log numbers, can begin with just 'logs' instead of 'logs1'.
    log_nb_list = ([log_arr[len(logstr):] for log_arr in os.listdir(RamsesDir) if logstr in log_arr])
    log_nb_list.sort()

    # Go through the log files in reversed order
    for log_nb in reversed(log_nb_list):
        aexp_now, run_now = 0, 0    # Values for the current line.
        exit_flag         = False   # Used when the last aexp of the log is reached.
        begfil            = True    # Beginning of the file, avoids bug if there are two runs in the same log..
        
        with open(RamsesDir+f'/{logstr}{log_nb}') as log_file:
            for log_line in log_file:
                # Find the restart aexp for the next log in loop (previous in real time since reversed).
                # One of the following 'if' necessarily happens in a log file and only once at the beginning.
                if "Stellar RT turned on at a=" in log_line:
                    if begfil==False:   # Not sure this part is perfect, done quickly to debug when there are two runs in a single log file.
                        restart_times.append(run_now)
                        aexp_restart.append(aexp_now)
                        aexp2match = aexp_next
                        aexp_now, run_now = 0, 0
                        exit_flag = False
                    aexp_next = float(log_line.split(' ')[8])
                    temparray = aexp_out_list[(aexp_next<=aexp_out_list) & (aexp_out_list<=aexp2match)]
                    if len(temparray)==0:
                        print(f'No output for {logstr+log_nb} yet')
                        break
                    aexp_out = temparray[0]
                    aexp_prev = aexp_next
                    begfil = False
                # This is the beginning of the first log file: should be zero.
                if "aexp= " in log_line:
                    aexp_next = float(log_line.split(' ')[2])
                    run_prev = 0
                    temparray = aexp_out_list[(aexp_next<=aexp_out_list) & (aexp_out_list<=aexp2match)]
                    if len(temparray)==0:
                        print(f'\t No output for {logstr+log_nb} yet')
                        break
                    aexp_out = temparray[0]
                    aexp_prev = aexp_next

                # Find output and restart_times aexp and extract corresponding running time
                if "running time:" in log_line:
                    for sublog_line in line_history:
                        # Find aexp in log while avoid non-floats.
                        if ('a=' in sublog_line):
                            try:
                                aexp_now = float(sublog_line.split(' ')[-4])
                            except ValueError:
                                continue
                            run_now = float(log_line.split('   ')[1])

                            ############### outputs ###############
                            # If before aexp: just store value
                            if aexp_now <= aexp_out:
                                aexp_prev = aexp_now
                                run_prev = run_now
                            # If after: take the closest value to the output aexp between the current and previous aexp.
                            else:
                                if abs(aexp_now-aexp_out) <= abs(aexp_out-aexp_prev):
                                    output_times.append(run_now)
                                    aexp_log.append(aexp_now)
                                    check_var += 1
                                else:
                                    output_times.append(run_prev)
                                    aexp_log.append(aexp_prev)
                                try:
                                    aexp_out = min(temparray[temparray>aexp_out])
                                except ValueError:
                                    # If have gone through all outputs within this log -> take unmatchable aexp_out for this log.
                                    aexp_out = 1

                            ############### restart ###############
                            # Break and go to next log when found. Might improve this and include it into previous if condition.
                            if aexp_now >= aexp2match:
                                if abs(aexp_now-aexp2match) < abs(aexp2match-aexp_prev):
                                    restart_times.append(run_now)
                                    aexp_restart.append(aexp_now)
                                else:
                                    restart_times.append(run_prev)
                                    aexp_restart.append(aexp_prev)
                                exit_flag = True
                                break
                    line_history.clear()
                    if exit_flag:
                        aexp2match = float(f"{aexp_next:.3E}") 
                        break
                else:
                    line_history.append(log_line)
    output_times    = np.array(output_times)/3600
    aexp_log   = np.array(aexp_log)
    restart_times   = np.array(restart_times)/3600
    aexp_restart    = np.array(aexp_restart)

    # Correct output times by restart_times time.
    for ind, rest in enumerate(restart_times):
        output_times[(aexp_log>aexp_restart[ind])] += rest      # Have to find a way to ignore NaN or re-think this loop...
        aexp_log[(aexp_log==aexp_restart[ind])] = np.nan        # Can't directly remove them otherwise the index don't correspond anymore.
    # Remove all NaN from both lists which were created in the line above (there's gotta be a smarter way)
    output_times = output_times[~np.isnan(aexp_log)]
    aexp_log = aexp_log[~np.isnan(aexp_log)]
    
    # Correct restart_times by previous restart_timess
    tot_run = 0
    for ind, rest in enumerate(reversed(restart_times)):
        tot_run += rest
        restart_times[len(restart_times)-1-ind] = tot_run
    
    output_times = np.concatenate((output_times, restart_times))
    aexp_log = np.concatenate((aexp_log, aexp_restart))

    # sort them
    idx   = np.argsort(aexp_log)
    aexp_log = np.array(aexp_log)[idx]
    output_times = np.array(output_times)[idx]
    idx2   = np.argsort(aexp_restart)
    aexp_restart = np.array(aexp_restart)[idx2]
    restart_times = np.array(restart_times)[idx2]

#     Uncomment this to check for bugs.
#     print(' /-----------------------------------------\\')
#     print('|------------------B-U-G-S------------------|')
#     print(' \-----------------------------------------/')
#     aexp_log.sort()
#     print(f'There should be {check_var} "False" in the list \n\t{aexp_log==aexp_out_list}.\n\n')
#     plt.scatter(aexp_restart, restart_times, marker='+', s=400)
#     plt.scatter(aexp_log, output_times)

    return aexp_log, output_times, aexp_restart, restart_times

def plot_runtime(genpath, folders, logstr='logs', fmt='h', xlims=None, ylims=None):
    """
    Plots the runtime of a set of simulations.
    H0 and omega_m are hardcoded.

    Parameters:
    -----------
        logstr: str
            String before the log number.
        fmt: 'h' or 'd'
            Specify if you want the runtime in hours or days.

    Example:
    --------
        genpath = '/path/to/some/folder/'
        folders = ['simu1/', 'simu2/', 'simu3/']
        plot_runtime(genpath, folders, fmt='d', xlims=None, ylims=None, logstr='logs')
    """
    info  = ras.read_info(genpath+folders[0], 1)
    cosmo = FlatLambdaCDM(H0=info['H0'],Om0=info['omega_m'])
    _, ax = plt.subplots(nrows=1, ncols=1)
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color'] 

    for ind_f, folder in enumerate(folders):
        RamsesDir = genpath+folder
        aexp_output, output_times, aexp_restart, restart_times = runtime(RamsesDir, logstr=logstr)
        gyr_output  = cosmo.age(1/aexp_output - 1)
        gyr_restart = cosmo.age(1/aexp_restart - 1 )  

        if fmt=='h':
            ax.plot(gyr_output, output_times, color=colors[ind_f], label=foldername(folder))
            ax.scatter(gyr_restart, restart_times, color=colors[ind_f], marker='+', s=150)
        else:
            ax.plot(gyr_output, output_times/24, color=colors[ind_f], label=foldername(folder))
            ax.scatter(gyr_restart, restart_times/24, color=colors[ind_f], marker='+', s=150)

    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ax.set_xlabel('Time [Gyr]')
    if fmt=='h':
        ax.set_ylabel('Runtime [hours]')
    else:
        ax.set_ylabel('Runtime [days]')
    add_z_top_axis(ax, cosmo)
    ax.legend()

    return

def strongscaling(path2file, ncpus=[128,256,384,448], mincoarse=100):
    """
    Estimates how the simulation time improves with more cpu by comparing how long the coarse timesteps take (important thing: the coarse timesteps are equivalent in the simulation)
    You should name the files filename128, filename256, filename384, etc.. In that example, use path2file='filename' and ncpus=[128,256,384].
    mincoarse is used to avoid the first coarse steps when averaging as they are shorter.
    """
    coarsetime = []
    for ncpu in ncpus:
        file_name = path2file+str(ncpu)+'/logs'
        temp_tcoarse, coarsenb = [], []
        with open(file_name, 'r') as read_obj:
            for line in read_obj:
                if 'Main' in line:
                    coarsenb.append(int(line[line.find('    '):line.rfind('mcons')]))
                if 'Time elapsed since last coarse step' in line:
                    temp_tcoarse.append(float(line[line.find(':')+1:line.rfind('s  ')]))
            coarsenb = np.array(coarsenb)
            temp_tcoarse = np.array(temp_tcoarse)
            if ncpu==min(ncpus):
                maxcoarse=max(coarsenb)
            coarsetime.append(np.average(temp_tcoarse[(coarsenb>mincoarse)&(coarsenb<maxcoarse)]))
    ncpus = np.array(ncpus)
    coarsetime = np.array(coarsetime)

    speedup = coarsetime[np.where(ncpus==min(ncpus))] / coarsetime # speedup = t_min / t_coarse
    plt.plot(ncpus/min(ncpus), speedup, marker='x', label='$t_{coarse,min} / t_{coarse}$')
    yisx = np.linspace(1,max(ncpus)/min(ncpus),3)
    plt.plot(yisx, yisx, c='k')
    plt.legend()
    plt.xlabel('$n_{cpu} / min(n_{cpu})$')

def axis_format_10pow(value, tick_number, int_only=True): # The one that supposedly works. Have to check though.
    # Make it s.t. one can choose between nothing and 3.6 (dec or only int)
    """
    From Marion Farcy, improved by Maxime Rey
    Replace plot tick labels by powers of 10. If the tick is '2' for example, returns '10²'.
    This version writes the values of the non integers instead to using them as exponents. 
    It is usefull is the data used is already in logarithmic scale.

    Parameters:
    -----------
        Don't try to understand the parameters... just use them as shown afterwards.
    
    Example:
    --------
        ax.xaxis.set_major_formatter(plt.FuncFormatter(axis_format_10pow))
        ax.yaxis.set_major_formatter(plt.FuncFormatter(axis_format_10pow))
    """
    val=''
    sign = np.sign(int(value))
    # nb_dec = int(str(value)[::-1].find('.'))
    dec = abs(value)%1 
    
    if sign==0:
        if value%1==0:
            val = r'$10^0$'
        if not int_only:
            print(10**dec)
            if value >0:
                val = str(int(10**dec))
            else:
                val = '0.' + str(int(10**dec))
    else:
        # If not integer exponent
        if (value%1!=0 and not int_only):
            if value>0:
                val = str(int(10**dec)) + '.'
            else:
                val = '.' + str(int(10**dec)) + '.'
            if sign==-1:
                val += r'$10^{-}$'
            else:
                val += r'$10$'
            for v in str(abs(int(value))):
                val = val + fr'$^{v}$'
        elif value%1==0:
            if sign==-1:
                val += r'$10^{-}$'
            else:
                val += r'$10$'
            for v in str(abs(int(value))):
                val = val + fr'$^{v}$'
    return val


def add_z_top_axis(ax, cosmo):
    """ Adds an x axis at the top of the figure with redhsifts. """
    age_min, age_max = ax.get_xlim()  # [Gyr]
    new_tick_redshift = (10,7,6,5,4,3,2,1.5,1,0) if age_min < 4 else (1.5,1.4,1.3,1.2,1.1,1,0.9,0.8,0.7,0.6,0.5,0)
    new_tick_locations = []
    new_tick_labels = []
    for zz in new_tick_redshift:
        age_redsh = cosmo.age(zz).value
        if age_min <= age_redsh <= age_max:
            new_tick_locations.append(age_redsh)
            new_tick_labels.append(f"{zz:.0f}" if zz == round(zz) else f"{zz:.1f}")
    ax2 = ax.twiny()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_xticks(new_tick_locations)
    ax2.set_xticklabels(new_tick_labels)
    ax2.set_xlabel('Redshift')
    ax2.minorticks_off()


def get_label(some_str):
    ax_labels = {
        # T and rho
        ('T', 'temperature', 'temp'): 'T [K]',
        ('rho', 'density', 'dens'): r'$\rho$ [g cm$^{-3}$]',
        ('nH', 'H'): r'n$\rm_{H}\ [cm^{-3}]$',
        'N_HI':      r'$\rm N_{{HI}}\ {{[cm^{{-2}}]}}$',
        # Others
        'pres':       'P',
        'mass':       r'M$_\mathrm{gas}$ [M$_\odot$]',
        ('M_Z', 'Z'): 'Z', 
        'c_length':   r'$\rm\lambda_{cool}$ [??? CHECK CODE !!!]',
        # Velocities
        'M_totflow_ide': r'Flow rate [$M_\odot/yr$]',
        'M_outflow_ide': r'Outflow rate [$M_\odot/yr$]',
        'norm_v': r'V$\rm_r\ [km\,s^{-1}]$',
        'vz':     r'V$\rm_z\ [km\,s^{-1}]$',
        # Star stuff
        'star_mass':  r'Stellar mass [M$_\odot$]',
        'star_minit': r'Stellar mass [M$_\odot$]',
        'star_age':   'Stellar age [Myr]',
        'star_mets':  r'Stars metallicity [Z$_\odot$]',
    }
    ax_labels_flat = {key: label for keys, label in ax_labels.items() for key in (keys if isinstance(keys, tuple) else (keys,))}
    if some_str in ax_labels_flat:
        return ax_labels_flat[some_str]
    elif some_str[1] == '_':
        return fr'{some_str[0]}$_\mathrm{{{some_str[2:]}}}$'
    else:
        return some_str


def foldername(folder):
    folder = os.path.basename(os.path.normpath(folder))
    folder_to_label = {
        '1_AG': 'AG HR',                # Isolated zooms
        '2_KI': 'KI_HR',
        '3_KR': 'KR_HR',
        '5_Kimm_nobug_f':                   'KI',
        '6_Kimm_rnw_nobug_f':               'KI_rnw',
        '8_Agertz_f_jeans_sflmax':          'AG',
        '9_Kretschmer_strong_jeans_sflmax': 'KR_str',
        '10_Kretschmer_jeans_sflmax':       'KR_weak',
        '11_Kretschmer_boost':              'KR_boost',
        '1_KI_LR_E2SSN':'KI',           # New main runs
        '2_KR_LR':'KR',         
        '3_DC_LR':'DC',         
        '0_LR_NFB':     'No fb',        # Old main runs
        '4_KI_rnw':     'KIrnw_HR',
        '5_LR_KI':      'KI',
        '5_LR_KI_SSN':  'KI SSN',
        '6_LR_KR':      'KR',
        '7_LR_KI_rnw':  'KIrnw',
        '8_LR_DC':      'DC 50 Myr',
        '8_LR_DC_10Myr':'DC',
        '1_KI_LR':      'KI nobug',      # Nurion runs
        '2_KI_LR_E5':   'KI E5',
        '3_KI_LR_E7SSN':'KI E7 SSN',
    }
    
    # Check for direct mapping first
    if folder in folder_to_label:
        return folder_to_label[folder]
    
    # Gx thesis simulations
    for prefix in ['Ag8', 'Ki8', 'Kr8', 'Ag9', 'Ki9', 'Kr9']:
        if prefix in folder:
            return prefix + folder.split(prefix)[1]
    
    # Default case: format 'xx_some_name/' into 'some name'
    return folder.strip('/').replace('_', ' ')


def CGxM_colors(folder):
    default_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    color_map = {
        'GTT': default_colors[0],
        'SINK': default_colors[1]}
    ls_map = {
        '2pc': '-',
        '4pc': '--',
        '9pc': '-.',
        '18pc': '.'
    }
    lw_map = {
        '2pc': 2.5,
        '4pc': 2,
        '9pc': 1.5,
        '18pc': 1
    }
    color = next((color for key, color in color_map.items() if key in folder), None)
    ls = next((ls for key, ls in ls_map.items() if key in folder), None)
    lw = next((lw for key, lw in lw_map.items() if key in folder), None)
    return color, ls, lw


def get_aout(RamsesDir):
    """ Get list of outputs from any file ending with nml in the RamsesDir. """
    for file_name in os.listdir(RamsesDir):
        if file_name.endswith('.nml'):
            file_path = os.path.join(RamsesDir, file_name)
            with open(file_path, 'r') as file:
                for line in file:
                    if line.startswith('aout'):
                        aout_part = line.split('=')[1]
                        aout = [float(value) for value in aout_part.split(',')]
            break
    return aout