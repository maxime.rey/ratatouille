

"""
Functions related to ion fraction computation:
==============================================

    - comp_ions,
    - make_network, run_krome, run_Ions, run_fract_ions
    - get_subionising_UVB, setup_bpass_grid, sed_generate_bpass, compute_SED, Flux_ratio_bin0over1
    - nmlbool2dat, roman_to_int, extract_ions_info

Important points: 
    - Need to be compiled with intel. Otherwise, has to modify the makefile.
    - Parallelised for 32 cores. Ctrl-f in here if this needs to be changed.
    - If not running it in a job, there will be leftover processes even after ctrl-c'ing it because of shell=True. Check with `ps faxu | grep [m]piexec.hydra`
"""
from ratatouille import readNsave as ras
import os, subprocess, shutil
import fileinput, glob, argparse
import numpy as np

###################################################################################################################################
########################################################## Main function ##########################################################
###################################################################################################################################
def main():
    exmp = 'Example: python3 ion.py -gp /path/to/main/ -f simu1/ simu2/ simu3/ -t 100 204 -i MgII, CIV, OVI -s ~/some/dir/ > logs'
    desc = 'Compute the ionisation fraction of any chosen ion in the simulation. Relies on KROME'
    parser = argparse.ArgumentParser(description=desc, epilog=exmp)
    parser.add_argument("-gp", "--genpath"  , help="General path of the folders")
    parser.add_argument("-f" , "--folders"  , nargs='+', help="List of folders", default=["./"])
    parser.add_argument("-t" , "--timesteps", nargs='+', help="List of timesteps. If there are two input, all outputs in between are also computed.", type=int)
    parser.add_argument("-i" , "--ions"     , nargs='+', help="List of ions")
    parser.add_argument("-s" , "--savepath" , help="Path to save the data in. Default is simulation folder/ratadat.", default="default")
    parser.add_argument("-cd" , "--chng_dat", help="Input something like T2, or Z0.5. Only supports multiplication and Z, T or D (rho).", default="")
    parser.add_argument("-np" , "--nbproc"  , help="Number of processors used in the job.", type=int, default=32)
    
    # Returns the example if there is an argument problem
    try:
        args = parser.parse_args()
    except:
        print(parser.epilog)

    if len(args.timesteps)==2:
        args.timesteps = np.linspace(args.timesteps[0], args.timesteps[1],abs(args.timesteps[1]-args.timesteps[0])+1, dtype=int)

    comp_ions(args.genpath, args.folders, args.timesteps, args.ions, nb_proc=args.nbproc, savepath=args.savepath, chng_dat=args.chng_dat)


def comp_ions(genpath, folders, timesteps, ions, nb_proc, savepath='default', chng_dat=''):
    """
    Computes the ionization fraction of metals in Ramses-RT simulations, using Krome.

    Notes:
        - Proper time is used automatically when the runs are cosmo.
        - Need a copy of Krome (custom version) and Ions, and to specify their path below.
    """
    path_krome = '/Xnfs/cosmos/mrey/clones/krome'       # Path to modified krome.
    path_ions  = '/Xnfs/cosmos/mrey/clones/ions'        # Path to the ions' clone.
    if not os.path.exists(path_krome) or not os.path.exists(path_ions):
        raise ValueError('One of the necessary paths (Krome or ions) is incorrect.')
    
    for folder in folders:
        RamsesDir = genpath+folder
        if savepath=='default':
            path_sv = f'{RamsesDir}/ratadat/'
        else:
            path_sv = f'{savepath}/'
        os.makedirs(path_sv, exist_ok=True)
        path_sv += f'ions{chng_dat}/'
        os.makedirs(path_sv, exist_ok=True)
        for timestep in timesteps:
            path_save = f'{path_sv}{timestep:05d}/'
            ions_cp = ions.copy()
            # TODO: define alt_path
            if not os.path.exists(path_save):
                os.mkdir(path_save)
            else:                       # Check whether everything is computed by counting the number of files created for each ion.
                for ion in ions:
                    nb_ionfiles = len(glob.glob(f'{path_save}/compute_fractions/fract_ions/{ion}_{timestep:05d}.out*'))
                    with open(f"{RamsesDir}/output_{timestep:05d}/info_{timestep:05d}.txt", 'r') as file:
                        nb_cores = int(file.readlines()[-1].split()[0])
                    if nb_ionfiles==nb_cores:
                        print(f'{folder}: timestep {timestep} - {ion:5} already computed')
                        ions_cp.remove(ion)
                if len(ions_cp)==0:     # Is everything is already computed, go to the next iteration.
                    continue
            print(f'{folder}: timestep {timestep} - computing {ions_cp}...', end='\r')
            atoms, ion_states = extract_ions_info(ions_cp)

            make_network(path_ions, atoms, ion_states, path_save, photoion=True)
            run_krome(path_krome, path_save)
            run_Ions(path_save, path_ions, RamsesDir, atoms, ion_states, chng_dat, nb_proc)   # UVB parameters and few others are set here.
            run_fract_ions(path_save, nb_proc)
            print(f'{folder}: timestep {timestep} - {ions_cp} finished computing', end='\r')


###################################################################################################################################
########################################################## The big four  ##########################################################
###################################################################################################################################
def make_network(path_ions, atoms, ion_states, path_save, photoion=True):     # elems and ions_states must be an array of str
    """ Create a file react_ions in the current folder. """
    HHe = 1         # Method used to deal with H and He (1,2 or 3).
    make_ntw = os.path.dirname(ras.__file__)+'/utils/make_network.py'
    make_network_cmd = f'python {make_ntw} -pti {path_ions}/ -e {" ".join(map(str, atoms))} -i {" ".join(map(str, ion_states))} -HHe {HHe}'
    if not photoion:
        make_network_cmd += ' -np'
    print(f'Computing network: {make_network_cmd}', flush=True)
    exit_code = os.system(make_network_cmd)
    if exit_code != 0:
        raise IOError(f'The command {make_network_cmd} returned an error.')
    shutil.move('./react_ions', path_save+'/react_ions')


def run_krome(path_krome, path_save):
    """ Create a folder build in the current folder. """
    path_here = os.getcwd()
    os.chdir(path_krome)
    krome_cmd = f'python3 krome -n {path_save}/react_ions -compact -useN -photoBins=1'
    krome_cmd+= ' -unsafe'
    print(f'Running Krome: {krome_cmd}',flush=True)
    yes       = subprocess.Popen('yes', stdout=subprocess.PIPE) # Otherwise, user input needed in KROME.
    exit_code = subprocess.run(krome_cmd, shell=True, stdin=yes.stdout).returncode
    if exit_code != 0:
        raise IOError(f'The command {krome_cmd} returned an error.')

    path_build = path_save + '/build'
    if os.path.exists(path_build):
        shutil.rmtree(path_build)
    shutil.copytree('./build', path_build)  # Unsure if I should use path_save instead
    os.system('python3 clean')
    os.chdir(path_here)


def run_Ions(path_save, path_ions, RamsesDir, atoms, ion_states, chng_dat, nb_proc):
    """ 
    Create a folder compute_fractions in the path_save folder.
    Then, create params_krome.dat, update the Makefile and params_example.dat.
    Finally, remove csn file (avoid using a previous version) and copy reactions_verbatim.
    """
    # Hardcoded variables (= would be nice to make it automatic)
    use_initial_mass = 'T'          # Pbbly related to m_init not being written in older versions?
    ion_T_ne         = 'T'          # Krome parameter 
    last_file_worker = 1            # No clue what that is
    # [UVB]
    # UVB           = '7.8d8'         # Some integral. Valentin knows.
    threshold_UVB = '1d2'
    # bin1_factor   = '1d1'           # Ratio between bin0 and bin1. Computed in Flux_ratio_bin0over1.

    # Create dir compute_fractions and go there
    path_cf = f'{path_save}/compute_fractions'
    if os.path.exists(path_cf):
        os.system(f'rm -rf {path_cf}/*.mod {path_cf}/*.o {path_cf}/*.f90 {path_cf}/*.dat {path_cf}/Makefile')
    shutil.copytree(f"{path_ions}/compute_fractions", path_cf, dirs_exist_ok=True)
    os.chdir(path_cf)

    # Artificially change the density, temperature or metallicity upon request through chng_dat.
    if chng_dat != '':
        chng_field  = chng_dat[0]
        chng_factor = chng_dat[1:]
        file2modif = f'{path_cf}/module_cell.f90'
        with open(file2modif, 'r') as file:
            lines = file.readlines()

        if chng_field == 'Z':
            for i, line in enumerate(lines):
                if 'cellgrid(i)%Z = mets(i)' in line:
                    lines[i] = f'       cellgrid(i)%Z = {chng_factor}*mets(i)\n'
                    break
        elif chng_field == 'T':
            for i, line in enumerate(lines):
                if 'cellgrid(i)%T = Tgas(i)' in line:
                    lines[i] = f'       cellgrid(i)%T = {chng_factor}*Tgas(i)\n'
                    break
        elif chng_field == 'D':
            for i, line in enumerate(lines):
                if 'cellgrid(i)%nHI = nH(i)*(1d0-fractions(1,i))' in line:
                    lines[i] = f'       cellgrid(i)%nHI = {chng_factor}*nH(i)*(1d0-fractions(1,i))\n'
                elif 'cellgrid(i)%nHII = nH(i)*fractions(1,i)' in line:
                    lines[i] = f'       cellgrid(i)%nHII = {chng_factor}*nH(i)*fractions(1,i)\n'
                elif 'cellgrid(i)%nHeII = 7.895d-2*nH(i)*fractions(2,i)' in line:
                    lines[i] = f'       cellgrid(i)%nHeII = 7.895d-2*{chng_factor}*nH(i)*fractions(2,i)\n'
                elif 'cellgrid(i)%nHeIII = 7.895d-2*nH(i)*fractions(3,i)' in line:
                    lines[i] = f'       cellgrid(i)%nHeIII = 7.895d-2*{chng_factor}*nH(i)*fractions(3,i)\n'
        with open(file2modif, 'w') as file:
            file.writelines(lines)

    # Create params_krome which consists of lines like "6 4 -1d0" (CIV)
    if os.path.exists('./params_krome.dat'):
        os.remove('./params_krome.dat')
    with open("params_krome.dat", "a") as file:
        for i in range(len(atoms)):
            line = f"{atoms[i]} {ion_states[i]} -1d0\n"
            file.write(line)

    # Replace the path to krome build in the Makefile
    with fileinput.FileInput('Makefile', inplace=True) as file:
        for line in file:
            if line.startswith('VPATH ='):
                line = f"VPATH = :{path_save}/build:./\n"
            print(line, end='')

    # Initialise namelist parameters to default values
    gr_decomp       = '6.000, 13.60, 100000.0'
    nb_grp          = 2
    self_shielding  = 'false'
    ramses_rt       = 'false'
    cosmo           = 'false'
    sed_dir         = ''
    isH2            = False
    # Read parameters from the namelist
    path_nml = [file for file in os.listdir(RamsesDir) if file.endswith('.nml')][-1]
    with open(RamsesDir+path_nml, 'r') as file:
        for line in file:
            nml_var = line.split('=')[0].strip()
            if nml_var=='groupL0':
                grp_str = line.split('=')[1].strip()
                nb_grp = len(grp_str.split(",")) + 1
                gr_decomp = f'6.000, {grp_str}, 100000.0'
            if nml_var=='self_shielding':
                self_shielding = nmlbool2dat(line.split('=')[1].split('.')[1])
            if nml_var=='rt':
                ramses_rt = nmlbool2dat(line.split('=')[1].split('.')[1])
            if nml_var=='cosmo':
                cosmo = nmlbool2dat(line.split('=')[1].split('.')[1])
            if nml_var=='sed_dir':
                sed_dir = line.split('=')[1].split("'")[1]
            if nml_var=='isH2':
                isH2 = nmlbool2dat(line.split('=')[1].split('.')[1])
    # If the SED is not found, replace it with the same from a local path.
    if not os.path.exists(sed_dir):
        if 'bpass_v221_135_100' in sed_dir:
            sed_dir = '/Xnfs/cosmos/mrey/SED/bpass_v221/bpass_v221_135_100/'
        else:
            raise ValueError(f'The SED directory ({sed_dir}) does not exist.')

    # Read parameters from the hydro_file_descriptor
    timestep05d = (path_save.split('/')[-2])
    hydro_fd = f'{RamsesDir}/output_{timestep05d}/hydro_file_descriptor.txt'
    with open(hydro_fd, 'r') as file:
        last_line = file.readlines()[-1].strip()
        try:    # Format is different between old and new RAMSES.
            iGamma1 = int(last_line.split(',')[0]) + 1
            star_read_meth  = 'Teyssier'
        except:
            iGamma1 = int(last_line.split("variable # ", 1)[1][0])+ 1
            star_read_meth = 'default'  # Older version of RAMSES
    rt_file = f'{RamsesDir}/output_{timestep05d}/info_rt_{timestep05d}.txt'
    with open(rt_file, 'r') as file:
        for line in file:
            if line.startswith('iIons'):
                iIons = int(line.split('=')[-1].strip())
                break
    
    # Compute UVB (Cloudy) and the RT bin0 by extrapolating from bin1.
    # We compute the galaxy SED and check the ratio between the wavelengths of bin0 and bin1 (bin1_factor).
    # Then, we compute the RT bin1 and multiply it by bin1_factor to estimate bin0.
    timestep = int(timestep05d)
    UVB = get_subionising_UVB(ras.read_info(RamsesDir, timestep, alt_path=None)['redshift'], table_model='hm12')    # cf docstring
    bin1_factor = Flux_ratio_bin0over1(RamsesDir, timestep, ncpu=nb_proc, hnum=None, alt_path=None)                 # cf docstring

    # List of parameters to change
    param_dict = {
        "repository =" : RamsesDir,
        "snapnum ="    : timestep05d,
        "output_path =": "./fract_ions/",
        "star_reading_method =" : star_read_meth,
        "self_shielding   ="    : self_shielding,
        "ramses_rt        ="    : ramses_rt,
        "cosmo            ="    : cosmo,
        "use_initial_mass ="    : use_initial_mass,
        "use_proper_time "      : cosmo,
        "read_rt_variables "    : ramses_rt,
        "iGamma1="  : iGamma1,
        "ihii   ="  : iIons+isH2,
        "iheii  ="  : iIons+isH2+1,
        "iheiii ="  : iIons+isH2+2,
        "UVB         =": UVB,
        "threshold   =": threshold_UVB,
        "bin1_factor =": bin1_factor,
        "sed_dir ="     : sed_dir,
        "nSEDgroups ="  : nb_grp,
        "group_decomp =": gr_decomp,
        "n_elements ="  : len(atoms),
        "krome_parameter_file =": "./params_krome.dat",
        "ion_T_ne ="    : ion_T_ne,
        "last_file ="   : last_file_worker
    }

    # Replace parameters in params_example.dat 
    nb_changes = 0
    with fileinput.FileInput('params_example.dat', inplace=True) as file:
        for line in file:
            for parameter, value in param_dict.items():
                if parameter in line:
                    line = f'{line.split("=")[0]}= {value}\n'
                    nb_changes += 1
            print(line, end='')
    nb_attempts = len(param_dict)
    if nb_changes!=nb_attempts:
        print(f'Changed {nb_changes}/{nb_attempts} parameters. Looks like there is a problem in the format or if.')

    # Remove the csn file if it exists
    with open('params_example.dat', 'r') as file:
        for line in file:
            if 'csn_file' in line:
                csn_name = line.split('=')[1].strip()
                if os.path.exists(csn_name):
                    os.remove(csn_name)
                break

    # Copy reactions_verbatim from the krome folder (list of ionisation/recombination reactions)
    shutil.copy(f'{path_save}/build/reactions_verbatim.dat', './')


def run_fract_ions(path_save, nb_proc):
    """
    Create a folder fract_ions with (nb_cpus_used)*(nb_ions+1) files in the folder compute_fractions. 
    """
    path_here = os.getcwd()
    os.chdir(f'{path_save}/compute_fractions')
    print( "Creating fract_ions")
    os.makedirs('./fract_ions', exist_ok=True)
    os.system('make')
    os.system('make clean')
    if not os.path.exists('ion_fractions'):
        print('Run this or the quivalent for your machine:')
        print('\t module purge')
        print('\t module use /applis/PSMN/debian11/Lake/modules/all')
        print('\t module load intel/2021a')
        raise ImportError('Compilation did not work, load modules before running this.')
    cmd_ion = f'mpirun -np {nb_proc} ./ion_fractions params_example.dat'
    print(f'Running the computation: {cmd_ion}', flush=True)
    try:
        subprocess.check_call(cmd_ion, shell=True)
    except subprocess.CalledProcessError as e:
        raise IOError(f'The command {cmd_ion} returned an error: {e}')
    print( "fract_ions folder done")
    os.chdir(f'{path_here}')


###################################################################################################################################
########################################################## UVB/RT stuff  ##########################################################
###################################################################################################################################
from ratatouille.constants import *
import numpy as np
from tqdm import tqdm
import subprocess, os, h5py

def get_subionising_UVB(redshift, table_model='hm12'):
    """
    Runs cloudy to compute the integral of the UVB within 6-13.6 eV (2066-911 Å).
    Available models:
        KS19        - Katz & Sargent 2019 background model
        HM96/05/12  - Haardt & Madau 1996/2005/2012 background model
    """
    filename = 'MR_UVB_dat'
    path2cloudy = '/Xnfs/cosmos/mrey/clones/cloudy/'
    UVB_string = f'UVB_bin0_{redshift}_{table_model}'
    try:
        with h5py.File(f'{path2cloudy}{filename}.h5', 'r') as hdf_file:
            UVB_bin0 = hdf_file[UVB_string][()]
    except (OSError, KeyError):
        # Write the Cloudy input file.
        input_name = 'UVB'
        input_content = (   # hden and temp have no impact since we just take the UVB.
            "hden -1                                         # Hydrogen density of -1 in units of cm^-3\n" 
            "abundances gass10                               # Set abundances according to the GASS10 model (Galactic ISM)\n"
            "const temp 3                                    # Set a constant temperature of 3 K for the gas\n"
            f"table {table_model} redshift {redshift}        # Use the selected UVB model with the given redshift\n"
            'save continuum units microns, file=".cont"      # Save the UV background continuum in microns to a file named "UVB.cont"\n'
            "stop zone 1                                     # Stop at zone 1 (single-zone simulation)\n"
            "radius 30                                       # Set a radius (this is just a dummy parameter in this context)\n"
        )
        with open(f'{input_name}.in', 'w') as f:
            f.write(input_content)

        # Run Cloudy
        try:
            subprocess.run([f'{path2cloudy}/source/cloudy.exe', f'{input_name}.in'], check=True)
        except subprocess.CalledProcessError as e:
            raise ValueError(f"Error running Cloudy: {e}")

        # Compute the integral and save it.
        l, Flambda = np.genfromtxt(f'{input_name}.cont', usecols=(0,1), unpack=True)
        l = np.flip(l*1e4)
        Flambda = np.flip(Flambda)
        Flambda = Flambda/l
        UVB_bin0 = 1e-8/(h_cgs*c_cgs)*np.trapezoid(Flambda[(l>911)&(l<2066)]*l[(l>911)&(l<2066)],l[(l>911)&(l<2066)])
        with h5py.File(f'{path2cloudy}{filename}.h5', 'a') as hdf_file:
            hdf_file.create_dataset(UVB_string, data=UVB_bin0)

        # Delete all files
        for file in [f'{input_name}.in', f'{input_name}.out', f'{input_name}.cont']:
            try:
                if os.path.exists(file):
                    os.remove(file)
                else:
                    print(f"{file} not found.")
            except Exception as e:
                print(f"Error deleting {file}: {e}")
    return UVB_bin0


def Flux_ratio_bin0over1(RamsesDir, timestep, ncpu=1, hnum=None, alt_path=None):
    """ 
    Creates the full SED from stars within 0.1 Rvir.
    Then, computes the ratio between bin0 (subionising) and bin1. 
    Used in KROME postprocessing (ions) to extrapolate bin0 from RT bin1 in the CGM.
    """
    # Initialisation
    imf='chab300'
    stellar_system='bin'
    BPASS_directory='/Xnfs/cosmos/mrey/outputs/bpass/'
    E0, E1, E2, E3 = 6, 13.6, 24.59, 54.42  # [E1, E2), [E2, E3), [E3, +infty) = bin1, bin2, bin3 are the 3 bins commonly used in RT.
    
    filename = 'SED_props'
    try:
        bin0 = ras.read_prop(RamsesDir, timestep, 'bin0', filename, hnum=hnum, alt_path=alt_path)
        bin1 = ras.read_prop(RamsesDir, timestep, 'bin1', filename, hnum=hnum, alt_path=alt_path)
    except (KeyError, FileNotFoundError):
        star_mets,star_mass,star_age = ras.extract_stars(RamsesDir, timestep, ['star_mets','star_mass','star_age'], factor=0.1) # Within 0.1 R200
        lams_bpass, sed_bpass, z_bins_bpass, age_bins_bpass = setup_bpass_grid(wvlgth_min=wvlgth(E2), wvlgth_max=wvlgth(E0), imf=imf, stellar_system=stellar_system, BPASS_directory=BPASS_directory)
        star_mets[star_mets>=0.0399999999] = 0.0399999999
        SED = sed_generate_bpass(star_mets, star_age*1e6, star_mass*g2Msun, sed_bpass, z_bins_bpass, age_bins_bpass, ncpu=ncpu, verbose=False)
        # plt.plot(lams_bpass, SED) # Plot the SED.

        ibin0 = (lams_bpass>wvlgth(E1))&(lams_bpass<wvlgth(E0))
        ibin1 = (lams_bpass>wvlgth(E2))&(lams_bpass<wvlgth(E1))
        bin0  = np.trapezoid(SED[ibin0]*lams_bpass[ibin0], lams_bpass[ibin0])   # Number of photons
        bin1  = np.trapezoid(SED[ibin1]*lams_bpass[ibin1], lams_bpass[ibin1])
        ras.add_prop(RamsesDir, timestep, 'bin0', bin0, filename, hnum=hnum, alt_path=alt_path)
        ras.add_prop(RamsesDir, timestep, 'bin1', bin1, filename, hnum=hnum, alt_path=alt_path)
    return bin0/bin1


def setup_bpass_grid(wvlgth_min=500, wvlgth_max=2100, imf='chab300', stellar_system='bin', BPASS_directory = '/Xnfs/cosmos/mrey/outputs/bpass/'):
    """
    Sets up the BPASS model. After this, run sed_generate_bpass.
        wvlgth_min/max      Wavelength range of the SED
        imf                 '100_100', '100_300', '135_300', 'chab100', 'chab300'
        stellar_system      'sin' (singular star system), 'bin' (binary)
        BPASS_directory     Directory where the BPASS models are stored
    """
    path=f'{BPASS_directory}/bpass_v2.2.1_imf_{imf}/spectra-{stellar_system}-imf_{imf}.z'

    metal_list=['em5','em4','001','002','003','004','006','008','010','014','020','030','040']
    wave_bpass=np.arange(1,100001)  # wavelength range of BPASS SEDs
    age_bins_bpass=10**np.arange(5.9,11.1,0.1)
    age_bins_bpass[0]=1e-50
    z_bins_bpass=np.array([1.0e-50, 1.0e-05, 1.0e-04, 1.0e-03, 2.0e-03, 3.0e-03, 4.0e-03, 6.0e-03, 8.0e-03, 1.0e-02, 1.4e-02, 2.0e-02, 3.0e-02, 4.0e-02])
    M_cluster=1e6   # cluster mass in solar mass
    index = np.where((wave_bpass >= wvlgth_min) & (wave_bpass < wvlgth_max))[0]
    lams_bpass = wave_bpass[index]

    sed_bpass=np.zeros((len(wave_bpass),len(z_bins_bpass),len(age_bins_bpass)))
    for i in range(len(metal_list)):
        data = np.loadtxt(path + metal_list[i] + '.dat')
        for j in range(100000):     # TODO: Replace by len(wave_bpass)?
            sed_bpass[j, i+1, 1:] = np.float64(data[j, 1:])

    sed_bpass[:,:,0]=sed_bpass[:,:,1]
    sed_bpass[:,0,:]=sed_bpass[:,1,:]
    sed_bpass=sed_bpass / M_cluster * L_sun
    sed_bpass=sed_bpass[index,:,:]

    return lams_bpass, sed_bpass, z_bins_bpass, age_bins_bpass


def sed_generate_bpass(star_mets, star_age, star_mass, sed_bpass, z_bins_bpass, age_bins_bpass, ncpu=1, verbose=True):
    """
    Compute the total SED of an array of particles.
    The three input arrays are stellar metallicity (absolute), age [year] and mass [Msun].
    """
    # Single core case
    if ncpu == 1:
        return compute_SED((star_mets, star_age, star_mass), verbose)

    # Multi-core case
    from multiprocessing import Pool
    chunks1 = np.array_split(star_mets, ncpu)
    chunks2 = np.array_split(star_age, ncpu)
    chunks3 = np.array_split(star_mass, ncpu)
    chunks = zip(chunks1, chunks2, chunks3)
    with Pool(processes=ncpu) as pool:
        results = pool.starmap(compute_SED, [(chunk, sed_bpass, z_bins_bpass, age_bins_bpass, verbose) for chunk in chunks])
    return sum(results)


def compute_SED(chunk, sed_bpass, z_bins_bpass, age_bins_bpass, verbose):
    """
    Interpolates the SED (in log space) based on the star properties (given through chunk).
    Subfunction of sed_generate_bpass.
    """
    star_mets,star_age,star_mass = chunk
    sed_sum=np.zeros(sed_bpass.shape[0])
    z_bins_log   = np.log10(z_bins_bpass)
    age_bins_log = np.log10(age_bins_bpass)

    # Loop on stars
    loop = tqdm(range(len(star_mets))) if verbose else range(len(star_mets))
    for i in loop:
        # Skip stars with zero mass
        if star_mass[i] == 0:   
            continue

        # Find the appropriate metallicity and age bins
        ageind1 = np.searchsorted(age_bins_log, np.log10(star_age[i]),  side='right') - 1
        zind1   = np.searchsorted(z_bins_log,   np.log10(star_mets[i]), side='right') - 1
        if ageind1 < 0 or ageind1 >= len(age_bins_bpass) - 1:                   # Check not out of bounds
            print(f"Warning: Age {star_age[i]} out of bounds for star {i}.")
            continue
        ageind2 = ageind1 + 1
        zind2   = zind1 + 1
        
        # Extract the relevant bins for metallicity and age
        z1, z2 = z_bins_bpass[zind1], z_bins_bpass[zind2]
        age1, age2 = age_bins_bpass[ageind1], age_bins_bpass[ageind2]

        # Interpolate over metallicity and age
        temp1 = (sed_bpass[:, zind2, ageind1] - sed_bpass[:, zind1, ageind1]) / (np.log10(z2) - np.log10(z1)) * (np.log10(star_mets[i]) - np.log10(z1)) + sed_bpass[:, zind1, ageind1]
        temp2 = (sed_bpass[:, zind2, ageind2] - sed_bpass[:, zind1, ageind2]) / (np.log10(z2) - np.log10(z1)) * (np.log10(star_mets[i]) - np.log10(z1)) + sed_bpass[:, zind1, ageind2]
        data = ((temp2 - temp1) / (np.log10(age2) - np.log10(age1)) * (np.log10(star_age[i]) - np.log10(age1)) + temp1) * star_mass[i]
        sed_sum += data
    
    return sed_sum


###################################################################################################################################
############################################################## Utils ##############################################################
###################################################################################################################################
def extract_ions_info(ions):
    """ Returns a list of the atomic numbers and integer ionisation states from a list such as ['MgII', 'CV', 'OIX']. """
    atm_list = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe']
    atoms = []
    ion_states = []

    for ion in ions:
        atom = ion.strip('IVX')                     # Atom
        atm_nb = atm_list.index(atom)+1             # Atomic index
        ion_state = roman_to_int(ion.strip(atom))   # Integer ionsiation state

        # If multiple ionisation states, just keep the highest one.
        if atm_nb in atoms:
            other_ion_state = ion_states[atoms.index(atm_nb)]
            max_ion_state = max(other_ion_state, ion_state)
            ion_states[atoms.index(atm_nb)] = max_ion_state
        else:
            atoms.append(atm_nb)
            ion_states.append(ion_state)

    # Sanity check
    if len(atoms)!=len(ion_states):
        raise ValueError(f"Error: there is a problem with the list {ions}.")

    return atoms, ion_states


def nmlbool2dat(nmlbool):
    """ Convert boolean from namelist to boolean for the dat file. """
    if nmlbool=='true':
        return 'T'
    elif nmlbool=='false':
        return 'F'
    else:
        raise IOError(f'Could not read namelist parameter {nmlbool}')


def roman_to_int(roman):
    roman_map = {"I": 1, "II": 2, "III": 3, "IV": 4, "V": 5, "VI": 6, "VII": 7, "VIII": 8, "IX": 9, "X": 10}
    return roman_map.get(roman, 0)


###################################################################################################################################
############################################################ Real main ############################################################
###################################################################################################################################
if __name__ == "__main__":
    main()