"""
Functions to plot 2D maps:
============================================

- Maps of the stars
    starmap
- Level maps:
    levelmap
- Other maps (from cell quantities):
    qty2plotmap, get_cmap_color, Harleymap
- change_basis

The use of these functions is shown in miniRATS_maps.ipynb.


Common parameters:
==================
    Simulation folder-related:
    --------------------------
        RamsesDir: path
            Path to the folder containing the Ramses output. 
        timestep: int
            Timestep of the output considered.
        factor: 'fullbox' or float
            Multiplicative factor to increase the radius of data read. This works for cosmological simulations. For non_cosmological simulations, the whole data box is loaded. 
            For a cosmological simulation, by default the radius is r200 and the center is the center of the zoom region.

        qty2plot: array of scalars
            Quantity to plot.
        weight: array of scalars or None
            The data used to weight the map if style='proj'. If None is selected, an array of 1 is used.
        proj: 'x', 'y' or 'z'
            Axis along which the projection is done.
        style: 'slice', 'column' or 'proj'
            The method used to plot the map: a slice of the data, the column density or the weighted projection.
        slice_shift: scalar
            If style='slice', this is the distance shift from cen_plot in [kpc] perpendicularly to the projection axis.


    Plot-related:
    -------------
        cen_plot, rad_plot: array of size 3, scalar
            Center and radius considered in [kpc].
        center, radius: array of size 3, scalar
            Center and radius considered in [kpc].
        logscale: boolean
            Plots in logscale if set to True and in linear scale otherwise.
        vmin, vmax: scalar
            Limit values of the colorbar.
        centered: boolean
            Set the origin of the plot on the center of the halo if set to True.
        remove_ticks: boolean
            If set to true, removes the ticks in the plotted map.
        mapof: str (see functions for the options available)
            Selects the colorscale and title depending on the quantity considered.
        imsave: boolean or str (path)
            Path to save the images to. If True, uses to current directory to save the img.
"""
# Astro libraries
import minirats.utils.py.readwrite.ramses_info as ri
from minirats.utils.py import cellutils as cu
from astropy.cosmology import FlatLambdaCDM
from ratatouille import readNsave as ras
from ratatouille import plotutils as put
from ratatouille.constants import *
import numpy as np
import warnings

# Plotting libraries
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm, SymLogNorm
from matplotlib.colors import ListedColormap
from collections import Counter
import matplotlib.pyplot as plt

# I dunno... Harley
from scipy.io import FortranFile
from pathlib import Path
from tqdm import tqdm
import configparser
import argparse, os


###################################################################################################################################
############################################################# Starmap #############################################################
###################################################################################################################################
def starmap(RamsesDir, timestep, zoom=1/0.7, factor=1, var1='x', var2='y',bins=300, centered=True, vmin=None, vmax=None, imsave=False, hnum=None, alt_path=None):
    """
    Returns a map of the stars.

    Parameters:
    -----------
        var1, var2: 'x', 'y' or 'z'
            Variables to specify the projection plane of the map.
        bins: int
            Number of new bins for the SFR map.
        rad_circle: boolean
            If set to True, plots the circle corresponding to the radius.

    Example:
    --------
        starmap(star_x, star_y, star_z, star_mass, info, center, radius)
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2kpc =  info['unit_l'] * info['boxlen'] / kpc2cm
    center_dat, rad_dat  = ras.get_frad(RamsesDir,timestep,factor,hnum=hnum)
    center    , rad_plot = [cen*cu2kpc for cen in center_dat], rad_dat*cu2kpc/zoom
    star_mass,star_x,star_y,star_z = ras.extract_stars(RamsesDir, timestep, ['star_mass','star_x','star_y','star_z'], factor=factor, alt_path=alt_path)

    # Conversions
    star_mass_Msun =  star_mass / 1.989e33
    star_x_kpc     =  star_x / kpc2cm
    star_y_kpc     =  star_y / kpc2cm
    star_z_kpc     =  star_z / kpc2cm
    
    # Edges of maps
    xmin, xmax = center[0]-rad_plot,center[0]+rad_plot
    ymin, ymax = center[1]-rad_plot,center[1]+rad_plot
    zmin, zmax = center[2]-rad_plot,center[2]+rad_plot

    mapping = {
        'x': (star_x_kpc, xmin, xmax, center[0]),
        'y': (star_y_kpc, ymin, ymax, center[1]),
        'z': (star_z_kpc, zmin, zmax, center[2])
    }

    var1, min1, max1, center1 = mapping[var1]
    minmax1 = min1, max1
    minmax1_c = min1 - center1, max1 - center1

    var2, min2, max2, center2 = mapping[var2]
    minmax2 = min2, max2
    minmax2_c = min2 - center2, max2 - center2

    starmap,_,_ = np.histogram2d(var1,var2,bins=(bins,bins),\
        range=[[minmax1[0] ,minmax1[1]],[minmax2[0],minmax2[1]]],\
            normed=False,weights=star_mass_Msun)

    dx = (minmax1[1]-minmax1[0])/float(bins)              # kpc
    dy = (minmax2[1]-minmax2[0])/float(bins)              # kpc
    starmap = starmap/dx/dy 

    if centered:
        extent = minmax1_c[0],minmax1_c[1],minmax2_c[0],minmax2_c[1]  # values plot (kpc)
    else:
        extent = minmax1[0],minmax1[1],minmax2[0],minmax2[1]  # values plot (kpc)

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,10))
    cmap = plt.cm.pink
    cmap.set_bad(color='k')
    im = plt.imshow(starmap.T,interpolation='nearest',origin='lower',norm=LogNorm(), extent=extent,cmap=cmap,vmin=vmin,vmax=vmax)
    plt.xlabel('[kpc]')
    plt.ylabel('[kpc]')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="4%", pad=0)
    plt.colorbar(im, cax=cax).set_label(label=r'$\Sigma_* \rm [M_\odot / kpc^2]$',size=16)
    
    c2 = plt.Circle((0, 0), ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2kpc, linewidth=1, edgecolor='k', facecolor=(0, 0, 0, 0)) 
    ax.add_artist(c2)

    cmap = plt.cm.pink
    cmap.set_bad(color='k')

    proj = "BLEH" # need to set it up properly.
    if imsave:
        if imsave==True:
            fig.savefig(f"{put.foldername(RamsesDir)}_starmap_{proj}_{timestep}.pdf", bbox_inches='tight',pad_inches=0)
        else:
            fig.savefig(f"{imsave}/{put.foldername(RamsesDir)}_starmap_{proj}_{timestep}.pdf", bbox_inches='tight',pad_inches=0)


###################################################################################################################################
############################################################ Level map ############################################################
###################################################################################################################################
def levelmap(RamsesDir, timestep, factor=1, in_pc=False, style='proj', proj='z', \
    vmin=None, centered=True, remove_ticks=True, lmax=None, slice_shift=0, imsave=False, hnum=None, alt_path=None):
    """
    Plot the of map of the resolution level of each cell in parsec or in level units (in_pc variable). 
    Only the options 'slice' and 'proj' are available.
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2cm  = info['cu2cm']
    cu2kpc =  cu2cm / kpc2cm
    factor_zoom = factor/0.7 if factor!='fullbox' else 'fullbox'        # Load more because load a sphere but display a square.
    cen_cu, rad_cu = ras.get_frad(RamsesDir,timestep,factor,hnum=hnum)  # Use factor for the map size
    cell_pos, cell_l = ras.extract_cells(RamsesDir,timestep,['cell_pos', 'cell_l'],factor=factor_zoom,alt_path=alt_path)
    cell_pos_cu    = cell_pos / cu2cm

    weight = np.ones_like(cell_l)
    lmax = lmax if lmax is not None else cell_l.max()
    vmin = vmin if vmin is not None else cell_l.min()

    # Axis along which the data is projected (all in cu ∈ [0,1]). The variables xloc, yloc and zloc are not the 'real' x, y, z anymore. 
    if proj=='z':
        xloc, yloc, zloc = ras.read_pos(cell_pos_cu)
        xmin, xmax, ymin, ymax, zmin, zmax = ras.compute_edges(cen_cu, rad_cu)
    elif proj=='y':
        xloc, zloc, yloc = ras.read_pos(cell_pos_cu)
        xmin, xmax, zmin, zmax, ymin, ymax = ras.compute_edges(cen_cu, rad_cu)
    elif proj=='x':
        zloc, yloc, xloc = ras.read_pos(cell_pos_cu)
        zmin, zmax, ymin, ymax, xmin, xmax = ras.compute_edges(cen_cu, rad_cu)
    else:
        raise ValueError(f"{proj} is not a valid projection axis. \n Pick 'x', 'y' or 'z'.")

    if style=='slice':
        slice_shift_cu = slice_shift/cu2kpc
        axis_index = {'x': 0, 'y': 1, 'z': 2}[proj]
        zmin, zmax = cen_cu[axis_index] + slice_shift_cu - 1/(2**lmax)/2, cen_cu[axis_index] + slice_shift_cu + 1/(2**lmax)/2
        if not np.all(weight == 1):
            warnings.warn("Using a weight is totally useless for a slice...")
    elif style!='proj':
        raise ValueError(f'{style} is not a valid plotting style')

    nx,ny = cu.py_cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)                         # bins at res lmax.
    map2plot,_ = cu.py_cell_utils.make_map_new(lmax,True,xmin,xmax,ymin,ymax,zmin,zmax,\
            cell_l,weight,xloc,yloc,zloc,cell_l,nx,ny)                                      # True -> max of proj.
    
    # Plot
    fig, ax = plt.subplots(nrows=1, ncols=1)
    if centered:
        rad_kpc = rad_cu*cu2kpc
        extent = -rad_kpc,rad_kpc,-rad_kpc,rad_kpc                 # [kpc]
    else:
        extent = xmin*cu2kpc,xmax*cu2kpc,ymin*cu2kpc,ymax*cu2kpc                            # [kpc]
    im = plt.imshow(map2plot.T,interpolation='nearest',origin='lower',norm=None,\
        extent=extent,cmap=plt.get_cmap('tab20c', lmax-vmin+1), vmin=vmin-0.5, vmax=lmax+0.5)

    if remove_ticks:
        plt.gca().get_yaxis().set_ticks([])
        plt.gca().get_xaxis().set_ticks([])
    else:
        plt.xlabel('[kpc]')
        plt.ylabel('[kpc]')
    
    # Colorbar
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="6%", pad=0)
    cbar = fig.colorbar(im, cax=cax)
    cax.yaxis.set_ticks_position('left')
    cax.tick_params(axis='y', which='both', length=5)
    cax.minorticks_off()

    if in_pc:
        cbar.set_ticklabels([int(cu2kpc*1e3/2**(yval)) for yval in cbar.ax.get_yticks()])
        cbar_label = 'Resolution [pc]'
    else:
        cbar.set_ticklabels([int(yval) for yval in (cbar.ax.get_yticks())])
        cbar_label = 'Resolution level'
    cbar.set_label(label=cbar_label,size=10, labelpad=-15)

    scalebar = True
    if scalebar:    # Make a scale bar ~ 20% of the map
        xlim = ax.get_xlim()
        print(xlim)
        scale_l = round((xlim[1] - xlim[0]) * 0.2, -int(np.floor(np.log10((xlim[1] - xlim[0]) * 0.2))) + 1) # 10% precision
        scale_u, scale_str = ('pc', scale_l * 1e3) if scale_l < 2 else ('kpc', scale_l) if scale_l < 2e3 else ('Mpc', scale_l/1e3)
        scale_str = f'{scale_str:.1f}'.rstrip('0').rstrip('.') if scale_str % 1 else f'{int(scale_str)}'
        scalebar = AnchoredSizeBar(ax.transData, scale_l, f'{scale_str} {scale_u}', label_top=True, loc='lower left', pad=1, sep=10, color='k', frameon=False)
        ax.add_artist(scalebar)
    if imsave:
        if imsave==True:
            fig.savefig(f"{put.foldername(RamsesDir)}_lvmap_{proj}_{timestep}.pdf", bbox_inches='tight',pad_inches=0)
        else:
            fig.savefig(f"{imsave}/{put.foldername(RamsesDir)}_lvmap_{proj}_{timestep}.pdf", bbox_inches='tight',pad_inches=0)



###################################################################################################################################
############################################################ Othermaps ############################################################
###################################################################################################################################
def qtymap(RamsesDir, timestep, factor=1, var_str='nH', wei_str='nH', style='proj', proj='y', ang_mom_basis=False,
    is_cbar=True, cbar_top=False, scalebar=True, vmin=None, vmax=None, centered=True, remove_ticks=True, logscale=True, lmax=None, 
    slice_shift = 0, imsave=False, edge_on=False, hnum=None, alt_path=None, use_yt=False, ax=None, fmt='pdf'):
    """
    Plots the map of a var_str weighted by wei_str. In cosmo sims, x is face on, y and z are edge on.
    The options available are 'T', 'rho', 'nH', vz', 'J', 'HI', 'MgII', 'CIV', 'OVI'.
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2cm  = info['cu2cm']
    cu2kpc =  cu2cm / kpc2cm
    factor_zoom = factor/0.7 if factor!='fullbox' else 'fullbox'        # Load more because load a sphere but display a square.
    cen_cu, rad_cu  = ras.get_frad(RamsesDir,timestep,factor,hnum=hnum) # Use factor for the map size
    rad_kpc = rad_cu*cu2kpc

    # Check user is not stupid.
    stupid_flag = False
    if wei_str and style in ['slice', 'column']:
        warnings.warn("Using a weight is totally useless here...")
    elif not wei_str:
        if style == 'proj':
            raise ValueError('You did not input any weight !')
        else:
            wei_str = 'nH'  # just to avoid crashing extract_cells
            stupid_flag = True

    cbar_label = put.get_label(var_str)
    cmap_color = get_cmap_color(var_str)
    cell_pos, cell_l, qty2plot, weight = ras.extract_cells(RamsesDir,timestep,['cell_pos', 'cell_l', var_str, wei_str],factor=factor_zoom,alt_path=alt_path,use_yt=use_yt)
    cell_pos_cu = cell_pos / cu2cm
    # nsat = put.is_sat(RamsesDir, timestep, factor=factor_zoom, alt_path=alt_path)
    # qty2plot[nsat], weight[nsat] = 0, 0

    if var_str == 'M_Z':
        qty2plot *= g2Msun
        cbar_label += r'[M$_\odot$]'
    elif var_str == 'Z':
        qty2plot *= 1 / Z_sun
        cbar_label += r'[Z$_\odot$]'
    elif var_str == 'vz' or var_str == 'norm_v':
        qty2plot *= 1e-5
    elif var_str == 'M_outflow_ide' and not info['cosmo']:
        vz = ras.extract_cells(RamsesDir, timestep, ['vz'], factor=factor_zoom, alt_path=alt_path)
        z_pos = cell_pos[:, 2] - cen_cu[2]
        outflow = vz * z_pos > 0
        cell_pos = cell_pos[outflow]
        cell_l = cell_l[outflow]
        weight = weight[outflow]

    if stupid_flag: wei_str, weight = None, np.ones_like(qty2plot)  # Necessary for makemap

    
    if lmax==None:
        lmax = cell_l.max()

    # Axis along which the data is projected (all in cu ∈ [0,1]). The variables xloc, yloc and zloc are not the 'real' x, y, z anymore. 
    if proj=='z':
        xloc, yloc, zloc = ras.read_pos(cell_pos_cu)
        xmin, xmax, ymin, ymax, zmin, zmax = ras.compute_edges(cen_cu, rad_cu)
    elif proj=='y':
        xloc, zloc, yloc = ras.read_pos(cell_pos_cu)
        xmin, xmax, zmin, zmax, ymin, ymax = ras.compute_edges(cen_cu, rad_cu)
    elif proj=='x':
        zloc, yloc, xloc = ras.read_pos(cell_pos_cu)
        zmin, zmax, ymin, ymax, xmin, xmax = ras.compute_edges(cen_cu, rad_cu)
    else:
        raise ValueError(f"{proj} is not a valid projection axis. \n Pick 'x', 'y' or 'z'.")

    if style=='slice':
        slice_shift_cu = slice_shift/cu2kpc
        axis_index = {'x': 0, 'y': 1, 'z': 2}[proj]
        zmin, zmax = cen_cu[axis_index] + slice_shift_cu - 1/(2**lmax)/2, cen_cu[axis_index] + slice_shift_cu + 1/(2**lmax)/2
        if not np.all(weight == 1):
            warnings.warn("Using a weight is totally useless for a slice...")
    elif style!='column' and style!='proj':
        raise ValueError(f'{style} is not a valid plotting style')

    nx, ny     = cu.py_cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)                         # bins at res lmax.
    map2plot,_ = cu.py_cell_utils.make_map_new(lmax,False,xmin,xmax,ymin,ymax,zmin,zmax,qty2plot,weight,xloc,yloc,zloc,cell_l,nx,ny)

    if style=='column':
        column_factor = (zmax-zmin)*cu2cm      # -> ergs s-1 cm-2
        map2plot = map2plot*column_factor
        # cbar_label = fr'$\rm N_{{{var_str}}}\ {{[cm^{{-2}}]}}$'
        cbar_label = cbar_label+'*cm'

    if logscale:
        if (var_str=='J') or (var_str=='J_cold'):
            norm = SymLogNorm(linthresh=2e6, base=10, vmin=vmin, vmax=vmax)
        if (var_str=='M_totflow_ide'):
            norm = SymLogNorm(linthresh=2e-7, base=10, vmin=vmin, vmax=vmax)
        else:
            norm = LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm=None

    if ax is None:
        fig, ax = plt.subplots(nrows=1, ncols=1, frameon=False)

    if ang_mom_basis:
        # Rotate image so that it's better aligned with the basis (like J1, J3 instead of J1, -J3)
        from scipy.ndimage import rotate
        if proj=='x':
            map2plot = rotate(map2plot, angle=180)  # normal vector, upwards vector = J1, J3
        if proj=='y' or proj=='z':
            map2plot = rotate(map2plot, 90)         # make it edge on with J1 facing up
            map2plot = np.flipud(map2plot)
            if edge_on:
                ax.set_ylim(-rad_kpc/3,rad_kpc/3)

    # Replace NaNs by the minimum value of the map.
    cmap_color = plt.get_cmap(cmap_color)
    cmap_color.set_bad(color=cmap_color(0.0))

    # Plot
    extent = (-rad_kpc, rad_kpc, -rad_kpc, rad_kpc) if centered else (xmin * cu2kpc, xmax * cu2kpc, ymin * cu2kpc, ymax * cu2kpc)
    im = ax.imshow(map2plot.T,interpolation='nearest',origin='lower',norm=norm,extent=extent,cmap=cmap_color)
    if remove_ticks:
        ax.get_yaxis().set_ticks([])
        ax.get_xaxis().set_ticks([])
    else:
        ax.set_xlabel('[kpc]')
        ax.set_ylabel('[kpc]')

    # Determine luminance of rightmost pixels to decide on text color
    image_data = im.get_array().data
    image_data = np.nan_to_num(image_data, nan=-1) # Take into account NaN
    main_color = Counter(image_data[:, -1].flatten()).most_common(1)[0][0]
    rgb_color = plt.get_cmap(cmap_color)(plt.Normalize(np.min(image_data), np.max(image_data))(main_color))[:3]
    luminance = 0.299 * rgb_color[0] + 0.587 * rgb_color[1] + 0.114 * rgb_color[2]
    c_lab = 'white' if luminance < 0.5 else 'black'
    if main_color <0: c_lab = 'k' # If there are NaNs, the color is black

    # Plot Rvir circle
    # if var_str=='H':
    #     r200 = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
    #     c1 = plt.Circle((0, 0), r200*cu2kpc, linewidth=1, edgecolor='w', facecolor=(0, 0, 0, 0)) 
    #     ax.add_artist(c1)
    #     c2 = plt.Circle((0, 0), 0.1*r200*cu2kpc, linewidth=1, linestyle='dashed', edgecolor='w', facecolor=(0, 0, 0, 0)) 
    #     ax.add_artist(c2)
    #     plt.text(0.1, 0.95, put.foldername(RamsesDir), color='w',  horizontalalignment='right',verticalalignment='top', transform=ax.transAxes)
    # plt.text(0.07, 0.55, var_str, color='w',  horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, size=20)

    if is_cbar:
        divider = make_axes_locatable(ax)
        if cbar_top:
            cax = divider.append_axes('top', size="4%", pad=0)
            cbar = plt.colorbar(im,cax=cax,orientation='horizontal')
            cbar.set_label(label=cbar_label,size=25, c=c_lab)
            cax.tick_params(axis='x', which='both', colors=c_lab)
        else:
            cax = divider.append_axes('right', size="8%", pad=0)
            cbar = plt.colorbar(im,cax=cax)
            cbar.set_label(label=cbar_label,size=10, labelpad=-15, c=c_lab)
            cax.yaxis.set_ticks_position('left')
            cax.tick_params(axis='y', which='both', colors=c_lab)
        cbar.ax.minorticks_off()
    if scalebar:    # Make a scale bar ~ 20% of the map
        xlim = ax.get_xlim()
        scale_l = round((xlim[1] - xlim[0]) * 0.2, -int(np.floor(np.log10((xlim[1] - xlim[0]) * 0.2))) + 1) # 10% precision
        scale_u, scale_str = ('pc', scale_l * 1e3) if scale_l < 2 else ('kpc', scale_l) if scale_l < 2e3 else ('Mpc', scale_l/1e3)
        scale_str = f'{scale_str:.1f}'.rstrip('0').rstrip('.') if scale_str % 1 else f'{int(scale_str)}'
        scalebar = AnchoredSizeBar(ax.transData, scale_l, f'{scale_str} {scale_u}', label_top=True, loc='lower left', pad=1, sep=10, color=c_lab, frameon=False)
        ax.add_artist(scalebar)
    if imsave:
        fdname = put.foldername(os.path.basename(os.path.normpath(RamsesDir)))
        if imsave==True:
            fig.savefig(f"{fdname}_{var_str}_{proj}_{timestep:05d}.{fmt}", bbox_inches='tight',pad_inches=0)
        else:
            fig.savefig(f"{imsave}/{fdname}_{var_str}_{proj}_{timestep:05d}.{fmt}", bbox_inches='tight',pad_inches=0)
    plt.show()
    plt.close() 


def get_cmap_color(var_str):
    colors = {  # gist_gray is noice lookin'.
        ('T', 'temperature', 'temp', 'vz', 'M_totflow_ide'): 'seismic',
        ('rho', 'density', 'dens', 'nH', 'H'): 'viridis',
        ('Z', 'M_Z', 'norm_v'): 'RdYlGn',
        ('M_outflow_ide', 'N_HI'): 'magma',
        ('c_length',): 'tab20c',
    }
    flat_colors = {key: color for keys, color in colors.items() for key in keys}    
    return flat_colors.get(var_str, 'viridis')


def Harleymap(min_frame):
    """
    Routine extracted from https://github.com/HarleyKatz/ramses_movie to superpose different maps.
    """
    # args.attrib = 'abc'
    # args.path = 'input/path'

    # Initialize the arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("config_file_name")
    # args = parser.parse_args()
    config_file = './moviz/ramses_movie_Katz/configs/config_inferno.ini' # Get the config file name

    config = configparser.ConfigParser()
    config.read(config_file)


    # Check that the config file actually exists and is a file
    f = Path(config_file)
    if not f.is_file():
        print(f"{config_file} is not a file")
        exit()

    # Get the config params (in dictionary format)
    params = config._sections


    movie_path = '/scratch/Cral/mrey/outputs/1_cheap_runs/2_Kimm/movie3/'  # Path to movie files output by ramses
    # min_frame = 500 # minimum frame to render
    max_frame = min_frame # maximum frame to render


    # Do something
    quants = {}
    for key in params:
        if key != "GENERAL":
            colorlist = list(params[key]["cmap"].split(","))
            try:
                var_cmap = plt.colors.LinearSegmentedColormap.from_list("", colorlist)
            except ValueError:
                var_cmap = plt.cm.get_cmap(params[key]["cmap"])

            # Only linear for now
            alpha_method = params[key]["alpha_method"]
            if alpha_method == "none":
                s_cmap = var_cmap
            elif alpha_method == "linear":
                s_cmap = var_cmap(np.arange(var_cmap.N))
                s_cmap[:,-1] = np.linspace(float(params[key]["alpha_min"]), float(params[key]["alpha_max"]), var_cmap.N)
                s_cmap = ListedColormap(s_cmap)
            else:
                print(f"alpha method {alpha_method} not allowed")
                exit()

            quants[key] = {
                    "name": key,
                    "min": float(params[key]["min"]),
                    "max": float(params[key]["max"]),
                    "data": np.array([]),
                    "cmap": s_cmap,
                    "log": "True",
                    }

    # Get the cosmology (Hard coded for sphinx???)
    info = open(f"{movie_path}info_{str(1).zfill(5)}.txt").readlines()
    H0 = float(info[11].split("=")[-1].strip())
    omega_m = float(info[12].split("=")[-1].strip()) 
    cosmo = FlatLambdaCDM(H0=H0, Om0=omega_m)

    # Only make one figure (saves memory)
    plt.figure(figsize=(13,13))
    lr = np.arange(min_frame,max_frame+1)
    for i in tqdm(lr):
        all_ok = True
        # Load in the data
        for q in quants:
            fname = f"{quants[q]['name']}_{str(i).zfill(5)}.map"
            ffile = FortranFile(f"{movie_path}{fname}")
            [time, fdw, fdh, fdd] = ffile.read_reals('d')
            [frame_nx, frame_ny] = ffile.read_ints()
            data = np.array(ffile.read_reals('f4'), dtype=np.float64)
            age_in_myr = cosmo.age(1./time - 1.).value * 1e3

            try: 
                quants[q]["data"] = data.reshape(frame_nx,frame_ny)
                l_min = quants[q]["data"][quants[q]["data"] > 0].min()
                l_max = quants[q]["data"][quants[q]["data"] > 0].max()
                quants[q]["data"][quants[q]["data"] < l_min] = 1e-5 * l_min
            except ValueError:
                all_ok = False

        if all_ok:
            plt.clf()
            for q in quants:
                vals = quants[q]["data"]

                if quants[q]["log"]:
                    vals = np.log10(vals)
                plt.axis('off') # removes axis
                plt.imshow(vals,vmin=quants[q]["min"],vmax=quants[q]["max"],cmap=quants[q]["cmap"])


###################################################################################################################################
########################################################## Change basis ##########################################################
###################################################################################################################################
# def change_basis(RamsesDir,timestep,factor=1,hnum=None):
#     """
#     Rotates basis to one based on the angular momentum axis.

#     Example:
#     --------
#         timestep = 17
#         genpath = '/path/2/simu/'
#         cell_pos2, center2 = change_basis(RamsesDir,timestep)
#         bleh2_kpc = [cen/kpc2cm for cen in bleh2]    
#     """
#     # Extract center and r200 in cm.
#     info     = ras.read_info(RamsesDir, timestep)
#     cu2cm = info['cu2cm']
#     r200_cm   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*cu2cm
#     center_cm = [cen*cu2cm for cen in ras.get_Gcen(RamsesDir,timestep,hnum=hnum)]
#     # Extract data
#     cell_pos = ras.extract_cells(RamsesDir,timestep,['cell_pos'],factor=factor,alt_path=alt_path)[0] # TODO: should use J_stars.
#     vec_J = ras.extract_cells(RamsesDir,timestep,['vec_J'],factor=0.1)[0] # TODO: factor=0.1, justified?
#     # Compute angular momentum and normalise it.
#     sat = put.is_sat(RamsesDir,timestep, factor=factor)
#     # nsat = np.logical_not(sat)
#     J = vec_J[nsat]
#     norm_J1 = np.linalg.norm(J)
#     J1 = [i/norm_J1 for i in J]
#     # Compute two other orthogonal axes.
#     norm_J2 = np.linalg.norm([-J1[1], J1[0], 0])
#     J2 = [-J1[1]/norm_J2, J1[0]/norm_J2, 0]
#     J3 = np.cross(J1, J2)
#     # Check for fuckups (might have to replace '==0' by <1e-10 because of precision error).
#     if np.dot(J1, J2)>1e-15 or np.dot(J1, J3)>1e-15 or np.dot(J3, J2)>1e-15:
#         print('\t They are not all orthogonal, a fuckup must hide ?')
#         print(np.dot(J1, J2), np.dot(J1, J3), np.dot(J2, J3), '| should be 0.')
#     if np.linalg.norm(J1)>1.1 or np.linalg.norm(J2)>1.1 or np.linalg.norm(J3)>1.1 or np.linalg.norm(J1)<0.9 or np.linalg.norm(J2)<0.9 or np.linalg.norm(J3)<0.9:
#         print('\t They are not all normalised, is it bad news though ?')
#         print(np.linalg.norm(J1), np.linalg.norm(J2), np.linalg.norm(J3), '| should be 1.')
#     # Transition matrix (=matrice de passage) and coordinates in the new basis.
#     P = [J3, J2, J1]
#     cell_pos2 = np.array([np.dot(cell_pos[i,:],P) for i in range(len(cell_pos[:,0]))])
#     center2 = np.dot(center_cm,P)/cu2cm
#     return cell_pos2, center2


###################################################################################################################################
############################################################# Savings #############################################################
###################################################################################################################################
def save_img(map,cmap='viridis', name_file='kiwi.pdf'):
    """
    Saves a map. This is mainly a note as I always forget how to do it with imshow.

    Parameters:
    -----------
        maps: ?
            The map to be saved
        cmap: string
            Colormap used.
        name_file: string
            Name of the image saved.
        

    Example:
    --------
        Dude...
    """
    plt.imsave(name_file,np.log(map.T),origin='lower',cmap=cmap)


def make_img_bigger(data, multip):
    """
    If size is to small when saved (which happends when the map is uniform), artificially makes it multiple times bigger.

    Parameters:
    -----------
        data: ?
            The image I guess ? I don't remember...
        multip: scalar
            The factor used to change the size of the image.
        

    Example:
    --------
        Don't remember...
    """
    new_data = np.zeros(np.array(data.shape) * multip)

    for j in range(data.shape[0]):
        for k in range(data.shape[1]):
            new_data[j * multip: (j+1) * multip, k * multip: (k+1) * multip] = data[j, k]