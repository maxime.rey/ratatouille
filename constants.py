##############################
###### hardcoded values ######
##############################
# T_c, T_h   = 10**4.5, 10**5.3   # From KI/KR/DC PDF, output 204.. trying to avoid overlap of MgII/CIV/OVI as much as possible...
T_c, T_h   = 10**3.5, 10**4.5 # cold neutral and ionized and hot, see prism 2022, katz
perc_min, perc_max = 15.9, 84.1


##############################
######### Constants  #########
##############################
# Conversions
def wvlgth(E_eV): return h_cgs/ev2erg * c_cgs*1e8  / E_eV   # Converts from eV to Angström.
g2Msun = 1./1.9891e33
kpc2cm = 3.086e21
Myr2s  = (303*31536000+97*31622400)/400*1e6  # Gregorian calendar: 400 year cycle of 303 common years + 97 leap years.
ev2erg = 1.602176634e-12

# Constants
G      = 6.67e-11               # Universal gravitationnal constant [m^3/kg/s^2]
e_lya  = 1.634e-11              # Lyman-alpha energy [erg]   = [g*cm^2/s^2]
kB_cgs = 1.3806200e-16          # Boltzmann constant [erg/K]
h_cgs  = 6.62607015e-27         # Planck constant    [erg*s]
L_sun  = 3.828e33               # Sun luminosity     [erg/s]
c_cgs  = 2.9979250e+10          # Speed of light     [cm/s]

# Cosmology (from info_xxxxx.txt)
H0      = 67.1100006103516      # ...but you should use info['H0']. Maybe.
omega_m = 0.317499995231628
omega_b = 0.049000000000000
f_b     = omega_b/omega_m


##############################
####### Ions and such. #######
##############################
roman_nb  = {"I": 1, "II": 2, "III": 3, "IV": 4, "V": 5, "VI": 6, "VII": 7, "VIII": 8, "IX": 9, "X": 10, "XI": 11, "XII": 12}
atom_dict = {'H': 1, 'He': 2, 'Li': 3, 'Be': 4, 'B': 5, 'C': 6, 'N': 7, 'O': 8, 'F': 9, 'Ne': 10,
            'Na': 11, 'Mg': 12, 'Al': 13, 'Si': 14, 'P': 15, 'S': 16, 'Cl': 17, 'Ar': 18, 'K': 19, 'Ca': 20,}
# exclude_atoms = {el for el, max_state in atom_dict.items() if any(f'x{el}{numerals[s]}' in dict_index for s in range(1, max_state + 2))} # atoms already computed in the simulation
# all_ions = [f"{el}{numerals[state]}" for el, max_state in atom_dict.items() if el not in exclude_atoms for state in range(1, max_state + 2)] # all ions not simulated on-the-fly

Z_sun  = 0.0139                 # Asplund+21
Y_sun  = 0.2423                 # Asplund+21 - Helium mass fraction on the sun
X_sun  = 0.7438                 # Asplund+21 - Hydrogen mass fraction on the sun
ZO_sun = 0.0057326442           # Taysun     - Oxygen mass fraction on the sun

X_frac = 0.76
Y_frac = 0.24

# Masses
amu = 1.66053906892e-24         # Dalton/unified atomic mass unit [g] # TODO: should use amu and me below? Compute auto with dict above?
me  = 9.1093837139e-28          # Electron mass [g]
mH  = 1.6735575e-24             # Hydrogen atom mass [g]
ion_mass = {'HI': mH,
            'MgII': 4.0359398e-23,
            'CIV': 1.9944237e-23,
            'OVI': 2.6561e-23}  # Ion mass [g]
atm_weight_O = 15.9994          # atomic weight [g/mol]
atm_weight_H = 1.00784          # atomic weight [g/mol]

# Solar abundance of the element, eg. N_C,sun / N_H,sun.From Val's thesis, from Asplund+21
abund_dict = {'C' : 2.88e-4,    # Had 2.69 before
              'N' : 6.76e-5,
              'O' : 4.90e-4,
              'Ne': 1.15e-4,
              'Mg': 3.98e-5,    # Had 35.5 before
              'Al': 2.69e-6,
              'Si': 3.24e-5,
              'S' : 1.32e-5,
              'Fe': 2.88e-5}