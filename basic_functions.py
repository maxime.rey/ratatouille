"""
Basic & one-time use functions.
==============================
    - h5_save, h5_read, h5_clean, h5_list, h5rename
    - compute_aout
    - minimalmap
    - SFR_ff
    - arcsec2kpc
    - HHEcoolrate_RT, coolrate_RT (cooltime = -T/coolrate [s])
"""
# Astro and basic libraries
import minirats.utils.py.readwrite.ramses_info as ri
from minirats.utils.py import cellutils as cu
from ratatouille import readNsave as ras
from ratatouille import plotutils as put
from ratatouille.constants import *
from astropy import units as u
from astropy import cosmology
import h5py, os

# Data manipulation and plotting libraries
from matplotlib import colors, cm, pyplot as plt
from scipy import special
import numpy as np


###################################################################################################################################
######################################################## h5 base functions ########################################################
###################################################################################################################################
def h5_save(data,key,filename):
    """
    Save data with name key in filename if it does not already exists.
    """
    if os.path.exists(filename):
        f = h5py.File(filename,'r+')
    else:
        f = h5py.File(filename,'w')

    f.create_dataset(key,data=data)
    f.close()


def h5_read(key,filename):
    """
    Returns the data corresponding to the key.
    """
    with h5py.File(filename,'r') as f:
        try:
            data = f[key][:]
        except:
            print(f"No key {key} like this in {filename}.")
    return data


def h5_clean(fields,filename):
    """
    Delete the fields specified (if they exist).
    """
    if os.path.exists(filename):
        with h5py.File(filename,'r+') as f:
            for field in fields:
                try:
                    del f[field]
                except:
                    print(f"No key {field} like this in {filename}." )
    else:
        print(f'Wrong filename {filename}')


def h5_list(genpath,folder):
    """
    Lists the keys in an hdf5 file.
    
    Ex: h5_list(genpath+folders[1]+'ratadat/radcol.h5')
    """
    thefile = h5py.File(genpath+folder,'r+')
    print(len(thefile.keys()), list(thefile.keys()))


def h5rename(genpath, folders, timesteps, filename):
    for folder in folders:
        RamsesDir = genpath+folder

        filepath = f'{RamsesDir}/ratadat/{filename}.h5'
        thefile = h5py.File(filepath,'r+')

        # print all keys
        # print(len(thefile.keys()), list(thefile.keys()))

        # Replace oldname by oldname.
        for timestep in timesteps:
                oldname = f"somenamingscheme_{timestep:05d}"
                newname = f"diffnamingscheme_{timestep:05d}"
                thefile.create_dataset(newname,data=thefile[oldname])
                del thefile[oldname]


###################################################################################################################################
##################################################### one-time use functions #####################################################
###################################################################################################################################
def compute_aout(dt=10, z_init=15, nbins=10, zmin=None, checktimes=False):
    """ Generate sequence of aexp for evenly-timed (dt in Myrs) ramses outputs in the ramses namelist format. JB & modifs from MR."""
    cosmo = cosmology.FlatLambdaCDM(H0=70 * u.km / u.s / u.Mpc, Tcmb0=2.725 * u.K, Om0=0.3)
    dt   *= u.Myr 
    time_at_zmax = cosmo.age(z_init)
    if zmin:
        nbins = int(((cosmo.age(zmin) - time_at_zmax) / dt).decompose())+1
    times = ((time_at_zmax + range(nbins)*dt).to(u.Gyr))
    aout = []
    for t in times:
        zs = cosmology.z_at_value(cosmo.age,t)
        aout.append(1./(1.+zs))
    if checktimes:
        for a in aout: # If you want to check
            print((cosmo.age(1/a-1)).to(u.Myr))
    return aout


def minimalmap():
    """ Plot whatever quickly with this."""
    timestep=1
    RamsesDir = '1_cheap_runs/9_Agertz_code_menml256/'
    info    = ras.read_info(RamsesDir, timestep)
    center  = np.array([0.5,0.5,0.5])
    radius  = 0.5
    readRT  = False
    idens   = 1
    ivx     = 2
    ivy     = 3
    ivz     = 4
    ipre    = info['iP']+1
    ixHII   = info['iIons']+info['isH2']+1
    ixHeII  = info['iIons']+info['isH2']+2
    ixHeIII = info['iIons']+info['isH2']+3
    iZ      = info['iZ']+1
    cells2load = [idens,ipre,ivx,ivy,ivz,ixHII,ixHeII,ixHeIII,iZ]
    ncells = cu.py_cell_utils.count_cells(RamsesDir,timestep,-1,center, radius)
    cells,cell_pos,cell_l = cu.py_cell_utils.read_cells_hydro(RamsesDir,timestep,-1,ncells,cells2load,center, radius,readRT)
    lmax = cell_l.max()
    nx,ny = cu.py_cell_utils.get_map_nxny(lmax,0,1,0,1)
    map2plot,_ = cu.py_cell_utils.make_map_new(lmax,True,0,1,0,1,0,1,cell_l,np.ones_like(cells[:,-1]),cell_pos[:,0],cell_pos[:,1],cell_pos[:,2],cell_l,nx,ny)
    im = plt.imshow(map2plot.T,interpolation='nearest',origin='lower',norm=None, extent=(0,1,0,1))


def SFR_ff(what, thresh=1e-3, nblines=25):
    """
    Plots SFR_ff as a function of alpha_vir and the Mach number for Kim, Kretschmer and the ratio of both.
    You might want to use "%matplotlib widget" if in a notebook to interact with the figure. Use "%matplotlib inline" to go back to normal.
    """

    nbpoints = 100
    avir_rg = np.linspace(0.01,100,nbpoints)
    avir_rg = np.logspace(-2,2,nbpoints)
    # Mach_rg = np.linspace(1,100,100)
    Mach_rg = np.logspace(0,2,100)
    avir, Mach = np.meshgrid(avir_rg, Mach_rg)
    sig2 = np.log(1+0.4**2*Mach**2)

    scritKi =  np.log(0.067/0.33**2*avir*Mach**2)
    scritKr = np.log(avir*(1+2*Mach**4/(1+Mach**2)))

    SFRKi = 0.5/2/0.57*np.exp(3/8*sig2) * (1 + special.erf((sig2-scritKi)/np.sqrt(2*sig2)))
    SFRKr = 1/2*np.exp(3/8*sig2) * (1 + special.erf((sig2-scritKr)/np.sqrt(2*sig2)))

    
    fig = plt.figure(figsize=(10,10))
    ax = plt.axes(projection ='3d')
    if what=='Kimm':
        SFR_ff = SFRKi.copy()
        if thresh:
            SFR_ff[SFRKi<thresh] = np.nan
        colors = cm.inferno(norm(SFR_ff))
        norm = colors.LogNorm(np.nanmin(SFR_ff), np.nanmax(SFR_ff))
        ax.set_zlabel(r'$SFR_\mathrm{ff}$')
        ax.set_xlim([-2,2])
        ax.set_ylim([0,2])
        ax.set_zlim([-3,2])
    elif what=='Kretschmer':
        SFR_ff = SFRKr.copy()
        if thresh:
            SFR_ff[SFRKi<thresh] = np.nan
        norm = colors.LogNorm(np.nanmin(SFR_ff), np.nanmax(SFR_ff))
        colors = cm.inferno(norm(SFR_ff))
        ax.set_zlabel(r'$SFR_\mathrm{ff}$')
        ax.set_xlim([-2,2])
        ax.set_ylim([0,2])
        ax.set_zlim([-3,2])
    elif what=='ratio':
        SFR_ff = SFRKr/SFRKi
        if thresh:
            SFR_ff[(SFRKi>thresh)&(SFRKr>thresh)] = np.nan
        norm = colors.Normalize(np.nanmin(SFR_ff), np.nanmax(SFR_ff))
        colors = cm.inferno(norm(SFR_ff))
        ax.set_zlabel(r'$SFR_\mathrm{ff, Kr} / SFR_\mathrm{ff, Ki}$')
    
    if nblines:
        rcount, ccount = nblines, nblines
    else:
        rcount, ccount, _ = colors.shape
    map = ax.plot_surface(np.log10(avir), np.log10(Mach), np.log10(SFR_ff), rcount=rcount, ccount=ccount, facecolors=colors, shade=False)
    map.set_facecolor((0,0,0,0))
    ax.xaxis.set_major_formatter(plt.FuncFormatter(put.axis_format_10pow))
    ax.yaxis.set_major_formatter(plt.FuncFormatter(put.axis_format_10pow))
    ax.zaxis.set_major_formatter(plt.FuncFormatter(put.axis_format_10pow))
    ax.set_ylabel(r'$\mathcal{M}$')
    ax.set_xlabel(r'$\alpha_\mathrm{vir}$')
    plt.show()

def arcsec2kpc(size, z):
#‘’'
# Converts a size in arcsec into kpc
# Needs redshift z to calculate the conversion factor
# Marion Farcy, 04/19
#‘’'
    """
    Converts a size in arcsec units into kpc.
    Needs redshift value z to calculate the conversion factor.
    """
    from astropy.cosmology import FlatLambdaCDM
    cosmo = FlatLambdaCDM(H0=67.11, Om0=0.3175)
    conv = cosmo.arcsec_per_kpc_proper(z)
    size_kpc =   size / conv
    size = size_kpc.value
    return size


##############################################################################
##############################  Cooling stuff  ##############################
##############################################################################
def HHEcoolrate_RT(T, nH, xHII, xHeII, xHeIII, aexp):
    nHI    = nH*(1-xHII)                # H
    nHII   = nH*xHII                    # H+
    nHe    = 0.25*nH*Y_frac/X_frac      # nH/x = nHe/y & 0.25: 2p+2n 
    nHeI   = nHe*(1-xHeII - xHeIII)     # He
    nHeII  = nHe*xHeII                  # He+
    nHeIII = nHe*xHeIII                 # He++
    ne     = nHII + nHeII + 2*nHeIII    # e-

    f = 1+np.sqrt(T/1e5)

    # Coll. Ionization Cooling from Cen 1992 (via Maselli et al 2003)
    ci_HI    = 1.27e-21 * np.sqrt(T) / f * np.exp(-157809.1/T) * ne * nHI
    ci_HeI   = 9.38e-22 * np.sqrt(T) / f * np.exp(-285335.4/T) * ne * nHeI 
    ci_HeII  = 4.95e-22 * np.sqrt(T) / f * np.exp(-631515. /T) * ne * nHeII

    # Collisional excitation cooling from Cen'92
    ce_HI    = 7.5e-19 / f * np.exp(-118348./T)                * ne * nHI
    ce_HeI   = 9.10e-27 * T**(-0.1687) / f * np.exp(-13179./T) * ne * nHeI
    ce_HeII  = 5.54e-17 * T**(-0.397)  / f * np.exp(-473638./T)* ne * nHeII

    # Recombination Cooling (Hui&Gnedin'97)
    laHII    = 315614/T
    laHeII   = 570670/T
    laHeIII  = 1263030/T
    # if not rt_otsa: # Case A
    #    f = 1+(laHII/0.541)**0.502
    #    tbl_cr_r_HII   = 1.778e-29 * laHII**1.965 / f**2.697 * T
    #    tbl_cr_r_HeII  = 3e-14     * laHeII**0.654 * kB_cgs * T
    #    f = 1+(laHeIII/0.541)**0.502
    #    tbl_cr_r_HeIII = 14.224e-29 * laHeIII**1.965 / f**2.697 * T
    # Case B (rt_otsa)
    f = 1+(laHII/2.25)**0.376
    r_HII    = 3.435e-30 * laHII**1.97 / f**3.72   * T * ne * nHII
    r_HeII   = 1.26e-14  * laHeII**0.75 * kB_cgs   * T * ne * nHeII
    f        = 1+(laHeIII/2.25)**0.376
    r_HeIII  = 27.48e-30 * laHeIII**1.97 / f**3.72 * T * ne * nHeIII

    # Bremsstrahlung from Osterbrock & Ferland 2006
    bre    = 1.42e-27 * 1.5 * np.sqrt(T) * (ne * nHII + ne * ( nHeII + 4. * nHeIII ))

    # Compton Cooling from Haimann et al. 96, via Maselli et al.
    # Need to make sure this is done whenever the redshift changes#
    Ta    = 2.727/aexp
    com   = 1.017e-37 * Ta**4 * (T-Ta) * ne

    # Dielectronic recombination cooling, from Black 1981
    f     = 1.24e-13*T**(-1.5)*np.exp(-470000/T)
    die   = f*(1+0.3*np.exp(-94000/T)) * ne * nHeII

    # Overall Cooling
    compCoolrate = ci_HI + r_HII + ce_HI + com + bre + ci_HeI + r_HeII + ce_HeI + ci_HeII + r_HeIII + ce_HeII  + die
    return compCoolrate


def coolrate_RT(T, nH, Z, xHII, xHeII, xHeIII, aexp):
    """Compute cooling due to metals"""
    # Cloudy at solar metalicity
    temperature_cc07 = [3.9684,4.0187,4.0690,4.1194,4.1697,4.2200,4.2703,4.3206,4.3709,4.4212,4.4716,4.5219,4.5722,4.6225,4.6728,4.7231,4.7734,4.8238,4.8741,4.9244,4.9747,5.0250,5.0753,5.1256,5.1760,5.2263,5.2766,5.3269,5.3772,5.4275,5.4778,5.5282,5.5785,5.6288,5.6791,5.7294,5.7797,5.8300,5.8804,5.9307,5.9810,6.0313,6.0816,6.1319,6.1822,6.2326,6.2829,6.3332,6.3835,6.4338,6.4841,6.5345,6.5848,6.6351,6.6854,6.7357,6.7860,6.8363,6.8867,6.9370,6.9873,7.0376,7.0879,7.1382,7.1885,7.2388,7.2892,7.3395,7.3898,7.4401,7.4904,7.5407,7.5911,7.6414,7.6917,7.7420,7.7923,7.8426,7.8929,7.9433,7.9936,8.0439,8.0942,8.1445,8.1948,8.2451,8.2955,8.3458,8.3961,8.4464,8.4967]
    # Cooling from metals only (without the contribution of H and He)
    # log cooling rate in [erg s-1 cm3]
    # S. Ploeckinger 06/2015
    excess_cooling_cc07 = [-24.9082,-24.9082,-24.5503,-24.0898,-23.5328,-23.0696,-22.7758,-22.6175,-22.5266,-22.4379,-22.3371,-22.2289,-22.1181,-22.0078,-21.8992,-21.7937,-21.6921,-21.5961,-21.5089,-21.4343,-21.3765,-21.3431,-21.3274,-21.3205,-21.3142,-21.3040,-21.2900,-21.2773,-21.2791,-21.3181,-21.4006,-21.5045,-21.6059,-21.6676,-21.6877,-21.6934,-21.7089,-21.7307,-21.7511,-21.7618,-21.7572,-21.7532,-21.7668,-21.7860,-21.8129,-21.8497,-21.9035,-21.9697,-22.0497,-22.1327,-22.2220,-22.3057,-22.3850,-22.4467,-22.4939,-22.5205,-22.5358,-22.5391,-22.5408,-22.5408,-22.5475,-22.5589,-22.5813,-22.6122,-22.6576,-22.7137,-22.7838,-22.8583,-22.9348,-23.0006,-23.0547,-23.0886,-23.1101,-23.1139,-23.1147,-23.1048,-23.1017,-23.0928,-23.0969,-23.0968,-23.1105,-23.1191,-23.1388,-23.1517,-23.1717,-23.1837,-23.1986,-23.2058,-23.2134,-23.2139,-23.2107]
    z_courty            = [0.00000,0.04912,0.10060,0.15470,0.21140,0.27090,0.33330,0.39880,0.46750,0.53960,0.61520,0.69450,0.77780,0.86510,0.95670,1.05300,1.15400,1.25900,1.37000,1.48700,1.60900,1.73700,1.87100,2.01300,2.16000,2.31600,2.47900,2.64900,2.82900,3.01700,3.21400,3.42100,3.63800,3.86600,4.10500,4.35600,4.61900,4.89500,5.18400,5.48800,5.80700,6.14100,6.49200,6.85900,7.24600,7.65000,8.07500,8.52100,8.98900,9.50000]
    phi_courty          = [0.0499886,0.0582622,0.0678333,0.0788739,0.0915889,0.1061913,0.1229119,0.1419961,0.1637082,0.1883230,0.2161014,0.2473183,0.2822266,0.3210551,0.3639784,0.4111301,0.4623273,0.5172858,0.5752659,0.6351540,0.6950232,0.7529284,0.8063160,0.8520859,0.8920522,0.9305764,0.9682031,1.0058810,1.0444020,1.0848160,1.1282190,1.1745120,1.2226670,1.2723200,1.3231350,1.3743020,1.4247480,1.4730590,1.5174060,1.5552610,1.5833640,1.5976390,1.5925270,1.5613110,1.4949610,1.3813710,1.2041510,0.9403100,0.5555344,0.0000000]
    c1     = 0.4
    c2     = 10.0
    T0    = 1e5
    TC    = 1e6
    alpha1 = 0.15

    rate = np.full(np.shape(T),np.nan)
    Crate = np.full(np.shape(T),np.nan)
    for icell, TT in enumerate(T):
        ZZ=1/aexp-1
        lT=np.log10(TT)
        nHcell = nH[icell]
        

        # This is a simple model to take into account the ionization background
        # on metal cooling (calibrated using CLOUDY).
        iZ=1+int(ZZ/z_courty[49]*49.)
        iZ=min(iZ,49)
        iZ=max(iZ,1)
        deltaZ=z_courty[iZ]-z_courty[iZ-1]
        ZZ=min(ZZ,z_courty[49])
        ux=1e-4*(phi_courty[iZ]*(ZZ-z_courty[iZ-1])/deltaZ + phi_courty[iZ-1]*(z_courty[iZ]-ZZ)/deltaZ )/nHcell
        g_courty=c1*(TT/T0)**alpha1+c2*np.exp(-TC/TT)
        f_courty=1/(1+ux/g_courty)

        if(lT>=temperature_cc07[90]):
            metal_tot=0
        elif(lT >= 1.0):
            lcool1=-100
            if(lT>=temperature_cc07[0]):
                iT=1+int((lT-temperature_cc07[0])/(temperature_cc07[90]-temperature_cc07[0])*90)
                iT=min(iT,90)
                iT=max(iT,1)
                deltaT=temperature_cc07[iT]-temperature_cc07[iT-1]
                lcool1        = excess_cooling_cc07[iT]*(lT-temperature_cc07[iT-1])/deltaT + excess_cooling_cc07[iT-1]*(temperature_cc07[iT]-lT)/deltaT
            # Fi+ne structure cooling from infrared lines
            lcool2=-31.522879+2*lT-20.0/TT-TT*4.342944e-5
            # Total metal cooling and temperature derivative
            metal_tot=10**lcool1+10**lcool2
            metal_tot=metal_tot*f_courty
        else:
            metal_tot=0
        metal_tot   = metal_tot*nHcell**2
                                        # [erg/K]
        X_nHkb   = X_frac/(1.5 * nHcell * kB_cgs)                                        # [K cm³/erg/]
        Crate[icell] = HHEcoolrate_RT(T[icell], nH[icell], xHII[icell], xHeII[icell], xHeIII[icell], aexp)  # [erg s⁻¹cm⁻³]
        Hrate = 0
        rate[icell] = X_nHkb*(Hrate - Crate[icell] - Z[icell]/Z_sun*metal_tot)   # [K/s]

    return rate