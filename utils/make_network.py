#!/usr/bin/env python

"""
Code that creates the equilibrium equations file react_ions.

There are 4 option you can change:
- the elements,
- their ionisation states,
- consider or not the photoionisation,
- how to deal with H and He (automatically included).

Examples:
-------
    python make_network.py -e 6 8 12 13 14 26 -i 4 1 2 2 4 2 -np -HHe 3
    python make_network.py -e 12 -i 2 -HHe 1

"""

__author__ = "Valentin Mauerhofer & Maxime Rey"

import numpy as np
import argparse



########################################################################
############################# Definitions #############################
########################################################################
def RR_Badnell(Z,N,M=1):
    if(N<0 or N>Z-1):
        raise ValueError('Impossible number %i of bounded electrons before recombination, for element %s'%(N,elements_name[Z-1]))
    else:
        M_max = np.amax(M_RR[(Z_RR==Z)&(N_RR==N)])
        if M>M_max:
            M=M_max
        RR = str(A[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '*( sqrt(T/' + str(T0[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + ') * (1+sqrt(T/' + str(T0[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '))**('
        if(C[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]==0e0):
            RR = RR + '1-' + str(B[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0])
        else:
            RR = RR + '1-(' + str(B[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '+' + str(C[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '*exp(-' + str(T2[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '/T))'
        RR = RR + ') * (1+sqrt(T/' + str(T1[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '))**('
        if(C[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]==0e0):
            RR = RR + '1+' + str(B[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0])
        else:
            RR = RR + '1+(' + str(B[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '+' + str(C[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '*exp(-' + str(T2[(Z_RR==Z)&(M_RR==M)&(N_RR==N)][0]) + '/T))'
        RR = RR + ') )**(-1)'
        RR = RR.replace('e-','d-')
        RR = RR.replace('e+','d+')
        return RR


def DR_Badnell(Z,N,M=1):
    if(N<0 or N>Z-1):
        raise ValueError('Impossible number %i of bounded electrons before recombination, for element %s'%(N,elements_name[Z-1]))
    elif(N==0):
        return '0d0'
    else:
        if(Z==1):
            M=1
        else:
            # ~ print M_DR[(Z_DR==Z)&(N_DR==N)]
            M_max = np.amax(M_DR[(Z_DR==Z)&(N_DR==N)])
            if M>M_max:	
                M=M_max
        DR = 'T**(-1.5)*(' + str(C_DR[0][(Z_DR==Z)&(M_DR==M)&(N_DR==N)][0]) + '*exp(-' + str(E_DR[0][(Z_DR==Z)&(M_DR==M)&(N_DR==N)][0]) + '/T)'
        i=1
        while((C_DR[i][(Z_DR==Z)&(M_DR==M)&(N_DR==N)][0] != 0e0) & (i<9)):
            DR = DR + ' + ' + str(C_DR[i][(Z_DR==Z)&(M_DR==M)&(N_DR==N)][0]) + '*exp(-' + str(E_DR[i][(Z_DR==Z)&(M_DR==M)&(N_DR==N)][0]) + '/T)'
            i = i+1
        DR = DR + ')'
        DR = DR.replace('e-','d-')
        DR = DR.replace('e+','d+')
        return DR


def Col_Voronov(Z,N):
    if(N<=0 or N>Z):
        raise ValueError('Impossible number %i of bounded electrons before collision, for element %s'%(N,elements_name[Z-1]))
    elif(Z < 3):
        return 'auto'
    else:
        Col = str(A_vor[(Z-1)*Z//2 + Z-N - 3]) + '*(1 + ' + str(P[(Z-1)*Z//2 + Z-N - 3]) + '*sqrt(' + str(dE[(Z-1)*Z//2 + Z-N - 3]) + '*1.16045d4/T)) / (' + str(X[(Z-1)*Z//2 + Z-N - 3]) + '+' + str(dE[(Z-1)*Z//2 + Z-N - 3]) + '*1.16045d4/T) * (' + str(dE[(Z-1)*Z//2 + Z-N - 3]) + '*1.16045d4/T)**' + str(K[(Z-1)*Z//2 + Z-N - 3]) + ' * exp(-' + str(dE[(Z-1)*Z//2 + Z-N - 3]) + '*1.16045d4/T)'
        Col = Col.replace('e-','d-')
        Col = Col.replace('e+','d+')
        return Col



########################################################################
################################# Main #################################
########################################################################

####################################
############ Arguments ############
####################################
parser=argparse.ArgumentParser(
    description='Code that creates equilibrium equations file react_ions.',
    epilog='', formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-e', '--elements', type=int, nargs='+', 
    help="Choose the list of element wanted (3=Li, 4=Be, etc. until 26=Fe). \nHydrogen and helium are automatically included.", required=True)
parser.add_argument('-i', '--ions_state', type=int, nargs='+', 
    help="Number of ionisation states followed for each element. \
    \nKrome will contain one more ionization stage than chosen here. \
    \nFor example, only OI is followed (-i 1), Krome will also contain OII to include reactions like OI + e -> OII + e + e", required=True)
parser.add_argument('-pti', '--path2ions', type=str,
    help="Path to the rates folder in ions (to get recombinations, collisions and photoionisation rates).")
parser.add_argument('-np', '--no_photoionisation', action="store_false",
    help="Use this option if you do not want to include photoionization reactions.")
parser.add_argument('-HHe', '--H_He_option', type=int,
    help="Select what you want to do with hydrogen and helium.\
    \n1 - Put rate to 0d0, useful if you want the variables for H and He in your code,\
    \n    but don't want to modify their state. It's what I do usually\
    \n2 - Use rate from Badnell for recombination and from Voronov for ionization, like the other elements\
    \n3 - Use rate from Krome, don't really know where it comes from.")
args = parser.parse_args()

elements_Z = args.elements
max_ion = args.ions_state
path_2_ions = args.path2ions

if args.no_photoionisation:
    Photo = True
else:
    Photo = False

if args.H_He_option is None:
    H_He_option = 1
else:
    H_He_option = args.H_He_option
    if H_He_option>3:
        raise ValueError('HHe is %i but should be 1, 2 or 3'%(H_He_option))


####################################
######### Initialisations #########
####################################
file_name       = 'react_ions'
elements_name   = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 
                    'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe']
count   = 1
network = ''

if len(max_ion)!=len(elements_Z):
    raise ValueError('The number of maximal ionisation states of the elements does not match the number of elements.')

#Data for radiative recombination, from Badnell
Z_RR, N_RR, M_RR, W_RR, A, B, T0, T1, C, T2 = np.genfromtxt(path_2_ions+'/rates/Radiative_rec/clist', unpack=True, skip_header=3)

#Data for dielectronic recombination, from Badnell
Z_DR, N_DR, M_DR, W_DR = np.genfromtxt(path_2_ions+'/rates/Dielectronic_rec/clist_c', unpack=True, skip_header=3, usecols=(0,1,2,3))
C_DR = np.genfromtxt(path_2_ions+'/rates/Dielectronic_rec/clist_c', unpack=True, skip_header=3, usecols=(4, 5, 6, 7, 8, 9, 10, 11, 12))
E_DR = np.genfromtxt(path_2_ions+'/rates/Dielectronic_rec/clist_E', unpack=True, skip_header=3, usecols=(4, 5, 6, 7, 8, 9, 10, 11, 12))

#Data for collisional ionization, from Voronov (taken from Krome database)
dE,P,A_vor,X,K = np.genfromtxt(path_2_ions+'/rates/Collisions/Voronov/clist', unpack=True)


####################################
########## Recombinations ##########
####################################
network += '@var: T = Tgas \n \n \n#RECOMBINATION, from Badnell website : \n#http://amdpp.phys.strath.ac.uk/tamoc/RR \n#http://amdpp.phys.strath.ac.uk/tamoc/DR \n \n@format:idx,R,R,P,rate \n'
for i in [1,2]:
    for j in range(i):
        network += str(count) + ',' + elements_name[i-1] + '+'*(j+1) + ',E,' + elements_name[i-1] + '+'*j
        if(H_He_option == 1):
            network += ', 0d0 \n'
        elif(H_He_option == 2):
            network += ', ' + RR_Badnell(i,i-j-1) + ' + ' + DR_Badnell(i,i-j-1) + '\n'
        elif(H_He_option == 3):
            network += ', auto \n'
        else:
            raise ValueError('Unknown option for H_He_option, choose either 1, 2 or 3.')
        count += 1
network += '\n'
k=0
for i in elements_Z:
    for j in range(min(i,max_ion[k])):
        network += str(count) + ',' + elements_name[i-1] + '+'*(j+1) + ',E,' + elements_name[i-1] + '+'*j
        if((i==16) or (i==26)):
            network += ', auto \n'
        else:
            network += ', ' + RR_Badnell(i,i-j-1) + ' + ' + DR_Badnell(i,i-j-1) + '\n'
        count += 1
    k+=1
    network += '\n'


####################################
############ Collisions ############
####################################
network += '\n \n#COLLISIONS, from Voronov 1997 \n \n@format:idx,R,R,P,P,P,rate \n'
for i in [1,2]:
    for j in range(i):
        network += str(count) + ',' + elements_name[i-1] + '+'*j + ',E,' + elements_name[i-1] + '+'*(j+1)  + ',E,E, '
        if(H_He_option == 1):
            network += '0d0 \n'
        elif(H_He_option == 2):
            print('Voronov rate for H and He not available yet, so I put krome automatic rates')
            network += 'auto \n'
        elif(H_He_option == 3):
            network += 'auto \n'
        else:
            raise ValueError('Unknown option for H_He_option, choose either 1, 2 or 3.')
        count+=1
k=0
for i in elements_Z:
    for j in range(min(i,max_ion[k])):
        network += str(count) + ',' + elements_name[i-1] + '+'*j + ',E,' + elements_name[i-1] + '+'*(j+1)  + ',E,E, '
        if((i==16) or (i==26)):
            network += 'auto \n'
        else:
            network += Col_Voronov(i,i-j) + '\n'
        count += 1
    k+=1
    network += '\n'


####################################
######### Photoionisation #########
####################################
if(Photo):
    network += '\n \n#Photoionization \n \n@photo_start \n@format:idx,R,P,P,rate \n'
    k=0
    for i in elements_Z:
        for j in range(min(i,max_ion[k])):
            network += str(count) + ',' + elements_name[i-1] + '+'*j + ',' + elements_name[i-1] + '+'*(j+1) + ',E, auto \n'
            count += 1
        k+=1
    network += '\n@photo_stop \n'
    

####################################
########### Writing file ###########
####################################
f=open(file_name,'w')
f.write(network)
f.close()