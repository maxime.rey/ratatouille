"""
Functions to make movies:
==============================

- Deal with images:
    use_Joki_code, combine_img
- Make movies:
    make_mov, make_movs, make_mov_from_maps

------------------------------

The use of the main function (make_movs) is shown in movies.ipynb.
"""
import ratatouille.ram.ram as ram
import ratatouille.maps as maps
import subprocess, os, sys, re
import numpy as np


###################################################################################################################################
############################################################## Steps ##############################################################
###################################################################################################################################
def use_Joki_code(dirsimu, dirinit):
# os.system('python /home/mrey/clones/minirats/utils/py/ram/ram.py -c ' + '/scratch/mrey/fullbox_movie_1.ini')
    """
    Creates images from the binary files in the runs specified and saves them in a file called 'pngs' in each dirsimu.
    They should and will all share the same .ini file.

    Parameters:
    -----------
        dirsimu: path
            Path to the simulation output.
        dirinit: path
            Path to the initialisation file (.ini).

    Example:
    --------
        dirsimu = [path/to/simu1, path/to/simu2]
        dirinit = /path/to/init.ini
        use_Joki_code(dirsimu, dirinit)
    """
    if not isinstance(dirsimu, list):
        raise ValueError("'dirs' is not a list. It should be.")
    current_dir = os.getcwd()
    for dir_run in dirsimu:
        os.chdir(dir_run)   # this way, ram knows where the folder is. TODO: adapt if no access to it (if someone else ran the sim).
        # You might have to adapt str(ram)[36:-2] for it to be like ~/some_path/ratatouille/ram/ram.py
        os.system('python ' + str(ram)[36:-2] + ' -c ' + dirinit)
    os.chdir(current_dir) # return in original directory.


def combine_img(dirs, size_png ='', tiles_geom='', pix_btw_img=10, bckgrnd='black', add_label=True):
    """
    Combine images from different runs. 
    The created images are then saved in the folder 'fusion_png' in the current directory.
    
    Parameters:
    -----------
    dirs: array
        Array containing the path to the simulation considered.
    size_png: string (str(int) + 'x' + str(int))
        Size of the result in pixels (for ex. '1920x1080')
    tiles_geom: string (str(int) + 'x' + str(int))
        Geometry of the figure  (for ex. '3x1')
    pix_btw_img: int
        Number of pixels between the images.
    bckgrnd: string
        Color of the background.
    add_label: boolean
        Parameter to give a label to the videos.

    Example:
    --------
        dirs = [path/to/simu1, path/to/simu2]
        combine_img([dir+'pngs' for dir in dirs], tiles_geom="2x1")
    """
    # Some tests.
    if not isinstance(dirs, list):
        raise Warning(f"Please input your entries as a list, not \n {dirs}")
    if len(dirs)==1:
        raise Warning("What are you doing ? You just used one file. Directly use movies.py")

    # Make directory to store the images.
    newpath = os.getcwd() + '/fusion_png' 
    print('The combined images are stored in ', newpath)
    if os.path.exists(newpath):
        import shutil
        print("File /fusion_png already exists. Overwriting it.")
        shutil.rmtree(newpath)
    os.makedirs(newpath)

    # Montage parameters.
    space_imgs = '+' + str(pix_btw_img) + '+' + str(pix_btw_img)
    geometry = '-geometry ' + size_png + space_imgs + ' '

    if tiles_geom != '':  tiles_geom = '-tile ' + tiles_geom + ' '

    background = '-background ' + bckgrnd + ' '

    # Loop on the pngs and combine 'em.
    data=dirs.copy()
    for index, filename in enumerate(dirs):
        data[index] = [dirs[index] + '/' + os.listdir(filename)[index2] for index2,_ in enumerate(os.listdir(filename))]

    nb_img = len(data[0])
    if nb_img<100:
        print("Not enough images, the loading bar is not going to be precise")
    for folder in data:
        if len(folder)!=nb_img: 
            raise Warning("Some folders do not have the same number of images")
    
    print('Combining images')
    for index,_ in enumerate(data[0]):
        [pngs_list.sort() for pngs_list in data]
        pngs_tuple = [pngs_list[index] for pngs_list in data]       # the i-th png of each list in data (= in each file) 
        pngs_string = ' '.join(pngs_tuple)                          # Obtain the string 'file1/pngx file2/pngx [...]'.
        out_name = 'fusion_png/'+pngs_string[-9:]
        if add_label:
            label = ' '.join(['-label ' + png.rsplit('/', 4)[-3]  + ' ' + png + ' -fill white -gravity south ' for png in pngs_tuple])
            command = 'montage ' + label + ' ' + ' -pointsize 30 ' + geometry + tiles_geom + background + ' ' + out_name 
        else: # not sure this one works
            command = 'montage ' + ' ' + ' -pointsize 30 ' + geometry + tiles_geom + background + ' ' + pngs_string + ' ' + out_name 
        os.system(command) 
        
        # Making progress bar.
        bar_size=50
        progress = index/nb_img
        bar_progress = round(bar_size*progress)
        pourcentage = int(progress*100)+1
        prefix = "Computing " if pourcentage!=100 else "   Done   "
        bar_content = f'{prefix}[{"#"*bar_progress + "."*(bar_size-bar_progress)}] {pourcentage}% \r'
        sys.stdout.write(bar_content)
        sys.stdout.flush() 
    sys.stdout.write(f'{prefix}[{"#"*bar_size}] {100}% \r')
    sys.stdout.write("\n")
    sys.stdout.flush()
    print('Images combined')


def make_mov(dir_pngs, fps=25, quality=23, output='movie.mp4', hush=False, kill=True):
    """ 
    Creates and saves a movie in the current folder.
    
    Parameters:
    -----------
    dir_pngs: path (ex. fusion_png/%05d.png)
        Path to the folder containing the pngs (called 'pngs'). It should contain the folder path with the file format contained (eg. fusion_png/%05d.png)
    fps: int
        Number of frames per second in the video.
    quality: int
        Quality of the video, a lower value corresponds to a better quality, 15-25 is usually good.
    output: string
        Name of the movie created.
    hush: bolean
        If set to True, prints less stuff.
    kill: bolean
        If set to True and the movie already exists, overwrites it.

    Example:
    --------
        actual_folder = os.getcwd()
        make_mov(actual_folder + '/fusion_png/' + '%05d.png')
    """
    # check tis a file format.
    if os.path.exists(os.getcwd()+'/'+output) and not kill:
        raise Warning("You were about to erase another innocent movie !")

    loglevel='-loglevel quiet' if hush else ''
    
    print("Making movie [...]")
    print(f"ffmpeg {loglevel} -i {dir_pngs} -y -vcodec h264 -pix_fmt yuv420p\
             -r {fps} -filter:v 'setpts={60./float(fps)}*PTS'  -vf 'pad=ceil(iw/2)*2:ceil(ih/2)*2' -crf {quality} {output}")
    subprocess.call(f"ffmpeg {loglevel} -i {dir_pngs} -y -vcodec h264 -pix_fmt yuv420p\
             -r {fps} -filter:v 'setpts={60./float(fps)}*PTS'  -vf 'pad=ceil(iw/2)*2:ceil(ih/2)*2' -crf {quality} {output}", shell=True)              
    print("The movie is done !")


###################################################################################################################################
####################################################### THE function to use #######################################################
###################################################################################################################################


def make_movs(dirs, dirinit, tiles_geom="3x1", fps=25, quality=23, output='movie.mp4', hush=False, kill=True, keepimg=True):
    # Did not test everything, might be some bugs !
    """
    Creates a movie from the binary file(s) in the run(s) specified.
    
    Parameters:
    -----------
        dirs: array
            Array containing the paths to the simulations considered.
        dirinit: path
            Path to the initialisation file (.ini).
        tiles_geom: string (str(int) + 'x' + str(int))
            Geometry of the figure  (for ex. '3x1')
        keepimg (TO BE CODED): bolean
            If sets to True, keeps the images created. Erases them otherwise.

    Example:
    --------
        dirsimu = [path/to/simu1, path/to/simu2]
        dirinit = /path/to/init.ini
        make_movs(dirsimu, dirinit)
    """
    len_dirs = len(dirs)
    nb_tiles = int(tiles_geom[0])*int(tiles_geom[-1])

    # several tests
    if not isinstance(dirs, list):
        raise ValueError("'dirs' is not a list. It should be.")
    
    if int(tiles_geom[-1])!=int(tiles_geom[2]):
        raise ValueError("Seems like the 'tiles_geom' you input is incorrect. \
            The 2 values should be inferior to 10 and look like 1x3")

    if len_dirs!=nb_tiles:
        raise ValueError("The number of tiles does not correspond to the number of dirs")

    # check if images do not already exist.
    for dir in dirs:
        if not isinstance(dir, str):
            raise ValueError(f'{dir} is not a string')
        if os.path.exists(dir+'/pngs/') and os.listdir(dir+'/pngs/'): # If folder axists and is not empty
            print(f'Using images previously existing for {dir.rsplit("/", 1)[-1]}.')
        else:
            if dir.rsplit('/', 1)[-1]=='':
                print('Creating images for the simulation ', dir.rsplit('/', 1)[-2])
            else:
                print('Creating images for the simulation ', dir.rsplit('/', 1)[-1])
            use_Joki_code([dir], dirinit)
            if not os.listdir(dir+'/pngs/'):
                raise ValueError(f"No images were created for {dir}, there is a problem.")

    # check if the movie is made for a single dir or if the images have to be combined
    if len_dirs>1:
        if not output:
            output = 'movie.mp4'
        combine_img([dir+'pngs' for dir in dirs], tiles_geom=tiles_geom)
        make_mov(os.getcwd() + '/fusion_png/' + '%05d.png', fps=fps, quality=quality, output=output, hush=hush, kill=kill)
    else:
        if not output:
            output = os.path.basename(os.path.normpath(dir))
        make_mov(dirs[0]+'pngs/%*.png', fps=fps, quality=quality, output=output, hush=hush, kill=kill)

    if not keepimg: # TO DO
        raise ValueError("Haven't done this function yet")
        # import shutil
        # print('Erasing generated images')
        # shutil.rmtree([dir+'/pngs/' for dir in dirs])
        # print('Images successfully erased.')


# def del_pngs(): # To include
#     import subprocess
#     subprocess.call("rm -r fusion_png/", shell=True)
#     print("Images erased")

# if __name__ == "__main__":
#     dirs = sys.argv[1:]
#     make_movs(dirs)


def make_mov_from_maps(RamsesDir, timesteps, factor=1, var_str='nH', wei_str='nH', style='proj', proj='y',
    is_cbar=True, scalebar=True, vmin=None, vmax=None, logscale=True, lmax=None, imsave=None, alt_path=None, movie_output=None):

    if imsave:
        fdname = os.path.basename(os.path.normpath(RamsesDir))
        base_name = f"{fdname}_{var_str}_{factor}{proj}"
        if not movie_output: movie_output = f"{base_name}.mp4"      # Set movie title
        pattern = re.compile(rf"{base_name}_(\d{{5}})\.png")        # Remove maps already computed
        tstep_done = [int(pattern.match(f).group(1)) for f in os.listdir(imsave) if pattern.match(f)]
        timesteps = np.setdiff1d(timesteps, tstep_done)

    if not os.path.exists(imsave):
        os.makedirs(imsave)
    
    for timestep in timesteps:
        maps.qtymap(RamsesDir, timestep, factor=factor, var_str=var_str, wei_str=wei_str, style=style, proj=proj, noshow=True, # TODO: change lmax back
               is_cbar=is_cbar, scalebar=scalebar, vmin=vmin, vmax=vmax,logscale=logscale, lmax=None, imsave=imsave, alt_path=alt_path, fmt="png")

    # Create a movie using ffmpeg
    overwrite=True
    apple_version = True # True = IOS version

    quality = 18
    slowmo  = 2
    fps     = 25
    ow      = '-y' if overwrite else ''
    img_name= f"{imsave}/{base_name}" if imsave != True else base_name
    adapt_to_os = f"-vf 'pad=ceil(iw/2)*2:ceil(ih/2)*2,setpts={str(slowmo)}*PTS' -pix_fmt yuv420p" if apple_version else f"-filter:v 'setpts={slowmo}*PTS'" 
    cmd = f"ffmpeg -r {fps} -i {img_name}_%05d.png {adapt_to_os} -vcodec libx264 -crf {quality} {imsave}/../{movie_output} {ow}"

    result = subprocess.run(cmd, shell=True, capture_output=True, text=True)
    if result.returncode != 0:
        raise IOError(f"Error: {result.stderr}")
    else:
        print(f"Done!")