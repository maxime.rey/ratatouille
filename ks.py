"""
Utils to compute and plot KS-related quantities: 
================================================
    - KS & plot:  KS_from_file, KS_th, plot_KS, plot_KS_better.
    - KS & plot2: ks_annuli, ks_square
    - sig_gas:    compute_sig_gas_HR, rebinv1, rebinv2, rebinv3, rebinv4.
    - sig_sfr:    compute_2Dsfr, compute_2Dsfr_conv.
    - utils:      mefit, reshape.

The use of thos functions is shown in KS_law.ipynb.

Common parameters:
==================
        center, rad_kpc: array of size 3, scalar
            Center and radius considered in [kpc].
        rad_tot: scalar
            Total radius considered in [kpc] (equivalent to rad_kpc in square methods).

        sig_gas, nb_makemap_bins:
            Result of the function compute_sig_gas_HR (high resolution).
        rebin: '1' (or 'div'), '2' (or 'avg'), '3' (or 'sum') or '4' (conv).
            It is possible to choose between 4 methods to rebin ('group') the gas surface density to the proper bin size.
            If method 1 is chosen, the limit is set to 1.1. If you want to change it, change the code...
            If method 4 is chosen (it is advised to do so), a added parameter "thresh" is available to set a lower SFR limit as the resolution has an impact on the result.
            For each of them it is also possible to plot the 2D histograms of either the gas or the sfr.

        lim_sfr: int
            Age limit for the SFR in [Myr], usually 10 or 100.
        thresh:
            If different than None, sets a threshold in sfr. This is usefull to remove possible resolution effects, typically seen with rebinv4.
        hist_lims: (float, float)
            The lower and upper range of the bins. Set to the min and max values if set to None.
        binsize_aim: scalar
            Wished size of the bins in [kpc].
        bestfit:
            If set to true, returns values of the best_fit with sigma_sfr and its coef. Otherwise, returns None and None.

        logscale: boolean
            Plots in logscale if set to True and in linear scale otherwise.
        plot_data: boolean
            Plots data points if set to True and only the best fit otherwise.
"""
# Astro libraries
from minirats.utils.py import cellutils as cu
import minirats.utils.py.readwrite.ramses_info as ri
from ratatouille import readNsave as ras
from ratatouille.constants import *
from ratatouille import maps

# Other libraries
import matplotlib.pyplot as plt
import scipy.ndimage.filters as fil
import numpy as np
import sys


###################################################################################################################################
########################################################### KS and plot ###########################################################
###################################################################################################################################

def KS_from_file(RamsesDir, timestep, rad_kpc, rebin='4', binsize_aim=1., lim_sfr=100, thresh=None, bestfit=True, factor=1, saveinfile=True, plot_gas=False, plot_sfr=False, hnum=None):
    """
    Computes the two components of the Kennicutt Shmidt relation by re-binning a minirats high resolution gas map and computing the SFR density.
    Returns the arrays sigma_gas [Msun/pc²],  sigma_sfr [Msun/yr/kpc²], sig_sfr_fit [Msun/yr/kpc²] and the coefs of the best fit.
    
    Parameters:
    -----------
        plot_gas, plot_sfr: booleans
            If set to true, plots the map corresponding to the variable name. 
            Note that it is not possible to plot_gas and use a threshold at the same time because I'm too lazy to make it possible.

    Example:
    --------
        timestep = 17
        genpath = '/path/to/simu/'
        rad_kpc = 20
        sig_gas, sig_sfr, sig_sfrfit, coefs = KS_from_file(RamsesDir, timestep, rad_kpc, rebin='4', binsize_aim=1., lim_sfr=100, thresh=None)
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2cm  = info['unit_l'] * info['boxlen']
    cu2kpc =  cu2cm / kpc2cm
    center_kpc = [cen * cu2kpc for cen in ras.get_Gcen(RamsesDir,timestep,hnum=hnum)]
    # read stars
    star_x,star_y,star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_x','star_y','star_age','star_minit'],factor=factor)

    # read cells
    rho, cell_pos, cell_l = ras.extract_cells(RamsesDir,timestep,['rho','cell_pos','cell_l'],factor=factor) # [cm] and [g/cm^3]

    # computes high resolution sigma gas
    sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center_kpc, rad_kpc, cell_pos, cell_l, info)

    # Pick rebinning method
    if rebin == '1' or rebin == 'div':
        limit = 1.1
        sig_gas_rebin, nb_newbins, rad_kpc = rebinv1(sig_gas_HR, rad_kpc, binsize_aim, limit, nb_makemap_bins)
        sig_sfr_rebin = compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_newbins, lim_sfr, center_kpc, rad_kpc)

    if rebin == '2' or rebin == 'avg':
        sig_gas_rebin, nb_newbins = rebinv2(sig_gas_HR, rad_kpc, binsize_aim, nb_makemap_bins)
        sig_sfr_rebin = compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_newbins, lim_sfr, center_kpc, rad_kpc)

    if rebin == '3' or rebin == 'sum':
        sig_gas_rebin, nb_newbins = rebinv3(sig_gas_HR, rad_kpc, binsize_aim, nb_makemap_bins)
        sig_sfr_rebin = compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_newbins, lim_sfr, center_kpc, rad_kpc)
    
    if rebin == '4' or rebin == 'conv':
        sig_gas_rebin, nb_newbins = rebinv4(sig_gas_HR, rad_kpc, binsize_aim)
        sig_sfr_rebin = compute_2Dsfr_conv(star_age, star_minit, star_x, star_y, nb_newbins, nb_makemap_bins, lim_sfr, center_kpc, rad_kpc)

    # Plot (or not) maps of sigma_gas and/or sigma_sfr
    if plot_gas:
        if thresh!=None:
            print("The map can't be printed (vmin is in sigma_sfr -> it'd be a pain to do)")
        else:
            xmin_kpc,xmax_kpc = center_kpc[0]-rad_kpc,center_kpc[0]+rad_kpc
            ymin_kpc,ymax_kpc = center_kpc[1]-rad_kpc,center_kpc[1]+rad_kpc
            extent  = xmin_kpc,xmax_kpc,ymin_kpc,ymax_kpc
            maps.plot_map(sig_gas_rebin, cbar_label=r'$\Sigma_{gas}$', extent=extent, cmap_color='viridis', remove_ticks=False, vmin=None, vmax=None)
    if plot_sfr:
        xmin_kpc, xmax_kpc = center_kpc[0]-rad_kpc,center_kpc[0]+rad_kpc
        ymin_kpc, ymax_kpc = center_kpc[1]-rad_kpc,center_kpc[1]+rad_kpc
        extent  = xmin_kpc,xmax_kpc,ymin_kpc,ymax_kpc
        maps.plot_map(sig_sfr_rebin, cbar_label=r'$\Sigma_{SFR}$', extent=extent, cmap_color='BuPu_r', remove_ticks=False, vmin=thresh, vmax=None)

    if thresh!=None: # won't be plottable in 2D
        sig_gas_rebin, sig_sfr_rebin = sig_gas_rebin[sig_sfr_rebin>thresh], sig_sfr_rebin[sig_sfr_rebin>thresh]
    if bestfit:
        sig_gas_rebin, sig_sfr_rebin, sig_sfr_fit, coefs = mefit(sig_gas_rebin, sig_sfr_rebin, forlog=True)
    else:
        sig_sfr_fit, coefs = None, None

    return sig_gas_rebin, sig_sfr_rebin, sig_sfr_fit, coefs


def KS_th(min_gas, max_gas):
    """ 
    Returns the arrays sigma_gas [Msun/pc²] and sigma_sfr [Msun/yr/kpc²] from the observational Kennicutt-Schmidt relation.

    Parameters:
    -----------
        min_gas, max_gas: scalar
            Values between which the observational KS relation will be computed.

    Example:
    --------
        timestep = 17
        genpath = '/path/to/simu/'
        rad_kpc = 20
        sig_gas, sig_sfr, sig_sfrfit, coefs = KS_from_file(RamsesDir, timestep, rad_kpc)
        sig_gas_th, sig_sfr_th = KS_th(sig_gas.min(),sig_gas.max())
    """
    sig_gas_th = np.linspace(min_gas,max_gas)
    sig_sfr_th = 1.5e-4 * sig_gas_th**1.4       # Chabrier IMF

    return sig_gas_th, sig_sfr_th


def plot_KS(genpath, folders, timestep, labels, binsize_aim, lim_sfr, rad_kpc, rebin='4', \
    thresh=None, logscale=True, plot_data=True, xlims=None, ylims=None, saveinfile=True):
    """ 
    Plots Kennicutt-Schmidt relation from one or several output files.
    To plot only only from one simulation, use either a single folder or RamsesDir instead of genpath with None or '' instead of folders.

    Example:
    --------
        timestep = 17
        genpath = '/mnt/lyoccf/scratch/mrey/outputs/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        labels = ['1', '2', '3']

        rad_kpc = 20           # [kpc]
        binsize_aim = 1.       # [kpc]
        lim_sfr = 100          # [Myr]

        plot_KS(genpath, folders, timestep, labels, binsize_aim, lim_sfr,
            rad_kpc, thresh=None, logscale=True, plot_data=True)
    """
    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
        labels=['']
    if (not isinstance(folders,list)):
        folders=[folders]

    datas = []
    for index, fold in enumerate(folders):
        sys.stdout.write("[{}/{}]\r".format(index+1,len(folders)))  # progress bar
        sys.stdout.flush()
        RamsesDir = genpath + fold
        sig_gas, sig_sfr, sig_sfr_fit, coef = KS_from_file(RamsesDir, timestep, rad_kpc, rebin=rebin, \
            binsize_aim=binsize_aim, lim_sfr=lim_sfr, thresh=thresh)
        datas.append([sig_gas, sig_sfr, sig_sfr_fit, labels[index]])
        print('Eq is {:.2f} * Sig_gas + {:.2f}'.format(coef[0],coef[1]))

    _, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,6))
    if xlims!=None:
        if xlims[0]==0:
            raise ValueError("Just... don't. A value of 0 doesn't work.")
        sig_gas_th, sig_sfr_th = KS_th(xlims[0], xlims[1])
        ax.plot(sig_gas_th, sig_sfr_th, 'k', label="KS relation")

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']   
    for i, sig in enumerate(datas):
        if np.shape(sig[1])[0] > 100 and plot_data:
            ax.plot(sig[0], sig[1], c=colors[i], linestyle='',marker='.', label=sig[3], alpha=0.08)
            ax.plot(sig[0], sig[2], c=colors[i], linestyle='-', lw=2)
        elif plot_data:
            ax.plot(sig[0], sig[1], c=colors[i], linestyle='',marker='o', label=sig[3])
            ax.plot(sig[0], sig[2],c=colors[i], linestyle='-', lw=2)
        else:
            ax.plot(sig[0], sig[2],c=colors[i], linestyle='-', label=sig[3], lw=2)

    ax.set_title('Kennicutt-Schmidt relation')
    ax.set_xlabel(r'$\Sigma_{gas}$  [M$_\odot$ pc$^{-2}$]')
    ax.set_ylabel(r'$\Sigma_{SFR}$  [M$_\odot$ yr$^{-1}$ kpc$^{-2}$]')

    if logscale:
        ax.set_xscale('log')
        ax.set_yscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)

    # Overwrite alpha for legend and show either a point or a line depending on plot_data
    leg = ax.legend()
    for lh in leg.legendHandles[1:]: 
        lh._legmarker.set_alpha(1)
        if plot_data:
            lh._legmarker.set_marker('o')
    if not plot_data:
        for legobj in leg.legendHandles:
            legobj.set_linewidth(1.0)
            
    plt.tight_layout()
    plt.show()


def plot_KS_better(genpath, folders, timestep, labels, binsize_aim, lim_sfr, rad_kpc, nbins=30, rebin='4', \
    thresh=None, logscale=True, plot_err=True, plot_data=False, factor=1, xlims=None, ylims=None, saveinfile=True):
    """ 
    Same as plot_KS but with binning of the data on sig_gas. Also includes bars showing 15% and 85%.
    To plot only only from one simulation, use either a single folder or RamsesDir instead of genpath with None or '' instead of folders.
    
    Parameters:
    -----------
        plot_err: boolean
            Plots bars from 15% to 85% if set to True.

    Example:
    --------
        timestep = 17
        genpath = '/mnt/lyoccf/scratch/mrey/outputs/2_G8/'
        folders = ['1_base/','2_nstar/1_n_star=1/', '2_nstar/2_n_star=50/'] 
        labels = ['1', '2', '3']

        rad_kpc = 20           # [kpc]
        binsize_aim = 1.       # [kpc]
        lim_sfr = 100          # [Myr]

        plot_KS_better(genpath, folders, timestep, labels, binsize_aim, lim_sfr, rad_kpc, \
            nbins=30, thresh=5e-5, logscale=True, plot_err=True, plot_data=False, saveinfile=True)
    """
    # If there is just one folder
    if folders==None or folders=='':
        folders=['']
        labels=['']
    if (not isinstance(folders,list)):
        folders=[folders]

    _, ax = plt.subplots(nrows=1, ncols=1, figsize=(10,6))

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']   
    lims = [1e3, 1e-3]

    for index, fold in enumerate(folders):
        sys.stdout.write("[{}/{}]\r".format(index+1,len(folders)))  # progress bar
        sys.stdout.flush()
        sig_gas, sig_sfr, _, _ = KS_from_file(genpath+fold, timestep, rad_kpc, rebin=rebin, binsize_aim=binsize_aim, \
            lim_sfr=lim_sfr, thresh=thresh, bestfit=True, factor=factor)
        bins = np.linspace(sig_gas.min(), sig_gas.max(), nbins)
        new_sig_sfr = np.histogram(sig_gas,bins,weights=sig_sfr)[0] / np.histogram(sig_gas, bins)[0]
        plt.plot((bins[:-1]+bins[1:])/2, new_sig_sfr, c=colors[index], linestyle='',marker='.', label=labels[index]) # mean
        
        if plot_err:
            err_bot, err_top = [], []
            val1=bins[0]
            for i in bins[1:]:
                val2=i
                the_thing = sig_sfr[(sig_gas > val1) & (sig_gas < val2)]
                if len(the_thing)==0:
                    raise IndexError("You might want to consider using less bins.")
                err_bot.append(np.percentile(the_thing,25))
                err_top.append(np.percentile(the_thing,75))
                val1=i
            plt.vlines((bins[:-1]+bins[1:])/2, np.array(err_bot), np.array(err_top), color=colors[index], alpha=0.8) # error
        if plot_data:
            plt.scatter(sig_gas, sig_sfr, c=colors[index], alpha=0.003) # data
        lims = [min(lims[0], bins.min()), max(lims[1], bins.max())]

    # Observationnal relation
    sig_gas_th, sig_sfr_th = KS_th(lims[0], lims[1])
    ax.plot(sig_gas_th[1:], sig_sfr_th[1:], 'k', label="KS relation")

    plt.yscale('log')
    plt.xscale('log')

    ax.set_xlim(xlims)
    ax.set_ylim(ylims)

    plt.legend()
    ax.set_title('Kennicutt-Schmidt relation')
    ax.set_xlabel(r'$\Sigma_{gas}$  [M$_\odot$ pc$^{-2}$]')
    ax.set_ylabel(r'$\Sigma_{SFR}$  [M$_\odot$ yr$^{-1}$ kpc$^{-2}$]')
    # plt.show()

###################################################################################################################################
######################################################## Later KS methods ########################################################
###################################################################################################################################

def ks_annuli(RamsesDir, timestep, rad_tot, dr, lim_sfr=100, use_minirats=True, bestfit=True, factor=1, saveinfile=True, hnum=None):
    """
    Equivalent to KS_from_file, replacing square method with annuli.
    Returns the arrays sigma_gas [Msun/pc²],  sigma_sfr [Msun/yr/kpc²], sig_sfr_fit [Msun/yr/kpc²] and the coefs of the best fit.
    
    Parameters:
    -----------
        dr: scalar
            Width of the shell in [kpc] (equivalent to binsize_aim/2 in square methods).
        use_minirats: boolean
            Wether to use a method relying on minirats or not.

    Example:
    --------
        timestep = 17
        RamsesDir = '/path/to/simu/'
        tot_rad = 10
        dr = .5
        sig_gas_2, sig_sfr_2, sig_sfr2_fit, coefs2 = ks_annuli(RamsesDir, timestep, tot_rad, \
            dr, lim_sfr=100, bestfit=True, factor=1)
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2cm  = info['unit_l'] * info['boxlen']
    cu2kpc =  cu2cm/kpc2cm
    center = ras.get_Gcen(RamsesDir,timestep,hnum=hnum)
    center_kpc = [cen * cu2kpc for cen in center]
    
    nb_newbins = int(rad_tot/dr)
    if (rad_tot/dr)%1 != 0:
        raise ValueError("We don't deal with half bins here sir. Please make it s.t. rad_tot/dr is an integer.")

    
    # Make a high resolution map sigma_gas maps (through minirats) and then bin them radially
    str_var ='rho' if use_minirats else 'mass'                                                                  # [g/cm^3] or [g]
    qty, cell_pos, cell_l = ras.extract_cells(RamsesDir,timestep,[str_var,'cell_pos','cell_l'], factor=factor)      # [cm]
    sig_gas = np.zeros(nb_newbins)

    if use_minirats:
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(qty, center_kpc, rad_tot, cell_pos, cell_l, info)      # [Msun/pc^2]
        HRbin2LRbins = nb_newbins / nb_makemap_bins
        for i_gas in range(nb_makemap_bins):
            pos_x = (i_gas+0.5)*HRbin2LRbins - nb_newbins/2                                                     # [newbins] centered position
            for j_gas in range(nb_makemap_bins):
                pos_y = (j_gas+0.5)*HRbin2LRbins - nb_newbins/2                                                 # [newbins] centered position
                rad_gas_bin = np.sqrt(pos_x**2 + pos_y**2)                                                      # [newbins] radius
                if rad_gas_bin <= nb_newbins:
                    sig_gas[int(rad_gas_bin)]+=sig_gas_HR[i_gas][j_gas]*(2*rad_tot*1000/nb_makemap_bins)**2     # [Msun/pc^2*area_mkmp_bin]
    else:
        rad_gas_bin = np.sqrt((cell_pos[:,0]-center[0]*cu2cm)**2+(cell_pos[:,1]-center[1]*cu2cm)**2)/kpc2cm/rad_tot*nb_newbins
        for i_gas, gas_mass in enumerate(qty):
            if rad_gas_bin[i_gas] <= nb_newbins:
                sig_gas[int(rad_gas_bin[i_gas])]+= gas_mass                                                     # [g]
        sig_gas *= g2Msun                                                                                       # [Msun]
        
    # Go through stars and bin them depending on their radius from the center.
    star_x,star_y,star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_x','star_y','star_age','star_minit'],factor=factor)
    
    sig_sfr = np.zeros(nb_newbins)
    star_minit_Msun = star_minit * g2Msun                                                                       # [Msun]

    for index, minit in enumerate(star_minit_Msun):
        if (star_age[index] <= lim_sfr)|(star_age[index] >= 0):                                                 # Ignore all stars not "newly" formed.
            rad_star = np.sqrt((star_x[index]/kpc2cm-center_kpc[0])**2 + (star_y[index]/kpc2cm-center_kpc[1])**2)
            rad_star_bin = int(rad_star/rad_tot*nb_newbins)
            sig_sfr[rad_star_bin]+=minit
    
    rad = 0
    for indix in range(nb_newbins):
        area=np.pi*dr*(2*rad+dr)                                                                                # [kpc] shell area: pi(dr^2+2rdr)
        sig_gas[indix] /= area*1000**2                                                                          # [Msun/pc^2]
        sig_sfr[indix] /= area*lim_sfr*1e6                                                                      # [Msun/kpc^2/yr]
        rad += dr

    if bestfit:
        sig_gas, sig_sfr, sig_sfr_fit, coefs = mefit(sig_gas, sig_sfr, forlog=True)
    else:
        sig_sfr_fit, coefs = None, None
    
    return sig_gas, sig_sfr, sig_sfr_fit, coefs

def ks_square(RamsesDir, timestep, rad_tot, binsize=1, lim_sfr=100, bestfit=True, factor=1, saveinfile=True, hnum=None):
    """
    Equivalent to KS_from_file, replacing square method with annuli.
    Returns the arrays sigma_gas [Msun/pc²],  sigma_sfr [Msun/yr/kpc²], sig_sfr_fit [Msun/yr/kpc²] and the coefs of the best fit.

    Example:
    --------
        timestep = 17
        RamsesDir = '/path/to/simu/'
        tot_rad = 10
        dr = .5
        sig_gas_2, sig_sfr_2, sig_sfr2_fit, coefs2 = ks_annuli(RamsesDir, timestep, tot_rad, \
            dr, lim_sfr=100, bestfit=True, factor=1)
    """
    info   = ras.read_info(RamsesDir, timestep)
    cu2cm  = info['unit_l'] * info['boxlen']
    cu2kpc =  cu2cm/kpc2cm
    center_kpc = [cen * cu2kpc for cen in ras.get_Gcen(RamsesDir,timestep,hnum=hnum)]
    
    nb_newbins = int(rad_tot/binsize)
    if (rad_tot/binsize)%1 != 0:
        raise ValueError("We don't deal with half bins here sir. Please make it s.t. rad_tot/dr is an integer.")

    # Go through cells and bin them by pixels of binsize if they are within a square defined from the center +- rad_tot
    mass, cell_pos = ras.extract_cells(RamsesDir,timestep,['mass','cell_pos'],factor=factor)      # [cm]
    sig_gas = np.zeros(shape=(nb_newbins,nb_newbins))

    for ind_gas, gas_mass in enumerate(mass):
        ind_x = int((cell_pos[ind_gas,0]/kpc2cm - center_kpc[0] + rad_tot)/rad_tot*nb_newbins)
        ind_y = int((cell_pos[ind_gas,1]/kpc2cm - center_kpc[1] + rad_tot)/rad_tot*nb_newbins)
        if (ind_x >= 0) and (ind_x < nb_newbins) and (ind_y >= 0) and (ind_y < nb_newbins):
            sig_gas[ind_x,ind_y]+= gas_mass                                                                     # [g]
    sig_gas *= g2Msun/(binsize*1e3)**2                                                                          # [Msun/pc^2]
        
    # Go through stars and bin them depending on their radius from the center.
    star_x,star_y,star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_x','star_y','star_age','star_minit'],factor=factor)
    star_minit_Msun = star_minit * g2Msun                                                                       # [Msun]
    sig_sfr = np.zeros(shape=(nb_newbins,nb_newbins))

    for ind_star, star_mass in enumerate(star_minit_Msun):
        ind_x_star = int((star_x[ind_star]/kpc2cm - center_kpc[0] + rad_tot)/rad_tot*nb_newbins)
        ind_y_star = int((star_y[ind_star]/kpc2cm - center_kpc[1] + rad_tot)/rad_tot*nb_newbins)
        if (ind_x_star >= 0) and (ind_x_star < nb_newbins) and (ind_y_star >= 0) and (ind_y_star < nb_newbins):
            if (star_age[ind_star] <= lim_sfr)|(star_age[ind_star] >= 0):                                       # Ignore all stars not "newly" formed.
                sig_sfr[ind_x_star,ind_y_star]+= star_mass
    sig_sfr /= (binsize)**2*lim_sfr*1e6                                                                         # [Msun/yr/kpc^2]

    if bestfit:
        sig_gas, sig_sfr, sig_sfr_fit, coefs = mefit(sig_gas, sig_sfr, forlog=True)
    else:
        sig_sfr_fit, coefs = None, None
    
    return sig_gas, sig_sfr, sig_sfr_fit, coefs

###################################################################################################################################
############################################################ Sigma gas ############################################################
###################################################################################################################################
def compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info):
    """
    Compute the surface density at the maximum resolution using make_map from minirats.
    The outputs are sig_gas [Msun/pc²] and nb_makemap_bins.

    Example:
    --------
        info = ras.read_info(RamsesDir, timestep)
        center = [75,75,75]
        rad_kpc = 20
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
    """
    cu2cm = info['unit_l'] * info['boxlen']
    cu2kpc =  cu2cm / kpc2cm

    lmax = cell_l.max()
    rho_Msun = rho *  g2Msun * kpc2cm**3/1e9                                    # [Msun/pc^3]

    # Convert in cu ∈ [0,1] because make_map wants it
    cell_pos_cu    = cell_pos / cu2cm                                           # [cu]
    center_cu      = [cen/cu2kpc for cen in center]                             # [cu]
    rad_cu         = rad_kpc / cu2kpc                                           # [cu]
    
    xloc, yloc, zloc = cell_pos_cu[:,0], cell_pos_cu[:,1], cell_pos_cu[:,2]     # [cu]
    xmin,xmax = center_cu[0]-rad_cu,center_cu[0]+rad_cu                         # [cu]
    ymin,ymax = center_cu[1]-rad_cu,center_cu[1]+rad_cu                         # [cu]
    zmin, zmax = 0, 1                                                           # [cu]

    nx,ny = cu.py_cell_utils.get_map_nxny(lmax,xmin,xmax,ymin,ymax)
    sig_gas,weight = cu.py_cell_utils.make_map_new(lmax,False,xmin,xmax,ymin,ymax,zmin,zmax,\
        rho_Msun,np.ones(len(rho_Msun)),xloc,yloc,zloc,cell_l,nx,ny)            # [Msun/pc^3], []

    weight  *= (zmax-zmin)*cu2kpc*1e3                                           # [pc]
    sig_gas *= weight                                                           # [Msun/pc²]

    nb_makemap_bins = min(np.shape(sig_gas))
    res_makemap = 2*rad_kpc / nb_makemap_bins                                   # [kpc]
    # print('The resolution right after make_map is ~ {:2f} kpc'.format(res_makemap))

    # Test that there is not too much difference in resolution after mapping.
    res_min = cu2kpc/2**cell_l.max()
    if abs(1-res_min/res_makemap)*100 > 1:
        raise ValueError('Error of {:.2f} % induced by using it: using {} pixels instead of ~ {:.2f} '.\
        format(abs(1-res_min/res_makemap)*100, np.shape(sig_gas)[0], 1 / ((res_min/rad_kpc)/2)))

    return sig_gas, nb_makemap_bins


def rebinv1(sig_gas, rad_kpc, binsize_aim, limit, nb_makemap_bins):
    """
    Method 1 (a.k.a. divisor)
    -------------------------
    /!\\ CAREFUL: this method is outdated and can give errors. /!\\  
    Takes the map of the gas at highest resolution and rebins it as close as possible to binsize_aim [kpc].  
    A first step is to compute the optimal_group to create a bin of binsize_aim [kpc].
    Then, an iterative loop goes through value to find the divisor of the original number of pixels the closest to this value.
    The value is computed within the range [optimal_group/limit, optimal_group*limit].
    A value rad_kpc is returned as its value can be change for mathematical reasons: if nb_makemap_bins is odd, \
        the code will make it even so that it has more divisors.

    Parameters:
    -----------
        limit: scalar
            Factor setting the size limit of the bin.
            
    Example:
    --------
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
        sig_gas_rebin1, nb_newbins, rad_kpcv1 = rebinv1(sig_gas_HR, 20., 1., 1.1, np.shape(sig_gas_HR)[0])
    """
    res_makemap = 2*rad_kpc / nb_makemap_bins

    # Make the number of bins in sig_gas even if it's not to get more divisors.
    if nb_makemap_bins%2 != 0:
        sig_gas_even = sig_gas[0:-1, 0:-1]           # Remove last column and row
        nb_makemap_bins_even = nb_makemap_bins - 1   # Update number of bins
        rad_kpc = rad_kpc* (1 - 2/nb_makemap_bins)   # Update radius (a pixel of size 2/nb_makemap_bins is removed)
        print('/!\\/!\\/!\\ BEWARE: the radius considered has changed /!\\/!\\/!\\ ')
    else:
        sig_gas_even = sig_gas  
        nb_makemap_bins_even = nb_makemap_bins
        
    # Compute the optimal group to get binsize_aim
    optimal_group = binsize_aim/res_makemap
    # print('The best grouping would be ~ {:.2f} pixels binned together'.format(optimal_group))
    group_max, group_min = optimal_group*limit, optimal_group/limit
    groupN, groupP = np.round(optimal_group), np.round(optimal_group)
    # print("The limiting conditions are {:.3f} pc and {:.3f} pc.".format(group_min*res_makemap, group_max*res_makemap))


    # Try to find a binning value within the limits given which is a divisor of nb_makemap_bins.
    resteN = nb_makemap_bins_even % groupN
    resteP = nb_makemap_bins_even % groupP
    while resteN!=0 and resteP!=0:
        if groupN<group_min or groupP>group_max:
            raise ValueError('Limit condition reached.')
        groupN -= 1
        groupP += 1
        resteN = nb_makemap_bins_even%groupN
        resteP = nb_makemap_bins_even%groupP

    if resteN==0:
        closest_group = groupN
    elif resteP==0:
        closest_group = groupP
    else:
        raise ValueError('The code is wrong, no remainder is zero.')
        
    nb_newbins = int(nb_makemap_bins_even/closest_group)
    size_newbin_kpc = closest_group * res_makemap
    
    sig_gas_rebin1 = reshape(sig_gas_even,[nb_newbins,nb_newbins])      # [Msun/pc²]
    print('The size of a bin is now ~ {:.3f} kpc.'.format(size_newbin_kpc))
    # print('{} bins successfully rebinned to {} bins.'.format(nb_makemap_bins**2, nb_newbins**2))

    return sig_gas_rebin1, nb_newbins, rad_kpc


def rebinv2(sig_gas, rad_kpc, binsize_aim, nb_makemap_bins):
    """
    Method 2 (a.k.a. average)
    -------------------------
    /!\\ CAREFUL: this method is outdated, use rebinv3 instead. /!\\
    Takes the map of the gas at highest resolution and rebins it as close as possible to binsize_aim [kpc].  
    Creates a new grid with the appropriate number of 'low resolution' cells and populates them with the
        average of the surface density of the surrounding cells on the high resolution grid. 

    Example:
    --------
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
        sig_gas_rebin2, nb_newbins2 = rebinv2(sig_gas_HR, 20., 1., , np.shape(sig_gas_HR)[0])
    """
    radius_rebin = binsize_aim/2    # [kpc]
    nb_newbins = int(rad_kpc/radius_rebin)
    if rad_kpc%radius_rebin != 0:
        raise ValueError('Select another bin size (or an other radius) s.t. radius_kpc%radius_newbin = 0 \
            (i.e. s.t. the radius of the new bins is a divisor of the input radius).')
    
    rad_mkmp_bins = radius_rebin/(rad_kpc*2) * nb_makemap_bins
    sig_gas_rebin2 = np.zeros(shape=(nb_newbins,nb_newbins))
    for i in range(nb_newbins):
        h_center = (i+0.5)/nb_newbins*nb_makemap_bins
        h_min = int(h_center-rad_mkmp_bins)
        h_max = int(h_center+rad_mkmp_bins)
        for j in range(nb_newbins):
            v_center =  (j+0.5)/nb_newbins*nb_makemap_bins
            v_min = int(v_center-rad_mkmp_bins)
            v_max = int(v_center+rad_mkmp_bins)
            if h_min==0:            # Otherwise the 0 is skipped
                h_min -= 1
            if v_min==0:
                v_min -= 1
            sig_gas_rebin2[i][j] = np.mean(sig_gas[h_min+1:h_max+1,v_min+1:v_max+1])

    return sig_gas_rebin2, nb_newbins


def rebinv3(sig_gas, rad_kpc, binsize_aim, nb_makemap_bins):
    """
    Method 3 (a.k.a. sum)
    ---------------------
    /!\\ CAREFUL: rebinv4 has been chosen as the best one. /!\\ 
    Takes the map of the gas at highest resolution and rebins it as close as possible to binsize_aim [kpc].    
    Creates a new grid with the appropriate number of 'low resolution' cells and populates them with the
        sum of the mass of the surrounding cells on the high resolution grid divided afterwards by
        the size in pc of the low resolution cell.  

    Example:
    --------
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
        sig_gas_rebin3, nb_newbins3 = rebinv3(sig_gas_HR, 20., 1., np.shape(sig_gas_HR)[0])
    """
    radius_rebin = binsize_aim/2
    rad_mkmp_bins = radius_rebin/(rad_kpc*2) * nb_makemap_bins 
    nb_newbins = int(rad_kpc/radius_rebin)

    sig_gas_rebin = np.zeros(shape=(nb_newbins,nb_newbins))

    res_makemap = 2*rad_kpc/np.shape(sig_gas)[0]
    sig_gas_mass = sig_gas*(res_makemap*1e3)**2
    for i in range(nb_newbins):
        h_center = (i+0.5)/nb_newbins*nb_makemap_bins
        h_min = int(h_center-rad_mkmp_bins)
        h_max = int(h_center+rad_mkmp_bins)
        for j in range(nb_newbins):
            v_center =  (j+0.5)/nb_newbins*nb_makemap_bins
            v_min = int(v_center-rad_mkmp_bins)
            v_max = int(v_center+rad_mkmp_bins)
            if h_min==0:    # Otherwise the 0 is skipped
                h_min -= 1
            if v_min==0:
                v_min -= 1
            sig_gas_rebin[i][j] = np.sum(sig_gas_mass[h_min+1:h_max+1,v_min+1:v_max+1])
    sig_gas_rebin = sig_gas_rebin/(binsize_aim*1e3)**2

    return sig_gas_rebin, nb_newbins


def rebinv4(sig_gas, rad_kpc, binsize_aim, kernel=None):
    """
    Method 4 (a.k.a. conv/smooth)
    -----------------------------
    /!\\ CAREFUL: the threshold can influence the orientation of the plot. /!\\
    Takes the map of the gas at highest resolution and rebins it to binsize_aim [kpc].    
    Smooth it using the kernel given which should be a 2D array. By defaut it is a square filled with ones.

    Example:
    --------
        sig_gas_HR, _ = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
        sig_gas_rebin4, nb_newbins4 = rebinv4(sig_gas_HR, 20., 1., np.shape(sig_gas_HR)[0])
    """
    res_makemap = 2*rad_kpc/np.shape(sig_gas)[0]
    nb_kernel_bins = int(binsize_aim/res_makemap)

    if kernel == None:                                                  # Could improve this kernel by making it circular
        kernel = np.ones((nb_kernel_bins,nb_kernel_bins))               # Error in selecting the radius !
    else:
        if len(np.shape(kernel)) != 2:
            raise ValueError('The kernel does not have the good shape')
        # check it is indeed an array ?
    
    sig_gas_rebin = fil.convolve(sig_gas*(res_makemap*1e3)**2, kernel)  #, output=None, mode='reflect', cval=0.0, origin=0)
    sig_gas_rebin = sig_gas_rebin/(nb_kernel_bins*res_makemap*1e3)**2
    print('The size of the bins is ~ {:.3f} kpc'.format(nb_kernel_bins*res_makemap))

    return sig_gas_rebin, nb_kernel_bins


###################################################################################################################################
############################################################ Sigma SFR ############################################################
###################################################################################################################################

def compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_newbins, lim_sfr, center, rad_kpc):
    """
    Important: goes in pair with rebinning methods through nb_newbins.
    Returns a 2D map of the SFR surface density in Msun/yr/kpc² by using the number of bins (given by rebinning functions).
    The only parameter changing between the different rebinning methods is nb_newbins EXCEPT for v1 where the radius may also be changed.

    Parameters:
    -----------
        nb_newbins: int
            Number of new bins for the SFR map.

    Example:
    --------
        
        star_x,star_y,star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_x','star_y','star_age','star_minit'],factor=factor)
        
        rad_kpc      = 20             # 20          [kpc]
        lim_sfr      = 100            # 100         [Myr]
        nb_newbins   = 40             # -> bins of 2*rad_kpc/nb_newbins = 1 [kpc]
        sig_sfr = compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_newbins, lim_sfr, center, rad_kpc)
    """
    star_minit_Msun = star_minit * g2Msun                                       # [Msun]

    # Remove all stars not "newly" formed stars (not within lim_sfr Myrs)
    no_sfr = np.where((star_age > lim_sfr)|(star_age < 0))[0]
    star_minit_Msun[no_sfr] = 0                                                 # Not np.nan, otherwise problem with mefit function.
    
    xmin_kpc, xmax_kpc = center[0]-rad_kpc,center[0]+rad_kpc
    ymin_kpc, ymax_kpc = center[1]-rad_kpc,center[1]+rad_kpc
    rge=[[xmin_kpc ,xmax_kpc],[ymin_kpc ,ymax_kpc]]

    snap_sfr = star_minit_Msun/lim_sfr/1e6                                      # [Msun/yr]
    sig_sfr,xedges,_ = np.histogram2d(star_x/kpc2cm,star_y/kpc2cm,\
                                 bins=nb_newbins, range=rge, weights=snap_sfr)  # [Msun/yr]
    sig_sfr /= (xedges[1]-xedges[0])**2                                         # [Msun/yr/kpc²]
    return sig_sfr


def compute_2Dsfr_conv(star_age, star_minit, star_x, star_y, nb_newbins, nb_makemap_bins, lim_sfr, center, rad_kpc):
    """
    Important: goes in pair with rebinning methods through nb_newbins.
    Returns a 2D map of the SFR surface density in Msun/yr/kpc² at the highest resolution. 
    Then, rebins it using a convolution (same as in rebinv4).
    The only parameter changing between the different rebinning methods is *nb_newbins* EXCEPT for v1 where the radius may also be changed.

    Parameters:
    -----------
        nb_newbins: int
            Number of new bins for the SFR map.
        nb_makemap_bins:
            Result of the function compute_sig_gas_HR, number of bins at the highest resolution.

    Example:
    --------
        
        star_x,star_y,star_age,star_minit = ras.extract_stars(RamsesDir,timestep,['star_x','star_y','star_age','star_minit'],factor=factor)
        
        center          = [75, 75, 75]   # [kpc]
        rad_kpc         = 20             # [kpc]
        lim_sfr         = 100            # [Myr]
        nb_newbins      = 40             # -> bins of 2*rad_kpc/nb_newbins = 1 [kpc]
        nb_makemap_bins = 1093           # At high res, there are 1093 bins.

        sig_sfr = compute_2Dsfr_conv(star_age, star_minit, star_x, star_y, nb_newbins, nb_makemap_bins, lim_sfr, center, rad_kpc)
    """
    res_makemap = 2*rad_kpc / nb_makemap_bins     # [kpc]
    weight_conv = np.ones((nb_newbins,nb_newbins))
    sig_sfr_2conv = compute_2Dsfr(star_age, star_minit, star_x, star_y, nb_makemap_bins, lim_sfr, center, rad_kpc)

    sig_sfr_conv = fil.convolve(sig_sfr_2conv*(res_makemap)**2, weight_conv)
    sig_sfr_conv = sig_sfr_conv/(nb_newbins*res_makemap)**2
    
    return sig_sfr_conv

###################################################################################################################################
############################################################## Utils ##############################################################
###################################################################################################################################

def mefit(data_gas, data_sfr, forlog=True):
    """
    Removes the values of 0 Msun/yr/kpc² from both arrays and computes their best fit.
    Then returns them with their best_fit and the corresponding coefficients.
    Can be returned for use with either a log or a linear scale (using the parameter 'forlog')
    
    Parameters:
    -----------
    data_gas, data_sfr: arrays of scalar:
        Data on x and y for which you want the best fit.
    forlog: boolean
        If true, returns an array to be plotted in logscale. In linear scale otherwise.

    Example:
    --------
        sig_gas_HR, nb_makemap_bins = compute_sig_gas_HR(rho, center, rad_kpc, cell_pos, cell_l, info)
        sig_gas_rebin4, nb_newbins4 = rebinv4(sig_gas_HR, 20., 1., np.shape(sig_gas_HR)[0])
        sig_gas, sig_sfr, sig_sfr_fit, coef_ = mefit(sig_gas_rebin4, nb_newbins4, forlog=logscale)

    """
    index = np.nonzero(data_sfr)
    data_gas_nz, data_sfr_nz = data_gas[index], data_sfr[index]
    try: 
        coefs = np.polyfit(np.log10(data_gas_nz),np.log10(data_sfr_nz),1)
    except np.linalg.LinAlgError:
        raise ValueError("SVD did not converge in Linear Least Squares.\
            \n-> The radius you used might be bigger than the data loaded...")
    
    if forlog:
        best_fit = data_gas_nz**coefs[0]*10**coefs[1]
    else:
        poly1d = np.poly1d(coefs)
        best_fit = poly1d(data_gas_nz)

    return data_gas_nz, data_sfr_nz, best_fit, coefs


def reshape(arr, new_shape):
    """
    Rebin a 2D array into a new shape. 
    The new_shape values must be a divisor of the initial shape of the array.

    Parameters:
    -----------
    arr: array
        Array tp reshape.
    new_shape: array of size 2
        The shape that the array arr will be reshaped into.

    Example:
    --------
        rebinned_array = rebin(array,[nb_newbins,nb_newbins])
    """
    if arr.ndim != len(new_shape):
        raise ValueError("Shape mismatch: {} -> {}".format(arr.shape,new_shape))
    if arr.shape[0] % new_shape[0] != 0:
        raise ValueError("Can't rebin from {} to {}.".format(arr.shape[0],new_shape[0]))
    if arr.shape[1] % new_shape[1] != 0:
        raise ValueError("Can't rebin from {} to {}.".format(arr.shape[1],new_shape[1]))
        
    if arr.shape[0] == new_shape[0]:
        print('You know that you did not rebin the first axis, right ?')
    if arr.shape[1] == new_shape[1]:
        print('You know that you did not rebin the second axis, right ?')
    
    shape = (new_shape[0], arr.shape[0] // new_shape[0],
             new_shape[1], arr.shape[1] // new_shape[1])
    
    return arr.reshape(shape).mean(-1).mean(1)
