"""
Functions from other people.
============================

compute_runtime, plot_runtime_estimate,
plot_halos_dm_map, vir_circle, add_cb_dmhalomap, plot_halo_dm_map,
truncate, makemedemoutputs.
"""
# Astro libraries
from minirats.HaloFinder.py.haloCatalog import haloCatalog as hC
import minirats.utils.py.readwrite.ramses_info as ri   
from ratatouille import readNsave as ras
import minirats.utils.py.f90utils as dm
from ratatouille.constants import *

# Data manipulation and plotting libraries
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
import numpy as np, math
import shutil, os, re, glob, argparse


###################################################################################################################################
###############################################################  J. ###############################################################
###################################################################################################################################
def clean_outputs(RamsesDir):
    """
    Clean up a RAMSES run directory and keep only outputs which where specified in the namelist file.
    From JB - 2015, adapted by MR - Dec. 2024
    """
    # List of aexp we want to keep from the namelist file
    path_nml = RamsesDir+[file for file in os.listdir(RamsesDir) if file.endswith('.nml')][-1]
    with open(path_nml) as f:
        is_cosmo = any(line.split('=')[1].strip() == '.true.' for line in f if line.strip().startswith("cosmo"))
    if is_cosmo:
        for line in open(path_nml):
            if "aout" in line:
                aexps = line.split("=")[-1]
                aexps = aexps.split(",")
                aexps = np.array(aexps,dtype=float)
                break
    else:
        with open(path_nml, 'r') as file:
            content = file.read()
            tend = float(re.search(r'tend\s*=\s*([\d\.]+)', content).group(1))
            delta_tout = float(re.search(r'delta_tout\s*=\s*([\d\.]+)', content).group(1))
            if delta_tout == 0:
                delta_tout = tend
            noutput = min(int(tend / delta_tout), 1000)
            aexps = [float(i) * delta_tout for i in range(noutput + 1)]

    # List of aexp we want to keep from the namelist file up to the last output generated
    last_out_nb = max([int(output_arr[7:]) for output_arr in os.listdir(RamsesDir) if 'output' in output_arr])
    last_info = f"{RamsesDir}/output_{last_out_nb:05d}/info_{last_out_nb:05d}.txt"
    for line in open(last_info):
        if "aexp" in line and is_cosmo: # Don't want to do it if not cosmo (i.e. I'm lazy)
            amax = float(line.split("=")[-1].strip())
            aexps = aexps[aexps<=amax]

    # Get aexp of each output directory
    outs = glob.glob("{}/output_*".format(RamsesDir))  # list of output dirs present
    aout = np.zeros(len(outs))
    i    = 0
    for out in outs:
        snapstr  = out.split("_")[-1].strip()
        infoFile = "{}/info_{}.txt".format(out,snapstr)
        for line in open(infoFile):
            if is_cosmo and "aexp" in line:
                a = float(line.split("=")[-1].strip())
                break
            elif not is_cosmo and "time" in line:
                a = float(line.split("=")[-1].strip())
                break
        aout[i]  = a
        i        = i + 1

    # For each desired aexp, keep closest output and check if the difference in a is bigger than 0.0001.
    keep = []
    keeps = []
    for a in aexps:
        da = abs(aout - a)
        ii = np.where(da == min(da))[0][0]
        big_diff = 0.0001 if is_cosmo else delta_tout/10
        if da[ii] > big_diff:
            print("Oh noo ... \u0394a = {:f} for {}.".format(da[ii], outs[ii]))
        keep.append(ii)
        keeps.append(outs[ii])

    ####### Delete outputs #######
    destroy_these = []      # list of directories to destroy
    for out in outs:
        if out not in keeps:
            snapstr  = out.split("_")[-1].strip()
            if snapstr != "00001":
                print("erasing {}".format(out))
                destroy_these.append(out)
    print("destroy_these = ", destroy_these)
    if destroy_these:       # Ask for user input
        if input("Shall I continue ? True or whatever:")!="True":
            print("Ok... I won't destroy everything :'(.")
            exit()
        else:
            print("Let us destrooooy ! (•̀ ᴗ •́)")
        for out in outs:    # Delete outputs
            if out not in keeps:
                snapstr  = out.split("_")[-1].strip()
                if snapstr != "00001":
                    print("erasing {}".format(out))
                    shutil.rmtree(out)

    ####### Rename outputs #######
    i = 1 if is_cosmo else 0
    for a, k, out in zip(aexps, keep, keeps):
        i = i + 1
        snapstr = out.split("_")[-1].strip()
        key = "_{}".format(snapstr)
        newkey = '_{:05d}'.format(i)
        if key == newkey:
            continue
        print("Will rename {} into {}".format(key, newkey))
    if input("Shall I continue ? True or whatever:")!="True":   # Ask for user input
        print("Ok... I won't rename anything :'(.")
        exit()
    else:
        print("Let us renaaaaame ! (•̀ ᴗ •́)")
        i = 1 if is_cosmo else 0
        for a,k,out in zip(aexps,keep,keeps):
            i = i + 1
            snapstr  = out.split("_")[-1].strip()
            key      = "_{}".format(snapstr)
            newkey   = '_{:05d}'.format(i)
            if key == newkey:
                continue
            print("renaming {} into {}".format(key,newkey))
            for fname in glob.glob('{}/*{}.*'.format(outs[k],key)):  # Rename files
                index = fname.rfind(key)                            # Find last occurence of key
                newfname = "{}{}{}".format(fname[:index],newkey,fname[index+len(key):])
                os.rename(fname,newfname)
            index  = out.rfind(key)                             # Rename directory
            newout = "{}{}".format(out[:index],newkey)
            os.rename(out, newout)

###################################################################################################################################
############################################################ DM stuff ############################################################
###################################################################################################################################
def plot_halos_dm_map(RamsesDir,snap,MyHalos=[],show_halonumber=False,figname=None):
#'''
# Plot DM map of selected Halos
# Marion Farcy 09/20
#'''
    """
    Plot a dark matter map of selected Halos in RamsesDir at choosen snap.
    If no Halo list given (MyHalos = []), select halos with virial mass > 10e10 Msun by default.
    Save figure at figname location if figname is not None.
    Print the Halo numbers above the Halo positions if show_halonumber = True (False by default).
    """
    hcat = hC(RamsesDir,snap,HaloDir='Halos/',load=True)
    fig,ax = plt.subplots(figsize=(10,10))
    ax = fig.add_axes((0,0,1,1))
    if MyHalos == []:
        MyHalos = np.where((hcat.level == 1) & (hcat.mvir > 1e10*1e-11))[0]
    minmass = min(hcat.mvir[MyHalos])
    conv = 10**(-np.log10(minmass))
    plt.scatter(hcat.x_cu[MyHalos],hcat.y_cu[MyHalos],marker='o',s=(hcat.mvir[MyHalos]*conv).astype(int)*70,\
            facecolor='none',edgecolor='blue',lw=3)
    if show_halonumber:
        labels = [str(int(e)) for e in hcat.hnum[MyHalos]]
        for i, label in enumerate(labels):
            plt.annotate(label,# this is the text
                (hcat.x_cu[MyHalos][i],hcat.y_cu[MyHalos][i]), # this is the point to label
                textcoords="offset points", # how to position the text
                xytext=(0,10), # distance from text to points (x,y)
                ha='center',fontsize=10,fontweight='bold',color='blue')
    #Plotting options
    plt.title("z = "+str(round(hcat.info['redshift'],0)))
    if figname != None:
        plt.savefig(figname,bbox_inches="tight")
    plt.show()
    return

"""Plot a virial radius circle on a map"""
def vir_circle(x,y,r,color='black',alpha=1,linestyle='-'):
    #---------------------------------------------------------------------
    fig=plt.gcf()
    c = plt.Circle((x,y),radius=r,fill=False,color=color,alpha=alpha,linestyle=linestyle,lw=3)
    fig.gca().add_artist(c)


def add_cb_dmhalomap(ax,im,label,loc='top',label_size=20,color='black',do_bold=False):
    #---------------------------------------------------------------------
    if loc=='right':
        axins = inset_axes(ax,width="3%",height="100%",loc=1,borderpad=0)
        cbar = plt.colorbar(im,cax=axins,orientation='vertical')
    if loc=='top':
        axins = inset_axes(ax,width="80%",height="5%",loc=9,borderpad=-15)
        cbar = plt.colorbar(im,cax=axins,orientation='horizontal')
    if do_bold:
        for l in cbar.ax.yaxis.get_ticklabels():
            l.set_weight("bold")
        for l in cbar.ax.xaxis.get_ticklabels():
            l.set_weight("bold")
    cbar.set_label('%s' % (label), color=color, fontsize=label_size)
    if do_bold:
        cbar.ax.yaxis.set_tick_params(color=color, labelsize=label_size*0.8,size=label_size*0.5,\
                                      width=label_size*0.15,which='major')
        cbar.ax.xaxis.set_tick_params(color=color, labelsize=label_size*0.8,size=label_size*0.5,\
                                      width=label_size*0.15,which='major')
        cbar.ax.yaxis.set_tick_params(size=label_size*0.2,width=label_size*0.15,which='minor')
        cbar.ax.xaxis.set_tick_params(size=label_size*0.2,width=label_size*0.15,which='minor')
    else:
        cbar.ax.yaxis.set_tick_params(color=color, labelsize=label_size*0.8,size=label_size*0.5,width=label_size*0.06,which='major')
        cbar.ax.xaxis.set_tick_params(color=color, labelsize=label_size*0.8,size=label_size*0.5,width=label_size*0.06,which='major')
        #,top='on',bottom='off', labeltop='on',labelbottom='off')
        cbar.ax.yaxis.set_tick_params(size=label_size*0.2,width=label_size*0.06,which='minor')
        cbar.ax.xaxis.set_tick_params(size=label_size*0.2,width=label_size*0.06,which='minor')
    cbar.outline.set_edgecolor(color)
    if do_bold:
        cbar.outline.set_linewidth(label_size*0.15)
    else:
        cbar.outline.set_linewidth(label_size*0.06)
    plt.setp(plt.getp(cbar.ax.axes, 'yticklabels'), color=color)
    plt.setp(plt.getp(cbar.ax.axes, 'xticklabels'), color=color)
    return cbar


def plot_halo_dm_map(RamsesDir,snap,halo_num=None,figname=None,nbins=300,rmax=1,\
                  target_radius=None,proj_axis='z',verbose=False,cmap='BuPu_r',vmin=1e4,vmax=1e6):
#'''
# Plots a DM density map of one chosen halo or galaxy given its DM particules coordinates and mass
# Marion Farcy 05/20
#'''
    """
    Plot a DM density map of a given object from RamsesDir at chosen snap.
    * If the run is cosmo, assumes that that a halo catalog has been processed.
    In this case, a blue circle for Rvir, a green for target_radius*Rvir and a red for R_contam are plotted (last only for a zoom run).
    target_radius parameter only usefull for a zoom where ones wants to check if there is low res° DM particules.
    * If halo_number is not None, assumes it is the object number as read from the halo catalog.
    * For isolated disk from a non cosmo run, just use halo_num=None, the default.
    rmax expected to be a float number defining the limit of the plot, keeping the DM particules within rmax*Rvir. 
    If rmax=1, rmax=1*Rvir (the default).
    """
    # Constants
    pc2cm  = 3.086e18
    nx, ny = nbins, nbins
    info = ras.read_info(RamsesDir,snap)
    if halo_num != None:
        hcat = hC(RamsesDir,snap,HaloDir='Halos/',load=True)
        cu2Msun = 1e11
        unit_l = hcat.info['unit_l']
        hmain = np.where((hcat.level==1) &(hcat.contam < 0.5) &(hcat.mvir > 4e7*1e-11))[0]
        myhalo = hmain[np.where(hcat.mvir[hmain]==max(hcat.mvir[hmain]))[0][0]]
        xc,yc,zc,rc = hcat.x_cu[myhalo],hcat.y_cu[myhalo],hcat.z_cu[myhalo],hcat.rvir_cu[myhalo]
        rc_kpc = round(rc*hcat.info['unit_l']/3.086e21,2)
        mvir_Msun = "%.2e"%(hcat.mvir[myhalo]*1e11)
        if verbose: print(f"Selected Halo n°{hcat.hnum[myhalo]} with rvir = {rc_kpc} kpc and mvir = {mvir_Msun} Msun.")
    else:
        cu2Msun = info['unit_m']/2e33
        unit_l = info['unit_l']
        xc,yc,zc,rc = info['boxlen']/2,info['boxlen']/2,info['boxlen']/2,info['boxlen']/2
    cu2pc = unit_l/kpc2cm/1e3
    rmax = rmax*rc
    # read particles
    ndm = dm.dmpart_utils.get_npart_in_sphere(RamsesDir,snap,xc,yc,zc,rmax)
    xdm,ydm,zdm,mdm,iddm = dm.dmpart_utils.read_parts_in_sphere(RamsesDir,snap,xc,yc,zc,rmax,ndm)
    minmass = dm.dmpart_utils.get_min_mass(RamsesDir,snap)
    dLR = None
    # define High-res. and Low-res. particles - relevant for zoom only
    if info['is_zoom']:
        lr   = np.where(mdm > minmass*2.)[0]
        d2 = (xdm[lr] - xc)**2 + (ydm[lr]-yc)**2 + (zdm[lr]-zc)**2
        if d2 != []:
            dLR = np.sqrt(d2.min()) ## distance minimum d'une particule au halo -> à comparer avec Rvir. 
            if verbose: print("Contam rad =",round(dLR*hcat.info['unit_l']/3.086e21,2),"kpc")
        else:
            dLR = None
            if verbose: print("Not possible to get contam rad with asked rmax. Contam rad > rmax.")
    xmin,xmax = xc-1.1*rmax,xc+1.1*rmax
    ymin,ymax = yc-1.1*rmax,yc+1.1*rmax
    zmin,zmax = zc-1.1*rmax,zc+1.1*rmax
    if proj_axis == "z":
        xplot, yplot = xdm, ydm
        xlabel, ylabel = 'x', 'y'
    elif proj_axis == "y":
        xplot, yplot = xdm, zdm
        ymin, ymax = zmin, zmax
        yc = zc
        xlabel, ylabel = 'x', 'z'
    elif proj_axis == "x":
        xplot, yplot = zdm, ydm
        xmin, xmax = zmin, zmax
        xc = zc
        xlabel, ylabel = 'z', 'y'
    else:
        print("Unknown projection axis.")
        return
    dmmap,_,_ = np.histogram2d(xplot,yplot,bins=(nx,ny), \
                    range=[[xmin,xmax],[ymin,ymax]],normed=False,weights=mdm)
    dmmap = dmmap * cu2Msun
    dx = (xmax-xmin)/float(nx)*unit_l/pc2cm * 1e-3  # kpc
    dy = (ymax-ymin)/float(ny)*unit_l/pc2cm * 1e-3  # kpc
    dmmap = dmmap / dx / dy 
    fig,ax = plt.subplots(figsize=(6,6))
    ax = fig.add_axes((0,0,1,1))
    extent = xmin*cu2pc/1e3,xmax*cu2pc/1e3, ymin*cu2pc/1e3,ymax*cu2pc/1e3
    im = plt.imshow(dmmap.T,interpolation='nearest',origin='lower',norm=LogNorm(),
               extent=extent,cmap=cmap,vmin=vmin,vmax=vmax)    
    if halo_num != None:
        #Rvir circle
        vir_circle(xc*cu2pc/1e3,yc*cu2pc/1e3,rc*cu2pc/1e3
                  ,color='blue',alpha=1,linestyle='-')
        #Target_radius circle
        if target_radius != None:
            vir_circle(xc*cu2pc/1e3,yc*cu2pc/1e3,target_radius*rc*cu2pc/1e3
                  ,color='green',alpha=1,linestyle='-')
        #R_contam circle
        if dLR != None:
            vir_circle(xc*cu2pc/1e3,yc*cu2pc/1e3,dLR*cu2pc/1e3
                  ,color='red',alpha=1,linestyle='-')
        #title for cosmo run only          
        plt.title(f"Halo n°{int(hcat.hnum[myhalo])} at z = "+str(round(hcat.info['redshift'],1)),\
              fontsize=18,pad=20)
    plt.xlabel(xlabel,size=16)
    plt.ylabel(ylabel,size=16)
    plt.xticks(size=14)
    plt.yticks(size=14)
    ax.spines['bottom'].set_linewidth(3)
    ax.spines['top'].set_linewidth(3)
    ax.spines['left'].set_linewidth(3)
    ax.spines['right'].set_linewidth(3)
    ax.yaxis.set_tick_params(direction='in',size=10,width=3)
    ax.xaxis.set_tick_params(direction='in',size=10,width=3)
    legend = r'$ \Sigma_{\rm DM} \ [\rm M_{\odot} \ / \ \rm kpc^2]$'
    add_cb_dmhalomap(ax,im,legend,loc='right',label_size=20,do_bold=False)
    if figname != None:
        plt.savefig(figname,bbox_inches="tight")
    plt.show()
    return


###################################################################################################################################
########################################################## Make outputs ##########################################################
###################################################################################################################################
def truncate(number, decimals=0):
    """
    Function copied randomly from the net and not checked.
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor

def makemedemoutputs(aout, dt_gtstep=10, stackof=10, stacksat=[2.5,2.0,1.5,1.0], dt=200):
    # Instructions
    # In minirats/scrips, create a list of outputs every 10 Myrs with the command
    # python get_timesteps.py -zfirst 15 -zlast 0.9879 -dt 10 -nml nml.nml
    # and add it in the aout list. 


    # For ex. aout = [0.062499999923, 0.064037935300, 0.065557649333, 0.067059978815, ..., 0.502404665995, 0.503020101177, 0.503635404764]
    # dt_gtstep = 10 # Frequency used for the stacks (entered in get_timesteps.py -dt).
    # stackof = 10   # Number of outputs you want after specific redshifts.
    # stacksat = [2.538,2.028,1.519,1.010] # Redshift at which you want stacks.
    # dt = 200 # Frequency for non-stack outputs.
    # Makes a list ofoutputs every 200yrs from Z~15 to 5 and 10 outputs separated by 10 Myrs around redshift 2.5, 2.0, 1.5 and 1.0.

    aout_nml, i, j, nout = [], 0, 0, 2
    for a in aout:
        # Take 10 outputs following chosend redshifts.
        if (j%stackof!=0) or any([truncate(1/a-1, 3)==zstack for zstack in stacksat]):
            aout_nml.append(a)
            print('       ', 1/a-1,'\t',a,'\t',i, '\t\t','output ', nout)
            j += 1
            i += dt_gtstep
            nout+=1
            if j==40:
                break
            continue
        # Else, take output every dt Myrs.
        if i%dt==0:
            aout_nml.append(a)
            print(1/a-1,'\t',a,'\t',i, '\t\t','output ', nout)
            nout+=1
        i+=dt_gtstep

    return aout