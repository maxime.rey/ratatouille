"""
Functions related to column densities:
======================================

    - Computing and reading column densities
        compute_coldens, read_CD, list_ions_CD, get_path_CD_ion,
        RUdone_ions, RUdone_coldens

    - Column densities and related
        rad_col4, distrib_ion, compute_Nthreshold, plot_cov_frac_obs, fc_50

    - Math tools:
        expdec, expfit, geteqbin, bootstrap, find_nearest

Common parameters: (to do properly, simply copied from plotutils)
==================

    RamsesDir: path
        Path to the folder containing the Ramses output. Basically genpath+folders[i].
    genpath: path
        Path to the main folder.
    folders: array of subpaths
        Subpaths to the folders containing the simulations.

    timestep: int
        Timestep of the output considered.
    timesteps: array
        Array containing the timesteps to consider. If only one is used as input, a list from the third output up to the chosen one will be used.
"""
# Astro and basic libraries
import minirats.utils.py.readwrite.ramses_info as ri
from ratatouille import readNsave as ras
from ratatouille import plotutils as put
from ratatouille.constants import *
import subprocess, fileinput, shutil
import glob, os, warnings

# Data manipulation and plotting libraries
from matplotlib.colors import LogNorm
import matplotlib.markers as mmarkers
import matplotlib.pyplot as plt
from scipy.interpolate import interpn
from scipy import stats
import scipy.optimize
import pandas as pd
import numpy as np
import random, time


###################################################################################################################################
############################################################ Computing ############################################################
###################################################################################################################################
def compute_coldens(genpath, folders, from_tstep, to_tstep, ion_list, dmax_rvir=5, bmax_rvir=2, nrays=100000, \
        path_rascas='/Xnfs/cosmos/mrey/clones/rascas/f90/', savepath='default', hnum=None, alt_path=None, chng_dat=''):
    """
    Traces nrays over dmax_rvir [Rvir], up to impact parameters of bmax_rvir [Rvir] for all the ions specified (list of strings).
    They are saved by default by atomic species in ratadat/rascas/00xxx/ColDens_{atom}.dat.
    Need to have compiled the files CreateDomDump and ColumnDensities in rascas. 
    """

    # Sanity checks
    if 2*bmax_rvir>dmax_rvir:
        raise ValueError('Impact parameter greater than the region loaded.')
    if not isinstance(ion_list, list):
        raise SyntaxError("Mah man/girl/snail, this is no list. I won't work.")

    # Create atomic_data_dir files.
    rat_path = os.path.dirname(ras.__file__)+'/utils/'
    path2datadir = rat_path+'/dations/'
    for ion in ion_list:
        if not os.path.exists(path2datadir+f'{ion}.dat'):
            with open(path2datadir+f'{ion}.dat', 'w') as f: f.write(f'name_ion  = {ion}')

    # Group ions requested by element and compute them together.
    timesteps = np.linspace(from_tstep, to_tstep, abs(to_tstep-from_tstep)+1, dtype=int)
    elem_list = {ion.strip('IVX') for ion in ion_list}
    for elem in elem_list:
        ions_set = set([ion for ion in ion_list if elem in ion])
        print('Checking', elem, {chng_dat})
        for folder in folders:
            RamsesDir = genpath+folder
            if savepath=='default':
                path_sv = RamsesDir+'/ratadat/'
            else:
                path_sv = savepath
            ras_path = f'{path_sv}/rascas{chng_dat}/'
            ion_path = f'{path_sv}/ions{chng_dat}/'
            if not os.path.exists(ras_path):
                os.mkdir(ras_path)
            for timestep in timesteps:
                print(f'{folder}: timestep {timestep}')
                path_tstep = f'{ras_path}{timestep:05d}/'
                if not os.path.exists(path_tstep):
                    os.mkdir(path_tstep)

                # Remove ions already computed from the list.
                if os.path.exists(f'{path_tstep}ColDens_{elem}.dat'):
                    ions2compute = ions_set.copy()
                    ions_done = list_ions_CD(ras_path, timestep, elem)
                    ions2compute.difference_update(ions_done)
                    if len(ions2compute)==0:
                        print(f'    All {elem} ions requested are already computed.')
                        continue
                    else:
                        filt_ions = ', '.join(ions2compute)
                        print(f'    Computing {filt_ions}')
                else:
                    ions2compute = ions_set
                    filt_ions = ', '.join(ions_set)

                # Copy params.cfg from ratatouille to the timestep folder.
                shutil.copy(rat_path+'/params.cfg', path_tstep)
                params_path = path_tstep+'/params.cfg'
                # Update the params config file depending on the parameters given above.
                info   = ras.read_info(RamsesDir, timestep)
                center = ras.get_Gcen(RamsesDir, timestep,hnum=hnum,alt_path=alt_path)
                r200   = ras.get_r200(RamsesDir, timestep,hnum=hnum,alt_path=alt_path)
                for line in fileinput.FileInput(params_path,inplace=True):
                    sline=line.strip().split("=")
                    # Paths
                    if sline[0].startswith("snapnum"):
                        sline[1]=f" {timestep}"
                    elif sline[0].startswith("repository"):
                        sline[1]=f" {RamsesDir}/"
                    elif sline[0].startswith("DomDumpDir"):
                        sline[1]=f" {ras_path}{timestep:05d}/"
                    elif sline[0].startswith("OutputFile"):
                        sline[1]=f" {ras_path}{timestep:05d}/ColDens_{elem}.dat"
                    elif sline[0].startswith("krome_data_dir"):
                        sline[1]=f" {ion_path}/{timestep:05d}/compute_fractions/fract_ions/"
                    elif sline[0].startswith("atomic_data_dir"):
                        sline[1]=f" {path2datadir}"
                    # Center and rvir. Precision of 14 to avoid floating point errors.
                    elif sline[0].startswith("comput_dom_pos") or sline[0].startswith("TargetCoords_cu"):
                        sline[1] = f" {center[0]:.14f} {center[1]:.14f} {center[2]:.14f}"
                    elif sline[0].startswith("decomp_dom_xc"):
                        sline[1] = f" {center[0]:.14f}"
                    elif sline[0].startswith("decomp_dom_yc"):
                        sline[1] = f" {center[1]:.14f}"
                    elif sline[0].startswith("decomp_dom_zc"):
                        sline[1] = f" {center[2]:.14f}"
                    # Simu params
                    elif sline[0].startswith("self_shielding"):
                        sline[1]=f" {'T' if info['self_shielding'] else 'F'}"
                    elif sline[0].startswith("cosmo"):
                        sline[1]=f" {'T' if info['is_cosmo'] else 'F'}"
                    elif sline[0].startswith("itemp"):
                        sline[1]=f" {info['iPT']}"
                    elif sline[0].startswith("imetal"):
                        sline[1]=f" {info['iPT']+1+info['rt_isIRtrap']}"
                    elif sline[0].startswith("ihii"):
                        sline[1]=f" {info['iIons']+info['isH2']}"
                    elif sline[0].startswith("iheii "):
                        sline[1]=f" {info['iIons']+info['isH2']+1}"
                    elif sline[0].startswith("iheiii"):
                        sline[1]=f" {info['iIons']+info['isH2']+2}"
                    # Ions selected
                    elif sline[0].startswith("nscatterer"):
                        sline[1]=f" {len(ions2compute)}"
                    elif sline[0].startswith("scatterer_names"):
                        sline[1]=f" {filt_ions}"
                    # Ray settings
                    elif sline[0].startswith("nrays"):
                        sline[1]=f" {nrays}"
                    elif sline[0].startswith("comput_dom_rsp") or sline[0].startswith("MaxDistance_cu") or sline[0].startswith("decomp_dom_rsp"):
                        sline[1]=f" {dmax_rvir*r200}"
                    elif sline[0].startswith("MaxImpactParameter_cu"):
                        sline[1]=f" {bmax_rvir*r200}"
                    line='='.join(sline)
                    print(line)

                # Run the stuff
                print(subprocess.run([f'{path_rascas}/CreateDomDump   {params_path}'], shell=True)) # Have to rerun it for each new computation!
                print(subprocess.run([f'{path_rascas}/ColumnDensities {params_path}'], shell=True)) # , capture_output=True if want to see output


def read_CD(RamsesDir, timesteps, ion, savepath='default'):
    """ Returns impact parameters and column densities of the requested ion. """

    Nion, b_kpc = [], []
    if '_' in ion:
        ion, chng_dat = ion.split('_')[0], ion.split('_')[1]
    else:
        chng_dat = ''
    for timestep in timesteps:
        info   = ras.read_info(RamsesDir, timestep)
        cu2kpc = info['unit_l'] * info['boxlen'] / kpc2cm
        if savepath=='default':
            ras_path = f'{RamsesDir}/ratadat/rascas{chng_dat}/'
        else:
            ras_path = f'{savepath}/rascas{chng_dat}/'
        path   = get_path_CD_ion(ras_path, timestep, ion)
        with open(path, 'r') as file:
            first_line = file.readline()
            a = ion.strip('IVX')
            ion_list = [tempion for tempion in first_line.split() if tempion.startswith(a)]
            iion = ion_list.index(ion)+1
        my_data = np.genfromtxt(path, comments="#", delimiter='')
        b_kpc.append(my_data[:,0]*cu2kpc)
        Nion.append(my_data[:,iion])
    b_kpc = np.array(b_kpc)
    Nion  = np.array(Nion ) # [time, ions, elements]
    return b_kpc, Nion


def list_ions_CD(ras_path, timestep, element):
    """ Returns the list of ions for which the column density has been computed and their path. """
    ion_list = []
    for coldens_file in glob.glob(f'{ras_path}{timestep:05d}/ColDens_{element}*.dat'):
        with open(coldens_file, 'r') as file:
            ions = [word for word in file.readline().split() if word.startswith(element)]
            ion_list.extend(ions)
    return ion_list


def get_path_CD_ion(ras_path, timestep, ion):
    element = ion.strip('IVX')
    file_pattern = f'{ras_path}{timestep:05d}/ColDens_{element}*.dat'
    matching_file = next((file_path for file_path in glob.glob(file_pattern) if ion in open(file_path).readline().split()), None)
    if matching_file:
        return matching_file
    else:
        chng_dat = '_'+ras_path.split('rascas')[1].split('/')[0] if ras_path.split('rascas')[1].split('/')[0] else ''
        raise ValueError(f'There is an error with the ion {ion}{chng_dat}. The corresponding column densities might not be computed yet.')


def RUdone_ions(genpath, folders, ions_max, chng_dat=[''], min_tstep=0):
    """ Check whether all ionic species up to those specified have been computed.\nUsage: RUdone_ions(genpath, foldersLR, timesteps3, ['MgIII', 'CIV', 'OVIII'])"""
    # Compute list of all ions that should be computed.
    rom2dec = {'I': 1, 'II': 2, 'III': 3, 'IV': 4, 'V': 5, 'VI': 6, 'VII': 7, 'VIII': 8, 'IX': 9, 'X': 10}
    dec2rom = {1: 'I',2: 'II',3: 'III',4: 'IV',5: 'V',6: 'VI',7: 'VII',8: 'VIII',9: 'IX',10: 'X'}
    list_ions = []
    if chng_dat=='': chng_dat=['']
    for ions_m in ions_max:
        element = ions_m.strip('IVX')
        max_state = rom2dec[ions_m.strip(element)]
        for state in range(1, max_state+1):
            list_ions.append(element+dec2rom[state])
    # Check if they're done
    for folder in folders:
        for chng_dat_str in chng_dat:
            print(folder, chng_dat_str)
            output_dirs = glob.glob(os.path.join(genpath+folder, "*output_*"))
            output_dirs = [output_dir for output_dir in output_dirs if int(os.path.basename(output_dir)[-5:]) >= min_tstep] # Only after min_tstep
            nb_tstep = len(output_dirs) # min_tstep is the first timestep to consider in the count.
            with open(f"{genpath}{folder}/output_{1:05d}/info_{1:05d}.txt", 'r') as file:
                nb_cores = int(file.readlines()[-1].split()[0])
            nope_ion = []
            done_ion = []
            comp_str = ''
            count_snap = 0
            count_comp = 0
            for the_ion in list_ions:
                nope_tstep = []
                done_tstep = []
                comp_tstep = []
                for out_dir in output_dirs:
                    timestep = int(out_dir[-5:])
                    nb_ionfiles = len(glob.glob(f'{genpath}{folder}/ratadat/ions{chng_dat_str}/{timestep:05d}/compute_fractions/fract_ions/{the_ion}_{timestep:05d}.out*'))
                    if nb_ionfiles==0:
                        nope_tstep.append(timestep)
                    elif nb_ionfiles==nb_cores:
                        done_tstep.append(timestep)
                    else:
                        comp_tstep.append(timestep)
                if not done_tstep:
                    nope_ion.append(the_ion)
                elif len(done_tstep)==nb_tstep:
                    done_ion.append(the_ion)
                else:
                    count_snap += 1
                    if count_snap==len(list_ions):
                        print(f'{" "*len(folder)}\t {len(done_tstep)}/{nb_tstep} timesteps done.')
                if comp_tstep:
                    if str(comp_tstep[0]) not in comp_str:
                        if len(comp_str) != 0:
                            comp_str = comp_str
                        comp_str += str(comp_tstep[0])
                        count_comp+=1
                    else:
                        count_comp+=1
            if done_ion:
                if len(done_ion)==len(list_ions):
                    print(f'{" "*len(folder)}\t All done \\o/')
                else:
                    print(f'{" "*len(folder)}\t Done: {done_ion}')
            if nope_ion:
                print(f'{" "*len(folder)}\t Not started: {nope_ion}')
            if comp_str:
                print(f'{" "*len(folder)}\t Currently running (or crashed huehuehue): {comp_str} ({count_comp}/{len(list_ions)} ions)')


def RUdone_coldens(genpath, folders, atoms, chng_dat=[''], min_tstep=0):
    """ Check whether all column densities have been computed.\nUsage: RUdone_coldens(genpath, foldersLR, timesteps3, ['H', 'Mg', 'C', 'O'])"""
    if chng_dat=='': chng_dat=['']
    for folder in folders:
        for chng_dat_str in chng_dat:
            output_dirs = glob.glob(os.path.join(genpath+folder, "*output_*"))
            output_dirs = [output_dir for output_dir in output_dirs if int(os.path.basename(output_dir)[-5:]) >= min_tstep] # Only after min_tstep
            nb_tstep = len(output_dirs) # min_tstep is the first timestep to consider in the count.
            nope = []
            done = []
            print(f'{folder} - {chng_dat_str if chng_dat_str else ""}')
            f_str = ' '*(len(folder)+3)
            for atom in atoms:
                missing_dirs = []
                for out_dir in output_dirs:
                    timestep = int(out_dir[-5:])
                    data_dir = f'{genpath}{folder}/ratadat/rascas{chng_dat_str}/{timestep:05}/ColDens_{atom}.dat'
                    if not (os.path.exists(data_dir)):
                        missing_dirs.append(timestep)
                missing_dirs = np.array(missing_dirs)
                dir_not_done = len(missing_dirs)
                if dir_not_done==nb_tstep:
                    nope.append(atom)
                elif dir_not_done > 0:
                    nb_done = (nb_tstep-len(missing_dirs))
                    if dir_not_done>5:
                        print(f'{f_str}{atom:2} {nb_done}/{nb_tstep} timesteps done.')
                    else:
                        print(f'{f_str}{atom:2} {sorted(missing_dirs.tolist())} left to compute')
                else:
                    done.append(atom)
            if nope:
                print(f'{f_str}{nope} not started.')
            if done:
                if len(done)==len(atoms):
                    print(f'{f_str}Yay, all done')
                else:
                    print(f'{f_str}{done} done.')      


###################################################################################################################################
########################################################### Main plots  ###########################################################
###################################################################################################################################
def rad_col4(genpath, folders, timesteps, ions, rad_col=True, w_min=0.3, t_evol=False, nbins=50, xlims=None, ylims=None, logx=True, logy=True, savefig=False, savepath='default', ax=None):
    """ 
    Plots column densities (rad_col=True) or covering fraction (else) against radius using results from rascas.
    w_min is important to compute the column density threshold for the covering fractions! 
    """
    warnings.filterwarnings("ignore")

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    ls = ['-','--',':','-.']
    mark = list(mmarkers.MarkerStyle.markers.keys())[1:]

    # Initialisation
    percentiles, do_bootstrap = False, False
    bestfit = False     # Fit data to an exponential 
    if any(ion.split('_')[0].strip('IVX') == ion for ion in ions):    # E.g. comparing MgII to Mg
        plot_type = 'max_CD'
    elif any('_' in ion for ion in ions):               # E.g. comparing MgII to MgII_Z2
        plot_type = 'chng_dat'
    elif len(ions)>1:
        plot_type = 'many_ions'
    else:                                               # E.g. comparing MgII in different folders
        plot_type = 'default'
        percentiles = True
        do_bootstrap = False    # Doesn't change much.
    default_runs = True
    max_CD = False
    
    if not ax: fig, ax = plt.subplots(nrows=1, ncols=1)

    for iion, ion in enumerate(ions):
        if plot_type == 'chng_dat':             # TODO: do something better
            marker = mark[iion]
            linestyle = '-'
        else:
            marker = ''
            linestyle = ls[iion % len(ls)]
        if ion==ion.split('_')[0]:
            linestyle = '-'
            marker = ''


        for ifold, folder in enumerate(folders):
            # Read data and make x_axis
            RamsesDir = genpath+folder
            colour = colors[ifold] if len(folders)>1 else None
            lab = put.foldername(folder) if plot_type=='default' else ''
            if len(folders)>1:
                lab = put.foldername(folder)
            else:
                if '_' in str(ion): 
                    ion_chng = str(ion).split('_')[1] if str(ion).split('_')[1] else str(ion).split('_')[0]
                    lab = rf'{ion_chng[0]}$\times${ion_chng[1:]}'
                elif plot_type=='chng_dat':
                    lab = 'default'
                else: # if plot_type!='chng_dat'
                    lab = rf'$N_\mathrm{{{str(ion)}}}$'

            b_kpc, Nion = read_CD(RamsesDir, timesteps, ion, savepath=savepath)
            min_b, max_b = np.min(b_kpc), np.max(b_kpc)
            if logx:
                bins = np.logspace(np.log10(min_b), np.log10(max_b), num=nbins)
                x_axis = 10**((np.log10(bins[:-1])+np.log10(bins[1:]))*0.5)
                ax.set_xscale('log')
            else:
                bins = np.linspace(min_b, max_b, num=nbins)
                x_axis = (bins[:-1]+bins[1:])*0.5

            # Plot simulations
            if not t_evol:
                b_kpc = b_kpc.reshape(-1)
                Nion  = Nion.reshape(-1)
            else:
                redshifts = np.array([1 / ras.read_info(RamsesDir, timestep)['aexp'] - 1 for timestep in timesteps])
                cmap = plt.get_cmap('jet')
                norm = plt.Normalize(vmin=min(redshifts), vmax=max(redshifts))
            if rad_col: # Plot column density
                if t_evol:      # Time evolution
                    for itime in range(np.shape(b_kpc)[0]):
                        med_dens, _, _ = stats.binned_statistic(b_kpc[itime,:], Nion[itime,:], statistic='median', bins=bins)
                        ax.plot(x_axis, med_dens, label=None, color=cmap(norm(redshifts[itime])), lw=1)
                else:           # Basic
                    med_dens, _, _ = stats.binned_statistic(b_kpc, Nion, statistic='median', bins=bins) # Median
                    ax.plot(x_axis, med_dens, label=lab, color=colour, ls=linestyle, marker=marker)
                    if percentiles:                                                                    # Percentiles
                        perc_lower, _, _ = stats.binned_statistic(b_kpc, Nion, statistic=lambda x: np.nanpercentile(x, perc_min), bins=bins)
                        perc_upper, _, _ = stats.binned_statistic(b_kpc, Nion, statistic=lambda x: np.nanpercentile(x, perc_max), bins=bins)
                        ax.fill_between(x_axis, perc_lower, perc_upper, alpha=0.25, color=colour)
                    if bestfit:                                                                         # Best fit
                        med_dens = np.array(med_dens)
                        sel = ~np.isnan((med_dens))
                        ax.plot(x_axis[sel], expfit(x_axis[sel], med_dens[sel], ion=ion), '--', color=colour)
            else:       # Plot covering fraction
                N_min_det = compute_Nthreshold(ion, w_min)
                if t_evol:      # Time evolution
                    for itime in range(np.shape(b_kpc)[0]): # Loop through timesteps/redshifts.
                        b_kpc2, Nion2 = b_kpc[itime,:], Nion[itime,:]
                        Nsup, _, _ = stats.binned_statistic(b_kpc2[Nion2>N_min_det], Nion2[Nion2>N_min_det], statistic='count', bins=bins)
                        Ntot, _, _ = stats.binned_statistic(b_kpc2, Nion2, statistic='count', bins=bins)
                        ax.plot(x_axis, Nsup/Ntot, color=cmap(norm(redshifts[itime])), lw=1)
                else:           # Basic
                    Nsup, _, _ = stats.binned_statistic(b_kpc[Nion>N_min_det], Nion[Nion>N_min_det], statistic='count', bins=bins)
                    Ntot, bin_edges, _ = stats.binned_statistic(b_kpc, Nion, statistic='count', bins=bins)
                    ax.plot(x_axis, Nsup/Ntot, c=colour, label=lab, ls=linestyle, marker=marker)
                    if do_bootstrap:   # Estimate error with bootstrap
                        hist, bin_edges = np.histogram(b_kpc, bins=bins)
                        smallest_bin = min(count for i, count in enumerate(hist) if bin_edges[i] > 10) # Compute the size of the smallest bin after 10 kpc (=146/snapshot with nbins=75)
                        try:
                            err = bootstrap(b_kpc, np.array([Nion>N_min_det]).reshape(-1), bins, n_iter=50, len_sample=round(smallest_bin/10)) # Boostrap 50 times over samples of size smallest_bin/10
                        except ValueError:
                            raise ValueError('If the error is "Sample larger than population or is negative", you have too many bins compared to your subsample size.'
                                f'Your smallest bin size is {min(len(b_kpc[(b_kpc>bin_edges[0])&(b_kpc<=bin_edges[1])]), len(b_kpc[(b_kpc>bin_edges[-2])&(b_kpc<=bin_edges[-1])]))}.') # min(firstbin,lastbin)
                        ax.fill_between(x_axis,list(zip(*err))[0],list(zip(*err))[1], alpha=0.25, color=colour)

    if logy: ax.set_yscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)

    # Plot observation or colorbar
    ion = ions[0]
    if t_evol:  # File name and colorbar
        fname = f'{fname.split(f"_{w_min}")[0]}_tevol_{w_min}'
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])
        cbar = plt.colorbar(sm, ax=ax)
        cbar.set_label('Redshift')
    else:       # Plot observations
        nolab = plot_type=='max_CD' or plot_type=='chng_dat'
        if not rad_col:
            plot_cov_frac_obs(genpath, folders, timesteps, ion, w_min, ax=ax, nolab=nolab)
            plot_cov_frac_compil(ion, w_min, nbins, logx, n_iter=100, ax=ax, nolab=nolab)
        else:
            if plot_type=='default' or plot_type=='max_CD' or plot_type=='chng_dat':
                put.plot_dexter(ion, path2files='/Xnfs/cosmos/mrey/outputs/data/Ions/', ax=ax, nolab=nolab)
                put.plot_data(ion, path2files='/Xnfs/cosmos/mrey/outputs/data/Ions2/', ax=ax, nolab=nolab, logx=logx)

    # Additional features
            if plot_type=='default':    # N_thresh grey hline (paper)
                ax.axhline(compute_Nthreshold(ion, w_min), color='grey', alpha=0.5)

    # Add legend for ions or chng_dat
    if not plot_type=='default' and len(folders)>1:
        for iions, ion in enumerate(ions):
            if '_' in str(ion): 
                ion_chng = str(ion).split('_')[1] if str(ion).split('_')[1] else str(ion).split('_')[0]
                lab_ion = rf'{ion_chng[0]}$\times${ion_chng[1:]}'
            elif plot_type=='chng_dat':
                continue
            else: # if plot_type!='chng_dat'
                lab_ion = rf'$N_\mathrm{{{str(ion)}}}$'
            ax.plot(np.nan, np.nan, c='k', ls=ls[iions] if plot_type!='chng_dat' else '', marker=mark[iions] if plot_type=='chng_dat' else '', label=lab_ion)           # TODO: here and above, marker and ls consistent... ifelse?
    

    # Labels
    if rad_col:
        ax.set_ylabel(rf'$N_\mathrm{{{ion.split('_')[0]}}}\rm\ [cm^{{-2}}]$')# if ions[0][-1]!='_' else rf'$N_\mathrm{{{ions[0][:-1]}}}\rm\ [cm^{{-2}}]$')
        fname = f'N{ion}_VS_R_{w_min}'
    else:
        ax.set_ylabel(rf'$\rm f_c \left(N_{{{ion.split('_')[0]}}} > 10^{{{np.log10(N_min_det):.1f}}}\rm\ \ cm^{{-2}}\right)$')
        fname = f'{ion}_abs_fraction_{w_min}'
    ax.legend(frameon=False)
    ax.set_xlabel('Impact parameter [kpc]')
    plt.tight_layout()
    plt.show()

    # save fig
    if max_CD:
        fname+='_max'
    elif not default_runs:
        fname+='_other'
    if savefig:
        if savefig==True:
            fig.savefig(fname+'.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/{fname}.pdf',bbox_inches='tight')


def distrib_ion(genpath, folders, timesteps, ions=None, nbins=1000, xlims=None, ylims=None, savefig=False, vmin=None, savepath='default'):
    """ Weighted scatter plot of radial column densities. """
    warnings.filterwarnings("ignore")

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    for ion in ions:
        fig, ax = plt.subplots(nrows=1, ncols=1)
        # Load column densities and impact parameters into arrays
        for ifold, folder in enumerate(folders):
            RamsesDir = genpath + folder
            b_kpc, Nion = read_CD(RamsesDir, timesteps, ion, savepath=savepath)
            bins1 = np.logspace(np.log10(np.min(b_kpc)), np.log10(np.max(b_kpc)), num=nbins)
            bins2 = np.logspace(np.log10(np.min(Nion)), np.log10(np.max(Nion)), num=nbins)
            PD, xedges, yedges = np.histogram2d(b_kpc.flatten(),Nion.flatten(), bins=(bins1,bins2))
            im = ax.pcolormesh(xedges, yedges, PD.T, norm=LogNorm(vmin=vmin), cmap='gray_r')
            fig.colorbar(im, ax=ax, pad=0).set_label(label='Number of sightlines',size=16)

            # x, y = b_kpc.flatten(),Nion.flatten()
            # z = interpn((0.5*(xedges[1:] + xedges[:-1]), 0.5*(yedges[1:]+yedges[:-1]) ), PD, np.vstack([x,y]).T, method="splinef2d", bounds_error=False)
            # z[np.where(np.isnan(z))] = 0.0          # To be sure to plot all data
            # idx = z.argsort()                       # Sort the points by density, so that the densest points are plotted last
            # x, y, z = x[idx], y[idx], z[idx]
            # scat = ax.scatter(x, y, c=np.log10(z), s=5, vmin=vmin, vmax=None, cmap='gray_r') #gray_r
            ax.loglog()

            # cbar
            # cbar = fig.colorbar(scat, ax=ax)
            # cbar.ax.set_ylabel('Density')

        if xlims:
            ax.set_xlim(xlims)
        if ylims:
            ax.set_ylim(ylims)

        put.plot_dexter(ion, path2files='/Xnfs/cosmos/mrey/outputs/data/Ions/', ax=ax)
        put.plot_data(ion, path2files='/Xnfs/cosmos/mrey/outputs/data/Ions2/', ax=ax, logx=False)

        plt.xlabel('Impact parameter [kpc]')
        plt.ylabel(rf'$N_{{{str(ion)}}}\rm\ [cm^{{-2}}]$')

        # Previous results
        # plt.text(0.95, 0.95, ' '.join(folders[ifold].split('_')[1:])[:-1], horizontalalignment='right',verticalalignment='top', transform=ax.transAxes, size=20)
        median_t3 = [[np.array([  0.95993713,   1.07230547,   1.19782742,   1.33804273, 1.49467136,   1.66963463,   1.86507875,   2.08340117, 2.32727999,   2.59970677,   2.90402329,   3.24396251, 3.62369433,   4.04787681,   4.52171325,   5.05101604, 5.64227797,   6.30275185,   7.04053949,   7.8646911 , 8.78531626,   9.81370797,  10.96248118,  12.24572751, 13.67918811,  15.28044676,  17.06914559,  19.06722595, 21.29919764,  23.79243952,  26.57753534,  29.68864897, 33.1639434 ,  37.04604891,  41.38258599,  46.22674951, 51.63796122,  57.68259865,  64.43480936,  71.97742048, 80.40295471,  89.81476528, 100.32830375, 112.07253621, 125.19152524, 139.84619714, 156.2163159 , 174.50268832, 194.92962727]),
                            [2.832855e+17, 1.48209e+17, 1.463615e+17, 1.713875e+17, 1.40855e+17, 7.36603e+16, 7.83355e+16, 4.212065e+16, 1.98336e+16, 5096530000000000.0, 8780420000000000.0, 2799620000000000.0, 2614225000000000.0, 1251790000000000.0, 722279000000000.0, 425277500000000.0, 225643500000000.0, 64640950000000.0, 8764360000000.0, 382578000000.0, 111441000000.0, 11703850000.0, 7289500000.0, 7105690000.0, 6175720000.0, 5656970000.0, 5131870000.0, 4733750000.0, 4438900000.0, 4255260000.0, 3904230000.0, 3674120000.0, 3412870000.0, 3215275000.0, 2985195000.0, 2682255000.0, 2513310000.0, 2340710000.0, 2113950000.0, 1926955000.0, 1703600000.0, 1464550000.0, 1249760000.0, 1027660000.0, 848533000.0, 698635000.0, 581079500.0, 491541000.0, 415661500.0],
                            '', '#1f77b4', '-'],
                    [np.array([  0.95002432,   1.06114114,   1.18525442,   1.32388424, 1.4787285 ,   1.65168366,   1.84486802,   2.06064762, 2.30166525,   2.57087281,   2.87156744,   3.20743195, 3.58257986,   4.00160586,   4.469642  ,   4.99242063, 5.57634454,   6.2285654 ,   6.95707137,   7.77078492, 8.67967209,   9.6948646 ,  10.82879614,  12.09535467, 13.51005252,  15.09021637,  16.85519948,  18.82661869, 21.02861919,  23.48817024,  26.23539551,  29.30394197, 32.731392  ,  36.55972372,  40.83582508,  45.61206816, 50.94695056,  56.90581192,  63.5616341 ,  70.99593509, 79.29976739,  88.5748332 ,  98.93473001, 110.50634191, 123.43139361, 137.86818626, 153.99353621, 172.00494066, 192.12299645]),
                            [1.23976e+18, 6.330435e+17, 1.132415e+18, 1.16364e+18, 1.50311e+17, 5.02419e+17, 6.808025e+17, 3.11405e+17, 1.71588e+17, 9.900395e+16, 3.736475e+16, 2.730025e+16, 3.53076e+16, 1.0043e+16, 1.61561e+16, 3681910000000000.0, 1685795000000.0, 181361000000.0, 18912700000.0, 13727700000.0, 11118600000.0, 9598395000.0, 8153670000.0, 8281580000.0, 7247250000.0, 6471750000.0, 6249880000.0, 5741170000.0, 5397265000.0, 4928525000.0, 4871030000.0, 4519150000.0, 4134195000.0, 3803600000.0, 3376505000.0, 2904520000.0, 2531430000.0, 2285090000.0, 1880540000.0, 1643990000.0, 1381045000.0, 1175340000.0, 972360500.0, 780515000.0, 624506000.0, 510806000.0, 414916000.0, 351008000.0, 312830500.0],
                            '', '#ff7f0e', '-'],
                    [np.array([  0.9391304 ,   1.04945048,   1.17272992,   1.31049105, 1.46443505,   1.63646291,   1.82869898,   2.0435171 , 2.28357001,   2.55182204,   2.85158577,   3.18656288, 3.56088991,   3.97918931,   4.44662653,   4.96897383, 5.5526815 ,   6.20495759,   6.93385686,   7.74838026, 8.6585861 ,   9.67571425,  10.81232492,  12.08245378, 13.50178527,  15.08784629,  16.86022263,  18.84080085, 21.05403853,  23.52726626,  26.29102521,  29.37944422, 32.83066124,  36.68729434,  40.99696794,  45.8129009 , 51.19456375,  57.20841306,  63.92871204,  71.43844769, 79.83035549,  89.20806462,  99.68737761, 111.39770039, 124.48364026, 139.10679159, 155.44773134, 173.70824891, 194.113838  ]),
                            [2.89776e+16, 8.12888e+16, 1.18727e+17, 1.32055e+17, 4.207605e+16, 5621975000000000.0, 1.0022685e+16, 1.61154e+16, 9026390000000000.0, 1.55866e+16, 1.791535e+16, 8658770000000000.0, 2.272985e+16, 1.047725e+16, 4258760000000000.0, 4473495000000000.0, 4684045000000000.0, 2824375000000000.0, 1501370000000000.0, 1116850000000000.0, 310507500000000.0, 293479500000000.0, 120627500000000.0, 43669700000000.0, 14181500000000.0, 2440370000000.0, 825668500000.0, 203351000000.0, 91709000000.0, 64878100000.0, 47753050000.0, 38779700000.0, 33638200000.0, 29502100000.0, 24985100000.0, 22247200000.0, 19802250000.0, 17518800000.0, 15396800000.0, 13803600000.0, 12232100000.0, 10947600000.0, 9960070000.0, 9044525000.0, 8196175000.0, 7393480000.0, 6608320000.0, 5771520000.0, 5235420000.0],
                            '', '#2ca02c', '-']]
        np.shape(median_t3[0])
        for i in range(3):
            x_axis, med_dens, lab, col, ls = median_t3[i]
            plt.loglog(x_axis, med_dens, label=lab, color=col, ls=ls)

    for w_min in [0.1,0.3,1]:
        N_min_det = compute_Nthreshold(ion, w_min)    # Paper
        ax.axhline(N_min_det, color='grey', alpha=0.5)
    if savefig:
        if savefig==True:
            fig.savefig(f'bimodality_{ion}.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/bimodality_{ion}.pdf',bbox_inches='tight')


def compute_Nthreshold(ion, w_min):
    """
    Compute the column density threshold from the minimal equivalent width for a given ion.
    If the ion is not HI, MgII, CIV, or OVI, the value is based on one of them.
    N_min_GS is the minimal column density detected in complete samples.
    """
    # Compute column density threshold. Based on eq. width for MgII and CIV, not (yet ?) for HI and OVI.
    if ion[:2]=='Mg':
        lambd, f12 = 2796.352, 0.608 # https://bit.ly/3Mr9Zg8
        # lambd, f12 = 2803.530, 0.303
        N_min_det = w_min / (f12 * (lambd/1000)**2 * 8.8e-15)
        N_min_GS  = 2.40e+12
    elif ion[:1]=='H':
        lambd, f12 = 1215.6701, 0.41641 # https://bit.ly/3BpWhE5
        w_min = None
        N_min_det = 1e14     # Wilde+23 | w_min ~ 0.5415
        N_min_GS  = 1.18e+14 #          | w_min ~ 0.6390
        N_min_det = N_min_GS
    elif ion[:1]=='C':
        lambd, f12 = 1548.202, 0.190 # https://bit.ly/3OclEB2
        # lambd, f12 = 1550.774, 0.0952
        N_min_det = w_min / (f12 * (lambd/1000)**2 * 8.8e-15)
        N_min_GS  = 2.59e+13
    elif ion[:1]=='O':
        lambd, f12 = 1031.912, 0.133 # https://bit.ly/3M5TpRz
        if w_min == 0.1:
            N_min_det = w_min / (f12 * (lambd/1000)**2 * 8.8e-15)
        else:
            w_min = None
            # N_min_det = 1.622E14  # Lowest detection also above highest lower limits
            N_min_det = 10**14.3  # Tchernyshyov+22  | w_min ~ 0.2487
        N_min_GS  = 5.348E13  # Lowest detection | w_min ~ 0.06665
        # w_min = 0.1 <-> N = 8.024e+13
    else:
        raise ValueError(f"Column density threshold not available for {ion}.")

    # Avoid having a threshold lower than the lowest detection of my combined sample.
    # *0.99 to avoid an error because for MgII, 0.1 Å <-> 2.39e+12 cm-2 < 2.40e+12 cm-2... close enough.
    if N_min_det<0.96*N_min_GS:
        print(f'The w_min chosen leads to a column density {N_min_det:.2e} '
            f'lower than the lowest observation of the GS ocompilation {N_min_GS:.2e}, choose a higher one.')
        raise ValueError
    
    return N_min_det


def plot_cov_frac_compil(ion, w_min, nbins, logx, n_iter=100, ax=None, nolab=False):
    """Plot observations (above threshold and non-lower limits)."""
    # Load observed column densities and impact parameters
    coldens_all = []
    lim_all     = []
    impact_all  = []
    path2files_dex    = '/Xnfs/cosmos/mrey/outputs/data/Ions/'                      # Also used in coldens
    path2files2_padex = '/Xnfs/cosmos/mrey/outputs/data/Ions2/'
    b_max = 200
    # Read observation data
    for fileuh in sorted(os.listdir(path2files_dex)):
        if ion in fileuh:
            dat = np.loadtxt(path2files_dex+fileuh, usecols=(0,1,2,3,4,5), dtype=str)
            impact_param = dat[:,0].astype(np.float32)
            selp2 = impact_param<b_max   # Restrict to b_min-b_max
            coldens_all = np.append(coldens_all, dat[:,3].astype(np.float32)[selp2])
            lim_all     = np.append(lim_all , dat[:,2][selp2])
            impact_all  = np.append(impact_all, impact_param[selp2])
    for fileuh in sorted(os.listdir(path2files2_padex)):
        if ion in fileuh:
            dat = np.loadtxt(path2files2_padex+fileuh, usecols=(0,1,2,3,4,5,6), dtype=str)
            impact_param = dat[:,0].astype(np.float32)
            selp = impact_param<b_max   # Restrict to b_min-b_max
            coldens_all = np.append(coldens_all, 10**dat[:,3].astype(np.float32)[selp])
            impact_all  = np.append(impact_all , impact_param[selp])
            lim_all     = np.append(lim_all    , dat[:,2][selp])
    # Do an array with non-detections being np.nan instead of '<' or '>'.
    coldens_det = coldens_all.copy()
    coldens_det[(lim_all=='<')|(lim_all=='>')]=np.nan
    b_min = np.partition(impact_all, 3-1)[3-1]  # Will consider data from the 3rd smallest impact parameter ############# WHY NOT CHANGE ? b_min to real min.
    rge_obs = np.logspace(np.log10(b_min),np.log10(b_max), nbins)

    # Make special bins of at least ten points.
    N_min_det = compute_Nthreshold(ion, w_min)
    sel = (lim_all!='<') & (coldens_all>N_min_det)
    bins_detec = geteqbin(impact_all[sel], minsize=10, init_quant=10)
    N_obs, bin_edges2 = np.histogram(impact_all[sel], bins=bins_detec, weights=None)
    N_all, bin_edges2 = np.histogram(impact_all,      bins=bins_detec, weights=None)
    f_supNmin = N_obs/N_all
    if logx:
        x_axis = 10**((np.log10(bin_edges2[:-1])+np.log10(bin_edges2[1:]))*0.5)
    else:
        x_axis = (bin_edges2[:-1]+bin_edges2[1:])*0.5
    ax.scatter(x_axis, f_supNmin, c='k', marker='x', s=100, label='' if nolab else 'GS compilation') # galaxy selected compilation... r'$\rm ({N_{det} + N_{inf}) / N_{tot}}}$'
    ax.hlines(f_supNmin,bin_edges2[:-1],bin_edges2[1:], color='k')#, ls='--')
    err = bootstrap(impact_all, sel, bins_detec, n_iter=n_iter)
    ax.vlines(x_axis,list(zip(*err))[0],list(zip(*err))[1], color='k')#, ls='--')


def plot_cov_frac_obs(genpath, folders, timesteps, ion, w_min, ax=None, hnum=None, alt_path=None, nolab=False):
    """
    Plot covering fractions from observations (Wlim for MgII | split criterion | redshift range for MgII | other ion   | redshift range for other ion)
        Nielsen+13:                    0.1 - 0.3 - 0.6 - 1.0 | low vs high lux + all | 0.07 ≤ z ≤ 1.1    | 
        Lan+20:                                 0.4 -    1.0 | sf VS quiescent       | 0.04 ≤ z ≤ 1.3    | shade
        Dutta+21:               0.03 - 0.1                   | Group VS single       | 0.09 ≤ z ≤ 1.6    | D           | CIV: 0.03 - 0.1 | 0.1  ≤ z ≤ 1   
        Huang+21:                            0.3             | sf VS quiescent       | 0.10 ≤ z ≤ 0.48   | D
        Schroetter+21:                 0.1 ???               | sf VS quiescent       |    1 ≤ z ≤ 1.5    | k shade     | CIV:        0.1 ???
        Kacprzak+15:                                                                                     | OVI: 0.1    | 0.08  ≤ z ≤ 0.67 
        Tchernyshyov+21:                                                                                 | OVI: 1e14.3 | 0.12  ≤ z ≤ 0.6 
        Wilde+23:                                                                                        | HI : 1e14   | 0.003 ≤ z ≤ 0.48
    """
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color'][3:]
    # col_sf  = 'b'
    # col_ps  = 'r'
    col_all = 'k'

    ################################## 0.04 ≤ z ≤ 1.3 (model depends on z though)
    ############# Lan+20 ############# Strong: W_MgII > 1 Å
    ################################## Weak: 0.4 Å < W_MgII < 1 Å
    rge_Lan = np.logspace(np.log10(50),np.log10(600), 1000)
    col_Lan = colors[1]
    if ion=='MgII':
        r200_tmp = []
        Ms_tmp = []
        z_tmp = []
        # Take min and max redshift, mass and virial radius assuming they evolve monotically with time 
        # (= just check first and last timestep)
        for ifold, folder in enumerate(folders):
            RamsesDir = genpath+folder
            for timestep in [min(timesteps), max(timesteps)]:
                info   = ras.read_info(RamsesDir, timestep)
                center = ras.get_Gcen(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
                r200   = ras.get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
                r200_tmp.append(r200)
                cu2cm = info['unit_l'] * info['boxlen']
                cu2kpc = cu2cm / kpc2cm
                center_cm = [cen*cu2cm for cen in center]
                star_mass = ras.extract_stars(RamsesDir, timestep, ['star_mass'], factor=0.1)
                Ms_tmp.append(np.sum(star_mass)*g2Msun)
                z_tmp.append(1/info['aexp']-1)
        Rv_min, Rv_max = min(r200_tmp)*cu2kpc, max(r200_tmp)*cu2kpc 
        Ms_min, Ms_max = min(  Ms_tmp), max(  Ms_tmp) 
        z_min,  z_max  = min(   z_tmp), max(   z_tmp)

        f_power = lambda A, alph, bet, gam, z, Ms, rge_Lan, R_vir:       A*(1+z)**alph * (Ms/1e10)**bet * (rge_Lan/R_vir)**gam
        f_exp   = lambda cc, dd, z, rge_Lan, R_vir:       np.exp( (-rge_Lan/R_vir) / (cc*(1+z)**dd))
        acti_strabs_min = f_power(0.0024, 3.0, 0.11 , -0.94, z_min, Ms_min, rge_Lan, Rv_min) + f_exp(0.06, 1.4, z_min, rge_Lan, Rv_min)
        pass_strabs_min = f_power(0.004 , 4.0, -0.64, -1.08, z_min, Ms_max, rge_Lan, Rv_min)
        acti_strabs_max = f_power(0.0024, 3.0, 0.11 , -0.94, z_max, Ms_max, rge_Lan, Rv_max) + f_exp(0.06, 1.4, z_max, rge_Lan, Rv_max)
        pass_strabs_max = f_power(0.004 , 4.0, -0.64, -1.08, z_max, Ms_min, rge_Lan, Rv_max)
        if w_min==1:
            ax.fill_between(rge_Lan,acti_strabs_min , acti_strabs_max , color=col_Lan, alpha=0.25, label='Lan+20' if ~nolab else None)
            ax.fill_between(rge_Lan,acti_strabs_min , acti_strabs_max , facecolor='None', edgecolor=col_Lan, alpha=0.99, hatch='.', linewidth=0.0) # Hatch not saved in pdf so have to add this line
            ax.fill_between(rge_Lan,pass_strabs_min , pass_strabs_max , color=col_Lan, alpha=0.25)
            print(f'Lan+20 - W_min = 1 Å - model with z as a variable we use {z_min:.2e}-{z_max:.2e}, with 0.04 ≤ z ≤ 1.3')
        # Should be 0.4 but he's the ONLY one using this so match it with 0.3 sims...
        elif w_min==0.3:    # Add weak absorber fracttion + strong absorber fraction.
            acti_weakabs_min = acti_strabs_min + f_power(0.014 , 1.4, 0.18 , -1.14, z_min, Ms_min, rge_Lan, Rv_min) 
            pass_weakabs_min = pass_strabs_min + f_power(0.026 , 0.8, -0.32, -0.77, z_min, Ms_max, rge_Lan, Rv_min) 
            acti_weakabs_max = acti_strabs_max + f_power(0.014 , 1.4, 0.18 , -1.14, z_max, Ms_max, rge_Lan, Rv_max) 
            pass_weakabs_max = pass_strabs_max + f_power(0.026 , 0.8, -0.32, -0.77, z_max, Ms_min, rge_Lan, Rv_max) 
            ax.fill_between(rge_Lan,acti_weakabs_min, acti_weakabs_max, color=col_Lan, alpha=0.25, label='Lan+20' if ~nolab else None)
            ax.fill_between(rge_Lan,acti_weakabs_min, acti_weakabs_max, facecolor='None', edgecolor=col_Lan, alpha=0.99, hatch='.', linewidth=0.0) # Hatch not saved in pdf so have to add this line
            ax.fill_between(rge_Lan,pass_weakabs_min, pass_weakabs_max, color=col_Lan, alpha=0.25)
            print(f'Lan+20 - W_min = 0.4 Å - model with z as a variable we use {z_min:.2e}-{z_max:.2e}, with 0.04 ≤ z ≤ 1.3')

    ##################################
    ############ Dutta+21 ############ MgII:0.09 ≤ z ≤ 1.6
    ################################## CIV: 0.1  ≤ z ≤ 1
    # if MgII from previous block
    #     col_Dutt = colors[2]      # IT'S CUMULATIVE!!! fc(<r). Non cumulative is Dutta+20 Fig.18 left plot.
    #     if w_min < 0.1: # 0.03 Å
    #         # xD_grp     = np.array([75.0750245836072, 224.97949243130597, 374.9914657943944, 525.4227616179835]) 
    #         # yD_grp     = np.array([0.41673799734038486, 0.23327040948901762, 0.18593168049972236, 0.16956001711936175]) 
    #         # yD_grp_min = np.array([0.2877059036322598, 0.1687537257539397, 0.13238272472983514, 0.1237542607339927]) 
    #         # yD_grp_max = np.array([0.5593178240068477, 0.31068966571389267, 0.2517374114098508, 0.22891541710611463]) 
    #         # ax.scatter(    xD_grp, yD_grp, c=col, marker='D')
    #         # a = ax.errorbar(xD_grp, yD_grp, c=col, yerr=[yD_grp-yD_grp_min,yD_grp_max-yD_grp], ls='none')
    #         # a[-1][0].set_linestyle('--')
    #         # a[-1][1].set_linestyle('--')
    #         xD_iso     = np.array([74.45037168376057, 224.46183554203668, 374.89109334637664, 524.9366940270751]) 
    #         yD_iso     = np.array([0.12577126290957819, 0.07778737345174225, 0.05883506819721929, 0.054076930131605194]) 
    #         yD_iso_min = np.array([0.07480294901385343, 0.05327000188516795, 0.04012477772852574, 0.03794728153707416]) 
    #         yD_iso_max = np.array([0.2077053686522069, 0.11133508093483946, 0.08270536865220701, 0.07472142824388983]) 
    #         ax.scatter( xD_iso, yD_iso, c=col_Dutt, marker='D', label='Dutta+21' if ~nolab else None)
    #         ax.errorbar(xD_iso, yD_iso, c=col_Dutt, yerr=[yD_iso-yD_iso_min,yD_iso_max-yD_iso], ls='none')

    #     elif w_min==0.1:
    #         # xD_grp     = np.array([80.10944163367624, 230.05314135192006, 380.07836184012757, 530.1219245015768]) 
    #         # yD_grp     = np.array([0.2915685869903041, 0.15777835521656502, 0.12721379840932606, 0.11987501846954951]) 
    #         # yD_grp_min = np.array([0.1625358564011637, 0.09519715288710906, 0.07624612139461662, 0.07406798832214978]) 
    #         # yD_grp_max = np.array([0.4335038900692417, 0.23519761144144008, 0.19302016620046986, 0.17729366328864982]) 
    #         # ax.scatter(    xD_grp, yD_grp, c=col, marker='D')
    #         # a = ax.errorbar(xD_grp, yD_grp, c=col, yerr=[yD_grp-yD_grp_min,yD_grp_max-yD_grp], ls='none')
    #         # a[-1][0].set_linestyle('--')
    #         # a[-1][1].set_linestyle('--')
    #         xD_iso     = np.array([80.3361712751377, 230.33031196979653, 380.01314522415663, 530.4403650092474]) 
    #         yD_iso     = np.array([0.07866499549088246, 0.032617224319683724, 0.023342962974285397, 0.02310031130744039]) 
    #         yD_iso_min = np.array([0.027052794888647758, 0.008745650102665259, 0.005922993442673108, 0.007616460062465302]) 
    #         yD_iso_max = np.array([0.16318101686970432, 0.06293912946007774, 0.04463325843612598, 0.043100285832199825]) 
    #         ax.scatter( xD_iso, yD_iso, c=col_Dutt, marker='D', label='Dutta+21' if ~nolab else None)
    #         ax.errorbar(xD_iso, yD_iso, c=col_Dutt, yerr=[yD_iso-yD_iso_min,yD_iso_max-yD_iso], ls='none')
    #         print('Dutta+21 - W_min = 0.03 Å or 0.1 Å - ? ≤ z ≤ ?')

    # elif ion=='CIV':
    #     col_Dutt = colors[2]
    #     if w_min < 0.1: # 0.03 Å
    #         xD_iso     = np.array([74.27075707556943, 224.22503694904486, 374.8147022666685, 524.2264182217497])
    #         yD_iso     = np.array([0.5008909208945119,0.4567012445267067,0.2894677956268905,0.20425984502120254])
    #         yD_iso_min = np.array([0.39933146401093955,0.3922510601268008,0.24845228393441698,0.17887205270936613])
    #         yD_iso_max = np.array([0.6688592068732128,0.534828791248256,0.33829578573697794,0.23551252123706767])
    #         ax.scatter( xD_iso, yD_iso, c=col_Dutt, marker='D', label='Dutta+21' if ~nolab else None)
    #         ax.errorbar(xD_iso, yD_iso, c=col_Dutt, yerr=[yD_iso-yD_iso_min,yD_iso_max-yD_iso], ls='none')

    #     elif w_min==0.1:
    #         xD_iso     = np.array([79.54058869842675, 230.0208572178405, 379.42041797312044, 529.374697846596])
    #         yD_iso     = np.array([0.186463527494233,0.21258891943036318,0.1488652844731133,0.10467560810530796])
    #         yD_iso_min = np.array([0.08880754727405815,0.15008632954404177,0.11175877453485628,0.08124093539787536])
    #         yD_iso_max = np.array([0.35638493307733743,0.28875782145669016,0.1957429175242067,0.13788140392557668])
    #         ax.scatter( xD_iso, yD_iso, c=col_Dutt, marker='D', label='Dutta+21' if ~nolab else None)
    #         ax.errorbar(xD_iso, yD_iso, c=col_Dutt, yerr=[yD_iso-yD_iso_min,yD_iso_max-yD_iso], ls='none')

    ##################################
    ############ Huang+21 ############ 0.10 ≤ z ≤ 0.48
    ################################## W_MgII > 0.3 Å
    col_Huang = colors[3]
    if ion=='MgII':
        if w_min==0.3:
            # Star forming
            x_H_sf        = np.array([27.639325871228408, 55.84916783863622, 293.61648117767703])
            y_H_sf        = np.array([0.8258928571428572, 0.3694196428571428, 0.0])
            x_H_sf_min = np.array([8.621063801916105,42.664985173327715,100.0])
            x_H_sf_max = np.array([38.85964302007221,94.65284050527012,494.8923491767343])
            y_H_sf_min = [0.7633928571428572, 0.3024553571428572, 0]
            y_H_sf_max = [0.8738839285714286, 0.4430803571428571, 0.024553571428571397]
            ax.scatter( x_H_sf, y_H_sf, c=col_Huang, marker='D', label='Huang+21' if ~nolab else None)
            ax.errorbar(x_H_sf, y_H_sf, c=col_Huang, xerr=[x_H_sf-x_H_sf_min,x_H_sf_max-x_H_sf], yerr=[y_H_sf-y_H_sf_min,y_H_sf_max-y_H_sf], ls='none')
            # Quiescent
            x_H_ps        = np.array([29.361648117767675, 54.03770515798784, 298.49724980745646])
            y_H_ps        = np.array([0.5714285714285714, 0.2779017857142857, 0.0234375])
            x_H_ps_min = np.array([12.322309821089508, 41.50863240620575, 103.92174819219943])
            x_H_ps_max = np.array([39.7233023662019, 94.65284050527012, 497.61948186814635])
            y_H_ps_min = np.array([0.3883928571428571, 0.1875, 0.010044642857142794])
            y_H_ps_max = np.array([0.7354910714285714, 0.3928571428571428, 0.05691964285714268])
            ax.scatter(     x_H_ps, y_H_ps, c=col_Huang, marker='D')
            a = ax.errorbar(x_H_ps, y_H_ps, c=col_Huang, xerr=[x_H_ps-x_H_ps_min,x_H_ps_max-x_H_ps], yerr=[y_H_ps-y_H_ps_min,y_H_ps_max-y_H_ps], ls='none')
            a[-1][0].set_linestyle('--')
            a[-1][1].set_linestyle('--')
            print('Huang+21 - W_min = 0.3 Å - 0.10 ≤ z ≤ 0.48')

    ################################## 
    ########### Nielsen+13 ########### Mg ii absorption or a 3σ upper limit on absorption less than or equal to 0.3 Å
    ################################## 0.07 ≤ z ≤ 1.1,
    # if MgII from previous block
        col_Niel = colors[4]
        xW_all = np.array([12.5, 37.5, 75.5, 150])
        xW_min = np.array([0   , 25  , 50  , 100])
        xW_max = np.array([25  , 50  , 100 , 200])

        if w_min==0.1: # Wcut = 0.1 Å
            # yW_all  = [1.00, 0.94, 0.61, 0.29]
            # yW_amin = [0.07, 0.05, 0.08, 0.10]
            # yW_amax = [0.00, 0.03, 0.08, 0.12]
            # plt.scatter( xW_all, yW_all, c=col_Niel, marker='s')
            # plt.errorbar(xW_all, yW_all, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_amin,yW_amax], ls='none')
            yW_low  = [1.00, 0.91, 0.63, 0.00]
            yW_lmin = [0.23, 0.11, 0.16, 0.00]
            yW_lmax = [0.00, 0.06, 0.14, 0.26]
            ax.scatter( xW_all, yW_low, c=col_Niel, marker='s', label='Nielsen+13' if ~nolab else None)
            ax.errorbar(xW_all, yW_low, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_lmin,yW_lmax], ls='none')
            yW_hii  = [1.00, 0.96, 0.68, 0.50]
            yW_hmin = [0.26, 0.08, 0.12, 0.16]
            yW_hmax = [0.00, 0.06, 0.14, 0.26]
            a = ax.scatter( xW_all, yW_hii, c=col_Niel, marker='s')
            a = ax.errorbar(xW_all, yW_hii, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_hmin,yW_hmax], ls='none')
            a[-1][0].set_linestyle('--')
            a[-1][1].set_linestyle('--')

        elif w_min==0.3: # Wcut = 0.3 Å
            # yW_all  = [0.96, 0.79, 0.40, 0.25]
            # yW_amin = [0.08, 0.06, 0.07, 0.09]
            # yW_amax = [0.03, 0.05, 0.08, 0.11]
            # plt.scatter( xW_all, yW_all, c=col_Niel, marker='s')
            # plt.errorbar(xW_all, yW_all, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_amin,yW_amax], ls='none')
            yW_low  = [0.86, 0.91, 0.44, 0.00]
            yW_lmin = [0.26, 0.11, 0.15, 0.00]
            yW_lmax = [0.12, 0.06, 0.15, 0.26]
            ax.scatter( xW_all, yW_low, c=col_Niel, marker='s', label='Nielsen+13' if ~nolab else None)
            ax.errorbar(xW_all, yW_low, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_lmin,yW_lmax], ls='none')
            yW_hii  = [1.00, 0.76, 0.42, 0.29]
            yW_hmin = [0.26, 0.10, 0.10, 0.13]
            yW_hmax = [0.00, 0.08, 0.11, 0.15]
            a = ax.scatter( xW_all, yW_hii, c=col_Niel, marker='s')
            a = ax.errorbar(xW_all, yW_hii, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_hmin,yW_hmax], ls='none')
            a[-1][0].set_linestyle('--')
            a[-1][1].set_linestyle('--')

        # elif w_min==0.6: # Wcut = 0.6 Å
        #     yW_all  = [0.77, 0.53, 0.20, 0.09]
        #     yW_amin = [0.11, 0.07, 0.06, 0.05]
        #     yW_amax = [0.09, 0.07, 0.07, 0.08]
        #     plt.scatter( xW_all, yW_all, c=col_Niel, marker='s')
        #     plt.errorbar(xW_all, yW_all, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_amin,yW_amax], ls='none')
        #     yW_low  = [0.71, 0.48, 0.19, 0.00]
        #     yW_lmin = [0.26, 0.13, 0.10, 0.00]
        #     yW_lmax = [0.18, 0.13, 0.15, 0.26]
        #     plt.scatter( xW_all, yW_low, c=col_Niel, marker='s', label='Nielsen+13' if ~nolab else None)
        #     plt.errorbar(xW_all, yW_low, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_lmin,yW_lmax], ls='none')
        #     yW_hii  = [0.83, 0.59, 0.24, 0.14]
        #     yW_hmin = [0.29, 0.11, 0.08, 0.07]
        #     yW_hmax = [0.14, 0.10, 0.10, 0.12]
        #     plt.scatter( xW_all, yW_hii, c=col_Niel, marker='s')
        #     plt.errorbar(xW_all, yW_hii, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_hmin,yW_hmax], ls='none')

        elif w_min==1: # Wcut = 1.0 Å
            # yW_all  = [0.39, 0.31, 0.13, 0.06]
            # yW_amin = [0.11, 0.06, 0.05, 0.04]
            # yW_amax = [0.12, 0.07, 0.06, 0.08]
            # plt.scatter( xW_all, yW_all, c=col_Niel, marker='s')
            # plt.errorbar(xW_all, yW_all, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_amin,yW_amax], ls='none')
            yW_low  = [0.29, 0.24, 0.13, 0.00]
            yW_lmin = [0.18, 0.10, 0.08, 0.00]
            yW_lmax = [0.26, 0.13, 0.14, 0.26]
            ax.scatter( xW_all, yW_low, c=col_Niel, marker='s', label='Nielsen+13' if ~nolab else None)
            ax.errorbar(xW_all, yW_low, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_lmin,yW_lmax], ls='none')
            yW_hii  = [0.50, 0.45, 0.15, 0.09]
            yW_hmin = [0.26, 0.11, 0.06, 0.06]
            yW_hmax = [0.26, 0.11, 0.09, 0.11]
            a = ax.scatter( xW_all, yW_hii, c=col_Niel, marker='s')
            a = ax.errorbar(xW_all, yW_hii, c=col_Niel, xerr=[xW_all-xW_min,xW_max-xW_all], yerr=[yW_hmin,yW_hmax], ls='none')
            a[-1][0].set_linestyle('--')
            a[-1][1].set_linestyle('--')

    ##################################
    ######### Schroetter+21  ######### z = 1 - 1.5
    ################################## W_MgII/CIV = Wlim= 0.05 Å. Can use 0.1 because this is the lowest detection. 
    if w_min==0.1:
        rge_Schroe = np.logspace(0.2,np.log10(250), 1000)
        z_tmp = []
        for ifold, folder in enumerate(folders):
            for timestep in [min(timesteps), max(timesteps)]:
                info = ras.read_info(genpath+folder, timestep)
                z_tmp.append(1/info['aexp']-1)
        z_min, z_max = min(z_tmp), max(z_tmp) 
        if z_min<1.2 and 1.2<z_max:
            z_S = 1.2 # erorr bars in Schroetter+21
        else:
            z_S = round((z_min+z_max)/2,2)

        f6  = lambda A, C, b:       A*(np.log10(b)-C)
        f7  = lambda A, B, C, b, z: A*(np.log10(b)-(B*np.log10(1+z)+C))
        f_c = lambda t: 1/(1+np.exp(-t))
        if ion=='MgII':
            f6   = f6(-4.7, 1.6, rge_Schroe)
            f7   = f7(-4.2, 2.0, 1.0, rge_Schroe, z_S)
            if z_S==1.2:
                ax.plot(rge_Schroe,f_c(f7), c=col_all)
                Y_min = np.array([0.9664626906646012, 0.9664626906646012, 0.9664626906646012, 0.9525987973719533, 0.9344224768021033, 0.9210181999130206, 0.9078062071816713, 0.8947837402945994, 0.8692965480716929, 0.8445353379360261, 0.812614040216546, 0.7818992867378147, 0.7451332374655046, 0.7032887637219546, 0.6606048131137637, 0.6086715964761695, 0.5554448726848654, 0.5044373090210676, 0.4537222343772459, 0.40225167362798747, 0.3382182890742798, 0.3042145279139581, 0.2749504913624581, 0.2334194841153198, 0.2109656047367682, 0.17653027400883628, 0.1448971967169565, 0.12301061452856911, 0.10442997952717085, 0.0857168062059361, 0.07454302730799596, 0.06451435850845261, 0.054244487682686866, 0.0445242281908846, 0.03466000482988331, 0.028586507205791464, 0.023239057308466895, 0.01844240174378895, 0.01463579944593491, 0.011783938992171365])
                X_min = np.array([1.6295005566554486, 2.1308846068482414, 2.436759352600014, 3.0784974092186967, 3.6860723855541293, 4.329792769409906, 5.047096819459173, 5.7937360870526975, 6.831661242216252, 8.117506851567379, 9.794368892546787, 11.286463797836012, 13.308389635085538, 15.75279005655106, 18.362508513972728, 21.902450831174264, 25.826189216188123, 30.220331001131036, 35.36210465171822, 41.22044065896334, 50.11816363355703, 55.369243654395746, 61.40537607590403, 70.76000114724962, 76.69012700038033, 87.02888915868675, 98.76144224089511, 108.6917300861665, 119.16294194202075, 132.15360697237887, 141.591622208931, 151.70367225729112, 164.41737850774382, 180.25712272539414, 201.44614703964004, 219.16689459022092, 240.2811319220266, 264.44097355286635, 292.1475097114425, 320.2925991761478])
                Y_max = np.array([1.0289091714259861, 0.9947987290716058, 0.9852622552303409, 0.9758172012217188, 0.9711286824564203, 0.9711286824564203, 0.9664626906646012, 0.9434668662115303, 0.9344224768021033, 0.9121890109382854, 0.8947837402945994, 0.8568265016865129, 0.7415530882714437, 0.6965468020546439, 0.6574307984941685, 0.6086715964761695, 0.5527761210934697, 0.5020136339861317, 0.4537222343772459, 0.42007134041740934, 0.3583416964807323, 0.289909915398833, 0.24493676964574593, 0.21198412701348535, 0.1773825457183281, 0.14771572683859205, 0.13742046517988915, 0.11610270306923505, 0.09668479557752356, 0.0767285789130158, 0.06420438571834465, 0.054244487682686866, 0.04473918701473, 0.03689950703281931, 0.031025571686909628, 0.022253243190975155, 0.016271724699612144, 1.9903905797941894])
                X_max = np.array([1.6295005566554486, 7.634715423817362, 9.498642711273492, 11.029903532461743, 12.138940769035896, 13.881407446720768, 14.929882893781594, 18.08316968886375, 19.598651180382205, 22.326185545659676, 25.046406424919486, 28.75163166982681, 38.91770155308433, 42.66697941978301, 46.242736254576194, 50.69769789454817, 56.009498032778474, 61.64115371656254, 67.0635821908228, 71.85306259196719, 80.91714306415662, 95.04817711490034, 107.86182777904203, 120.07979642592593, 138.37300698813323, 159.4530439993652, 168.2417969297129, 192.39182208607858, 223.40699640438217, 270.5919835005854, 313.0118108163958, 356.57350610255037, 417.24194345709105, 486.3651906495859, 566.9398831662157, 738.5468764625267, 947.4617128421213, 973.2226570590941])
                ax.fill(np.append(X_min, X_max[::-1]), np.append(Y_min, Y_max[::-1]), alpha=0.25, color=col_all, label='Schroetter+21' if ~nolab else None)
            else:
                ax.plot(rge_Schroe,f_c(f7), c=col_all, label=f'Schroetter+21, z={z_S}' if ~nolab else None)
                print(f'Schroetter+21 - W_min = 0.05 Å - z={z_S} in model from 1 ≤ z ≤ 1.5')
                
        elif ion=='CIV':
            f6   = f6(-3.0, 1.4, rge_Schroe)
            f7   = f7(-2.8, -0.7, 1.6, rge_Schroe, z_S)
            if z_S==1.2:
                ax.plot(rge_Schroe,f_c(f7), c=col_all)
                Y_max = np.array([1.006293797050794, 0.9875302520924798, 0.9752159987978365, 0.9570319363588758, 0.9332978924264302, 0.9178739385596534, 0.9111354740139659, 0.8900246539513943, 0.8896549485669772, 0.8560446998930457, 0.8410396306440088, 0.8231570850982188, 0.8048639956649352, 0.7770764168299319, 0.7216234317746917, 0.669288062613367, 0.6168658607149586, 0.5808698173797184, 0.5370579412492544, 0.5046108653184196, 0.46954271739040315, 0.43161462093656144, 0.38861099602055127, 0.35786581956567587, 0.3153814454594393, 0.2876543832475802, 0.26206423408339274, 0.2412713372330558, 0.222872594118098, 0.2031898021088269, 0.19083320795275038, 0.17568073618446003, 0.16246000719959258, 0.1515245069797662, 0.14240159365608995, 0.13173306815202146, 0.12428889285014746, 0.11800631190861575, 0.11247404456948115, 0.1069635965398125, 0.10137108580800648, 0.09621274118578652, 0.09173553422174606, 0.08730615544113755, 0.0833503522711883, 0.07820689371004119, 0.07512636926406759, 0.070995819293444, 0.06743987830776202, 0.06491999416258197, 0.0618053973638316, 0.06021967595776934])
                X_max = np.array([1.6283210676466617, 4.214421273301012, 5.8801991307119, 8.08642966441816, 10, 10.999999999999991, 12.1, 13.310000000000002, 14.641000000000014, 16.10510000000001, 17.715610000000016, 19.487171000000032, 21.435888100000017, 23.579476910000025, 27.031231261960773, 31.092989041771563, 35.250867676009115, 37.974983358324224, 41.77248169415661, 45.94972986357231, 50.54470284992952, 55.59917313492249, 61.15909044841478, 67.27499949325625, 74.00249944258188, 81.40274938684017, 89.54302432552412, 98.4973267580765, 108.34705943388417, 120.133621746758, 129.15496650148827, 144.20993610649987, 158.63092971714931, 174.49402268886436, 191.94342495775095, 211.13776745352578, 232.25154419887852, 255.47669861876656, 281.02436848064286, 309.12680532870735, 340.0394858615784, 374.04343444773497, 411.4477778925079, 452.592555681759, 497.8518112499353, 547.6369923749293, 602.4006916124214, 662.6407607736626, 728.904836851031, 801.7953205361314, 881.9748525897434, 970.1723378487203])
                Y_min = np.array([0.5846050159853953, 0.572910334966601, 0.5616742553039715, 0.5504297816222601, 0.5419659399839643, 0.5308481734703359, 0.5186159494717584, 0.5019266512681166, 0.4884581524589902, 0.48050261145131967, 0.4689578750619587, 0.4577724767483856, 0.4428698854478124, 0.4276365630807316, 0.41970656400078626, 0.4094779343448564, 0.3922148375854178, 0.38064904826381857, 0.3681596134337235, 0.35725703357624017, 0.34761655773363703, 0.32877295008414437, 0.31734311677198845, 0.30812381346392553, 0.29583595204050606, 0.2867708431238453, 0.27344216574060415, 0.26125905360703733, 0.2519067241948918, 0.2424642622620671, 0.23202858924608322, 0.22301628409513222, 0.20927853786883385, 0.2025304891887035, 0.19384041839534735, 0.18103951009714161, 0.16970608700016837, 0.16267122420716004, 0.15103914548551833, 0.13978270525865555, 0.12832101243106203, 0.11525801763261849, 0.10377896464653646, 0.09216746796682511, 0.08108456089462873, 0.070578338872026, 0.06195348085587101, 0.053248933885843897, 0.04552104861000046, 0.03984004243548687, 0.033540653953564224, 0.028780785388716184, 0.0242899076132357, 0.02076892116958658, 0.017588613580991577, 0.015809206845513308, 0.014470813045380986, 0.012309299467460127, 0.010850702503758525])
                X_min = np.array([1.7715610000000006, 1.948717100000001, 2.143588810000001, 2.3579476910000015, 2.5937424601000014, 2.8531167061100025, 3.138428376721002, 3.4522712143931042, 3.7974983358324135, 4.1772481694156545, 4.594972986357221, 5.054470284992943, 5.559917313492236, 6.115909044841459, 6.727499949325605, 7.4002499442581655, 8.140274938683982, 8.95430243255238, 9.849732675807614, 10.834705943388384, 11.918176537727213, 13.109994191499943, 14.420993610649942, 15.863092971714948, 17.449402268886438, 19.194342495775086, 21.11377674535261, 23.225154419887854, 25.547669861876642, 28.10243684806433, 30.912680532870784, 34.00394858615783, 37.404343444773644, 41.144777789251044, 45.25925556817611, 49.785181124993755, 54.76369923749312, 60.240069161242445, 66.26407607736674, 72.89048368510339, 80.17953205361374, 88.19748525897523, 97.01723378487277, 105.4534509764992, 117.39085287969606, 129.1299381676655, 142.04293198443185, 156.24722518287516, 171.8719477011628, 189.05914247127885, 207.96505671840686, 228.76156239024772, 251.63771862927217, 276.80149049219847, 300.5989237895584, 318.5258285621434, 334.92980349556063, 368.4227838451162, 405.26506222962814])
                ax.fill(np.append(X_min, X_max[::-1]), np.append(Y_min, Y_max[::-1]), alpha=0.25, color=col_all, label='Schroetter+21' if ~nolab else None)
            else:
                ax.plot(rge_Schroe,f_c(f7), c=col_all, label=f'Schroetter+21, z={z_S}' if ~nolab else None)
                print(f'Schroetter+21 - W_min = 0.05 Å - z={z_S} in model from 1 ≤ z ≤ 1.5')

    ################################## Advised against using it by Nicolas, photometric z -> uncertainty of ±1000 km/s
    ############# Zou+23 ############# Mgii (z = 0.9 – 2.1, Wr ≥ 1.0  ̊A)
    ################################## Civ  (z = 1.5 – 2.1, Wr ≥ 0.5  ̊A)
    # if ion=='MgII' and w_min>=0.75:
    #     x_Z_min = np.array([50,100,200])
    #     x_Z_max = np.array([100,200,250])
    #     x_Z = (x_Z_min + x_Z_max)/2

    #     y_Z_all = [0.152, 0.074, 0.061] # all galaxies
    #     y_Z_sf  = [0.290, 0.154, 0.103] # star forming
    #     plt.scatter(x_Z, y_Z_all, marker='o', c=col_all, label='Zou+23' if ~nolab else None)
    #     plt.scatter(x_Z, y_Z_sf , marker='o', c=col_sf)
    #     # plt.errorbar(x_Z, y_Z_sf, xerr=[25, 50, 25], yerr=[0.076, 0.119, 0.187], ls='none')
    #     # plt.errorbar(x_Z, y_Z_sf, xerr=[25, 50, 25], yerr=[0.127, 0.293, 0.141], ls='none')
    #     print('Zou+23 - W_min = 1 Å - 0.09 ≤ z ≤ 2.1')
    # elif ion=='CIV' and w_min<=0.75:
    #     x_Z_min = np.array([50,100,200])
    #     x_Z_max = np.array([100,200,250])
    #     x_Z = (x_Z_min + x_Z_max)/2

    #     y_Z_all = [0.176, 0.071, 0.055] # all galaxies
    #     y_Z_sf  = [0.333, 0.167, 0.100] # star forming
    #     plt.scatter(x_Z, y_Z_all, marker='o', c=col_all, label='Zou+23' if ~nolab else None)
    #     plt.scatter(x_Z, y_Z_sf , marker='o', c=col_sf)
    #     # plt.errorbar(x_Z, y_Z_sf, xerr=[25, 50, 25], yerr=[0.024, 0.022, 0.017], ls='none')
    #     # plt.errorbar(x_Z, y_Z_sf, xerr=[25, 50, 25], yerr=[0.083, 0.051, 0.038], ls='none')
    #     print('Zou+23 - W_min = 0.5 Å - 1.5 ≤ z ≤ 2.1')

    ##################################
    ########## Kacprzak+15  ########## 0.08 ≤ z ≤ 0.67
    ################################## W_MgII > 0.1 Å
    if ion=='OVI':
        if w_min == 0.1: # 0.1 Å
            col_Kac = colors[5]
            xK     = np.array([25,  75,  125, 175])
            yK     = np.array([0.7996389891696751, 0.6137184115523466, 0.4169675090252708, 0.3339350180505415])
            yK_min = np.array([0.5956678700361011, 0.4747292418772564, 0.25270758122743686, 0.18231046931407935])
            yK_max = np.array([0.9277978339350181, 0.7382671480144405, 0.5992779783393503, 0.5198555956678701])
            xK_min = np.array([0, 50, 100, 150])
            xK_max = np.array([50, 100, 150, 205])
            ax.scatter( xK, yK, c=col_Kac, marker='D', label='Kacprzak+15' if ~nolab else None)
            ax.errorbar(xK, yK, c=col_Kac, xerr=[xK-xK_min,xK_max-xK], yerr=[yK-yK_min,yK_max-yK], ls='none')

    ################################## Kirill Tchernyshyov
    ######## Tchernyshyov+22  ######## OVI (z = 0.12 - 0.6, NOVI_min = 14.3)
    ################################## Select bin 10^10.1 < M∗ < 10^10.5.
    # if OVI from previous block
        elif w_min == None: # 0.1 Å
            ax.fill_between([0,200],[0.659, 0.659], [0.8730, 0.8730], alpha=0.25, color='b', label='Tchernyshyov+22' if ~nolab else None)
            ax.fill_between([0,400],[0.072, 0.072], [0.3611, 0.3611], alpha=0.25, color='r')

    # These are cumulative!
        # x_K_sf     = np.array([31.484113807455266, 31.452273192476518, 33.6043576155928, 34.02283426959904, 36.60476699492825, 36.599081170824896, 43.0541972753531, 43.04964861607042, 51.65798630853555, 52.516545748140736, 55.52775819327254, 64.13666446814801, 67.5777252154927, 86.51606813891604, 89.09743228183493, 109.75744274375128, 113.6294889581296, 130.84616434305988, 131.30899042507218, 148.9566512770361, 148.98451181514247, 149.84705133161998, 150.30021151265663, 154.60438035888922, 154.59926311719622, 200.65443835429505, 200.67433873865676, 207.13172917282628, 208.0101889967932, 234.26618754122217, 234.71252473333482, 252.36018558529872, 253.2352339148036, 322.9633378061816])
        # y_K_sf     = np.array([0.8306932156746799, 0.9046695778086076, 0.9046601014351021, 0.9323993419606238, 0.9337089767790944, 0.9469190414458672, 0.9495326252587051, 0.9601006769921233, 0.9600627714981009, 0.9653430068154079, 0.9692927592925319, 0.9679338473318323, 0.9732027110009325, 0.9731193189140834, 0.9757499601992313, 0.9756589870135778, 0.9796049489412996, 0.979529137953255, 0.9042298740779489, 0.9028311613485259, 0.8381018444813392, 0.8341350345319051, 0.7812928805901127, 0.7812739278431016, 0.7931629860431972, 0.7916391851835005, 0.7454039588497958, 0.7427335167959245, 0.7017785257795266, 0.7003419075560812, 0.6633518312144162, 0.6619531184849933, 0.6289241662686591, 0.6272961253004011])
        # x_K_sf_min = np.array([31.993563647115018, 34.145648070231296, 34.10641588391821, 37.119334076281014, 37.09545361504697, 43.55170688439582, 43.967340626350385, 52.574541153994865, 52.564875253019174, 54.71695967613545, 55.139984989424356, 66.76010370943166, 66.75384930291796, 88.27469353408082, 87.83972799017488, 111.94307352907731, 112.3695103368282, 112.36666742477655, 129.1529259250836, 129.16031749641792, 131.74281880415745, 131.78375673770157, 147.7091814687621, 147.7211216993791, 149.44278923787212, 149.4655325342855, 150.75735177056563, 150.78350656144102, 155.08767540767357, 155.07857808910822, 200.7027678591735, 200.72494257317655, 207.6110441447383, 208.06022424890264, 234.74550251313423, 234.76199140303396, 252.83950055721073, 252.85371511746905, 321.7198480747799])
        # y_K_sf_min = np.array([0.6470714215318369, 0.6470619451583313, 0.7382113913590636, 0.7381981244361558, 0.7936803960366017, 0.7936519669160849, 0.827996239774993, 0.8306003472143253, 0.8530574571478391, 0.8530479807743334, 0.870219169566437, 0.8728100100828614, 0.8873410812163115, 0.8872463174812557, 0.8978162644893751, 0.8977101291061127, 0.9069552790981525, 0.9135603114315389, 0.9134863957181955, 0.8963133116513908, 0.8963019400031841, 0.8011894744024199, 0.8011193492384787, 0.7733782134382559, 0.7733706323394514, 0.7205303736723601, 0.7192036813815794, 0.6584373839144246, 0.6584184311674135, 0.67955453463425, 0.6793536355159318, 0.6278343833155178, 0.6291250653869773, 0.5855299567119259, 0.586733456147134, 0.548424268613493, 0.5483446670760461, 0.5153195054091141, 0.5163372679236129])
        # x_K_sf_max = np.array([31.84573222042804, 34.4288021105779, 34.85751324797017, 131.69903795856172, 131.28340421660712, 149.7913302554072, 149.38024517273533, 150.6714958266051, 151.12181309559006, 155.42598194182264, 155.42257044736064, 201.0467602174259, 200.63453796993335, 207.52120812390544, 207.96754531601812, 235.08380904728335, 235.09916077236238, 252.74682162432617, 252.76160476699488, 321.62830630671607])
        # y_K_sf_max = np.array([0.9905331028679297, 0.9892007247530458, 0.9931618488783764, 0.9980194379373347, 0.9636751650784265, 0.9635936682662786, 0.9186813436739522, 0.9186756578498488, 0.8724385362414429, 0.8724195834944317, 0.8803456222944954, 0.8801447231761772, 0.8378744115172053, 0.8378440871219875, 0.8008540107803226, 0.8007346084741523, 0.7650674338738658, 0.7636687211444427, 0.7293225530108334, 0.729019309058655])
        # ax.plot(x_K_sf, y_K_sf, 'b')
        # ax.fill(np.append(x_K_sf_min, x_K_sf_max[::-1]), np.append(y_K_sf_min, y_K_sf_max[::-1]), alpha=0.25, color='b', label='Tchernyshyov+22' if ~nolab else None)

        # x_K_qs     = np.array([10.24699219904933, 11.139666583274582, 31.800245627601278, 31.81161727580797, 74.42402601833109, 74.42971184243444, 90.3557051559053, 90.79067069981124, 114.8934476563033, 115.69287452523369, 153.56899178966998, 154.00964315767925, 214.6989924719689, 215.5683549773704, 265.92769906070185, 266.36380176942845, 286.5939639291319, 286.59908117082495, 323.1833791989811, 323.5819554686256, 329.177374968728, 329.184197957652, 346.40087334258226])
        # y_K_qs     = np.array([0.17160575253777288, 0.09762559985444308, 0.09621362020211222, 0.06979349086856668, 0.06696384573980174, 0.05375378107302886, 0.052362649442410314, 0.041792702434290785, 0.04300757351770579, 0.1856724813694497, 0.18682670366242893, 0.16304669198753685, 0.16145845178800233, 0.14163956423844082, 0.14009681063173307, 0.12688485069025912, 0.12547476631262955, 0.11358570811253399, 0.11606662269629364, 0.19004108955552035, 0.1900164509844059, 0.17416437338427837, 0.17408856239623371])
        # x_K_qs_min = np.array([11.177761604767, 32.268188951306605, 32.699174418340185, 75.31101457845298, 75.31158316086334, 115.77077031544951, 115.74290977734313, 154.050012508813, 154.0545611680957, 215.60360708681114, 215.60701858127317, 266.8266278514407, 266.82890218108207, 287.0584957583752, 287.06077008801657, 324.07662216561664, 324.05387886920323, 329.21888148468236, 329.22172439673403, 345.14714912779453])
        # y_K_qs_min = np.array([0.009118166587065213, 0.009025298126710712, 0.007702396385332211, 0.006193757723244486, 0.004872751256567165, 0.004694595434662219, 0.06942391230184919, 0.06925523285344992, 0.05868718112003157, 0.05973716330444945, 0.05181112450438585, 0.05158558681495318, 0.04630156094824378, 0.04621248303729142, 0.040928457170582355, 0.04076546354628641, 0.09360572221337782, 0.09358297891696443, 0.08697794658357794, 0.0869078214196366])
        # x_K_qs_max = np.array([11.030498760490346, 31.689940639996372, 32.16015829334304, 74.77142987104548, 74.79474174986922, 90.71959789851942, 91.16593509063202, 116.12954581637061, 115.6172530646592, 153.92435579612908, 153.51327071345722, 215.06231663217264, 215.07709977484134, 266.2967090450089, 266.30864927562595, 286.96865973754234, 286.9788942209284, 323.99474629852847, 323.95665127703603, 329.5520707771384, 329.5617366781141, 345.05674452455133])
        # y_K_qs_max = np.array([0.3512588414564808, 0.35248887473750457, 0.2600165267953939, 0.2598288945999835, 0.20566762946621497, 0.20691851076895096, 0.169928434427286, 0.17113951496129864, 0.3613663414375281, 0.36119766198912884, 0.3162853373968024, 0.3173353195812202, 0.28298915144761083, 0.28276361375817816, 0.2550224779579553, 0.25493150477230175, 0.23115338837211075, 0.2309903947478149, 0.31949782801519266, 0.3194731894440781, 0.2970160795105643,0.29694784962132414])
        # ax.plot(x_K_qs, y_K_qs, 'r')
        # ax.fill(np.append(x_K_qs_min, x_K_qs_max[::-1]), np.append(y_K_qs_min, y_K_qs_max[::-1]), alpha=0.25, color='r')

    ################################## NHI_min = 1e14
    ############ Wilde+23 ############ M*:10-10.5
    ################################## 0.003 < z < 0.48
    if ion=='HI':
        col_Wil = colors[6]
        x_W     = np.array([49.862928076194, 134.09340146794835, 226.7543125870802, 386.0705432153815, 652.8521141127848, 1111.542124421734, 1892.5050063490116, 3222.1677616755546, 5448.742651320038, 9276.996813139342, 15580.901878706063])
        y_W     = np.array([0.8565713295198476, 1.00, 0.5013901889408909, 0.3830057202288092, 0.4409059917952275, 0.28664008782573536, 0.27589992488588433, 0.196517016236205, 0.2294562893626857, 0.18907494077540876, 0.17989021783093562])
        y_W_min = np.array([0.6802865892413474, 0.7367816490437396, 0.276743514185012, 0.2847241000751142, 0.37226555728895816, 0.23204021494193117, 0.23690067602704123, 0.16687583058877908, 0.20761541572774034, 0.17035419194545565, 0.1518137169931243])
        y_W_max = np.array([0.9392569480557, 1.00, 0.7229190500953372, 0.48908534119142544, 0.512668862310048, 0.3428046455191541, 0.31490379615184605, 0.22771826428612707, 0.2497394117986943, 0.21091581441035434, 0.2110960882879762])
        ax.scatter( x_W, y_W, c=col_Wil, marker='D', label='Wilde+23' if not nolab else '')
        ax.errorbar(x_W, y_W, c=col_Wil, yerr=[y_W-y_W_min,y_W_max-y_W], ls='none')


def fc_50(genpath, folders, timesteps, ions, nbins=1000, xlims=None, ylims=None, savefig=False, savepath='default'):
    """ Compute radius at which the covering fraction reaches 0.5 as a function of W_min.
        Sources for MgII and CIV oscillator strength:
            - https://physics.nist.gov/cgi-bin/ASD/lines1.pl?spectra=Mg+II&limits_type=0&low_w=2795&upp_w=2804&unit=0&submit=Retrieve+Data&de=0&format=0&line_out=0&en_unit=0&output=0&bibrefs=1&page_size=15&show_obs_wl=1&unc_out=1&order_out=0&max_low_enrg=&show_av=3&max_upp_enrg=&tsb_value=0&min_str=&A_out=0&f_out=on&intens_out=on&max_str=&allowed_out=1&forbid_out=1&min_accur=&min_intens=&conf_out=on&term_out=on&enrg_out=on&J_out=on
            - https://physics.nist.gov/cgi-bin/ASD/lines1.pl?spectra=C+IV&limits_type=0&low_w=1540&upp_w=1555&unit=0&submit=Retrieve+Data&de=0&format=0&line_out=0&en_unit=0&output=0&bibrefs=1&page_size=15&show_obs_wl=1&unc_out=1&order_out=0&max_low_enrg=&show_av=3&max_upp_enrg=&tsb_value=0&min_str=&A_out=0&f_out=on&intens_out=on&max_str=&allowed_out=1&forbid_out=1&min_accur=&min_intens=&conf_out=on&term_out=on&enrg_out=on&J_out=on
    """
    warnings.filterwarnings("ignore")

    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    fig, ax = plt.subplots(nrows=1, ncols=1)
    for i in range(len(folders)): 
        next(ax._get_lines.prop_cycler)

    w_min_arr = np.linspace(0.05,1,100) # Å
    ion = ions[0] # Have to remove it, used it because in fctions params I was using ['MgII'] instead of 'MgII'

    for ifold, folder in enumerate(folders):
        element = ion.strip('IVX')

        # Load simulation
        b_kpc, Nion = read_CD(genpath+folder, timesteps, ion, savepath=savepath)

        # Compute bins and real middle of simulated data
        b_min_sim, b_max_sim = np.min(b_kpc), np.max(b_kpc)
        b_max_sim = np.max(b_kpc)
        bins = np.linspace(b_min_sim, b_max_sim, num=nbins)
        x_axis = (bins[:-1]+bins[1:])*0.5
        iio = 0
        Nio = Nion[:,iio,:]
    
        x_fc50 = []
        N_fc50 = []
        N_fc50_min = []
        N_fc50_max = []
        for w_min in w_min_arr:
            N_min_det = compute_Nthreshold(ion, w_min)

            # Compute covering fraction for each timestep
            frac_VS_t = []
            for bkp, Nion_t in zip(b_kpc, Nio):
                sup, bin_edges, _ = stats.binned_statistic(bkp[Nion_t>N_min_det], Nion_t[Nion_t>N_min_det], statistic='count', bins=bins)
                tot, bin_edges, _ = stats.binned_statistic(bkp, Nion_t, statistic='count', bins=bins)
                frac_VS_t.append(sup/tot)

            # Compute fraction for each bin
            b_kpc_2 = b_kpc.reshape(-1)
            Nio_2  = Nio.reshape(-1)
            Nsup, bin_edges, _ = stats.binned_statistic(b_kpc_2[Nio_2>N_min_det], Nio_2[Nio_2>N_min_det], statistic='count', bins=bins)
            Ntot, bin_edges, _ = stats.binned_statistic(b_kpc_2, Nio_2, statistic='count', bins=bins)
            f_c = Nsup/Ntot
            if max(f_c)<0.5:
                x_fc50.append(np.nan)
                continue
            # Rad or N_ion at which fc(50%) is reached for diff W_min (comment one out)
            # x_fc50.append(x_axis[f_c == find_nearest(f_c, value=0.5)][0])
            N_ion_avg, _, _ = stats.binned_statistic(b_kpc_2, Nio_2, statistic='mean', bins=bins)
            N_ion_min, _, _ = stats.binned_statistic(b_kpc_2, Nio_2, statistic='min', bins=bins)
            N_ion_max, _, _ = stats.binned_statistic(b_kpc_2, Nio_2, statistic='max', bins=bins)
            N_fc50.append(N_ion_avg[f_c == find_nearest(f_c, value=0.5)][0])
            N_fc50_min.append(N_ion_min[f_c == find_nearest(f_c, value=0.5)][0])
            N_fc50_max.append(N_ion_max[f_c == find_nearest(f_c, value=0.5)][0])

        # ax.plot(w_min_arr, x_fc50, c=colors[ifold], label=put.foldername(folder))
        ax.plot(w_min_arr, N_fc50, c=colors[ifold], label=put.foldername(folder))
        ax.fill_between(w_min_arr, N_fc50_min, N_fc50_max, alpha=0.25, color=colors[ifold])

    ax.set_yscale('log')
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ax.set_xlabel(r'W$_\mathrm{lim}$ [Å]')
    # ax.set_ylabel(rf'$b (f_c=0.5)$ [kpc]')
    ax.set_ylabel(rf'$N_\mathrm{{{ion}}} (f_c=0.5)$ [kpc]')
    plt.tight_layout()

    # fname = f'{ion}_R_fc_50'
    fname = f'{ion}_N_fc_50'
    if len(folders)==1:
        fname = put.foldername(folders[0])+'_'+fname
    if savefig:
        if savefig==True:
            fig.savefig(fname+'.pdf',bbox_inches='tight')
        else:
            fig.savefig(f'{savefig}/{fname}.pdf',bbox_inches='tight')


###################################################################################################################################
########################################################### Math tools  ###########################################################
###################################################################################################################################
def expdec(x, m, t, b):
    """
    Adding the log allows for a better fit (expecially regarding the constant !)
    Equvalent to change the criteria of fitting from LMSE to LMSRE, see https://stackoverflow.com/questions/66848350/python-curve-fit-optimize-using-relative-deviation-instead-of-absolute-deviation
    """
    return np.log(m * np.exp(-t * x) + b)


def expfit(x_axis, med_dens, ion):
    """ Try to fit the simulated column densities. """    
    # start with values near those we expect
    if ion=='HI':
        p0 = (7.2e22, 1.89, 1e13) 
    elif ion=='MgII':
        p0 = (1.9e18, 1.78, 1e9) 
    elif ion=='CIV':
        p0 = (3.2e15, 0.63, 1e12) 
    elif ion=='OVI':
        p0 = (8.4e14, 0.45, 1e13) 
    
    # perform the fit
    ys = np.log(med_dens)
    params, _ = scipy.optimize.curve_fit(expdec, x_axis, ys, p0)
    a, b, c = params

    # determine quality of the fit
    squaredDiffs = np.square(ys - expdec(x_axis, a, b, c))
    squaredDiffsFromMean = np.square(ys - np.mean(ys))
    rSquared = 1 - np.sum(squaredDiffs) / np.sum(squaredDiffsFromMean)

    print(f"Y = {a:.2e} * e^(-{b:.2f} * x) + {c:.2e}        R² = {rSquared:.3f}")

    return np.exp(expdec(x_axis, params[0], params[1], params[2]))


def geteqbin(array, minsize=10, init_quant=10):
    """
    Returns equal-size bins (in number of objects) with a minimum of minsize points per bin.
    init_quant=10 means that the iteration while start at 10 quantiles (i.e. splitting the data in 10 bins).
    """
    nb_quant = init_quant # Number of quantiles to begin with.
    imdone = False
    while not imdone:
        _, bineuh = pd.qcut(array, nb_quant, retbins=True)
        val_in_bin, bin_edges, binnumber = stats.binned_statistic(array, array, statistic='count', bins=bineuh) # The second parameter is useless.
        if min(val_in_bin)<minsize: #min nb of objects in a bin
            nb_quant -= 1
        else:
            imdone = True
            # plt.plot(((bin_edges[:-1]+bin_edges[1:])/2), val_in_bin, label='Nieeeeh', color='k', lw=2, ls='--')

    return bineuh


def bootstrap(dist_array, bool_array, dist_bins, n_iter=50, len_sample=8):
    """ Do subsets of size 'len_sample' which are looped over 'n_iter' times to create whatever statistics (fractions here). """
    idx  = np.digitize(dist_array, dist_bins, right=False) # Link array of True and False to their respective bins.
    err = []
    for k in range(1, len(dist_bins)):      # Nope, in range max(idx) ou pareil -1
        subset = []
        if np.sum(idx == k) < len_sample:   # Skip bins which don't have enough items.
            err.append((np.nan, np.nan))
            continue
        for _ in range(n_iter):
            subset.append(sum(random.sample(bool_array[idx==k].tolist(), len_sample))/len_sample)  # Append fraction of detections = (number of True in subset)/(subset size)
        err.append((np.nanpercentile(subset,perc_min), np.nanpercentile(subset,perc_max)))
    return err



def find_nearest(array, value):
    """ Returns the value closest to 'value' in the array 'array'. """
    array = np.asarray(array)
    idx = np.nanargmin((np.abs(array - value)))
    return array[idx]