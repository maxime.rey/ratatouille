# Presentation of ratatouille

## What ?

This is a library developed by Maxime Rey to do various stuff related to astrophysics, more precisely regarding the exploitation of simulation outputs (typically from ramses).
For some of these functions, it is necessary to have other libraries such as minirats, h5py or f90nml.  

---
## How ?  
In each py file is a set of functions doing a similar job (like reading, plotting data, mapping data, etc.).   
At the top of the *.py* files, a list of the functions contained is listed. More detail on each function as well as an example is given in the docstring of each function. The following details the content of the different folders/files.   
You should go directly in _notebooks_ and see how to use the functions.

---
## Where ?
* ### Folders
    * notebooks  
        several notebooks with examples of the codes developed here. 
    * *tuto_YT*  
        small notebooks showing how to use YT (an other library).
    * dev  
        folder containing development of functions not implemented yet.
    * *ram*  
        folder copied from minirats useful for moviz.


* ### Python files

    * *readNsave*  
        Functions to read and save data in h5 files which are stored in the folder ratadat. For each data, there are 4 functions: read, h5_save, h5_read and extract.
        You generally want to use _extract_ as depending on if the h5 file already exists or not and on the parameter _saveinfile_, _extract_ selects the fastest way to read the data.
        Examples of use of these functions are shown in _maps.ipynb_.
        
    * *maps*  
        Functions to plot 2D maps. The tutorial for these functions is in _maps.ipynb_.
        ![](./images/maps_density_mgII.png)

    * *plotutils*  
        Utility functions to compute and plot several useful quantities. The tutorial for these functions is in plots.ipynb though the most useful function are used in _output\_comparison.ipynb_.
        ![](./images/sfr_pd_ks.png)

    * *moviz*  
        Functions to make movies. The tutorial for the main function (make_mov) is in _movies.ipynb_.

        ![](./images/vid_simu_diff_res.gif)

    * *basic_functions*  
        Basic functions showing how to use h5py, pretty much useless.    