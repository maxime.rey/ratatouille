# README #

### RAM ###

* This is a repository for RAMSES Animation Maker
* Current version: 1.11 (December 2016) modified by Maxime Rey
* Wiki with examples is here: (https://bitbucket.org/biernacki/ram/wiki/Home)

RAM relies on 
    - [ffmpeg](https://ffmpeg.org) to produce movies, please adjust your bin_ffmpeg path in `ram.py`
    - From version 1.11 RAM requres `f90nml` (https://github.com/marshallward/f90nml.git), which can be installed with `pip install f90nml`.

Maxime Rey stole this from minirats, none of the RAM files are from him. He is just a thief.
RAM does not create videos anymore (commented out).
