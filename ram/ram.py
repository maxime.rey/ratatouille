#!/usr/bin/env python
"""This script takes stadard RAMSES movie outputs
   and creates movie with ffmpeg"""
import sys
import os
import time
import subprocess
import warnings
from argparse import ArgumentParser
import multiprocessing as mp
import matplotlib
from matplotlib.ticker import LogFormatter
try:
    matplotlib.use('agg')
except ValueError:
    pass
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from numpy.polynomial.polynomial import polyfit
import numpy
from scipy import signal
from ratatouille.ram.pyramid import load_namelist
from ratatouille.ram.pyramid import load_units, load_sink, load_map, load_times_sec, load_aexp, load_boxlen, load_info_rt
from ratatouille.ram.pyramid import C

#warnings.filterwarnings("error")
warnings.filterwarnings("ignore", "Passing one of 'on', 'true', 'off', 'false' as a boolean is deprecated;")

bin_ffmpeg = "ffmpeg"


def a2z(aexp):
    """Converts expansion factor to redshift"""
    return 1./aexp-1.


def make_image(i, args, nml, cmin, cmax, all_sinks, all_times, all_aexp, units, crvir):
    """Makes one image made up of specified projections"""

    if not os.path.exists("%s/movie1/info_%05d.txt" % (args.dir, i)):
        return
    nx = nml['movie_params']['nw_frame']
    ny = nml['movie_params']['nh_frame']
    nypic=ny  #int(ny*0.6)
    boxlen = load_boxlen(args.dir, i)

    len_proj_axis = len(nml['movie_params']['proj_axis'])
    centre_frame = numpy.zeros((3, 4*len_proj_axis))
    delta_frame = numpy.zeros((3, 2*len_proj_axis))

    ax_code = 'xyz'
    for v in range(0, 3):
        for p in range(len_proj_axis):  # set default of frame
            centre_frame[v][4*p] = boxlen/2.
            delta_frame[v][2*p] = boxlen
        cf_axis = nml['movie_params'][ax_code[v]+'centre_frame']
        df_axis = nml['movie_params']['delta'+ax_code[v]+'_frame']
        centre_frame[v][0:len(cf_axis)] = cf_axis
        delta_frame[v][0:len(df_axis)] = df_axis
    del ax_code, cf_axis, df_axis, len_proj_axis

    fig = plt.figure(frameon=False)
    fig.set_size_inches(nx/100.*args.geometry[1]*args.size,
                        nypic/100.*args.geometry[0]*args.size)

    #fig.set_size_inches(nx/100.*args.geometry[1],
    #                    ny*0.6/100.*args.geometry[0])

    # need to load units here due to cosmo
    if nml['run_params']['cosmo']:
        units = load_units(args.dir, i, 1)

    if nml['run_params']['rt']:
        if 'Fp' in args.kind:
            # load rt parameters to convert photon densities into units of Kelvin
            groupE = load_info_rt(args.dir, i, 1)  # groupE: [eV]
            groupE = numpy.array(groupE)*C['eV2erg']  # eV to ergs
            # speed of light in cgs times radiation constant in g/cm/s^2/K^4:
            c_times_a = C['c']*C['a_rad']*units.v  

    # plot_sinks = all_sinks[:i-args.fmin]
    # plot_times = all_times[:i-args.fmin]

    filt_factor = [1.00/0.11, 0.25/0.36, 0.5/0.49]  # SDSS ugr filter factors

    for p in range(len(args.proj)):

        proj = args.proj[p]-1
        axis = nml['movie_params']['proj_axis'][proj]

        # calculate image extent
        frame_centre_w = 0.0
        frame_centre_h = 0.0
        frame_centre_d = 0.0
        frame_delta_w = 0.0
        frame_delta_h = 0.0
        frame_delta_d = 0.0

        if not nml['run_params']['cosmo']:
            a = 1.
        else:
            a = all_aexp[int((i-int(args.fmin))/args.step)]

        if axis == 'x':
            w = 1
            h = 2
            d = 0
        elif axis == 'y':
            w = 0
            h = 2
            d = 1
        else:
            w = 0
            h = 1
            d = 2

        for k in range(0, 4):
            frame_centre_w += (centre_frame
                               [w][4*proj+k]*a**k)
            frame_centre_h += (centre_frame
                               [h][4*proj+k]*a**k)
            frame_centre_d += (centre_frame
                               [d][4*proj+k]*a**k)

            if k < 2:
                frame_delta_w += (delta_frame
                                  [w][2*proj+k]/a**k)
                frame_delta_h += (delta_frame
                                  [h][2*proj+k]/a**k)
                frame_delta_d += (delta_frame
                                  [d][2*proj+k]/a**k)

        px_in_cm = frame_delta_w/float(nx)*units.l

        if args.outfile is None:
            #outfile = ("{}/pngs/{}_{:05f}.png"
            outfile = "%s/pngs/%s_%5.5i.png" \
                       %(args.dir, args.kind[p],
                               (i-int(args.fmin)/args.step))
        if sum(args.geometry) > 2:
            outfile = "%s/pngs/multi_%5.5i.png" \
                       %(args.dir, (i-int(args.fmin))/args.step)
        # load data
        dat = load_map(args, p, i)
        dat = numpy.array(dat, dtype=numpy.float64)

        if args.label[p] != '':  # load map_unit from label
            map_unit = args.label[p].translate(str.maketrans('','','(){}[]<>')).split()[-1]
             
        else:  # set defaults
            if args.kind[p] == 'dens':
                map_unit = 'H/cc'
            elif args.kind[p] in ['dm', 'stars']:
                map_unit = 'Msun'
            elif args.kind[p] == 'temp':
                map_unit = 'K'
            elif args.kind[p] in ['pres', 'pmag']:
                map_unit = 'barye'
            elif args.kind[p] in ['stars', 'dm']:
                map_unit = 'Msun/pc2'
            else:
                map_unit = ''

        #if args.kind[p] == 'dens' and (map_unit == 'H/cc' or map_unit == 'cm$^-3$'):
        if args.kind[p] == 'dens':
            dat *= units.d/1.6737236e-24  # in H/cc
        if args.kind[p] == 'dens' and map_unit == 'g/cc':
            dat *= units.d  # in g/cc
        if args.kind[p] == 'dens' and map_unit == 'Msun/pc3':
            dat *= units.d/6.7677922e-23  # in Msun/pc3
        if args.kind[p] == 'temp' and map_unit == 'keV':
            dat *= 8.617328149741e-8  # in keV
        if args.kind[p] in ["vx", "vy", "vz"]:
            dat *= (units.l/units.t)/1e5  # in km/s
        if args.kind[p] == 'var6':  # metals are stored as var6, usually
            dat /= 0.02  # in Z_solar
        #if args.kind[p] in ['pmag', 'pres'] and map_unit == 'barye':
        #    dat *= units.d*units.v**2  # in barye
        if args.kind[p] in ['pmag', 'pres']:
            dat *= units.d*units.v**2  # in barye
        if args.kind[p] in ['dm', 'stars'] and map_unit == 'Msun/pc2':
            dat *= units.inMsun()  # in Solar mass
            dat /= (px_in_cm/3.086e18)**2
        if args.kind[p] in ['dm', 'stars'] and map_unit == 'g/cm2':
            dat *= units.m
            dat /= px_in_cm**2
        if args.kind[p] == 'speed':
            dat *= units.v/1e3  # in km/s

        if nml['run_params']['rt']:
            if 'Fp' in args.kind[p]:
                aexp = all_aexp[int((i-int(args.fmin))/args.step)]
                if map_unit == 'erg cm$^{-2}$ s$^{-1}$':
                    dat *= float(groupE[int(args.kind[p][-1])-1])*units.l/units.t/aexp**3
                elif map_unit == 'sec$^-1$':
                    dat *= 1.#/aexp**3
                elif map_unit == 'K':
                    dat *= float(groupE[int(args.kind[p][-1])-1])/(c_times_a)
                    dat = numpy.power(dat, 0.25)
                else:
                    dat *= units.l/units.t/aexp**3
                    #if i<491: dat *= 1./aexp**3
                    #else: dat *= units.l/units.t/aexp**3
        # Reshape data to 2d
        dat = dat.reshape(ny, nx)

        # PSF convolution
        if args.kind[p] in ['stars', 'dm', 'filter1', 'filter2', 'filter3'] and args.smooth > 0:
            kernel = numpy.outer(signal.gaussian(100, args.smooth),
                                 signal.gaussian(100, args.smooth))
            dat = signal.fftconvolve(dat, kernel, mode='same')

        if args.kind[p:-1] == 'filter':
            dat *= 1e16
            dat *= filt_factor[int(args.kind[-1])-1]

        # Log scale?
        # never logscale for velocities
        if(args.logscale[p] and args.kind[p] not in ["vx", "vy", "vz"]):
            try:
                if numpy.all(dat <= 0.0):
                    dat[:, :] = numpy.finfo(numpy.float32).tiny
                else:
                    dat[dat <= 0.0] = numpy.nan  # log10 does not crash with nans
                    dat[numpy.where(numpy.isnan(dat))] = numpy.nanmin(dat)  # replace nans with min
                cbar_format = LogFormatter(10, labelOnlyBase=False)
            except RuntimeWarning:  # catching and passing
                print('Careful! Problem with logarithms! kind={}'.format(args.kind[p]))
        else:
            dat[numpy.where(numpy.isnan(dat))] = numpy.nanmin(dat)
            cbar_format = '%.0e'

        rawmin = numpy.amin(dat)
        rawmax = numpy.amax(dat)
        bg = numpy.where(dat == 0)  # outside of the box

        # Bounds
        if args.min[p] == "" or args.min[p][0] == 'p':
            plotmin = rawmin
        else:
            plotmin = float(args.min[p])
            dat[dat < plotmin] = plotmin

        if args.max[p] == "" or args.max[p][0] == 'p':
            plotmax = rawmax
        else:
            plotmax = float(args.max[p])
            dat[dat > plotmax] = plotmax

        # Polynomial limiting
        if len(args.min[p]) > 0:
            if args.min[p][0] == 'p':
                p_min = 0.
                for d in range(int(args.min[p][-1])+1):
                    p_min += cmin[p][d]*i**d
                plotmin = p_min

        if len(args.max[p]) > 0:
            if args.max[p][0] == 'p':
                p_max = 0.
                for d in range(int(args.max[p][-1])+1):
                    p_max += cmax[p][d]*i**d
                plotmax = p_max

        dat[bg] = plotmin

        # set middle of the scale at 0
        if args.kind[p] in ["vx", "vy", "vz"]:
            plotmax = max(abs(plotmin), plotmax)
            plotmin = -plotmax

        rollby = [0, 0]
        if args.roll_axis == "x":
            rollby[0] = nx/2
            dat = numpy.roll(dat, rollby[0], axis=1)
        if args.roll_axis == "y":
            rollby[1] = nypic/2
            dat = numpy.roll(dat, rollby[1], axis=0)

        ############
        # Plotting #
        ############
        ax = fig.add_subplot(args.geometry[0], args.geometry[1], p+1)
        ax.axis([0, nx, 0, nypic])
        fig.add_axes(ax)

        if args.kind[p] != 'plot':
            norm=None
            if(args.logscale[p] and args.kind[p] not in ["vx", "vy", "vz"]):
                norm=matplotlib.colors.LogNorm(vmin=plotmin, vmax=plotmax)                  # Max: moved vmin vmac from imshow below to here otherwise bug
            image = ax.imshow(dat, interpolation='none', cmap=args.colormap[p],
                              aspect='auto',norm=norm)
            # remove ticks
            #ax.tick_params(bottom='off', top='off', left='off', right='off')
            ax.tick_params(bottom=False, top=False, left=False, right=False)
            ax.tick_params(labelbottom=False, labeltop=False,
                           labelleft=False, labelright=False)
            height=0.03
            cb_x = args.cbar_cpos[0]
            cb_y = args.cbar_cpos[1]
            nfx=args.geometry[1]
            nfy=args.geometry[0]
            if args.colorbar[p]:  # add colorbar
                cbaxes = fig.add_axes([p % nfx * 1./nfx + cb_x / nfx - 
                                           args.cbar_width / 2. / nfx, #left
                                           ((nfy-1)-int(p/nfx))/float(nfy)
                                           + cb_y/nfy, # bottom
                                           args.cbar_width / nfx, #width
                                           height/nfy]) # height
                cbar = plt.colorbar(image, cax=cbaxes, orientation='horizontal')
                col=args.labelcolor[p]
                fsize=args.fontsize[p]
                cbar.set_label('%s' % (args.label[p]), color=col, fontsize=fsize)
                cbar.ax.yaxis.set_tick_params(color=col, labelsize=fsize*0.8, width=fsize*0.06)
                cbar.ax.xaxis.set_tick_params(color=col, labelsize=fsize*0.8
                                                  ,width=fsize*0.06,top=True
                                                  ,bottom=False, labeltop=True
                                                  ,labelbottom=False)
                cbar.ax.minorticks_off()
                cbar.outline.set_edgecolor(col)
                cbar.outline.set_linewidth(fsize*0.06)
                plt.setp(plt.getp(cbar.ax.axes, 'yticklabels'), color=col)
                plt.setp(plt.getp(cbar.ax.axes, 'xticklabels'), color=col)
                cbar.solids.set_rasterized(True)

            # Show label if set 
            if not args.colorbar[p] and args.label[p] != 'default':
                patch = []
                collection = PatchCollection(patch
                            ,facecolor=args.labelcolor[p],edgecolor='none')
                ax.add_collection(collection)
                ax.text(0.5,0.9
                            ,"%s" %(args.label[p]),verticalalignment='center'
                            ,horizontalalignment='center'
                            ,color=args.labelcolor[p]
                            ,fontsize=args.fontsize[p]
                            ,transform=ax.transAxes)
            
            # Show comment if set 
            if args.comment[p] != '':
                patch = []
                collection = PatchCollection(patch
                            ,facecolor=args.labelcolor[p],edgecolor='none')
                ax.add_collection(collection)
                #ax.text(0.97, 0.02
                ax.text(0.98, 0.96
                            ,args.comment[p],verticalalignment='bottom'
                            ,horizontalalignment='right'
                            ,color=args.labelcolor[p]
                            ,fontsize=args.fontsize[p]*0.7
                            ,transform=ax.transAxes)
            

            # plot rvir circle if measured
            if nml['run_params']['cosmo'] and len(args.aexp_rvir_file):
                max_index = numpy.unravel_index(dat.argmax(), dat.shape)
                aexp = all_aexp[int((i-int(args.fmin))/args.step)]
                if aexp > 0.15:  # limit the plotting to higher aexp
                    ax.scatter(max_index[1], max_index[0], marker='o',
                               facecolor='none', lw=2, alpha=0.5,
                               edgecolor=args.labelcolor[p],
                               s=((crvir[0]+crvir[1]*aexp +
                                   crvir[2]*aexp**2+crvir[3]*aexp**3) *
                                  args.scale_l[p]*nx /
                                  (float(boxlen)*units.l*3.24e-19 *
                                  frame_delta_w/float(boxlen))*1000*0.6777))

            patches = []
            if len(args.bar[p]) > 0:
                barlen, barunit = args.bar[p].split(" ")
                barlen = int(barlen)
                if barunit == 'AU':
                    px_in_user = px_in_cm/1.496e+13
                elif barunit == 'kilo-light-years':
                    px_in_user = px_in_cm/9.461e+20
                elif barunit == 'pc':
                    px_in_user = px_in_cm/3.086e18
                elif barunit == 'kpc':
                    px_in_user = px_in_cm/3.086e21
                else:  # Mpc
                    px_in_user = px_in_cm/3.086e24

                if barlen > 0:
                    barlen_px = barlen/px_in_user
                    if barlen_px < 100:
                        barlen_px = 100
                        barlen = int(barlen_px*px_in_user)
                    rect = mpatches.Rectangle((0.025*nx, 0.025*nypic),
                                              barlen_px, 3)

                    ax.text(0.025,
                            0.01+25./nypic, "%d %s" % (barlen, barunit),
                            #0.01+15./nypic, "%d %s" % (barlen, barunit),
                            verticalalignment='bottom',
                            horizontalalignment='left',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p]*0.7)

                    patches.append(rect)

            # add timer to plot
            if args.timer[p]:
                if nml['run_params']['cosmo'] and args.timer_z[p]:
                    # redshift instead of time
                    aexp = all_aexp[int((i-int(args.fmin))/args.step)]
                    ax.text(0.97, 0.03, 'z={a:.2f}'.format(a=abs(a2z(aexp))),
                            verticalalignment='bottom',
                            horizontalalignment='right',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p]*0.7)
                else:
                    t = all_times[int((i-int(args.fmin))/args.step)]
                    t *= 1./86400/365.25  # time in years
                    time_print_set = '%.{}f %s'.format(1)
                    if t >= 1e3 and t < 1e6:
                        scale_t = 1e3
                        t_unit = 'kyr'
                    elif t > 1e6 and t < 1e9:
                        scale_t = 1e6
                        t_unit = 'Myr'
                    elif t > 1e9:
                        scale_t = 1e9
                        t_unit = 'Gyr'
                        time_print_set = '%.{}f %s'.format(2)
                    else:
                        scale_t = 1.
                        t_unit = 'yr'

                    ax.text(0.97, 0.03, time_print_set % (t/scale_t, t_unit),
                            verticalalignment='bottom',
                            horizontalalignment='right',
                            transform=ax.transAxes,
                            color=args.labelcolor[p],
                            fontsize=args.fontsize[p]*0.7)

            #if args.label[p] != "":  # adding projection label
            #    ax.text(0.85, 0.95, '%s' % (args.label[p]),
            #            verticalalignment='bottom',
            #            horizontalalignment='right',
            #            transform=ax.transAxes,
            #            color=args.labelcolor[p], fontsize=args.fontsize[p])

            collection = PatchCollection(patches, facecolor=args.labelcolor[p])
            ax.add_collection(collection)

    # corrects window extent
    plt.subplots_adjust(left=0., bottom=0.,
                        right=1.+1.0/nx, top=1.+1.0/nypic,
                        wspace=0., hspace=0.)
    try:
        plt.savefig(outfile, dpi=100)
    except DeprecationWarning:  # just a comparison warning
        pass
    plt.close(fig)

    return


def fit_min_max(args, p):
    """Fit polynomials to min and max of a given map"""
    mins = numpy.array([])
    maxs = numpy.array([])

    for i in range(args.fmin, args.fmax+1, args.step):
        dat = load_map(args, p, i)
        dat = numpy.array(dat, dtype=numpy.float64)
        if len(dat) == 0:
            mins = numpy.append(mins, mins[-1])
            maxs = numpy.append(maxs, maxs[-1])
            continue

        # unit loading must stay here for cosmo runs
        units = load_units(args.dir, i, args.proj[p])
        if args.kind[p] == 'dens':
            dat *= units.d/1.6737236e-24    # in H/cc
        if args.kind[p] in ['vx', 'vy', 'vz']:
            dat *= (units.l/units.t)/1e5  # in km/s
        if args.kind[p] in ['pres', 'pmag']:
            dat *= units.d*units.v**2  # in barye

        if args.logscale[p]:
            try:
                if numpy.all(dat <= 0.0):
                    datmin = numpy.finfo(numpy.float32).tiny
                    datmax = numpy.finfo(numpy.float32).tiny
                else:
                    dat[dat <= 0.0] = numpy.nan
                    datmin = numpy.nanmin(dat)
                    datmax = numpy.nanmax(dat)
                mins = numpy.append(mins, numpy.log10(datmin))
                maxs = numpy.append(maxs, numpy.log10(datmax))
            except RuntimeWarning:
                print('Log problem in poly fitting for {}'.format(args.kind[p]))
        else:
            mins = numpy.append(mins, numpy.amin(dat))
            maxs = numpy.append(maxs, numpy.amax(dat))

    indexes = range(args.fmin, args.fmax+1, args.step)

    cmin = polyfit(indexes, mins, args.poly[0])
    cmax = polyfit(indexes, maxs, args.poly[1])

    return p, cmin, cmax


def interpol_rvir(args):
    """Cosmo only. Given list of aexp and rvir,
    it interpolates the rvir for plotting in the movie"""

    aexp, rvir = numpy.loadtxt(args.dir+'/'+args.aexp_rvir_file).T

    crvir = polyfit(aexp, rvir, 3, full=True)[0]

    return crvir


def main():
    import pylab

    """Main body - parses arguments, create images and executes ffmpeg"""
    __version__ = "1.15.1"
    advert = """\
             _____ _____ _____
            | __  |  _  |     |
            |    -|     | | | |
            |__|__|__|__|_|_|_|

          RAMSES Animation Maker
   v {version} - 2017/03/14 - P. Biernacki
    """.format(version=__version__)
    print(advert)
    # Check if running python2
    #if sys.version_info.major != 2:
    #    print("This script works only in python2, sorry!")
    #    sys.exit()
    # Parse command line arguments
    parser = ArgumentParser(description="Script to create RAMSES movies")
    parser.add_argument("-o", "--output", dest="outfile", metavar="FILE",
                        type=str, help="output image file [<map_file>.png]",
                        default=None)
    parser.add_argument("-n", "--ncpu", dest="ncpu", metavar="VALUE",
                        type=int, help="number of CPUs for multiprocessing [%(default)d]",
                        default=1)
    parser.add_argument("-c", "--config", dest="config", metavar="VALUE",
                        type=str, help="config file  [%(default)s]",
                        default="./config.ini")

    for k in range(len(sys.argv)):
        if sys.argv[k] in ["-c", "--config"]:
            config_file = sys.argv[k+1]
            break
        else:
            config_file = "./config.ini"

    from ratatouille.ram.load_settings import load_settings
    args = load_settings(parser, config_file)
    params = {
            'font.family': 'Serif',
            #'font.weight': 'bold',
            'axes.formatter.use_mathtext': True,
            'axes.linewidth': 2,
            #'axes.linewidth': 0,
        }
    pylab.rcParams.update(params)

    # load basic info once, instead of at each loop
    nml = load_namelist(args.namelist, args.dir)

    # load units if not cosmo
    if not nml['run_params']['cosmo']:
        units = load_units(args.dir, args.fmax, 1)
    else:
        units = None

    # Progressbar imports
    try:
        from widgets import Percentage, Bar, ETA
        from progressbar import ProgressBar
        progressbar_avail = True
    except ImportError:
        progressbar_avail = False

    # for each projection fit mins and maxs with polynomial
    cmins = numpy.zeros(len(args.proj)*6).reshape(len(args.proj), 6)
    cmaxs = numpy.zeros(len(args.proj)*6).reshape(len(args.proj), 6)

    for p in range(len(args.proj)):
        if len(args.min[p]) > 0 and len(args.max[p]) > 0:
            if (args.min[p][0] == 'p') or (args.max[p][0] == 'p'):
                args.poly = [int(args.min[p][-1]), int(args.max[p][-1])]
                if args.ncpu > 1:
                    results = []
                    pool = mp.Pool(processes=min(args.ncpu, len(args.proj)))

                    results.append(pool.apply_async(fit_min_max,
                                                    args=(args, p,)))
                    pool.close()
                    pool.join()
                    output = [w.get() for w in results]

                    # just for safety if executed not in order
                    for w in range(len(output)):
                        for d in range(len(args.proj)):
                            if output[w][0] == d:
                                cmins[d][0:args.poly[0]+1] = output[w][1]
                                cmaxs[d][0:args.poly[1]+1] = output[w][2]

                elif args.ncpu == 1:
                    (temp,
                     cmins[p][0:args.poly[0]+1],
                     cmaxs[p][0:args.poly[1]+1]) = fit_min_max(args, p)

                else:
                    print('Wrong number of CPUs! Exiting!')
                    sys.exit()

                print('Polynomial coefficients fitted!')

    all_sinks = []
    all_times = []
    all_aexp = []

    for i in range(args.fmin, args.fmax+1, args.step):
        if True in args.sink_flags:
            sinks_i = load_sink(args.dir, i, nml['amr_params']['boxlen'])
            all_sinks.append(sinks_i)
        time_i = load_times_sec(args.dir, i, nml['run_params']['cosmo'])
        all_times.append(time_i)
        aexp_i = load_aexp(args.dir, i)
        all_aexp.append(aexp_i)

    # interpolating rvir
    crvir = 0
    if len(args.aexp_rvir_file):
        crvir = interpol_rvir(args)

    # creating images
    if progressbar_avail:
        widgets = ['Working...', Percentage(), Bar(marker='#'), ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=(args.fmax-args.fmin)/args.step+1).start()
    else:
        print('Working!')

    if not os.path.exists("%s/pngs/" % (args.dir)):
        os.makedirs("%s/pngs/" % (args.dir))

    if args.ncpu > 1:
        results = []
        pool = mp.Pool(processes=args.ncpu)
        for i in range(args.fmin, args.fmax+1, args.step):
            results.append(pool.apply_async(make_image,
                                            args=(i, args,
                                                  nml, cmins, cmaxs,
                                                  all_sinks, all_times, all_aexp, units,
                                                  crvir,
                                                 )))
        while True:
            inc_count = sum(1 for x in results if not x.ready())
            if inc_count == 0:
                break
            if progressbar_avail:
                pbar.update((args.fmax-args.fmin)/args.step+1-inc_count)
            time.sleep(.1)

        pool.close()
        pool.join()

    elif args.ncpu == 1:
        for i in range(args.fmin, args.fmax+1, args.step):
            make_image(i, args, nml,
                       cmins, cmaxs, all_sinks, all_times, all_aexp, units, crvir)
            if progressbar_avail:
                pbar.update((i+1-args.fmin)/args.step)

    else:
        print('Wrong number of CPUs! Exiting!')
        sys.exit()

    if progressbar_avail:
        pbar.finish()

    # movie name for montage
    if sum(args.geometry) > 2:
        frame = "{dir}/pngs/multi_%*.png".format(dir=args.dir)
        mov = "{dir}/multi.mp4".format(dir=args.dir)
    else:
        frame = "{dir}/pngs/{kind}_%*.png".\
                format(dir=args.dir, kind=args.kind[0])
        mov = "{dir}/{kind}{proj}.mp4".\
              format(dir=args.dir, kind=args.kind[0], proj=args.proj)
    if args.fname != "":
        mov = "{dir}/{fname}".format(dir=args.dir, fname=args.fname)

    print("Calling ffmpeg! Output: {mov}".format(mov=mov))
    print("{binffmpeg} -loglevel quiet -i {input}\
                     -y -vcodec h264 -pix_fmt yuv420p\
                     -r {fps} -filter:v 'setpts={speed}*PTS'\
                     -crf {quality} {output}".
                    format(binffmpeg=bin_ffmpeg, input=frame,
                           fps=args.fps, speed=60./float(args.fps),
                           quality=args.quality, output=mov))
    # subprocess.call("{binffmpeg} -loglevel quiet -i {input}\
    #                  -y -vcodec h264 -pix_fmt yuv420p\
    #                  -r {fps} -filter:v 'setpts={speed}*PTS'\
    #                  -crf {quality} {output}".
    #                 format(binffmpeg=bin_ffmpeg, input=frame,
    #                        fps=args.fps, speed=60./float(args.fps),
    #                        quality=args.quality, output=mov), shell=True)
    # print("Movie created! Cleaning up!")
    if not args.keep:
        subprocess.call("rm -r {dir}/pngs".format(dir=args.dir), shell=True)
    subprocess.call("chmod a+r {mov}".format(mov=mov), shell=True)

if __name__ == '__main__':
    main()
