"""
Functions to read and save data in h5 files:
============================================

- Read stuff from simulations 
    - for cells: extract_cells, add_dpdt_vars, get_ramses_index, extract_ions, get_ion_data.
    - for stars: extract_stars.
    - for info: read_info, read_info_glob, read_info_tstep,
        get_nml, headr2dict, hydro2dict, get_nvar

- Catalogs (for cosmological simulations)
    get_hcat, mehalo, track_halo.
    
- Galaxy/halo properties
    read_prop, add_prop, delete_prop, check_prop_exists,
    get_m200, get_r200, get_mstar, get_frad, get_Hcen, get_Gcen, barycentre, get_vcirc

- General utils 
    get_values, get_format, fort2py, param2dict, compute_edges, read_pos.

---------------------------------------------------

A use of some functions is shown in miniRATS.ipynb. Always propritise the use of the 'extract_' versions. They have a little something more (like being able to use timestep=-1).
  
  
===============================================================
======================== Outputs units ========================
===============================================================


------------------------------------------------------------
read_cells:

cell_pos      [cm]   (∈ [0,boxlen_in_cm], not [0,1_in_cm])
------------------------------------------------------------


------------------------------------------------------------
extract_cells:

cell_dx       [cm]   (∈ [0,boxlen_in_cm], not [0,1_in_cm])
rho           [g/cm^3]
nH            [H/cm^3]
pres          [g/cm/s²]
vx            [cm/s]
vy            [cm/s]
vz            [cm/s]
Tmu
xHII          [fraction]
xHeII         [fraction]
xHeIII        [fraction]
mu            []
T             [K]
mass          [g] 
------------------------------------------------------------


------------------------------------------------------------
read_stars:

star_mass       [g]
star_x          [cm]
star_y          [cm]
star_z          [cm]
star_id         [int]
star_age        [Myr]
star_vx         [cm/s]
star_vy         [cm/s]
star_vz         [cm/s]
star_mets       [Zsun]
star_minit      [g]
------------------------------------------------------------


Parameters:
-----------
    RamsesDir: path
        Path to the folder containing the Ramses output. 
    timestep: int
        Timestep of the output considered.
    timesteps: array
        Array of the containing the timesteps for which the outflows are computed.

    factor: 'fullbox' or float
        Multiplicative factor to increase the radius of data read. This works for cosmological simulations. For non_cosmological simulations, the whole data box is loaded. 
        For a cosmological simulation, by default the radius is r200 and the center is the center of the zoom region.
    rad_kpc: int
        Distance from the center (in kpc) at which the shell is taken.
"""
# Astro libraries
from minirats.utils.py.f90utils import star_utils as SU
import minirats.utils.py.readwrite.ramses_info as ri
import minirats.HaloFinder.py.haloCatalog as hC
import minirats.utils.py.cellutils as cu
import minirats.utils.py.f90utils as dm 
import minirats.TreeMaker.py.Tree as T
from ratatouille.constants import *
from astropy.cosmology import FlatLambdaCDM
import yt
yt.set_log_level(50) # Remove oververbose.

# Basic libraries
import numpy as np
import warnings, os
import h5py, re
import pickle, glob

    
###################################################################################################################################
###################################################### Read stuff from simus ######################################################
###################################################################################################################################
# TODO: add radius2d, 
#       mass_flow: rho*np.abs(vz)*cell_dx**2 * g2Msun * (Myr2s/1e6) # for idealised, flows


# Defining groups of variables to avoid repetition
J_str = ['vec_J', 'J', 'norm_J', 'vec_J_norm', 'vec_J_loc']
T_str = ['T', 'temperature']    # Or for the following 3, just pick one name and stick to it. Same as in yt? Don't care about flexibility.
r_str = ['norm_r', 'radius']
V_str = ['volume', 'vol', 'V']


def extract_cells(RamsesDir, timestep, var_list, factor=1, hnum=None, lmax =-1, alt_path=None, use_yt=False, savepath='default'):
    """ 
    Read the variables in var_list in halo hnum (most massive if zoom and none) up to level lmax (-1 means all).
    To add a new one, three steps: 
        - get the index of the variable: get_ramses_index() for default (and add_dpdt_vars() for derived if minirats),
        - add it to var_dict
    """
    if isinstance(var_list, str): var_list = [var_list]
    info        = read_info(RamsesDir, timestep, alt_path=alt_path)
    dict_index  = get_ramses_index(info)
    center_dat, rad_dat = get_frad(RamsesDir, timestep, factor, hnum=hnum, alt_path=alt_path)  # Region to load

    # Load the default variables needed.
    if not use_yt:
        dict_i2load = add_dpdt_vars(var_list, dict_index, info)   # Get default minimal number of variables needed for the computation.
        cell_var_str  = list(dict_i2load.keys())            # String list of dict_i2load to get the order of cells from read_cells_hydro
        cell_var_index = [dict_i2load[gas_var] for gas_var in cell_var_str]  # Index of default variables to load
        ncells = cu.py_cell_utils.count_cells(RamsesDir,timestep,lmax,center_dat,rad_dat)
        cells,cell_pos,cell_l = cu.py_cell_utils.read_cells_hydro(RamsesDir,timestep,lmax,ncells,cell_var_index,center_dat,rad_dat,False)
        def get_def(var_str):
            return cells[:,cell_var_str.index(var_str)]
    else:
        dict_index = {k: v for k, v in dict_index.items() if v is not None}     # Remove None values
        dict_index = dict(sorted(dict_index.items(), key=lambda item: item[1])) # Order by index
        dict_keys = list(dict_index.keys())                                     # Ordered list of hydro variables     
        bbox = None if factor=='fullbox' else [[c - rad_dat for c in center_dat], [c + rad_dat for c in center_dat]]                 # Restrict data if factor != 'fullbox'
        ds = yt.load(f'{RamsesDir}/output_{timestep:05d}', fields=dict_keys, bbox=bbox) # Load data in box # only this line is diff for stars
        ad = ds.all_data() if factor == 'fullbox' else ds.sphere(center=center_dat,radius=rad_dat)                                   # Make it a sphere if restricted
        cell_pos = np.stack((ad['index','x'], ad['index','y'], ad['index','z']), axis=-1).value                                      # .value to remove dimension
        cell_l = ad[("index", "grid_level")].to_ndarray().astype(int)+info['levelmin']
        def get_def(var_str):
            return ad[('ramses', var_str)].to_ndarray()

    # Define how to get variables.
    cu2cm, unit_nH, unit_d, unit_v, unit_P, unit_T2 = info['cu2cm'], info['unit_nH'], info['unit_d'], info['unit_v'], info['unit_P'], info['unit_T2']
    def get_vec_J(mass, vec_r, vec_v):                                              # TODO: restrict it to some region?
        return np.array(mass).reshape(len(mass),1) * np.cross(vec_r,vec_v)
    def get_mu():
        xHII, xHeII, xHeIII = (get_def(var) for var in ['xHII', 'xHeII', 'xHeIII'])
        xH2 = var_dict['xH2']()
        mu = 1./(X_frac*(1.+xHII-0.5*xH2)+Y_frac/4.*(1.+xHeII+2.*xHeIII))
        return mu
    def get_Mflow_ide(flow_dir, vz, rho, cell_pos, cell_dx):
        z_pos = cell_pos[:, 2] - center_dat[2]*cu2cm
        flow_condition = (vz * z_pos < 0) if flow_dir == 'in' else (vz * z_pos > 0) if flow_dir == 'out' else True
        vz_corrected = np.abs(vz[flow_condition]) if flow_dir in ['in', 'out'] else vz[flow_condition]*np.sign(z_pos)   # TODO: replace abs by sign(z_pos). Ain't that smart?
        return rho[flow_condition] * vz_corrected * cell_dx[flow_condition]**2 * g2Msun * (Myr2s / 1e6)

    var_dict = {
        # Default variables
        'cell_pos':     lambda: cell_pos * cu2cm,                                                                       # [cm] (∈ [0,boxlen_in_cm], not [0,1_in_cm])
        'cell_l':       lambda: cell_l,
        'rho':          lambda: get_def('rho')*unit_d,                                                                  # [g/cm^3]
        'vx':           lambda: get_def('vx')*unit_v,                                                                   # [cm/s]
        'vy':           lambda: get_def('vy')*unit_v,                                                                   # [cm/s]
        'vz':           lambda: get_def('vz')*unit_v,                                                                   # [cm/s]
        'P':            lambda: get_def('P')*unit_P,                                                                    # [g/cm/s²]
        'Z':            lambda: get_def('Z'),                                                                           # [g/cm/s²]
        'c_length':     lambda: get_def('cooling_length'),
        # Have to merge
        'nH':           lambda: get_def('rho')*unit_nH,                                                                 # [H/cm^3], unit_nH = X_frac*unit_d/mH    density
        'density':      lambda: get_def('rho')*unit_d,                                                                  # [g/cm^3]
        'pressure':     lambda: get_def('P')*unit_P,                                                                    # [g/cm/s²]
        'metallicity':  lambda: get_def('Z'),                                                                           #
        # Derived variables
        'cell_dx':      lambda: 1./2.**cell_l * cu2cm,                                                                  # [cm]
        (tuple(V_str)): lambda: var_dict['cell_dx']()**3,                                                               # [cm^3]
        'mu':           lambda: get_mu(),                                                                               # Mean molecular weight
        (tuple(T_str)): lambda: get_def('P') / get_def('rho') * var_dict['mu']() * unit_T2,                             # [K] /!\ P and rho in cu
        'mass':         lambda: var_dict['rho']()  * (var_dict['cell_dx']())**3,                                        # [g]
        'M_Z':          lambda: get_def('Z') * var_dict['mass'](),                                                      # [g]
        'M_totflow_ide':lambda: get_Mflow_ide(None , var_dict['vz'](), var_dict['rho'](), var_dict['cell_pos'](), var_dict['cell_dx']()), # [Msun/yr]
        'M_inflow_ide': lambda: get_Mflow_ide('in' , var_dict['vz'](), var_dict['rho'](), var_dict['cell_pos'](), var_dict['cell_dx']()), # [Msun/yr]
        'M_outflow_ide':lambda: get_Mflow_ide('out', var_dict['vz'](), var_dict['rho'](), var_dict['cell_pos'](), var_dict['cell_dx']()), # [Msun/yr]
        # Vector stuff
        'vec_r':        lambda: (cell_pos - center_dat) * cu2cm,                                                        # [cm]
        'vec_v':        lambda: np.array([get_def(var)*unit_v for var in ['vx', 'vy', 'vz']]).transpose(),              # [cm/s]
        'norm_v':       lambda: np.linalg.norm(var_dict['vec_v'](),2, axis=1),              # [cm/s]
        'v_rproj':      lambda: np.sum(var_dict['vec_v']()*var_dict['vec_r'](),axis=1)/get_values('norm_r', var_dict)(),# [cm/s] - Direction of the gas speed
        (tuple(r_str)): lambda: np.linalg.norm(var_dict['vec_r'](),2, axis=1),                                          # [cm]
        ('vec_J_loc'):  lambda: get_vec_J(var_dict['mass'](), var_dict['vec_r'](), var_dict['vec_v']()),                # [g*cm^2/s]    (n,3) for each cell
        ('vec_J', 'J'): lambda: np.sum(var_dict['vec_J_loc'](), axis=0),                                                # [g*cm^2/s]    (3,) for the whole region
        'vec_J_norm':   lambda: var_dict['vec_J']()/np.linalg.norm(var_dict['vec_J'](),2),                              # Useful for eg. np.dot(vec_r_gas,vec_J_norm) < hmax_star.
        # Ions fraction
        'xH2':          lambda: 1-get_def('xHI')-get_def('xHII') if info['isH2'] else 0,                                # [fraction]
        'xHI':          lambda: get_def('xHI') if info['isH2'] else 1-get_def('xHII'),
        'xHII':         lambda: get_def('xHII'),
        'xHeI':         lambda: 1-get_def('xHeII')-get_def('xHeIII'),
        'xHeII':        lambda: get_def('xHeII'),
        'xHeIII':       lambda: get_def('xHeIII'),
        # Ions density
        'nH2':          lambda: var_dict['nH' ]() * var_dict['xH2'](),                                                  # [g/cm^3]
        'nHI':          lambda: var_dict['nH' ]() * var_dict['xHI'](),                                                  #
        'nHII':         lambda: var_dict['nH' ]() *  get_def('xHII'),                                                   #
        'nHe':          lambda: var_dict['nH' ]() * 0.25*(Y_frac/X_frac),                                               # nH/x = 4nHe/y (2p+2n)
        'nHeI':         lambda: var_dict['nHe' ]() * var_dict['xHeI'](),                                                # nHeI   = nHe * (1-xHeII-xHeIII)
        'nHeII':        lambda: var_dict['nHe' ]() *  get_def('xHeII'),                                                 # nHeII  = nHe * xHeII
        'nHeIII':       lambda: var_dict['nHe' ]() *  get_def('xHeIII'),                                                # nHeIII = nHe * xHeIII
        'ne-':          lambda: var_dict['nHII']() + var_dict['nHeII']() + 2*var_dict['nHeIII'](),                      # ne = nHII + nHeII + 2*nHeIII
        # Ions number                                                                                                   # [N/cm^3]
        'N_HI':         lambda: var_dict['nHI'   ]() * get_values('volume', var_dict)(),
        'N_HII':        lambda: var_dict['nHII'  ]() * get_values('volume', var_dict)(),
        'N_He':         lambda: var_dict['nHe'   ]() * get_values('volume', var_dict)(),
        'N_HeI':        lambda: var_dict['nHeI'  ]() * get_values('volume', var_dict)(),
        'N_HeII':       lambda: var_dict['nHeII' ]() * get_values('volume', var_dict)(),
        'N_HeIII':      lambda: var_dict['nHeIII']() * get_values('volume', var_dict)(),
        'N_e-':         lambda: var_dict['ne'    ]() * get_values('volume', var_dict)(),
        # Mass                                                                                                          # [g]
        'M_HI':         lambda: var_dict['nHI'   ]() * get_values('volume', var_dict)() * ion_mass['HI'],
        'M_HII':        lambda: var_dict['nHII'  ]() * get_values('volume', var_dict)() * ion_mass['HII'],
    }

    # Add all requested variables to the list var2return
    var2return = []
    for gas_var_str in var_list:
        # If input is None, return None.
        if gas_var_str is None:
            var2return.append(None)
            continue
            
        # Default and derived variables
        gas_var_value = get_values(gas_var_str, var_dict)
        if gas_var_value is not None:
            var2return.append(gas_var_value())
            continue

        # Atoms and ions
        str_elem = gas_var_str[1:] if gas_var_str[0] == 'n' else gas_var_str[2:] if '_' in gas_var_str else None    # For ions: nMgII, M_MgII or N_MgII.
        if str_elem is None:
            raise KeyError(f'Variable not found: {gas_var_str}')
        conversions = {
            'n' : lambda n_elem: n_elem,                                                            # [#/cm^3] Number density
            'N_': lambda n_elem: n_elem * get_values('volume', var_dict)(),                         # [#]      Total number
            'M_': lambda n_elem: n_elem * get_values('volume', var_dict)() * ion_mass[str_elem],    # [g]      Mass
            'f_': lambda n_elem: n_elem / (abund_dict[str_elem.strip('IVX')] * get_def('Z')/Z_sun * get_def('nH')*unit_nH)
        }
        if str_elem in atom_dict:
            print('Computing element abundance, not using postprocessed ions')
            n_elem = abund_dict[str_elem] * get_def('Z') / Z_sun * get_def('nH') * unit_nH             # nX = N_X_sun/N_H_sun * Z/Z_sun * nH
        else:
            n_elem = extract_ions(RamsesDir, timestep, str_elem, factor=factor, savepath=savepath, hnum=hnum)
        str_elem = str_elem.split('_')[0]   # Remove the chng_dat part (e.g. Z2) s.t. ion_mass[str_elem] and abund_dict[str_elem.strip('IVX')] work.
        var2return.append(next(conversion(n_elem) for prefix, conversion in conversions.items() if gas_var_str.startswith(prefix)))

    # restriction=None in the def of the function, then user input should be something like 'nosat_CGM_inflow'
    # if restriction:
    #     var2return = restrict(somevars, restriction, var2return)

    return var2return if len(var_list)>1 else var2return[0]



# TODO : Choose where how to add restriction(sat, CGM, inflowing, outflowing, ...) + do it. Possible before derived computations?
# def restrict(somevars, restriction, var2return):
#     restr_dict = {
#         'no_sat':        lambda x: ~is_sat(x),
#         'is_cgm':        in_cgm,
#         'is_inflowing':  inflow,
#         'is_outflowing': outflow
#         'is_cold':       inflow,
#         'is_warm':       outflow
#         'is_hot':        outflow
#     }
#
#     restr = None
#     for restr_key in restriction.split('_'):
#         if restr_key in restr_dict:
#             new_restr = restr_dict[restr_key](somevars)
#             restr = new_restr if restr is None else np.logical_and(restr, new_restr)
#     return [var[restr] for var in var2return] if restr is not None else var2return


def add_dpdt_vars(var_list, dict_index, info):
    """ 
    Return list of default simulation variables to load (among those listed in dict_index, cf routine get_ramses_index + cell_pos and cell_dx).
        - add default variables needed for derived quantities (ex: mass needs rho and dx)
        - remove duplicates
        - restrict the dict simulation variables to load.
    """
    var_list_noNone = ['' if x is None else x for x in var_list]
    H_vars  = ('nHI', 'nHII', 'M_HI', 'M_HII', 'N_HI', 'N_HII')
    He_vars = ('nHeI', 'nHeII', 'nHeIII', 'M_HeI', 'M_HeII', 'M_HeIII', 'N_HeI', 'N_HeII', 'N_HeIII')
    der_vars_dpdce = {
        ('density', 'nH'):                      ['rho'],
        'pressure':                             ['P'],
        'metallicity':                          ['Z'],
        'mass':                                 ['rho'],
        'M_Z':                                  ['Z', 'rho'],
        'mu':                                   ['xHI', 'xHII', 'xHeII', 'xHeIII'] if info['isH2'] else ['xHII', 'xHeII', 'xHeIII'],
        ('e-'):                                 ['rho', 'xHII', 'xHeII', 'xHeIII'],
        H_vars:                                 ['rho', 'xHI', 'xHII'] if info['isH2'] else ['rho', 'xHII'],
        He_vars:                                ['rho', 'xHeII', 'xHeIII'],
        tuple(T_str):                           ['P', 'rho', 'xHI', 'xHII', 'xHeII', 'xHeIII'] if info['isH2'] else ['P', 'rho', 'xHII', 'xHeII', 'xHeIII'],
        tuple(r_str)+('vec_r',):                ['cell_pos'],
        ('vec_v', 'norm_v'):                                ['vx', 'vy', 'vz'],
        'v_rproj':                              ['cell_pos', 'vx', 'vy', 'vz'],
        tuple(J_str):                           ['cell_pos', 'rho', 'vx', 'vy', 'vz'],
        ('M_inflow_ide', 'M_outflow_ide', 'M_totflow_ide'): ['vz', 'rho'],
        'xH2':                                  ['xHI', 'xHII'],
    }
    var_list_ex = var_list_noNone[:]        # Copy to the list: we need to keep the original one to preserve the order
    for drv_var in var_list_noNone:         # Fill var_list_ex with the list of all variables necessary (like rho for mass)
        values = get_values(drv_var, der_vars_dpdce)
        if values is not None:              # If drv_var is in der_vars_dpdce: include it the dependances
            var_list_ex.extend(values)
        if drv_var.startswith('f_'):        # Special case: ion fraction
            var_list_ex.extend(['Z', 'nH'])
        if drv_var.startswith('N_'):        # SPecial case: number of ions
            var_list_ex.append('cell_dx')

    var_list_ex = list(dict.fromkeys(var_list_ex))  # Remove duplicates 
    # Then, keep only those in dict_index (i.e. saved in the hydro file) which are not None.
    dict_index  = {k: v for key, v in dict_index.items() for k in (key if isinstance(key, tuple) else (key,)) if k in var_list_ex and v is not None}  # Dict. of variables to load.

    return dict_index


def get_ramses_index(info, include_rt=False):                                                         # TODO: CR, MHD
    """ 
    Determine the index of the variables in the ramses output.
    """
    # In case the variable is not found
    info.setdefault('rt_isIRtrap', 0)
    # Variables obtained in info
    dict_index = {'rho':       info['idens'],
                  'vx':       info['ivel'],
                  'vy':       info['ivel']+1                    if info['ndim']>=2 else None,
                  'vz':       info['ivel']+2                    if info['ndim']>=3 else None,
                  'P':        info['iPT']+info['rt_isIRtrap'],
                  'Z':        info['iPT']+info['rt_isIRtrap']+1 if info['metal']   else None,
                  'xHI':      info['iIons']                     if info['isH2']    else None,
                  'xHII':     info['iIons']+info['isH2']        if info['rt']      else None,
                  'xHeII':    info['iIons']+info['isH2']+1      if info['rt']      else None,
                  'xHeIII':   info['iIons']+info['isH2']+2      if info['rt']      else None,
                }

    # Additional variables
    counter = info['iPT'] + info['rt_isIRtrap'] + info['metal']
    if info['delayed_cooling']:   dict_index['DC_var'],         counter = counter + 1, counter + 1  # delayed cooling
    if info['momentum_feedback']: dict_index['KR_turb'],        counter = counter + 1, counter + 1  # patch mom2 by Kretschmer
    counter += info['nIons']  # info['isH2'] already counted in nIons
    if info['is_zoom']:           dict_index['zoom_var'],       counter = counter + 1, counter + 1  # zoom-in simulations
    if info['is_cool_refine']:    dict_index['cooling_length'], counter = counter + 1, counter + 1  # cooling length refinement

    # RT variables: N_photons and flux_nx/y/z for each group n.
    if include_rt and info['rt']:
        for i in range(1, info['nGroups'] + 1):
            dict_index[f'N_photons{i}'] = info['nvar'] + 1 + (i - 1) * 4 if info['nGroups'] >= i else None
            for axis in ['x', 'y', 'z']:
                dict_index[f'flux_{i}{axis}'] = info['nvar'] + 2 + "xyz".index(axis) + (i - 1) * 4 if info['nGroups'] >= i else None

    # Safety check: two variables should not share the same index.
    values = list(dict_index.values())
    if len(values) != len(set(values)):
        raise ValueError("The index of the hydro variables are not unique, you need to fix get_ramses_index for your study case.")

    return dict_index


def extract_ions(RamsesDir, timestep, ions, factor=1, savepath='default', hnum=None, alt_path=None):
    """
    Reads the ionisation states of cells that was computed with krome/komions.
    In order to work you need a tree structure with the kromions folders created all in a folder "ions" in the simulation folder.
        simufolder -> ions -> 000xx -> compute_fractions -> fract_ions -> lot_of_files

    Example:
    --------
        cells_ions = extract_ions('/path/to/simu/', 17, ['MgI', 'MgII'])
        nMgI, nMgII = cells_ions[:,0], cells_ions[:,1]
    """
    if savepath=='default':
        path2ions = f'{RamsesDir}/ratadat/ions/{timestep:05d}/compute_fractions/fract_ions/'
    else:
        path2ions = f'{savepath}/ions/{timestep:05d}/compute_fractions/fract_ions/'
    if (not isinstance(ions,list)): ions=[ions]

    for the_ion in ions:
        if not os.path.exists(f'{path2ions}/{the_ion}_{timestep:05d}.out00001'): 
            raise ValueError(f"Ion {the_ion} not computed for timestep {timestep} of {RamsesDir}")

    path = path2ions.ljust(1000)
    lmax = -1 # read all levels
    if factor=='fullbox':
        center_dat = [0.5, 0.5, 0.5]
        rad_dat = 1.1
    else:
        center_dat = get_Gcen(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
        rad_dat = get_r200(RamsesDir,timestep,hnum=hnum, alt_path=alt_path)*factor
    ncells = cu.py_cell_utils.count_cells(RamsesDir,timestep,lmax,center_dat,rad_dat)
    ion_Z, ion_level = get_ion_data(ions)
    readRT=False
    _,_,_,cells_ions = cu.py_cell_utils.read_cells_hydro_ions(RamsesDir,timestep,lmax,ncells,[],path,ion_Z,ion_level,center_dat,rad_dat,readRT)
    if np.isnan(np.max(cells_ions)):
        raise ValueError(f"{ions} wasn't correctly computed, its maximum value was NaN.")
    if len(ions)==1:
        cells_ions = np.ndarray.flatten(cells_ions)

    return cells_ions


def get_ion_data(ions):
    """ 
    Returns the atomic number and roman numeralas arabic numbers. Inpyt can be string or array.
    Eg. get_ion_data(['CII', 'CIV', 'OII', 'OIII']) returns ([6, 6, 8, 8], [2, 4, 2, 3]).
    """

    if isinstance(ions, str): ions = [ions]
    ion_Z, ion_level = [], []
    for ion in ions:
        # Use regex to extract the element symbol and the Roman numeral
        match = re.match(r"([A-Z][a-z]*)([IVXLCDM]+)", ion)
        if match:
            element, level = match.groups()
        else:
            raise ValueError(f"Invalid ion format: {ion}")

        # Get the atomic number and ionization level
        if element in atom_dict and level in roman_nb:
            ion_Z.append(atom_dict[element])
            ion_level.append(roman_nb[level])
        else:
            raise ValueError(f"Unknown element or ionization level: {ion}")

    return ion_Z, ion_level


def extract_stars(RamsesDir, timestep, var_list, factor=1, hnum=None, use_yt=False, part_type='is_star', alt_path=None):
    """ Returns particle properties from the simulation. """
    if timestep<0: timestep = max([int(output_arr[7:]) for output_arr in os.listdir(RamsesDir) if 'output' in output_arr])
    if isinstance(var_list, str): var_list = [var_list]
    info   = read_info(RamsesDir, timestep, alt_path=alt_path)
    unit_l = info['unit_l']
    unit_v = info['unit_v']
    unit_m = info['unit_m']
    boxlen = info['boxlen']
    center, rad_dat = get_frad(RamsesDir, timestep, factor, hnum=hnum, alt_path=alt_path)   # [cu]
    center_cm = center * info['cu2cm']

    if use_yt:
        var_dict = {
            # Restrictions
            'is_dm':      lambda: ad[('io', 'particle_birth_time')].to_ndarray() == 0,
            'is_clump':   lambda: ad[('io', 'particle_imass'     )].to_ndarray() == 0,  # alternatively: particle_identity > 1e7
            'is_star':    lambda: ~(var_dict['is_dm']() | var_dict['is_clump']()),
            # Default data (in cgs + restricted to a particle type)
            'star_age':   lambda: info['t_myr'] - ad[('io', 'particle_birth_time')].to_ndarray()[var_dict[part_type]()]*info['unit_t'] / Myr2s if not info['cosmo'] else None, #  TODO: FIX THIS! Not working with cosmological simulations!
            'star_minit': lambda: ad[('io', 'particle_imass'      )].to_ndarray()[var_dict[part_type]()] * unit_m,          # [g]
            'star_mass':  lambda: ad[('io', 'particle_mass'       )].to_ndarray()[var_dict[part_type]()] * unit_m,          # [g]
            'star_x':     lambda: ad[('io', 'particle_position_x' )].to_ndarray()[var_dict[part_type]()] * unit_l * boxlen, # [cm] (∈ [0,boxlen_in_cm], not [0,1_in_cm])
            'star_y':     lambda: ad[('io', 'particle_position_y' )].to_ndarray()[var_dict[part_type]()] * unit_l * boxlen, # [cm]
            'star_z':     lambda: ad[('io', 'particle_position_z' )].to_ndarray()[var_dict[part_type]()] * unit_l * boxlen, # [cm]
            'star_id':    lambda: ad[('io', 'particle_identity'   )].to_ndarray()[var_dict[part_type]()],
            'star_vx':    lambda: ad[('io', 'particle_velocity_x' )].to_ndarray()[var_dict[part_type]()] * unit_v,          # [cm/s]
            'star_vy':    lambda: ad[('io', 'particle_velocity_y' )].to_ndarray()[var_dict[part_type]()] * unit_v,          # [cm/s]
            'star_vz':    lambda: ad[('io', 'particle_velocity_z' )].to_ndarray()[var_dict[part_type]()] * unit_v,          # [cm/s]
            'star_mets':  lambda: ad[('io', 'particle_metallicity')].to_ndarray()[var_dict[part_type]()],
            # Derived variables
            'star_rad':   lambda: np.linalg.norm([var_dict['star_x'](), var_dict['star_y'](), var_dict['star_z']()] - center_cm[:,None], axis=0),
            'vec_r':      lambda: np.vstack((var_dict['star_x'](), var_dict['star_y'](), var_dict['star_z']())).T - center_cm,
            'vec_v':      lambda: np.array([var_dict['star_vx'](),var_dict['star_vy'](),var_dict['star_vz']()]).transpose(),
            'vec_J_loc':  lambda: np.array(var_dict['star_mass']()).reshape(len(var_dict['star_mass']()),1) * np.cross(var_dict['vec_r'](),var_dict['vec_v']()),
            'vec_J':      lambda: np.sum(var_dict['vec_J_loc'](), axis=0),
            'vec_J_norm': lambda: var_dict['vec_J']()/np.linalg.norm(var_dict['vec_J'](),2),                                # Useful for eg. np.dot(vec_r_gas,vec_J_norm) < hmax_star.
        }
        if part_type not in ['is_dm', 'is_clump', 'is_star']:
            raise KeyError(f"part_type '{part_type}' is not valid, it must be either 'is_dm', 'is_clump' or 'is_star'.")
        if not all(var in var_dict for var in var_list):                                                            # Check whether all variables are defined
            if 'star_age' in var_list and info['cosmo']:
                raise ValueError("Did not manage to make star_age coherent for cosmological simulations.")
            raise KeyError(f"Missing: {[var for var in var_list if var not in var_dict]},\nAvailable variables: {list(var_dict.keys())}")
        extra_particle_fields = None if info['new_format'] else [("particle_birth_time", "float64"), ("particle_metallicity", "float64"), ("particle_imass", "float64")]    # TODO: make function load_yt
        bbox = None if factor=='fullbox' else [[c - rad_dat for c in center], [c + rad_dat for c in center]]        # Restrict data if factor != 'fullbox'
        ds = yt.load(f'{RamsesDir}/output_{timestep:05d}', extra_particle_fields=extra_particle_fields, bbox=bbox)  # Load data in box
        ad = ds.all_data() if factor == 'fullbox' else ds.sphere(center=center,radius=rad_dat)                      # Make it a sphere if restricted
        data = {key: func() for key, func in var_dict.items()}                                                      # Get data from var_dict
    else:
        nstars     = SU.get_tot_nstars(RamsesDir, timestep)
        star_props = SU.read_stars_all_props(RamsesDir, timestep, nstars, True, conf_time=False)
        data_keys = ['star_mass','star_x','star_y','star_z','star_id','star_age','star_vx','star_vy','star_vz','star_mets','star_minit']
        conversion_factors = {
            'star_x':    unit_l, 'star_y':     unit_l, 'star_z':  unit_l, # [cm] (∈ [0,boxlen_in_cm], not [0,1_in_cm])
            'star_vx':   unit_v, 'star_vy':    unit_v, 'star_vz': unit_v, # [cm/s]
            'star_mass': unit_m, 'star_minit': unit_m,                    # [g]
        } # TODO:raise error if not in list.
        data = {key: (star_props[i] * conversion_factors[key] if key in conversion_factors else star_props[i]) for i, key in enumerate(data_keys)}
        if factor != 'fullbox': # Keep all stars within r200*factor
            cu2cm   = info['cu2cm']
            rad_stars = np.sqrt(np.sum((np.vstack((data['star_x'], data['star_y'], data['star_z'])).T - center*cu2cm)**2, axis=1))
            in_rad     = rad_stars<rad_dat*cu2cm
            data = {key: arr[in_rad] for key, arr in data.items()}

    return (data[var] for var in var_list if var in data) if len(var_list)>1 else data[var_list[0]]


###################################################################################################################################
########################################################### Info stuff  ###########################################################
###################################################################################################################################
def read_info(RamsesDir, timestep, alt_path=None):
    """ Returns info from the simulation (dict). """
    savepath = get_alt_path(RamsesDir, alt_path=alt_path)
    check_ratapath(savepath)
    path_info = f'{savepath}/ratadat/info_dict.pkl'
    if not os.path.exists(path_info):                # If first time, save general data
        info_dict = read_info_glob(RamsesDir, timestep=timestep)
        with open(path_info, 'wb') as file: 
            pickle.dump({'00000': info_dict}, file)
    with open(path_info, 'rb') as file:              # Otherwise, load common data
        all_info = pickle.load(file)
        info_dict = all_info.get('00000', {}).copy() # Important to copy, otherwise '00000' will be modified
        if timestep != 0:
            t_group = f'{timestep:05d}'
            if t_group in all_info:                  # And add timestep specific data.
                info_dict = all_info[t_group]
            else:
                info_dict = read_info_tstep(RamsesDir, timestep, info_dict)
                all_info[t_group] = info_dict
                with open(path_info, 'wb') as file:
                    pickle.dump(all_info, file)
    return info_dict


def read_info_glob(RamsesDir, timestep=1):
    """ Read info parameters common to all timesteps of a given simulation. """
    tstep = f'{timestep:05d}'
    path_out = f'{RamsesDir}/output_{tstep}/'
    
    # Initialise some variables which might not be defined.
    info_dict = {}
    info_dict['new_format'] = get_format(f'{path_out}header_{tstep}.txt')                     # Get whether post-2017 or not
    for key in ['isH2', 'delayed_cooling', 'momentum_feedback', 'cosmo', 'rt', 'hydro']:
        info_dict[key] = False

    # Namelist
    info_dict  = param2dict(info_dict, get_nml(RamsesDir, timestep))                                    # Namelist
    if info_dict['rt']:    info_dict  = param2dict(info_dict, f'{path_out}info_rt_{tstep}.txt')         # info_rt_xxxxx.txt
    if info_dict['hydro']: info_dict  = hydro2dict(info_dict, f'{path_out}hydro_file_descriptor.txt', info_dict['new_format']) # hydro_file_descriptor
    info_dict  = get_nvar  (info_dict, path_out, tstep)                                                 # (after hydro2dict)

    # Corrections
    if info_dict['rt']:
        info_dict['X_fraction'] = round(info_dict['X_fraction'], 6) # Correct precision error in info_rt_xxxxx.txt
        info_dict['Y_fraction'] = round(info_dict['Y_fraction'], 6)

    # Determine simulation type
    # Note: original way is the following BUT have to read info_xxxxx.txt (resp header_xxx.txt), and some variables there depends on timestep
    #       info_dict['is_cosmo'] = info_dict['omega_m']!=1.0 and info_dict['aexp']!=1 and info_dict['H0']!=1 
    #       info_dict['is_zoom']  = info_dict['is_cosmo'] and info_dict['ndm'] != (2**info_dict['levelmin'])**info_dict['ndim']
    info_dict['is_cosmo'] = info_dict['cosmo']
    info_dict['is_zoom']  = info_dict['is_cosmo'] and 'initfile(2)' in info_dict
    info_dict['is_cool_refine'] = 'cooling_refine' in info_dict and any(value != -1 for value in info_dict['cooling_refine'])
    info_dict['nvarnoadvect'] = info_dict['cooling_time_ivar'] if info_dict['is_cool_refine'] else 0

    return info_dict


def read_info_tstep(RamsesDir, timestep, info_dict):
    """ Read info parameters for a given timestep anc add some units. """
    tstep = f'{timestep:05d}'
    path_out = f'{RamsesDir}/output_{tstep}/'
    info_dict  = param2dict(info_dict, f'{path_out}info_{tstep}.txt')               # info_xxxxx.txt
    info_dict  = headr2dict(info_dict, f'{path_out}header_{tstep}.txt', info_dict['new_format']) # header_xxxxx.txt

    # Units
    info_dict['unit_m']      = info_dict['unit_d']*info_dict['unit_l']**3
    info_dict['unit_v']      = info_dict['unit_l']/info_dict['unit_t']
    info_dict['unit_P']      = info_dict['unit_d']*info_dict['unit_v']**2
    info_dict['unit_T2']     = mH/kB_cgs*info_dict['unit_v']**2
    if info_dict['rt']:
        info_dict['unit_nH']     = info_dict['X_fraction']*info_dict['unit_d']/mH
        info_dict['unit_nHe']    = info_dict['unit_nH'] * info_dict['Y_fraction']/info_dict['X_fraction'] * 0.25
    info_dict['cu2cm']       = info_dict['unit_l'] * info_dict['boxlen']
    info_dict['boxlen_cMpc'] = info_dict['boxlen']*info_dict['unit_l']/info_dict['aexp']/kpc2cm/1e3 if info_dict['is_cosmo'] else None
    info_dict['redshift']    = 1./info_dict['aexp']-1
    info_dict['t_myr']       = info_dict['time']*info_dict['unit_t']/Myr2s if not info_dict['is_cosmo']\
        else FlatLambdaCDM(H0=info_dict['H0'],Om0=info_dict['omega_m']).age(1/info_dict['aexp']-1).value*1e3
    return info_dict


def get_nml(RamsesDir, timestep):
    """Return the path to the nml."""
    # Check the output directory
    path_nml = f'{RamsesDir}/output_{timestep:05d}/namelist.txt'    
    if os.access(path_nml, os.R_OK):            # Check it's readable
        with open(path_nml, 'r') as file:
            for line in file:
                if '&RUN_PARAMS' in line:       
                    return path_nml
    # If the file is doesn't exist/is corrupted, check the RamsesDir
    nml_files = glob.glob(f"{RamsesDir}/*.nml")
    path_nml = next((f for f in nml_files if os.access(f, os.R_OK)), None) # First nml with read access
    if path_nml is None:
        raise Exception(f"No accessible .nml file found in {RamsesDir}.")
    return path_nml


def headr2dict(info_dict, file_path, new_format):
    """ Add header variables (i.e. number of particles and particle fields) to info_dict. """
    header = {
        "Total number of particles": "npart",       # Old format
        "Total number of dark matter particles": "ndm",
        "Total number of star particles": "nstar",
        "Total number of sink particles": "nsink",
        "DM": "ndm",                                # New format
        "star": "nstar",
    }
    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            if "Particle fields" in line:
                info_dict["particle_fields"] = next(file).strip().split()
                break  # No more info after particle fields
            parts = line.split()
            if new_format and len(parts) == 2 and parts[0] in header:
                info_dict[header[parts[0]]] = int(parts[1])
            elif not new_format and 'Total' in line:
                info_dict[header[line]] = int(next(file).strip())
    return info_dict


def hydro2dict(info_dict, path_hydrofd, new_format):
    """ Read hydro_file_descriptor and add the indexes of the hydro parameters to info_dict. """
    with open(path_hydrofd, 'r') as file:
        for line in file:
            line = line.strip()
            if not line.startswith('#'): # skip the first in new format
                parts = line.split(',') if new_format else line.replace('#', ':').split(':')
                index1, index2 = (0, 1) if new_format else (-2, -1)
                if len(parts) >= 2:
                    info_dict[parts[index2].strip()] = int(parts[index1].strip())
    rename_map = {
        'density': 'idens',
        'velocity_x': 'ivel',
        'thermal_pressure': 'iPT',
        'pressure': 'iPT'
    }
    for old_name, new_name in rename_map.items():
        if old_name in info_dict:
            info_dict[new_name] = info_dict.pop(old_name)
    return info_dict


def get_nvar(info_dict, path_out, tstep):
    """ Try to get nvar from hydro_xxxxx.out00001, and in the hydro_file_descriptor if it fails. 
        First option is favoured as the hydro_file_descriptor can be badly coded. """
    hydro_file = f'{path_out}hydro_{tstep}.out00001'
    if os.path.exists(hydro_file):          # Try to read hydro_xxxxx.out00001
        with open(f'{path_out}hydro_{tstep}.out00001', 'rb') as f:  
            info_dict['nvar'] = np.fromfile(f, dtype=np.int32, count=5)[4]
    elif not 'dm' in path_out.lower():      # Else, skip if DM-only simulation and take the highest value from the hydro_file_descriptor.
        info_dict['nvar'] = info_dict[max((key for key in info_dict if 'scalar_' in key), key=lambda x: int(x.rsplit('_', 1)[1]), default=None)]
    return info_dict


###################################################################################################################################
########################################################### Zoom stuff  ###########################################################
###################################################################################################################################
def get_hcat(RamsesDir,timestep, alt_path=None):
    """
    Setup and load the halo catalog (wether it already exists or not).

    Example:
    --------
        RamsesDir = '/path/to/simu/'
        timestep = 17
        hcat  = get_hcat(RamsesDir,timestep,alt_path=alt_path)
    """
    if timestep<0: timestep = max([int(output_arr[7:]) for output_arr in os.listdir(RamsesDir) if 'output' in output_arr])

    hcat = hC.haloCatalog(RamsesDir,timestep)                       # read halos 
    try:                                                            # Load catalog
        hcat.load_catalog()
    except NameError:                                               # Create it if not previously existing
        info = read_info(RamsesDir, timestep, alt_path=alt_path)
        current_dir = os.getcwd()
        if info['is_zoom']:
            hcat.setup_HaloFinderRun(run=True,rhot=1000,npart=100,nvoisins=10,nhop=10,fudgepsilon=1e-5,alphap=1.)
        else:
            raise ValueError('You should not have to run this for full cosmo runs yet.')
            hcat.setup_HaloFinderRun(run=True,nvoisins=32,nhop=16,npart=20,rhot=80,fudgepsilon=1.e-5,alphap=1)
        os.chdir(current_dir)
        # If doesn't work, likely come from the following line in  /minirats/HaloFinder/py/haloCatalog.py
        # if runCmd==None: runCmd = '$MINIRATS/HaloFinder/f90/HaloFinder_zoom ./ > runHalos.log'
        hcat.load_catalog()
    return hcat


def mehalo(RamsesDir, timestep, hnum=None, alt_path=None):
    """ 
    Returns the index of the most massive halo. Could be better and backtrace from last snapshot, but would need to debug.

    Fyi:
        # myhalo2hnum = hcat.hnum[myhalo]               # From myhalo to hnum
        # hnum2myhalo = np.where(hcat.hnum==hnum)[0][0] # From hnum to my halo
    """
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    hcat = get_hcat(RamsesDir,timestep,alt_path=alt_path)
    if info['is_zoom'] and hnum is None:     # Method 1: Brutal and works - take the more massive one of the non contaminated.
        hmain = np.where((hcat.level==1) & (hcat.contam < 0.5) & (hcat.mvir > 4e7*1e-11))[0]
        myhalo = hmain[np.where(hcat.mvir[hmain]==max(hcat.mvir[hmain]))[0][0]]
    elif hnum:
        myhalo = np.where(hcat.hnum==hnum)[0][0]

    ### Method 2: Clean but doesn't work - backtrace the halo from the last snapshot (from Marion I think) | Todo: if info['is_zoom']
    # Can't make it work though... ^^'
    # if not os.path.exists(RamsesDir+'Trees/'):
    #     halo_finder_utils.run_treemaker(dir=RamsesDir)
    # filename = RamsesDir+'ratadat/snaphnum.h5'
    # if os.path.exists(filename):
    #     with h5py.File(filename,'r') as f:
    #         snap = f['snap'][:]
    #         hnum = f['hnum'][:]
    #     f.close()
    # else:
    #     with h5py.File(filename,'w') as f:
    #         max_tstep = max([int(output_arr[7:]) for output_arr in os.listdir(RamsesDir) if 'output' in output_arr])
    #         hcat_maxt = get_hcat(RamsesDir,max_tstep,alt_path=alt_path)
    #         hmain_maxt = np.where((hcat_maxt.level==1) &(hcat_maxt.contam < 0.5) &(hcat_maxt.mvir > 4e7*1e-11))[0]
    #         myhalo_maxt = hmain_maxt[np.where(hcat_maxt.mvir[hmain_maxt]==max(hcat_maxt.mvir[hmain_maxt]))[0][0]]  # Get list of hnum corresponding to each snap from the last one
    #         hnum_maxt = hcat_maxt.hnum[myhalo_maxt]               # From myhalo to hnum
    #         snap, hnum = track_halo(RamsesDir,max_tstep,hnum_maxt)
    #         f.create_dataset('snap',data=snap)
    #         f.create_dataset('hnum',data=hnum)
    #     f.close()
    # hcat = get_hcat(RamsesDir,timestep,alt_path=alt_path)
    # myhalo = [np.where(hcat.hnum==hnum[snap==timestep])[0][0]][0] # From hnum to my halo
    # print(hnum, myhalo)
    
    return myhalo


def track_halo(RamsesDir,snap,halo_num):
    """
    Returns a list of snapshots and the corresponding list of halo number.
    Method: starting from a snap and given a halo number (= ID, as defined with hcat.hnum), then track the halo number associated with all the previous snapshots. Based on TreeMaker files.
    From Marion Farcy.
    """
    warnings.filterwarnings('ignore')

    keep_snap, keep_num = [], []
    t = T.TreeMaker(RamsesDir)
    t.read_tree()
    # get the main branch of a selected halo:
    mp = t.get_main_progenitor(t.get_haloID(halo_num,snap)) 
    
    #Old not working way to print snap and halo number
    #for ts,hno in zip(mp.halo_ts,mp.halo_num):
    #    print(ts,hno)

    snaps = [int(odir[7:]) for odir in os.listdir(RamsesDir) if odir[0:6]=='output']
    nProgs = len(mp.halo_ts.values)

    for i in range(nProgs-1,-1,-1):
        isnap = mp['halo_ts'].values[i]
        ramses_isnap = t.treets2ramsests(int(isnap))
        halo_num =  mp['halo_num'].values[i]
        if int(ramses_isnap) not in snaps: continue
        halos = hC.haloCatalog(RamsesDir, ramses_isnap, load=True)
        ih=int(halo_num)-1
        keep_snap.append(ramses_isnap)
        keep_num.append(halos.hnum[ih])

    return np.array(keep_snap), np.array(keep_num)


###################################################################################################################################
########################################################### Zoom stuff ###########################################################
###################################################################################################################################
def get_alt_path(RamsesDir, alt_path=None):
    """ Try to get an alternative path to save the data in"""
    endpath = RamsesDir.split('rey', 1)[1] # somepath/rey/endpath
    if not alt_path:
        savepath = RamsesDir
    elif len(endpath) > 1:                  # If endpath is not empty, keep the same structure as the one who ran the simulation.
        savepath = alt_path+endpath         # Don't need to specify the simulation name, better for many simulations
    else:
        savepath = alt_path                 # Otherwise, save it in alt_path (must include the simulation name).
    return savepath


def check_ratapath(savepath):
    """ Check if ratadat folder exists, creates it otherwise. """
    ratapath = f'{savepath}/ratadat/'
    if not os.path.exists(ratapath):
        try: 
            os.makedirs(ratapath, exist_ok=True)  # Can create more than one level of directories.
        except PermissionError:
            raise PermissionError(f"Cannot mkdir {savepath}/ratadat. Add the argument alt_path='/general/path/to/allsimus'.")


def read_prop(RamsesDir, timestep, prop_str, filename, hnum=None, alt_path=None):
    """ Reads the property prop_str from the file filename.h5. """
    np.set_printoptions(precision=15)       # Otherwise, prop_val is truncated at 8th decimal.
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    savepath = get_alt_path(RamsesDir, alt_path=alt_path)
    with h5py.File(f'{savepath}/ratadat/{filename}.h5', 'r') as hdf_file:
        if info['is_cosmo'] and hnum is not None:
            prop_val = hdf_file[f"{timestep:05d}"][f"halo_{hnum}"][prop_str][()]
        elif info['is_zoom'] or not info['is_cosmo']:
            prop_val = hdf_file[f"{timestep:05d}"][prop_str][()]
        else:
            raise KeyError("For cosmological simulations, you must specify a halo number.")
    return prop_val


def add_prop(RamsesDir, timestep, prop_str, prop_val, filename, hnum=None, alt_path=None):
    """ Writes the property prop_str with the value prop_val to the file filename.h5. """
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    savepath = get_alt_path(RamsesDir, alt_path=alt_path)
    check_ratapath(savepath)
    with h5py.File(f'{savepath}/ratadat/{filename}.h5', 'a') as hdf_file:
        if info['is_cosmo'] and hnum is not None:
            hdf_file.require_group(f"{timestep:05d}").require_group(f"halo_{hnum}")[prop_str] = prop_val
        elif info['is_zoom'] or not info['is_cosmo']:
            hdf_file.require_group(f"{timestep:05d}")[prop_str] = prop_val
        else:
            raise KeyError("For cosmological simulations, you must specify a halo number.")


def delete_prop(RamsesDir, timestep, prop_str, filename, hnum=None, alt_path=None):
    """ Deletes the property prop_str from the file filename.h5. Made by chatGPT, tested once, seems to work... """
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    savepath = get_alt_path(RamsesDir, alt_path=alt_path)
    
    with h5py.File(f'{savepath}/ratadat/{filename}.h5', 'a') as hdf_file:
        if info['is_cosmo'] and hnum is not None:
            try:
                del hdf_file[f"{timestep:05d}"][f"halo_{hnum}"][prop_str]
            except KeyError:
                raise KeyError(f"Property '{prop_str}' not found for halo {hnum}.")
        elif info['is_zoom'] or not info['is_cosmo']:
            try:
                del hdf_file[f"{timestep:05d}"][prop_str]
            except KeyError:
                raise KeyError(f"Property '{prop_str}' not found for timestep {timestep}.")
        else:
            raise KeyError("For cosmological simulations, you must specify a halo number.")


def check_prop_exists(RamsesDir, timestep, prop_str, filename, hnum=None, alt_path=None):
    """ Check if the property `prop_str` exists in the HDF5 file. Made by chatGPT, tested once, seems to work... """
    # Get the information and alternate path
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    savepath = get_alt_path(RamsesDir, alt_path=alt_path)
    
    # Open the HDF5 file in read mode
    try:
        with h5py.File(f'{savepath}/ratadat/{filename}.h5', 'r') as hdf_file:
            # Construct the path to the desired property
            if info['is_cosmo'] and hnum is not None:
                prop_path = f"{timestep:05d}/halo_{hnum}/{prop_str}"
            elif info['is_zoom'] or not info['is_cosmo']:
                prop_path = f"{timestep:05d}/{prop_str}"
            else:
                raise KeyError("For cosmological simulations, you must specify a halo number.")
            
            # Check if the dataset exists
            if prop_path in hdf_file:
                print(f"{RamsesDir} - {timestep} - {prop_str} computed for halo {hnum if hnum else 'N/A'}.")
                return True
            else:
                print(f"Property '{prop_str}' does NOT exist for timestep {timestep} and halo {hnum if hnum else 'N/A'}.")
                return False
    except KeyError as e:
        print(f"Error: {e}")
        return False
    except Exception as e:
        print(f"An error occurred: {e}")
        return False


def get_m200(RamsesDir,timestep,hnum=None,alt_path=None):
    """ Returns the mass of a sphere with an average density 200*the critical density in solar mass. """
    filename = 'props'
    try:
        m200 = read_prop(RamsesDir, timestep, 'M_dm', filename, hnum=hnum, alt_path=alt_path)
    except:
        hcat = get_hcat(RamsesDir,timestep,alt_path=alt_path)
        hcat.get_mr200()
        myhalo = mehalo(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
        m200 = hcat.m200[myhalo]*1e11   # [Msun]
        add_prop(RamsesDir, timestep, 'M_dm', m200, filename, hnum=hnum, alt_path=alt_path)
    return m200


def get_r200(RamsesDir,timestep,hnum=None,alt_path=None):
    """ Returns the radius at 200*the critical density. For idealised simulations, it is assumed to be constant. """
    filename = 'props'
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    try:
        r200 = read_prop(RamsesDir, timestep, 'r200', filename, hnum=hnum, alt_path=alt_path)
    except (FileNotFoundError, KeyError):
        if info['is_cosmo']:
            hcat = get_hcat(RamsesDir,timestep,alt_path=alt_path)
            myhalo = mehalo(RamsesDir,timestep,alt_path=alt_path)
            r200 = float(hcat.get_r200_cu()[myhalo])
        else:
            cu2kpc = info['unit_l'] * info['boxlen'] / kpc2cm
            r200 = get_R200_idealised(RamsesDir,timestep)/cu2kpc
        add_prop(RamsesDir, timestep, 'r200', r200, filename, hnum=hnum, alt_path=alt_path)
    return r200


def get_R200_idealised(RamsesDir,timestep):
    nml = get_nml(RamsesDir,timestep)
    with open(nml, 'r') as file:    # Find the line with the initfile.
        for line in file:
            if "initfile(1)=" in line:
                init_file = line.split("=")[1].strip()
                break
    try:                            # Try to find the R200 value in the init file.
        with open(init_file, 'r') as file:
            for line in file:
                if "R200=" in line:
                    R200 = line.split("=")[1].strip()
                    break
    except:                         # If it fails, use default values.
        R200_Gx = {"G8": 41.0959, "G9": 89.0411, "G10": 191.781} # [kpc]
        for key in R200_Gx:
            if key in init_file:
                R200 = R200_Gx[key]
                return R200
        if 'hyunwoo' in RamsesDir:
            return 10e-3  # Hyunwoo's molecular cloud.
        raise ValueError("IC is not a Gx (or it's not obvious from its path).")


def get_mstar(RamsesDir,timestep,hnum=None,alt_path=None):
    """ Returns the mass of stars within 0.1 r200. """
    filename = 'props'
    try:
        mstar = read_prop(RamsesDir, timestep, 'M_star', filename, hnum=hnum, alt_path=alt_path)
    except:
        star_mass = extract_stars(RamsesDir, timestep, ['star_mass'], factor=0.1, hnum=hnum, alt_path=alt_path)
        mstar = np.sum(star_mass)*g2Msun
        add_prop(RamsesDir, timestep, 'M_star', mstar, filename, hnum=hnum, alt_path=alt_path)
    return mstar


def get_frad(RamsesDir, timestep, factor, hnum=None, alt_path=None):
    """
    Return the region (center and radius) over which the data should be loaded. 
    """
    if factor=='fullbox':
        center_dat = np.array([0.5, 0.5, 0.5])
        rad_dat = 1.1
    else:
        center_dat = get_Gcen(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
        rad_dat = get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)*factor
    return center_dat, rad_dat


def get_Hcen(RamsesDir, timestep, hnum=None, alt_path=None):
    """ Returns the center of the DM halo. """
    filename = 'props'
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    if info['is_cosmo']:
        try:
            center_halo = read_prop(RamsesDir, timestep, 'center_halo', filename, hnum=hnum, alt_path=alt_path)
        except:
            hcat = get_hcat(RamsesDir,timestep,alt_path=alt_path)
            myhalo = mehalo(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
            center_halo = [float(hcat.x_cu[myhalo]),float(hcat.y_cu[myhalo]),float(hcat.z_cu[myhalo])]
            add_prop(RamsesDir, timestep, 'center_halo', center_halo, filename, hnum=hnum, alt_path=alt_path)
    else:
        center_halo = [0.5, 0.5, 0.5]
    return center_halo


def get_Gcen(RamsesDir, timestep, hnum=None, alt_path=None):
    """ Returns the center of the galaxy. """
    filename = 'props'
    info = read_info(RamsesDir, timestep, alt_path=alt_path)
    if info['is_cosmo']:
        try:
            center_galaxy = read_prop(RamsesDir, timestep, 'center_galaxy', filename, hnum=hnum, alt_path=alt_path)
        except KeyError:
            center_galaxy = barycentre(RamsesDir,timestep,hnum=hnum)
            add_prop(RamsesDir, timestep, 'center_galaxy', center_galaxy, filename, hnum=hnum, alt_path=alt_path)
    else:
        center_galaxy = [0.5, 0.5, 0.5]
    return np.array(center_galaxy)


def barycentre(RamsesDir, timestep, hnum=None, alt_path=None):
    """
    Returns the stellar barycentre of the galaxy in cu.
    Method: Starts at r_init pc and find the center of mass. Reduces by 10% the radius and then compute the new center of mass. If distance to the previous center of mass is smaller than d_cv pc and the radius of the zone smaller than r_tol pc, ends the computation. Cap set at 100 iterations.
    For zoom satellites, if the center found is more than 0.1 Rvir away, use the halo center instead.
    From Jiyoung Choe, rewritten and modified by Maxime Rey. 
    """

    center  = get_Hcen(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
    r200    = get_r200(RamsesDir,timestep,hnum=hnum,alt_path=alt_path)
    info    = read_info(RamsesDir, timestep, alt_path=alt_path)
    max_res = 1./2.**info['levelmax'] * info['unit_l'] * info['boxlen']/info['aexp']
    cu2cm   = info['cu2cm']
    center_cm, r200_cm = [cen*cu2cm for cen in center], r200*cu2cm

    # Read stars within Rvir from the halo center. Need to keep 'fullbox' all stars or... ourobouros situation.
    star_mass, star_x, star_y, star_z = extract_stars(RamsesDir, timestep, ['star_mass', 'star_x', 'star_y', 'star_z'], factor='fullbox', alt_path=alt_path)
    if star_mass.size==0:
        raise ValueError(f'The simulation {RamsesDir} has no stars at timestep {timestep}: the barycenter of the galaxy cannot be computed.')
    mstar = star_mass*g2Msun

    # Starting position: center of the halo.
    x0 = center_cm[0]
    y0 = center_cm[1]
    z0 = center_cm[2]

    # Initialisation
    n_ite = 0
    n_max = 100             # Max number of iterations
    rtest = r200_cm         # Initial size of the sphere (big enough to avoid missing the galaxy, eg. in mergers) 
    d_cv = max_res          # Convergence distance between current and previous barycentre (higher CV is possible but useless)
    r_tol = kpc2cm          # Sphere radius must be smaller than that to accept convergence (to avoid CV out of galaxy)

    while True:
        n_ite = n_ite + 1
        sind, = np.where((star_x - x0) ** 2 + (star_y - y0) ** 2 + (star_z - z0) ** 2 <= rtest ** 2)
        x1 = np.sum(mstar[sind] * star_x[sind]) / np.sum(mstar[sind])
        y1 = np.sum(mstar[sind] * star_y[sind]) / np.sum(mstar[sind])
        z1 = np.sum(mstar[sind] * star_z[sind]) / np.sum(mstar[sind])

        # Check if condition for convergence met
        dist = np.sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2 + (z1 - z0) ** 2)    # Distance between the center at iteration n and n-1.
        if dist <= d_cv and rtest <= r_tol:                                 # If the distance is small enough, we assume we find the galaxy center.
            xcen, ycen, zcen = x1/cu2cm, y1/cu2cm, z1/cu2cm
            break
        elif n_ite > n_max:
            raise ValueError("Did not converge.")
        else:
            x0, y0, z0 = x1, y1, z1
            rtest = rtest * 0.9
    # print(f'{n_ite} iterations to reach convergence. Distance from the DM halo center: {round(dist/pc2cm)} pc.\nBarycenter of the {round(rtest/pc2cm)} pc sphere at [{xcen}, {ycen}, {zcen}]')
    g2h = np.sqrt((x1 - center_cm[0]) ** 2 + (y1 - center_cm[1]) ** 2 + (z1 - center_cm[2]) ** 2)
    if g2h>=0.1*r200_cm:
        if hnum is not None and info['is_zoom']:
            print(f'Galaxy center {g2h/kpc2cm} kpc away from halo center for timestep {timestep} of {RamsesDir} and halo {hnum}. Using halo center instead.')
            return center_cm
        else:
            print(f'Warning: the galaxy center is {g2h/kpc2cm} kpc away from the halo center for timestep {timestep} of {RamsesDir}')

    precision = 15
    return [round(xcen, precision), round(ycen, precision), round(zcen, precision)]


def get_vcirc(RamsesDir, timestep, factor=1, hnum=None, alt_path=None):
    """ Returns the circular or escape velocity at r200*factor [m/s]. """
    # v_esc  = sqrt(2GM(r)/r)
    # v_circ = sqrt( GM(r)/r)  
    info   = read_info(RamsesDir, timestep, alt_path=alt_path)
    center , rad_cu = get_frad(RamsesDir,timestep,factor,hnum=hnum)
    rad_m  = rad_cu*factor*info['cu2cm']*1e-2        # [m]
    M_g    = extract_cells(RamsesDir, timestep, ['mass'],      factor=factor, hnum=None, alt_path=None) # [g]
    M_s    = extract_stars(RamsesDir, timestep, ['star_mass'], factor=factor, hnum=None, alt_path=None) # [g]
    if factor==1:
        M_dm = get_m200(RamsesDir,timestep,hnum=None,alt_path=None)/g2Msun
    else:
        ndm = dm.dmpart_utils.get_npart_in_sphere(RamsesDir,timestep,center[0],center[1],center[2],rad_cu)
        _,_,_,mdm,_ = dm.dmpart_utils.read_parts_in_sphere(RamsesDir,timestep,center[0],center[1],center[2],rad_cu,ndm)
        mdm = mdm*info['unit_m']
        M_dm = np.sum(mdm)
    M_tot  = (np.sum(M_g) + np.sum(M_s) + M_dm)*1e-3                                                        # [kg]
    return np.sqrt(G*M_tot/rad_m)


###################################################################################################################################
####################################################### Utilitary functions #######################################################
###################################################################################################################################
def get_values(var, dictionary):    # TODO: restrict to one value, not T or temperature. Then, remove this function -> everything will be more readable.
    """ Helper function to be able to format dictionary keys as tuples. """
    for key in dictionary:
        if isinstance(key, tuple) and var in key:
            return dictionary[key]
        elif var==key:
            return dictionary[key]
    return None


def get_format(path_hdr):
    """ Returns whether the code is formatted following RAMSES post-2017 or not. """
    with open(path_hdr, 'r') as file:
        first_line = file.readline().strip()
        if 'Total number of particles' in first_line: return False
        elif '#      Family     Count' in first_line: return True
        else: raise ValueError("Unrecognized header format.")


def fort2py(value):
    """ Convert Fortran values to Python. """
    if value.lower() in ('.true.', '.false.'):  return value.lower() == '.true.'    # Booleans
    elif (value.startswith("'") and value.endswith("'")): return value[1:-1]        # Strings
    else:
        try: return float(value) if 'e' in value or '.' in value else int(value)    # Numerals
        except ValueError:  return value                                            # Other strings


def param2dict(info_dict, path_file):
    """ General function to get parameters from a file and add them to a dictionary. """
    with open(path_file, 'r') as file:
        for line in file:
            line = line.split('!')[0].strip()       # Remove comments and whitespace
            if '=' in line:
                key, value = map(str.strip, line.split('='))
                if key!='movie_vars_txt':           # Replace 'd' with 'e' for float compatibility
                    value = value.replace('d', 'e')
                if ',' in value or '*' in value:    # Convert lists
                    value_list = []
                    for element in value.split(','):
                        if '*' in element:
                            count, val = element.split('*')
                            value_list.extend([int(float(val.strip()))] * int(count))
                        else:
                            value_list.append(fort2py(element.strip()))
                    value = value_list
                else:
                    value = fort2py(value)          # Convert single values
                info_dict[key] = value
            elif "DOMAIN" in line or "Photon group properties" in line:
                break
    return info_dict


def read_pos(cell_pos):
    """
    Returns the location of the cells and the edges of the box.
    It's just that I always forget the syntax ^^'.

    Example:
    --------
        cell_pos  = extract_cells(RamsesDir,timestep,['cell_pos'])
        xloc, yloc, zloc = read_pos(cell_pos)
    """
    # Cell locations
    xloc = cell_pos[:,0]
    yloc = cell_pos[:,1]
    zloc = cell_pos[:,2]

    return xloc, yloc, zloc


def compute_edges(center, radius):
    """
    Returns the location of the cells and the edges of the box.

    Parameters:
    -----------
        center, radius: array of size 3, scalar
            Center and radius considered in the same unit which will be the unit of the ouput.

    Example:
    --------
        xmin, xmax, ymin, ymax, zmin, zmax = compute_edges([75, 75, 75], 20)
    """
    # Edges of maps
    xmin,xmax = center[0]-1*radius,center[0]+1*radius
    ymin,ymax = center[1]-1*radius,center[1]+1*radius
    zmin,zmax = center[2]-1*radius,center[2]+1*radius

    return xmin, xmax, ymin, ymax, zmin, zmax